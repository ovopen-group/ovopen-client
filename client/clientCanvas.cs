
function createClientCanvas(%windowName)
{
    videoSetGammaCorrection($pref::OpenGL::gammaCorrection);
    %canvasCreate = createEffectCanvas(%windowName);

    if (!%canvasCreate)
    {
        quitWithErrorMessage("Copy of Onverse is already running; exiting.");
        return 0;
    }

    setOpenGLTextureCompressionHint($pref::OpenGL::compressionHint);
    setOpenGLAnisotropy($pref::OpenGL::textureAnisotropy);
    setOpenGLMipReduction($pref::OpenGL::mipReduction);
    setOpenGLInteriorMipReduction($pref::OpenGL::interiorMipReduction);
    setOpenGLSkyMipReduction($pref::OpenGL::skyMipReduction);
    setShadowDetailLevel($pref::Shadows);
    
    return isObject(Canvas);
}

function resetCanvas()
{
    if (isObject(Canvas))
    {
        Canvas.repaint();
    }
}
