exec("./VMPlayer.cs");

$GameMusicPlaylistName = "";
$GameMusicPath = "onverse/data/live_assets/music/";
$GameMusicLoaded = 0;

function GameMusic::FadeIn(%this, %playlist, %disableFade)
{
    %disableFade = 1;
    if (%this.inited != 1)
    {
        GameMusic.init();
    }
    
    if (strlen(%playlist) > 0)
    {
        %this.LoadPlaylist(%playlist);
    }

    %this.Player.EnableFade(!%disableFade);
    %this.Player.EnableCrossFade(!%disableFade);
    %this.Player.CurrentTrackIndex = %this.lastTrack;
    %this.Player.Play();
    %this.Player.SetSongPosition(%this.getPosition());
    GameMusicNewTrack();
}

function GameMusic::FadeOut(%this, %disableFade)
{
    if (%this.inited != 1)
    {
        GameMusic.init();
    }

    %this.Player.EnableFade(!%disableFade);
    %this.savePosition();
    %this.Player.stop();
}

function GameMusic::LoadPlaylist(%this, %name)
{
    if (%this.inited != 1)
    {
        GameMusic.init();
    }
    
    $GameMusicPlaylistName = %name;
    if ($GameMusicLoaded[%name] == 0)
    {
        %this.Player.AddPlaylist($GameMusicPlaylistName);
        GameMusicPlaylistFile($GameMusicPath @ %name @ ".txt");
        if (%this.Player.IsFadeEnabled())
        {
            %this.Player.LoadPlaylist($GameMusicPlaylistName);
            %this.addEvents();
            %this.syncToSimTime();
        }
        $GameMusicLoaded[%name] = 1;
    }
    %this.Player.LoadPlaylist($GameMusicPlaylistName);
}

function GameMusic::init(%this)
{
    %this.inited = 1;
    %this.lastTrack = 0;
    %this.lastSimTime = 0;
    %this.lastSongTime = 0;
    
    %this.Player = new ScriptObject(VMPlayer) {
    };

    %this.Player.EnableRepeat(1);
    %this.Player.EnableCrossFade(0);
    %this.Player.EnableFade(1);
}

function GameMusic::savePosition(%this)
{
    %this.lastTrack = %this.Player.CurrentTrackIndex;
    %this.lastSongTime = %this.Player.GetSongPosition();
    %this.lastSimTime = getSimTime() / 1000;
}

function GameMusic::getPosition(%this)
{
    %time = %this.lastSongTime;
    if (%this.lastSimTime > 0)
    {
        %now = getSimTime() / 1000;
        %time += (%now - %this.lastSimTime);
    }
    return %time;
}

function GameMusic::syncToClock(%this)
{
    %this.syncToTime(getRealTime() / 1000);
}

function GameMusic::syncToSimTime(%this)
{
    %this.syncToTime(getSimTime() / 1000);
}

function GameMusic::syncToTime(%this, %seconds)
{
    %playlist = %this.Player.CurrentPlaylist;
    if (%playlist && %playlist.Tracks)
    {
        %offset = %seconds % %playlist.duration;
        for (%i=0; %i < %playlist.Tracks.getCount(); %i++)
        {
            %duration = %playlist.Tracks.GetAt(%i).EndPosition;
            if (%duration > %offset)
            {
                %this.lastTrack = %i;
                %this.lastSongTime = %offset;
                %this.lastSimTime = getSimTime() / 1000;
                break;
            }
            %offset -= %duration;
        }
    }
}

function GameMusic::addEvents(%this)
{
    %playlist = %this.Player.CurrentPlaylist;
    if (%playlist && %playlist.Tracks)
    {
        for (%i=0; %i < %playlist.Tracks.getCount(); %i++)
        {
            %this.Player.AddEventToTrack(%playlist.name, %i, "GameMusicNewTrack", 0);
        }
    }
}

function GameMusicNewTrack()
{
    %song = GameMusic.Player.GetSongTitle();
    %artist = GameMusic.Player.GetSongArtist();
    if ((%artist $= "Unknown") || (%artist $= ""))
    {
        return;
    }

    %ml = "<lmargin%:5><br>" @ "<font:Arial:15>Now Playing<br>" @ "<font:Arial:24>" @ %song @ "<br>" @ "<font:Arial:18>" @ %artist @ "<br>";
    FadingText::start(FadingTextGui, %ml);
}

function GameMusicTrack(%filename, %title, %artist)
{
    GameMusic.Player.AddSongToPlaylist($GameMusicPlaylistName, $GameMusicPath @ %filename, %title, %artist);
}

function GameMusicPlaylistFile(%playlistFile)
{
    %file = new FileObject("") {
    };

    %file.openForRead(%playlistFile);
    while (!%file.isEOF())
    {
        %line = %file.readLine();
        %comment = strpos(%line, "#");
        if (%comment > -1)
        {
            %line = getSubStr(%line, 0, %comment);
        }

        %line = trim(%line);
        if (strlen(%line) > 0)
        {
            %line = strreplace(%line, "\\,", "||");
            %filename = "";
            %song = "";
            %artist = "";
            %comma = strpos(%line, ",");
            if (%comma == -1)
            {
                %filename = %line;
            }
            else
            {
                %filename = getSubStr(%line, 0, %comma);
                %line = getSubStr(%line, %comma + 1, 1000);
                %comma = strpos(%line, ",");
                if (%comma == -1)
                {
                    %song = %line;
                }
                else
                {
                    %song = getSubStr(%line, 0, %comma);
                    %line = getSubStr(%line, %comma + 1, 1000);
                    if (%comma > -1)
                    {
                        %artist = %line;
                    }
                }
            }
            %filename = strreplace(trim(%filename), "||", "\\,");
            %song = strreplace(trim(%song), "||", "\\,");
            %artist = strreplace(trim(%artist), "||", "\\,");
            if (strlen(%filename) > 0)
            {
                GameMusicTrack(%filename, %song, %artist);
            }
        }
    }
    %file.close();
    %file.delete();
}
