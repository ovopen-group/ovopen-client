
function formatImageNumber(%number)
{
    if (%number < 10)
    {
        %number = 0 @ %number;
    }

    if (%number < 100)
    {
        %number = 0 @ %number;
    }

    if (%number < 1000)
    {
        %number = 0 @ %number;
    }

    if (%number < 10000)
    {
        %number = 0 @ %number;
    }
    
    return %number;
}

function formatSessionNumber(%number)
{
    if (%number < 10)
    {
        %number = 0 @ %number;
    }

    if (%number < 100)
    {
        %number = 0 @ %number;
    }

    return %number;
}

$screenshotNumber = 0;

function doScreenShot(%val)
{
    if (%val)
    {
        if ($pref::Video::screenShotHighRes $= "")
        {
            $pref::Video::screenShotHighRes = 1;
        }

        if ($pref::Video::screenShotSession $= "")
        {
            $pref::Video::screenShotSession = 0;
        }

        if ($screenshotNumber == 0)
        {
            $pref::Video::screenShotSession++;
        }

        if ($pref::Video::screenShotSession > 999)
        {
            $pref::Video::screenShotSession = 1;
        }
        
        $pref::interior::showdetailmaps = 0;
        $name = "screenshot_" @ formatSessionNumber($pref::Video::screenShotSession) @ "-" @ formatImageNumber($screenshotNumber++);
        
        if ($pref::Video::screenShotFormat $= "JPEG")
        {
            screenShot($name @ ".jpg", "JPEG");
        }
        else
        {
            screenShot($name @ ".png", "PNG");
        }

        if ($pref::Video::screenShotHighRes > 1)
        {
            doScreenShotHighRes($pref::Video::screenShotHighRes);
        }
    }
}

function doScreenShotHighRes(%num)
{
    $pref::interior::showdetailmaps = 0;
    $name = "screenshot_" @ formatSessionNumber($pref::Video::screenShotSession) @ "-" @ formatImageNumber($screenshotNumber++);
    
    %hideChat = 0;
    if (MainChatHUD.isAwake())
    {
        %hideChat = 1;
        Canvas.popDialog(MainChatHUD);
    }

    %hideAnim = 0;
    if (PlayAnimationUI.isAwake())
    {
        %hideAnim = 1;
        Canvas.popDialog(PlayAnimationUI);
    }

    %hideMouse = 0;
    if (Canvas.isCursorOn())
    {
        %hideMouse = 1;
        cursorOff();
    }

    %windowBar = 0;
    if (WindowBar.isAwake())
    {
        %windowBar = 1;
        Canvas.popDialog(WindowBar);
    }

    %currency = 0;
    if (getCurrentInGameUI()._(currencyBitmap).isVisible())
    {
        %currency = 1;
        getCurrentInGameUI()._(currencyBitmap).setVisible(0);
    }

    %hud = 0;
    if (getCurrentInGameUI()._(onverseHud).isVisible())
    {
        %hud = 1;
        getCurrentInGameUI()._(onverseHud).setVisible(0);
    }

    if ($pref::Video::screenShotFormat $= "JPEG")
    {
        highResScreenShot($name, "JPEG", %num);
    }
    else
    {
        highResScreenShot($name, "PNG", %num);
    }

    if (%hideChat)
    {
        MainChatHUD.show();
    }

    if (%hideAnim)
    {
        ShowAnimationDialog();
    }

    if (%hideMouse)
    {
        cursorOn();
    }

    if (%windowBar)
    {
        Canvas.pushDialog(WindowBar);
    }

    if (%currency)
    {
        getCurrentInGameUI()._(currencyBitmap).setVisible(1);
    }

    if (%hud)
    {
        getCurrentInGameUI()._(onverseHud).setVisible(1);
    }
}

GlobalActionMap.bind(keyboard, "ctrl p", doScreenShot);

