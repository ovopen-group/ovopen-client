
exec("./ui/defaultProfiles.cs");
exec("./ui/SplashScreen.gui");

function ShowSplash(%unused)
{
    Canvas.setContent(SplashScreen);
    Canvas.cursorOff();
    Canvas.repaint();
}

function isSplashDone()
{
    return SplashBitmap.done;
}

function SplashLoadingDone()
{
    SplashScreen._("loadingText").setVisible(0);
}
