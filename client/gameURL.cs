
package GameURL
{
    function GuiMLTextCtrl::onURL(%this, %url)
    {
        %tempurl = strreplace(%url, ":", "\t");
        %content = strchr(%url, ":");

        if (!(%content $= ""))
        {
            %content = getSubStr(%content, 1, 9999999);
            %protocol = getSubStr(%url, 0, (strlen(%url) - strlen(%content)) - 1);
            if (%protocol $= "http")
            {
                gotoWebPage(%url);
            }
            else
            {
                if (%protocol $= "gamelink")
                {
                    %this.parseGameURL(%content);
                }
                else
                {
                    error("Unknown game protocol:" @ %protocol);
                }
            }
        }
        else
        {
            gotoWebPage(%url);
        }
    }

    function GuiMLTextCtrl::parseGameURL(%this, %url)
    {
        %urlarray = strreplace(%url, "/", "\t");
        %i = 0;
        while ((%i < getFieldCount(%urlarray)) && (getField(%urlarray, %i) $= ""))
        {
            %i++;
        }

        %urlarray = getFields(%urlarray, %i);
        if (!%this.handleGameURL(%urlarray, getFieldCount(%urlarray)))
        {
            %string = "";
            for (%i=0; %i < getFieldCount(%urlarray); %i++)
            {
                %string = %string @ getField(%urlarray, %i);
                if (%i < getFieldCount(%urlarray))
                {
                    %string = %string @ "/";
                }
            }
            error("Unhandled game link:" @ %string);
        }
    }

    function GuiMLTextCtrl::handleGameURL(%this, %urlarray, %unused)
    {
        return 0;
    }
};

activatePackage(GameURL);

