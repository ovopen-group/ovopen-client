
$journalActive = 0;
setLogMode(5);

package OnverseClient
{
    function processArguments()
    {
        for (%i=1; %i < $Game::argc; %i++)
        {
            %arg = $Game::argv[%i];
            %nextArg = $Game::argv[%i + 1];
            %hasNextArg = ($Game::argc - %i) > 1;
            if (%arg $= "-jSave")
            {
                if (%hasNextArg)
                {
                    echo("Saving event log to journal: " @ %nextArg);
                    saveJournal(%nextArg @ ".jf");
                    %i++;
                    $journal::Active = 1;
                    $journal::Writing = 1;
                }
                else
                {
                    error("Error: Missing Command Line argument. Usage: -jSave <journal_name>");
                }
            }
            else
            {
                if (%arg $= "-jPlay")
                {
                    if (%hasNextArg)
                    {
                        playJournal(%nextArg @ ".jf", 0);
                        $journal::Active = 1;
                        $journal::File = %nextArg;
                        $journal::Reading = 1;
                    }
                    else
                    {
                        error("Error: Missing Command Line argument. Usage: -jPlay <journal_name>");
                    }
                }
                else
                {
                    if (%arg $= "-jDebug")
                    {
                        if (%hasNextArg)
                        {
                            playJournal(%nextArg @ ".jf", 1);
                            $journal::File = %nextArg;
                            $journal::Active = 1;
                            $journal::Reading = 1;
                        }
                        else
                        {
                            error("Error: Missing Command Line argument. Usage: -jDebug <journal_name>");
                        }
                    }
                    else
                    {
                        if (%arg $= "-WriteDoc")
                        {
                            setLogMode(1);
                            dumpConsoleClasses();
                            dumpConsoleFunctions();
                            return 0;
                        }
                    }
                }
            }
        }
        return 1;
    }

    function LoadCommonDatablocks()
    {
        exec("onverse/common/avatarAnimation.cs");
        exec("onverse/common/audio.cs");
    }

    function includeFiles()
    {
        echo("Including clientMain Files...");
        exec("onverse/common/TScriptArray.cs");
        exec("onverse/common/categoryID.cs");
        exec("onverse/common/onverseServer.cs");
        exec("onverse/common/materialMaps.cs");
        exec("onverse/common/skins.cs");
        exec("onverse/common/definitions.cs");

        LoadCommonDatablocks();

        exec("./screenshot.cs");
        exec("./gameURL.cs");
        exec("./ui/cursor.cs");
        exec("./ui/defaultProfiles.cs");
        exec("./ui/defaultCursors.cs");
        exec("./default.bind.cs");
        exec("./ui/messageBox.cs");
        exec("./ui/mainMenu.cs");
        exec("./ui/optionsDlg.cs");
        exec("./ui/SaveFileDlg.gui");
        exec("./ui/LoadFileDlg.gui");
        exec("./clientContextMenu.cs");
        exec("./messaging.cs");
        exec("./commandParser.cs");
        exec("./clientCommands.cs");
        exec("./adminCommands.cs");
        exec("./masterserver/masterserver.cs");
        exec("./locationsystem/locationsystem.cs");
        exec("./gameMode/gameMode.cs");

        MasterServer::includeFiles();
        gameMode::includeFiles();
        LocationSystem::includeFiles();

        // Compat with pre-2012 client executable(s)
        %ourversion = getVersionString();
        if (%ourversion $= "Beta Nightly_OSX 0.9.950.4385 (20120301031001)")
        {
            echo("Loading 2012 build compat script");
            exec("./compat2012.cs");
        }
    }

    function ActivateDebugJournal(%journalName)
    {
        $journal::File = %journalName;
        playJournal($journal::File @ ".jf", 1);
        $journal::Active = 1;
        $journal::Reading = 1;
    }

    function onStart()
    {
        if (isDebugBuild() || isInternalBuild())
        {
            enableWinConsole(1);
        }

        echo("\n--------- Initializing Onverse Client ---------");
        echo("Version: " @ getVersionString() @ "\n");

        if (processArguments() == 0)
        {
            quit();
        }

        exec("./defaultPrefs.cs");
        setDefaults();
        %defaultsNum = $pref::defaultsNum;
        $pref::defaultsNum = "";
        loadPreferences();

        if (%defaultsNum != $pref::defaultsNum)
        {
            setDefaults();
        }

        if (!($journal::Active || isDebugBuild()) || isInternalBuild())
        {
            echo("Using default journal");
            $journal::File = "journal";
            saveJournal($journal::File @ ".jf");
            $journal::Writing = 1;
        }

        exec("./clientCanvas.cs");
        if (!createClientCanvas("Onverse"))
        {
            return;
        }

        exec("./splash.cs");
        ShowSplash();
        HandleSplash();
        exec("./audio.cs");
        OpenALInit();
        setNetPort(0);
        activateDirectInput();
        enableJoystick();
        includeFiles();

        if ($platform $= "macos")
        {
            exec("./ui/macResizeGUI.gui");
            Canvas.pushDialog(MacResizeGUI, 200);
        }

        setModuleDefaults();
        loadModulePreferences();
        LoadKeyMappings();
        setRandomSeed();
        $loadingDone = 1;
    }

    function HandleSplash()
    {
        if (!isSplashDone() || !$loadingDone)
        {
            if ($loadingDone)
            {
                SplashLoadingDone();
            }
            schedule(100, 0, HandleSplash);
            return;
        }
        cursorOn();
        MainMenuGui.startMainMenu();
        Canvas.pushDialog(masterConnectDlg);
    }

    function onExit()
    {
        MasterServerConnection.disconnect();
        saveModulePreferences();
        savePreferences();
        OpenALShutdown();
    }

    function gotoWebPage(%url)
    {
        if (isFullScreen())
        {
            toggleFullScreen();
        }
        Parent::gotoWebPage(%url);
    }
};

activatePackage(OnverseClient);

