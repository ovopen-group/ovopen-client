
function ClientCmdOnChatMessage(%username, %message)
{
    MainChatHUD.onChatMessage(%message, %username);
}

function ClientCmdOnChatMessageNPC(%username, %message)
{
    MainChatHUD.onChatMessageNPC(%message, %username);
}

function ClientCmdOnShoutMessage(%username, %message)
{
    MainChatHUD.onShoutMessage(%message, %username);
}

function ClientCmdOnServerMessage(%message)
{
    MainChatHUD.onServerMessage(%message);
}

function ClientCmdOnGameMessage(%message)
{
    MainChatHUD.onGameMessage(%message);
}

function ClientCmdOnClientActionMessage(%message)
{
    MainChatHUD.onClientActionMessage(%message);
}

function ClientCmdOnFailedAttackMessage(%tooltarget)
{
    %tooltarget = LocationGameConnection.resolveGhostID(%tooltarget);
    MainChatHUD.onFailedAttackMessage(%tooltarget);
}

function ClientCmdOnMasterServerMessage(%message)
{
    MasterMessageBox.onServerMessage(%message);
    MainChatHUD.onServerAnnouncement(%message);
}

function Messaging::SendLocalMessage(%message)
{
    if (isObject(LocationGameConnection) && LocationGameConnection.isInGame)
    {
        MainChatHUD.onSentLocalMessage(%message);
        commandToServer('LocalMessage', %message);
    }
    else
    {
        MainChatHUD.onCommandError("You are not currently in an instance.", "No one can hear you.");
    }
}

function Messaging::SendShoutMessage(%message)
{
    if (isObject(LocationGameConnection) && LocationGameConnection.isInGame)
    {
        MainChatHUD.onSentShoutMessage(%message);
        commandToServer('ShoutMessage', %message);
    }
    else
    {
        MainChatHUD.onCommandError("You are not currently in an instance.", "No one can hear you.");
    }
}
