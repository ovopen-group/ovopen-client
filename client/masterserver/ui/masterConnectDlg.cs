exec("./masterConnectDlg.gui");
exec("./masterConnectStatusDlg.gui");

function masterConnectDlg::onWake(%this)
{
    %this._("ServerAddress").text = $pref::MasterServer::address;
    %this._("ServerPort").text = $pref::MasterServer::port;
    %this._("UserName").text = $pref::Player::Name;
    %this._("Password").text = $pref::Player::Password;
    %this.schedule(0, updateDisplayState);

    MasterServerConnection.RegisterForBroadcast(%this);

    if (MasterServerConnection.isConnected())
    {
        Canvas.schedule(0, popDialog, %this);
        MessageBoxOK("Error", "Master server already connected.");
    }
}

function masterConnectDlg::onSleep(%this)
{
    MasterServerConnection.UnRegisterForBroadcast(%this);
}

function masterConnectDlg::onConnectClick(%this)
{
    %this._("ServerAddress").makeFirstResponder(0);
    %this._("ServerPort").makeFirstResponder(0);
    %this._("UserName").makeFirstResponder(0);
    %this._("Password").makeFirstResponder(0);

    %address = $pref::MasterServer::default_address;
    %port = $pref::MasterServer::default_port;

    if ($pref::masterConnectDlg::flagAdvanced)
    {
        $pref::MasterServer::address = %this._("ServerAddress").getText();
        $pref::MasterServer::port = %this._("ServerPort").getText();
        %address = $pref::MasterServer::address;
        %port = $pref::MasterServer::port;
    }
    else
    {
        $pref::MasterServer::address = %this._("ServerAddress").getText();
        $pref::MasterServer::port = %this._("ServerPort").getText();
        %saved_address = $pref::MasterServer::address;
        %saved_port = $pref::MasterServer::port;
    }
    
    $currentPlayerName = %this._("UserName").getText();
    $currentPassword = %this._("Password").getText();
    $pref::Player::Name = $currentPlayerName;

    if (isDebugBuild() || isInternalBuild())
    {
        $pref::Player::Password = $currentPassword;
    }
    else
    {
        $pref::Player::Password = "";
    }
    ConnectMasterServer(%address, %port, %address $= $pref::MasterServer::default_address);
}

function masterConnectDlg::updateDisplayState(%this)
{
    %pos = %this._("Window").getPosition();

    if (!$pref::masterConnectDlg::flagAdvanced)
    {
        %this._("Normal").setVisible(0);
        %this._("Signup").setVisible(1);
        %this._("AdvServer1").setVisible(0);
        %this._("AdvServer2").setVisible(0);
        %extent = "261 128";
    }
    else
    {
        %this._("Normal").setVisible(1);
        %this._("Signup").setVisible(0);
        %this._("AdvServer1").setVisible(1);
        %this._("AdvServer2").setVisible(1);
        %extent = "261 178";
    }

    if (%this._("UserName").getText() $= "")
    {
        %this._("UserName").makeFirstResponder(1);
    }
    else
    {
        %this._("Password").makeFirstResponder(1);
    }

    %this._("Window").resize(getWord(%pos, 0), getWord(%pos, 1), getWord(%extent, 0), getWord(%extent, 1));
}

function masterConnectDlg::onAdvancedClick(%this)
{
    $pref::masterConnectDlg::flagAdvanced = !$pref::masterConnectDlg::flagAdvanced;
    %this.updateDisplayState();
}

function masterConnectDlg::onSignupClick(%this)
{
    gotoWebPage("http://www.onverse.com/");
}

function MasterConnectTextEdit::onTabComplete(%this, %shift)
{
    if (%this.internalName $= "ServerAddress")
    {
        %nextCtrl = !%shift ? "ServerPort" : "Password";
    }
    else
    {
        if (%this.internalName $= "ServerPort")
        {
            %nextCtrl = !%shift ? "UserName" : "ServerAddress";
        }
        else
        {
            if (%this.internalName $= "UserName")
            {
                if ($pref::masterConnectDlg::flagAdvanced)
                {
                    %nextCtrl = !%shift ? "Password" : "ServerPort";
                }
                else
                {
                    %nextCtrl = "Password";
                }
            }
            else
            {
                if (%this.internalName $= "Password")
                {
                    if ($pref::masterConnectDlg::flagAdvanced)
                    {
                        %nextCtrl = !%shift ? "ServerAddress" : "UserName";
                    }
                    else
                    {
                        %nextCtrl = "UserName";
                    }
                }
                else
                {
                    if ($pref::Player::Name $= "")
                    {
                        %nextCtrl = "UserName";
                    }
                    else
                    {
                        %nextCtrl = "Password";
                    }
                }
            }
        }
    }

    masterConnectDlg._(%nextCtrl).makeFirstResponder(1);
}

function masterConnectDlg::onServerError(%this, %code)
{
    CloseMessagePopup();
    MessageBoxOK("Error", "<just:center>Error occured on the Master Server, try again later..." NL %code, "");
}

function masterConnectDlg::onConnecting(%this)
{
    CloseMessagePopup();
    MessagePopup("Connecting...", "<just:center>Connecting to Master Server...");
}

function masterConnectDlg::onAuthenticating(%this)
{
    CloseMessagePopup();
    MessagePopup("Connecting...", "<just:center>Authenticating username/password...");
}

function masterConnectDlg::onConnectFailed(%this)
{
    CloseMessagePopup();
    MessageBoxOK("Attention", "<just:center>Could not connect. We are either in the process of updating and should be back up soon or Onverse cannot connect to the internet. Check your internet connection and be sure Onverse is not blocked by any firewall software.", "");
}

function masterConnectDlg::onAuthSuccess(%this, %unused, %unused)
{
    CloseMessagePopup();
    Canvas.popDialog(masterConnectDlg);
}

function masterConnectDlg::onAuthFail(%this, %code)
{
    CloseMessagePopup();
}
