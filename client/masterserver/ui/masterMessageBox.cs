
if (!isObject(MasterMessageBox))
{
    exec("./masterMessageBox.gui");
}

new GuiControlProfile(GuiMasterMessageWindowProfile : GuiWindowProfile)
{
    border = "1";
    borderColor = "255 255 255";
};

function MasterMessageBox::onServerMessage(%this, %message)
{
    %textCtrl = new GuiMLTextCtrl() {
        superClass = "MasterTextBlock";
        position = "4 4";
        Extent = getWord(%this._(fieldStack).Extent, 0) - 8 SPC 0;
    };

    %textCtrl.setText(%message);

    %windowCtrl = new GuiControl() {
        Profile = "GuiMasterMessageWindowProfile";
        Extent = getWord(%textCtrl.Extent, 0) + 8 SPC getWord(%textCtrl.Extent, 1) + 8;
    };

    %windowCtrl.add(%textCtrl);

    %this._(fieldStack).add(%windowCtrl);
    %this.showBox();
}

function MasterMessageBox::showBox(%this)
{
    Canvas.pushDialog(%this, 98);
}

function MasterMessageBox::closeBox(%this)
{
    Canvas.popDialog(%this);
}

function MasterTextBlock::onResize(%this, %x, %y)
{
    %this.getParent().resize(0, 0, %x + 8, %y + 8);
    %stackCtrl = %this.getParent().getParent();
    %stackCtrl.resize(0, 0, getWord(%stackCtrl.Extent, 0), 0);
}
