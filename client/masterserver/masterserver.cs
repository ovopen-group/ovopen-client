
$isServerDrop = 0;

function MasterServer::includeFiles()
{
    echo("Initializing master server component.");
    exec("./ui/masterConnectDlg.cs");
    exec("./ui/masterMessageBox.cs");

    new MasterServerObject(MasterServerConnection) {
    };

    MasterServerConnection.active = 0;
}

function ConnectMasterServer(%address, %port, %isDefault)
{
    echo("Connecting to master server " @ %address @ ":" @ %port);
    MasterServerConnection.connect("ip:" @ %address @ ":" @ %port);
    MasterServerConnection.connectedAddress = %address;
    MasterServerConnection.connectedPort = %port;
    MasterServerConnection.connectedIsDefault = %isDefault;
    MasterServerConnection.doReconnect = 0;
}

function MasterServerObject::disconnect(%this)
{
    MasterServerConnection.active = 0;
    Parent::disconnect(%this);
}

function MasterServerObject::onDisconnect(%this)
{
    if (MasterServerConnection.active)
    {
        MasterServerConnection.active = 0;
    }
    
    if (!isLighting())
    {
        %this.onDropped();
    }

    echo("Master server connection disconnected.");
    if (%this.doReconnect)
    {
        echo("Reconnecting...");
        ConnectMasterServer(%this.connectedAddress, %this.connectedPort, %this.connectedIsDefault);
    }
    else
    {
        if (!isLighting())
        {
            %this.onDropped();
        }
    }
}

function MasterServerObject::onDropped(%this)
{
    LocationSystem::disconnect(1);
    Canvas.pushDialog(masterConnectDlg);

    if (!$isServerDrop)
    {
        MessageBoxOK("Dropped", "Connection with master has been lost");
    }
    else
    {
        RePushMessageBox();
        $isServerDrop = 0;
    }

    error("Master connection dropped.");
}

function MasterServerObject::onForwardPacket(%this, %address)
{
    echo("Got forwarding address");
    %this.disconnect();
    if (%this.connectedAddress $= %address)
    {
        error("Forward recursion detected.");
        return ;
    }
    %this.connectedAddress = %address;
    %this.doReconnect = 1;
    return ;
}

function MasterServerObject::onConnectFailed(%this)
{
    MasterServerConnection.active = 0;
    echo("Connection to master server failed.");
}

function MasterServerObject::onServerError(%this, %code)
{
    LocationSystem::disconnect(1);
    Canvas.pushDialog(masterConnectDlg);
    LocationSystem::onServerError(%code);
    MasterServerConnection.active = 0;

    if (%code == 4)
    {
        if (%this.authFailCode == 2)
        {
            %msg = "Authorization failed bad username/password";
        }
        else
        {
            if (%this.authFailCode == 3)
            {
                %msg = "Connection denied. This usually happens while the servers are updating.";
            }
            else
            {
                if (%this.authFailCode == 4)
                {
                    %msg = "Your account is banned from Onverse.";
                }
                else
                {
                    if (%this.authFailCode == 5)
                    {
                        %msg = "Your account is currently suspended.";
                    }
                    else
                    {
                        %msg = "Unknown permissions error.";
                    }
                }
            }
        }
        MessageBoxOK("Error", "<just:center>" @ %msg, "");
        $isServerDrop = 1;
    }
    else
    {
        if (%code == 8)
        {
            MessageBoxOK("Kicked", "<just:center>Your connection was kicked from the server.");
            $isServerDrop = 1;
        }
    }

    error("Master server connection error.");
}

function MasterServerObject::onRequestAuth(%this)
{
    MasterServerConnection.active = 0;
    %this.SendAuthResponse($currentPlayerName, $currentPassword);
}

function MasterServerObject::onAuthSuccess(%this, %uniqueID, %unused)
{
    $currentPlayerID = %uniqueID;
    MasterServerConnection.active = 1;
    InitializeAdminCommands();
    EquipBar.Load();

    if (!$pref::tutorialShown)
    {
        resolveURL("onverse://location/hub/Hub 2/tutorial");
        $pref::tutorialShown = 1;
    }
    else
    {
        ShowDirectory();
    }

    MainChatHUD.onServerMessage("Welcome to Onverse! Please do not give your password to anybody while playing. We\'re not responsible for stolen accounts due to information sharing. If asked for this information, please report the user to a Guide.");
}

function MasterServerObject::onAuthFail(%this, %code)
{
    MasterServerConnection.active = 0;
    %this.authFailCode = %code;
    echo("Authorization failed with code: " @ %code);
}

function MasterServerObject::onInstanceReady(%this, %location, %instance, %address, %port)
{
    LocationSystem::onLocationReady(%location, %instance, %address, %port);
}

function MasterServerObject::onEntireFriendList(%this, %numFriends)
{
    echo("Receiving " @ %numFriends @ " friends.");
}

function MasterServerObject::onAddEntireFriend(%this, %friendID, %uname, %state)
{
}

function MasterServerObject::onFriendUpdate(%this, %friendID, %uname, %state)
{
    echo("Friend Update: " @ %uname @ "(" @ %friendID @ ") is now in state: " @ %state);
}

function MasterServerObject::onPrivateMessage(%this, %type, %from, %message)
{
    if (%type == 0)
    {
        MainChatHUD.onPrivateMessage(%from, %message);
    }
    else
    {
        if (%type == 1)
        {
            MainChatHUD.onFriendsMessage(%from, %message);
        }
        else
        {
            if (%type == 2)
            {
                MainChatHUD.onGuideMessage(%from, %message);
            }
            else
            {
                if (%type == 3)
                {
                    MainChatHUD.onUserToGuideMessage(%from, %message);
                }
                else
                {
                    error("Invalid private message type from:" @ %from);
                }
            }
        }
    }
}

function MasterServerObject::onGuideReplyMessage(%this, %from, %to, %message)
{
    MainChatHUD.onGuideReplyMessage(%from, %to, %message);
}

function MasterServerObject::onMasterVersion(%this, %serverversion)
{
    %ourversion = getVersionString();
    %ours = getWord(%ourversion, 2);
    %servers = getWord(%serverversion, 2);
    if (%ours $= %servers)
    {
        return;
    }

    if (getWord(%ourversion, 3) $= "(00000000000000)")
    {
        if (!(getWord(%serverversion, 3) $= "(00000000000000)"))
        {
            if ($journal::Reading || !(%this.connectedIsDefault))
            {
                return;
            }

            schedule(100, 0, "OnDisconnectForDownload", %ours, 2);
            %this.disconnect();
            return;
        }
    }
    else
    {
        if (getWord(%serverversion, 3) $= "(00000000000000)")
        {
            return;
        }
    }

    schedule(100, 0, "OnDisconnectForDownload", %ours, 1);
    %this.disconnect();
}

function OnDisconnectForDownload(%ours, %type)
{
    if (%type == 1)
    {
        if (!isMacAppstoreBuild())
        {
            MessageBoxOK("Update", "You must download the latest update for Onverse. Close and reopen Onverse to allow the patcher to download the latest update. If you\'re having trouble updating please email support@onverse.com.", "", 98);
        }
        else
        {
            MessageBoxOK("Update", "You must download the latest version of Onverse from the Apple Appstore. Alternatively you may download Onverse directly from <a:http://www.onverse.com/download/download-mac.php>Onverse.com</a>. If you\'re having trouble updating please email support@onverse.com.", "", 98);
        }
    }
    else
    {
        MessageBoxOK("Update", "Cannot run local build on production server.", "", 98);
    }
}

function MasterServerObject::onServerMessage(%this, %message)
{
    MasterMessageBox.onServerMessage(%message);
    MainChatHUD.onServerAnnouncement(%message);
}

function MasterServerObject::onServerAnnouncement(%this, %message)
{
    MainChatHUD.onServerAnnouncement(%message);
}
