
exec("./connectToLocationDlg.gui");
exec("./LoadTips.cs");

function connectToLocationDlg::onWake(%this)
{
    %this._(ProgressBar).setValue(0);
    %this._(ProgressBar).setVisible(1);
    %this.Connecting = 0;
    GameMusic.FadeOut(1);
    GameMusic.FadeOut();
}

function connectToLocationDlg::onSleep(%this)
{
    %this.Connecting = 0;
}

function connectToLocationDlg::close(%this)
{
    %this.Connecting = 0;
    Canvas.popDialog(%this);
    LocationSystem::disconnect(1);
}

function connectToLocationDlg::Connecting(%this)
{
    LoadTipsGui.show();
    Canvas.pushDialog(%this);
    %this._(ProgressBar).setVisible(1);
    %this._(ProgressBar).setValue(0);
    %this.Connecting = 1;
}

function connectToLocationDlg::onConnectionAccepted(%this)
{
}

function connectToLocationDlg::onConnectionDropped(%this, %reason)
{
    %this.close();
    MessageBoxOK("Error", "Connection dropped, " @ %reason, "");
}

function connectToLocationDlg::onDataBlock(%this, %index, %total)
{
    %val = (%index + 1) / %total;
    if (%val < 0.5)
    {
    }
    %this._(ProgressBar).setValue(%val / 2);
}

function connectToLocationDlg::onGhostAlwaysObject(%this, %number, %total)
{
    %val = %number / %total;
    if (%val < 0.5)
    {
    }
    %this._(ProgressBar).setValue((%val / 2) + 0.5);
}

function connectToLocationDlg::onFileChunkReceived(%this, %unused, %ofs, %size)
{
    %this._(ProgressBar).setValue(%ofs / %size);
}

function connectToLocationDlg::StartingReplication(%this)
{
}
