
new GuiControlProfile(GuiHomeSetButtonProfile : GuiDefaultProfile) {
   bitmap = "~/data/live_assets/engine/ui/images/icons/directory/house_icons";
};

new GuiControlProfile(GuiHeaderCommunitySetButtonProfile : GuiDefaultProfile) {
   bitmap = "~/data/live_assets/engine/ui/images/icons/directory/community_header";
};

new GuiControlProfile(GuiHeaderUsersSetButtonProfile : GuiDefaultProfile) {
   bitmap = "~/data/live_assets/engine/ui/images/icons/directory/instance_header_users";
};

new GuiControlProfile(GuiHeaderFriendsSetButtonProfile : GuiDefaultProfile) {
   bitmap = "~/data/live_assets/engine/ui/images/icons/directory/instance_header_friends";
};

new GuiControlProfile(GuiHeaderHomesSetButtonProfile : GuiDefaultProfile) {
   bitmap = "~/data/live_assets/engine/ui/images/icons/directory/instance_header_homes";
};

exec("./directoryGUI.gui");
exec("./Directory_World.gui");
exec("./Directory_AncientMoon_Region.gui");
exec("./Directory_Games_Region.gui");
exec("./Directory_Hub_Region.gui");
exec("./Directory_Island_Region.gui");
exec("./Directory_Burning_Region.gui");
exec("./Directory_Paradise_Region.gui");
exec("./Directory_Penthouse_Region.gui");
exec("./Directory_Events.gui");
exec("./Directory_Events_Region.gui");
exec("./Directory_Wonderland.gui");
exec("./Directory_Valentines.gui");
exec("./Directory_Mardi_Gras.gui");
exec("./Directory_SPD.gui");
exec("./Directory_Candyland_Region.gui");
exec("./Directory_Candyland.gui");
exec("./Directory_Candyland_Plots.gui");
exec("./Directory_Island.gui");
exec("./Directory_Island_Apt_Sm.gui");
exec("./Directory_Island_Apt_Med.gui");
exec("./Directory_Island_Estates.gui");
exec("./Directory_IslandMegaPlots.gui");
exec("./Directory_Hub.gui");
exec("./Directory_Hub_Apt_Sm.gui");
exec("./Directory_Hub_Apt_Med.gui");
exec("./Directory_Hub_Penthouse.gui");
exec("./Directory_Hub_Penthouse_Deluxe.gui");
exec("./Directory_Hub_Sharktank.gui");
exec("./Directory_Hub_Suburbia.gui");
exec("./Directory_Splatball.gui");
exec("./Directory_Tutorial.gui");
exec("./Directory_IceFall.gui");
exec("./Directory_Casino.gui");
exec("./Directory_GuideHall.gui");
exec("./Directory_AncientMoon.gui");
exec("./Directory_AncientMoon_Apt_Sm.gui");
exec("./Directory_AncientMoon_Apt_Med.gui");
exec("./Directory_AncientMoon_Estates.gui");
exec("./Directory_Lodges.gui");
exec("./Directory_BurningLands.gui");
exec("./Directory_IslandLands.gui");
exec("./Directory_AncientMoonLands.gui");
exec("./Directory_LodgesLands.gui");
exec("./Directory_Furniture_Shop.gui");
exec("./Directory_BurningHousing.gui");
exec("./Directory_EchoCanyon_Housing.gui");
exec("./Directory_EchoCanyon_Lands.gui");
exec("./Directory_MagicalForest.gui");
exec("./Directory_Forest_Estates.gui");
exec("./Directory_Forest_Region.gui");
exec("./Directory_ForestLand.gui");
exec("./Directory_Tilez.gui");
exec("./Directory_Trials_Ancients.gui");
exec("./Directory_Ancient_Cultures.gui");

function RPC_ResolveURL::onInstanceListReceived(%this, %location, %numInsts, %instanceListString)
{
    echo("RPC returned instance list for " @ %location @ " with " @ %numInsts @ " entries");
    DirectoryGUI.onInstanceList(%this, %numInsts, %instanceListString);
}

new SimGroup(PendingDirectories) {
};

new SimSet(ClearDirectories) {
};

function ShowDirectory()
{
    if (!DirectoryGUI.isAwake())
    {
        $CurrentResolveURL[locationID] = $location::locationID;
        $CurrentResolveURL[location] = $location::location;
        $CurrentResolveURL[instanceID] = $location::instanceID;
        $CurrentResolveURL[instance] = $location::instance;
        $CurrentSortInstances = 2;
        $CurrentResolveURL[poi] = "";
        if (DirectoryGUI.initCurrentURLState())
        {
            Canvas.pushDialog(DirectoryGUI);
        }
    }
    else
    {
        Canvas.popDialog(DirectoryGUI);
    }
}

function UpdateDirectory()
{
    if (DirectoryGUI.initCurrentURLState())
    {
        Canvas.pushDialog(DirectoryGUI);
    }
    else
    {
        Canvas.popDialog(DirectoryGUI);
    }
}

function DirectoryGUI::onSleep(%this)
{
    %this.clearTabs();
}

function DirectoryGUI::onWake(%this)
{
    if (!%this.directoryListActive)
    {
        %this.initializeActiveDirectoryList();
    }
    
    cursorOn();
}

function DirectoryGUI::initializeActiveDirectoryList(%this)
{
    %this.directoryListActive = 1;
    
    %this.clearRegions();
    %this.addRegion(Hub, Directory_Hub_Region);
    %this.addRegion(Penthouse, Directory_Penthouse_Region, Directory_Hub_Region);
    %this.addRegion(Island, Directory_Island_Region);
    %this.addRegion(Paradise, Directory_Paradise_Region, Directory_Island_Region);
    %this.addRegion(Burning, Directory_Burning_Region, Directory_Island_Region);
    %this.addRegion(Forest, Directory_Forest_Region);
    %this.addRegion(AncientMoon, Directory_AncientMoon_Region);
    %this.addRegion(Candy, Directory_Candyland_Region);
    %this.addRegion(Games, Directory_Games_Region);
    %this.addRegion(Events, Directory_Events_Region);
    
    %this.clearLocations();
    %this.addLocation(60, "Events", Directory_Events, Directory_Events_Region);
    %this.addLocation(67, "winter_wonderland", Directory_Wonderland, Directory_Events_Region);
    %this.addLocation(68, "valentines", Directory_Valentines, Directory_Events_Region);
    %this.addLocation(69, "mardi_gras", Directory_Mardi_Gras, Directory_Events_Region);
    %this.addLocation(70, "spd", Directory_SPD, Directory_Events_Region);
    %this.addLocation(55, "tang_coast", Directory_Tilez, Directory_Games_Region);
    %this.addLocation(23, "guide_hall", Directory_GuideHall);
    %this.addLocation(51, "trials_of_the_ancients", Directory_Trials_Ancients, Directory_Games_Region);
    %this.addLocation(10, "Island", Directory_Island, Directory_Island_Region);
    %this.addLocation(14, "paradisefree", Directory_Island_Apt_Sm, Directory_Island_Region);
    %this.addLocation(19, "paradisedeluxe", Directory_Island_Apt_Med, Directory_Island_Region);
    %this.addLocation(36, "paradise_estates", Directory_Island_Estates, Directory_Island_Region TAB Directory_Paradise_Region);
    %this.addLocation(42, "paradise_land", Directory_IslandLands, Directory_Island_Region TAB Directory_Paradise_Region);
    %this.addLocation(62, "paradise_megaplots", Directory_IslandMegaPlots, Directory_Island_Region TAB Directory_Paradise_Region);
    %this.addLocation(41, "burning_lands", Directory_BurningLands, Directory_Island_Region TAB Directory_Burning_Region);
    %this.addLocation(45, "burning_housing", Directory_BurningHousing, Directory_Island_Region TAB Directory_Burning_Region);
    %this.addLocation(21, "Tutorial", Directory_Tutorial, Directory_Hub_Region);
    %this.addLocation(9, "Hub", Directory_Hub, Directory_Hub_Region);
    %this.addLocation(20, "metroviewfree", Directory_Hub_Apt_Sm, Directory_Hub_Region);
    %this.addLocation(18, "metroviewdeluxe", Directory_Hub_Apt_Med, Directory_Hub_Region);
    %this.addLocation(28, "metroview_ph", Directory_Hub_Penthouse, Directory_Hub_Region TAB Directory_Penthouse_Region);
    %this.addLocation(56, "metro_ph_deluxe", Directory_Hub_Penthouse_Deluxe, Directory_Hub_Region TAB Directory_Penthouse_Region);
    %this.addLocation(59, "sharktank", Directory_Hub_Sharktank, Directory_Hub_Region);
    %this.addLocation(61, "suburbia", Directory_Hub_Suburbia, Directory_Hub_Region);
    %this.addLocation(63, "furniture_shop", Directory_Furniture_Shop, Directory_Hub_Region);
    %this.addLocation(64, "candyland", Directory_Candyland, Directory_Candyland_Region);
    %this.addLocation(65, "candyland_plots", Directory_Candyland_Plots, Directory_Candyland_Region);
    %this.addLocation(25, "ancientmoon", Directory_AncientMoon, Directory_AncientMoon_Region);
    %this.addLocation(29, "soldeluxe", Directory_AncientMoon_Apt_Med, Directory_AncientMoon_Region);
    %this.addLocation(30, "solfree", Directory_AncientMoon_Apt_Sm, Directory_AncientMoon_Region);
    %this.addLocation(37, "sol_estates", Directory_AncientMoon_Estates, Directory_AncientMoon_Region);
    %this.addLocation(43, "sol_land", Directory_AncientMoonLands, Directory_AncientMoon_Region);
    %this.addLocation(38, "echo_canyon", Directory_EchoCanyon_Housing, Directory_AncientMoon_Region);
    %this.addLocation(52, "echo_canyon_land", Directory_EchoCanyon_Lands, Directory_AncientMoon_Region);
    %this.addLocation(40, "magical_forest", Directory_MagicalForest, Directory_Forest_Region);
    %this.addLocation(53, "forest_land", Directory_ForestLands, Directory_Forest_Region);
    %this.addLocation(54, "forest_estates", Directory_Forest_Estates, Directory_Forest_Region);
    %this.addLocation(39, "lodges", Directory_Lodges, Directory_Forest_Region);
    %this.addLocation(44, "mountainview_land", Directory_LodgesLands, Directory_Forest_Region);
    %this.addLocation(24, "icefall", Directory_IceFall, Directory_Games_Region);
    %this.addLocation(32, "splatball", Directory_Splatball, Directory_Games_Region);
    %this.addLocation(58, "ancient_cultures", Directory_Ancient_Cultures, Directory_Games_Region);
    %this.addLocation(66, "casino", Directory_Casino, Directory_Games_Region);
    //ClearDirectories.add(Directory_VIP_Region);
}

function DirectoryGUI::clearLocations(%this, %locationID)
{
    for (%i=0; %i < %this.directoryLocations; %i++)
    {
        %locationID = %this.directoryLocations[%i];
        %this.directoryLocations[%i] = "";
        %this.directoryLocations[name,%locationID] = "";
        %this.directoryLocations[UI,%locationID] = "";
        %this.directoryLocations[Dep,%locationID] = "";
    }
    %this.directoryLocations = 0;
}


function DirectoryGUI::clearRegions(%this)
{
    for (%i=0; %i < %this.directoryRegions; %i++)
    {
        %regionName = %this.directoryRegions[%i];
        %this.directoryRegions[name,%regionName] = "";
        %this.directoryRegions[UI,%i] = "";
        %this.directoryRegions[Dep,%i] = "";
    }
    %this.directoryRegions = 0;
    return ;
}

function DirectoryGUI::addLocation(%this, %locationID, %locationName, %directoryUI, %dependents)
{
    %i = %this.directoryLocations;
    %this.directoryLocations[%i] = %locationID;
    %this.directoryLocations[name,%locationID] = %locationName;
    %this.directoryLocations[UI,%locationID] = %directoryUI;
    %this.directoryLocations[Dep,%locationID] = %dependents;

    if (%this.directoryLocations[UIRev,%directoryUI] !$= "")
    {
        error("Overriding UIRev for" SPC %directoryUI @ ".");
    }
    
    %this.directoryLocations[UIRev,%directoryUI] = %locationID;
    %this.directoryLocations++;
}

function DirectoryGUI::addRegion(%this, %regionName, %directoryUI, %dependents)
{
    %i = %this.directoryRegions;
    %this.directoryRegions[%i] = %regionName;
    %this.directoryRegions[name,%regionName] = %i;
    %this.directoryRegions[UI,%i] = %directoryUI;
    %this.directoryRegions[Dep,%i] = %dependents;
    %this.directoryRegions++;
    return ;
}

function DirectoryGUI::initCurrentURLState(%this)
{
    if (!%this.directoryListActive)
    {
        %this.initializeActiveDirectoryList();
    }
    Directory_World._(tutorialSplash).setVisible(0);

    if (($CurrentResolveURL[locationID] != -1) &&
        ($CurrentResolveURL[instanceID] != -1))
    {
        %connect = 1;
        if ($CurrentResolveURL[poi] $= "")
        {
            for (%i=0; %i < %this.directoryLocations; %i++)
            {
                if (%this.directoryLocations[%i] == $CurrentResolveURL[locationID])
                {
                    %connect = 0;
                    break;
                }
            }
        }
        if (%connect)
        {
            LocationSystem::ConnectToLocation($CurrentResolveURL[locationID], $CurrentResolveURL[location], $CurrentResolveURL[instanceID], $CurrentResolveURL[instance], $CurrentResolveURL[poi]);
            return 0;
        }
    }

    %this.clearTabs();
    %this._(worldTabControl).add(Directory_World);

    if ($CurrentResolveURL[locationID] != -1)
    {
        for (%i=0; %i < %this.directoryLocations; %i++)
        {
            if (%this.directoryLocations[%i] == $CurrentResolveURL[locationID])
            {
                %locationID = %this.directoryLocations[%i];
                return %this.showLocation(%locationID, $CurrentResolveURL[instanceID]);
            }
        }
    }

    %this._(worldTabControl).setSelectedTab(0);
    return 1;
}

function DirectoryGUI::showLocation(%this, %locationID, %instanceID)
{
    %selectedID = 1;
    %directoryUI = %this.directoryLocations[UI,%locationID];
    %dependents = %this.directoryLocations[Dep,%locationID];
    %count = getFieldCount(%dependents);

    for (%i=0; %i < %count; %i++)
    {
        %dependent = getField(%dependents, %i);
        %this._(worldTabControl).add(%dependent);
        %selectedID++;
        %dependentID = %this.directoryLocations[UIRev,%dependent];
        if (%dependentID !$= "")
        {
            %dependent._(InstanceList).parentUI = %dependent;
            %this.getInstanceData(%dependentID, %dependent._(InstanceList), -1);
        }
    }

    %this._(worldTabControl).add(%directoryUI);
    %this._(worldTabControl).setSelectedTab(%selectedID);
    %directoryUI._(InstanceList).parentUI = %directoryUI;
    %this.getInstanceData(%locationID, %directoryUI._(InstanceList), %instanceID);

    return 1;
}

function DirectoryGUI::showRegion(%this, %regionName)
{
    %this.clearTabs();
    %this._(worldTabControl).add(Directory_World);
    %selectedID = 1;
    %regionID = %this.directoryRegions[name,%regionName];
    if (%regionID $= "")
    {
        error("Could not find region with name \'" @ %regionName @ "\'");
        %this._(worldTabControl).setSelectedTab(0);
        return 1;
    }
    %directoryUI = %this.directoryRegions[UI,%regionID];
    %dependents = %this.directoryRegions[Dep,%regionID];
    %count = getFieldCount(%dependents);

    for (%i=0; %i < %count; %i++)
    {
        %dependent = getField(%dependents, %i);
        %this._(worldTabControl).add(%dependent);
        %selectedID = %selectedID + 1;
    }

    %this._(worldTabControl).add(%directoryUI);
    %this._(worldTabControl).setSelectedTab(%selectedID);
    return ;
}

function DirectoryGUI::clearTabs(%this)
{
    for (%i=0; %i < %this.directoryLocations; %i++)
    {
        %locationID = %this.directoryLocations[%i];
        %directoryUI = %this.directoryLocations[UI,%locationID];
        %dependents = %this.directoryLocations[Dep,%locationID];
        %count = getFieldCount(%dependents);

        for (%j=0; %j < %count; %j++)
        {
            %dependent = getField(%dependents, %j);
            PendingDirectories.add(%dependent);
        }

        PendingDirectories.add(%directoryUI);
    }

    for (%i=0; %i < ClearDirectories.getCount(); %i++)
    {
        PendingDirectories.add(ClearDirectories.getObject(%i));
    }
}

function DirectoryGUI::getInstanceData(%this, %locationID, %instanceList, %selInst)
{
    %instanceList.clear();
    %instanceList.addRow(0, "Retrieving instance data...");
    %instanceList.selected = %selInst;
    %rpcObject = new RPC_ResolveURL("") {
    };
    %rpcObject.InstanceList = %instanceList;
    %rpcObject.InstanceList.locationID = %locationID;
    %rpcObject.getInstances(%locationID);
    %rpcObject.fireRPC(MasterServerConnection);
}

function DirectoryGUI::onInstanceList(%this, %rpcObject, %numInsts, %instString)
{
    %rpcObject.InstanceList.clear();
    for (%i=0; %i < %numInsts; %i++)
    {
        %rowdata = getRecord(%instString, %i);
        %instID = getField(%rowdata, 0);
        %rpcObject.InstanceList.addRow(%instID, getFields(%rowdata, 1, 4) TAB " ");
    }
    
    %this.sortInstanceList(%rpcObject.InstanceList);
    %selected = %rpcObject.InstanceList.getRowId(0);

    if (%rpcObject.InstanceList.selected != -1)
    {
        %rpcObject.InstanceList.setSelectedById(%rpcObject.InstanceList.selected);
    }
    else
    {
        if (%selected != -1)
        {
            %rpcObject.InstanceList.setSelectedById(%selected);
        }
    }
}

function DirectoryGUI::sortInstanceList(%this, %instList)
{
    %column = $CurrentSortInstances % 4;
    if ($CurrentSortInstances > 3)
    {
        %invert = 1;
    }

    if (%column == 0)
    {
        %instList.sort(%column, %invert);
    }
    else
    {
        %instList.sortNumerical(%column, %invert);
    }

    %base = %instList.getParent().getParent();
    %base._(communityButton).curset = %column == 0 ? (%invert ? 2 : 1) : 0;
    %base._(usersButton).curset = %column == 1 ? (%invert ? 2 : 1) : 0;
    %base._(homesButton).curset = %column == 2 ? (%invert ? 2 : 1) : 0;
    %base._(friendsButton).curset = %column == 3 ? (%invert ? 2 : 1) : 0;
}

function DirectoryGUI::onInstanceClick(%this, %list)
{
    %instID = %list.getSelectedId();
    %instName = getField(%list.getRowTextById(%instID), 0);
    $CurrentResolveURL[locationID] = %list.locationID;
    $CurrentResolveURL[location] = %this.directoryLocations[name,%list.locationID];
    $CurrentResolveURL[instanceID] = %instID;
    $CurrentResolveURL[instance] = %instName;
    %rpcObject = new RPC_InstanceHousing() {
    };
    %rpcObject.locationID = $CurrentResolveURL[locationID];
    %rpcObject.instanceID = $CurrentResolveURL[instanceID];
    %rpcObject.currentDirectoryUI = %list.parentUI;
    %this.setHouseData(%list.parentUI, "");
    %rpcObject.fireRPC(MasterServerConnection);
}

function DirectoryGUI::setHouseData(%this, %UIctrl, %houseData)
{
    %count = %UIctrl.getCount();
    for (%i=0; %i < %count; %i++)
    {
        %ctrl = %UIctrl.getObject(%i);
        if (%ctrl.getClassName() $= "GuiOnverseHouseButtonCtrl")
        {
            %homeName = "";
            %accid = 0;
            if (%houseData !$= "")
            {
                %homeName = getField(%houseData, 0);
                %dispName = getField(%houseData, 1);
                %accid = getField(%houseData, 7);
                %username = getField(%houseData, 8);
                %isFriend = getField(%houseData, 9);
            }
            if (%homeName !$= "")
            {
                if (%homeName $= %ctrl.houseName)
                {
                    %ctrl.ToolTip = %dispName NL "";
                    %ctrl.houseData = %houseData;
                    if (%accid)
                    {
                        %ctrl.curset = %isFriend ? 3 : 2;
                        if (%accid == MasterServerConnection.getUniqueID())
                        {
                            %ctrl.curset = 4;
                            %ctrl.ToolTip = %ctrl.ToolTip @ "This is your home!";
                        }
                        else
                        {
                            %ctrl.ToolTip = %ctrl.ToolTip @ "Owned By:" SPC %username;
                        }
                    }
                    else
                    {
                        %ctrl.curset = 1;
                        %ctrl.ToolTip = %ctrl.ToolTip @ "Not Owned";
                    }
                    return 1;
                }
            }
            else
            {
                %ctrl.houseData = "";
                %ctrl.curset = 0;
                %ctrl.ToolTip = %ctrl.houseName;
            }
        }
        else
        {
            %ret = %this.setHouseData(%ctrl, %houseData);
            if (%ret)
            {
                return 1;
            }
        }
    }
    return 0;
}

function DirectoryGUI::onSortInstance(%this, %ctrl, %sort)
{
    %isInverse = 0;
    if ($CurrentSortInstances > 3)
    {
        %isInverse = 1;
    }

    %cursort = $CurrentSortInstances % 4;
    %sort = %sort % 4;
    if (%cursort == %sort)
    {
        %isInverse = !%isInverse;
    }
    else
    {
        %isInverse = 0;
    }

    $CurrentSortInstances = %sort;
    if (%isInverse)
    {
        $CurrentSortInstances += 4;
    }

    %this.sortInstanceList(%ctrl.getParent()._(InstanceList));
}

function RPC_InstanceHousing::onReturnInvalidInstance(%this)
{
    error("(RPC_InstanceHousing) onReturnInvalidInstance:" @ %this.locationID SPC %this.instanceID);
}

function RPC_InstanceHousing::onHousingList(%this, %dataArray)
{
    if (($CurrentResolveURL[instanceID] != %this.instanceID) ||
        ($CurrentResolveURL[locationID] != %this.locationID))
    {
        return;
    }

    %rows = getRecordCount(%dataArray);
    for (%i=0; %i < %rows; %i++)
    {
        %rowdata = getRecord(%dataArray, %i);
        if (!DirectoryGUI.setHouseData(%this.currentDirectoryUI, %rowdata))
        {
            error("Could not find house to set state:" @ getField(%rowdata, 0));
        }
    }
}

new GuiContextMenu(houseItemContextMenu) {
   Profile = "GuiMenuContextProfile";
   position = "0 0";
   Extent = "640 480";
};

function houseItemContextMenu::Initialize(%this)
{
    if (!%this.initialized)
    {
        %menuId = 0;
        %itemId = 0;
        %this.addMenu("owned", %menuId++);
        %this.addMenuItem("owned", "Teleport", %itemId++, "$tempVar[0].onTeleport();");
        %this.addMenuItem("owned", "Info", %itemId++, "$tempVar[0].onInfo();");
        %itemId = 0;
        %this.addMenu("notowned", %menuId++);
        %this.addMenuItem("notowned", "Teleport", %itemId++, "$tempVar[0].onTeleport();");
        %this.addMenuItem("notowned", "Info", %itemId++, "$tempVar[0].onInfo();");
        %itemId = 0;
        %this.addMenu("unknown", %menuId++);
        %this.addMenuItem("unknown", "Teleport", %itemId++, "$tempVar[0].onTeleport();");
    }
    %this.initialized = 1;
}

function houseItemContextMenu::showContextMenu(%this, %menu, %homeButton, %layer)
{
    %this.Initialize();
    %this.var[0] = %homeButton;

    Parent::showContextMenu(%this, %menu, %layer);
}

function GuiOnverseHouseButtonCtrl::onRightClick(%this)
{
    if (!(%this.houseData $= ""))
    {
        if (getField(%this.houseData, 7) != 0)
        {
            houseItemContextMenu.showContextMenu("owned", %this);
        }
        else
        {
            houseItemContextMenu.showContextMenu("notowned", %this);
        }
    }
    else
    {
        houseItemContextMenu.showContextMenu("unknown", %this);
    }
}

function GuiOnverseHouseButtonCtrl::onTeleport(%this)
{
    %this.onClickResolveHouse(%this.houseName);
}

function GuiOnverseHouseButtonCtrl::onInfo(%this)
{
    if (%this.houseData $= "")
    {
        return;
    }

    InspectItem(%this);
}

function GuiOnverseHouseButtonCtrl::GetPreview(%this, %unused, %bitView)
{
    %bitView.setBitmap(GetHousingIcon(getField(%this.houseData, 6)));
}

function GuiOnverseHouseButtonCtrl::GetDescription(%this)
{
    return ParseDescriptionText(getField(%this.houseData, 2));
}

function GuiOnverseHouseButtonCtrl::GetDisplayName(%this, %unused)
{
    return getField(%this.houseData, 1);
}

function GuiOnverseHouseButtonCtrl::GetStats(%this)
{
    %homeName = getField(%this.houseData, 0);
    %dispName = getField(%this.houseData, 1);
    %description = getField(%this.houseData, 2);
    %CC = getField(%this.houseData, 3);
    %PP = getField(%this.houseData, 4);
    %MaxFurn = getField(%this.houseData, 5);
    %iconID = getField(%this.houseData, 6);
    %accid = getField(%this.houseData, 7);
    %username = getField(%this.houseData, 8);
    %isFriend = getField(%this.houseData, 9);

    %stats = "Home Name: " @ %homeName @ "<br>";
    if (%CC)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/cc.png>" SPC %CC @ " (" @ mFloor(%CC * 0.8) @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)<br>";
    }

    if (%PP)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/pp.png>" SPC %PP @ " (" @ mFloor(%PP * 0.8) @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)<br>";
    }
    
    %stats = %stats @ "Max Furniture:" SPC %MaxFurn @ " (" @ mFloor(%MaxFurn * 1.2) SPC " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)<br>";
    if (%accid == 0)
    {
        %stats = %stats @ "Not owned<br>";
    }
    else
    {
        if (%accid == MasterServerConnection.getUniqueID())
        {
            %stats = %stats @ "This is your home!<br>";
        }
        else
        {
            %stats = %stats @ "Owned by \'<a:www.onverse.com/profile/profile.php?id=" @ %accid @ ">" @ %username @ "</a>\'<br>";
        }
    }

    return %stats;
}
