
exec("./goToLocationDlg.gui");

function goToLocationDlg::onWake()
{
    GTL_LocationsList.clear();
    %idx = 0;
    for (%i=0; %i < $pref::customURL::numURLS; %i++)
    {
        GTL_LocationsList.addRow(%idx++, $pref::customURL::URL[%i]);
    }
    GTL_LocationsList.setSelectedRow(0);
}

function goToLocationDlg::addLocation()
{
    MessageBoxEntryOk("Name", "Enter a name for the new location", "gtldGotName", "");
}

function gtldGotName(%name)
{
    $tempCustomURLName = %name;
    MessageBoxEntryOk("URL", "Enter the URL for the new location", "gtldGotURL", "ONVERSE://location/");
}

function gtldGotURL(%url)
{
    if ($pref::customURL::numURLS $= "")
    {
        $pref::customURL::numURLS = 0;
    }

    $pref::customURL::URL[$pref::customURL::numURLS] = $tempCustomURLName TAB %url;
    $pref::customURL::numURLS++;
    Canvas.popDialog(goToLocationDlg);
    Canvas.pushDialog(goToLocationDlg);
}

function goToLocationDlg::teleport()
{
    %id = GTL_LocationsList.getSelectedId();
    %location = getField(GTL_LocationsList.getRowTextById(%id), 1);
    Canvas.popDialog(goToLocationDlg);
    resolveURL(%location);
}
