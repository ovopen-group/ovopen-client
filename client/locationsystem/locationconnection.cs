
$Net::simPacketLoss = 0;
$Net::simLatency = 0;

function LocationConnection::MakeConnection(%address, %port)
{
    if (isObject(LocationGameConnection))
    {
        LocationGameConnection.delete();
        MainMenuGui.startMainMenu();
    }

    %connection = new GameConnection(LocationGameConnection) {
    };

    echo("Connecting to location server at: " @ %address @ ":" @ %port);
    %connection.setCredentials($currentPlayerName, $currentPassword);
    %connection.setConnectArgs($Net::simPacketLoss, $Net::simLatency);
    %connection.connect("ip:" @ %address @ ":" @ %port);

    if (($Net::simPacketLoss > 0) || ($Net::simLatency > 0))
    {
        warn("Setting simulated packparams:" SPC $Net::simPacketLoss @ "," @ $Net::simLatency);
    }

    %connection.setSimulatedNetParams($Net::simPacketLoss, $Net::simLatency);
    $pref::DBGConnectAddress = %address;
    $pref::DBGConnectPort = %port + 16385;
    $location::builder = 0;
    $location::running = 0;
    $isInSaveMode = 0;
    FriendListUI.clearLocalUsers();
    GameStatUI.disable(1);
}

function GameConnection::SetMountPermissions()
{
    commandToServer('SetMountPermissions', $pref::Permissions::Mounts);
}

function GameConnection::onConnectionAccepted(%this)
{
    echo("Connection Accepted");
    connectToLocationDlg.onConnectionAccepted();
}

function GameConnection::setLagIcon(%this, %state)
{
    if ((%state $= "true") && ($currentLocationDownloadPhase == 4))
    {
        if (!LagIcon.isAwake())
        {
            Canvas.pushDialog(LagIcon, 99);
        }
    }
    else
    {
        if (LagIcon.isAwake())
        {
            Canvas.popDialog(LagIcon);
        }
    }
}

function GameConnection::onConnectRequestRejected(%this, %msg)
{
    error("Location connection rejected: " @ %msg);
    LocationSystem::disconnect(0);

    if (%msg $= "CHR_SUBSCRIPTION")
    {
        MessageBoxOK("Connection Error", "You must have a VIP subscription to enter the testing grounds.", "");
    }
    else
    {
        MessageBoxOK("Connection Error", "Error occured while connecting (" @ %msg @ ")", "");
    }
}

function GameConnection::onConnectionDropped(%this, %reason)
{
    error("Location connection dropped for reason: " @ %reason);
    if (%reason $= "DIS_AVATAR")
    {
        %reason = "Your avatar is not configured. Click <a:www.onverse.com/signup/avatar.php>here</a> to configure it.";
    }

    $location::running = 0;
    if (connectToLocationDlg.isConnecting == 1)
    {
        connectToLocationDlg.onConnectionDropped(%reason);
    }
    else
    {
        LocationSystem::disconnect(0);
        MessageBoxOK("Connection Dropped", %reason, "");
    }
}

function GameConnection::onConnectRequestTimedOut(%this)
{
    error("Location connection request timed out");
    LocationSystem::disconnect(0);
    ShowDirectory();
    MessageBoxOK("Timeout", "Connection to server timedout.", "");
}

function GameConnection::onConnectionError(%this, %errorstring)
{
    error("Connection Error: " @ %errorstring);
    LocationSystem::disconnect(0);
    MessageBoxOK("Connection Error", %errorstring, "");
}

function GameConnection::onConnectionTimedOut(%this)
{
    error("Location connection timed out");
    LocationSystem::disconnect(0);
    ShowDirectory();
    MessageBoxOK("Timeout", "Connection to server timedout.", "");
}

function GameConnection::initialControlSet(%this)
{
    $location::running = 1;
    LocationSystem::onEnterLocation();
    LocationGameConnection.Player = LocationGameConnection.getControlObject();
    $currentPlayerSex = LocationGameConnection.Player.sex;
    Canvas.popDialog(connectToLocationDlg);
}

function ClientCmdPlayerObj(%objID)
{
    LocationGameConnection.Player = LocationGameConnection.resolveGhostID(%objID);
}

function GameConnection::GetPlayerObject(%this)
{
    if (isObject(%this.Player))
    {
        return %this.Player;
    }
    if (isObject(%this.getControlObject()) && (%this.getControlObject().getClassName() $= "Player"))
    {
        return %this.getControlObject();
    }
    return 0;
}

function GameConnection::onGhostAdded(%this, %obj)
{
    if ($location::builder)
    {
        BuilderTreePlaceObject(%obj);
    }
}

function onDataBlockObjectReceived(%index, %total)
{
    connectToLocationDlg.onDataBlock(%index, %total);
}

function onGhostAlwaysStarted(%ghostCount)
{
    $ghostCount = %ghostCount;
    $ghostsRecvd = 0;
}

function onGhostAlwaysObjectReceived()
{
    $ghostsRecvd++;
    connectToLocationDlg.onGhostAlwaysObject($ghostsRecvd, $ghostCount);
}

function onFileChunkReceived(%filename, %ofs, %size)
{
    connectToLocationDlg.onFileChunkReceived(%filename, %ofs, %size);
}

function ClientCmdBeginLocationDownload(%isbuilder, %sd_id)
{
    LocationGameConnection.setTurbo(1);
    LocationGameConnection.isInGame = 0;
    $currentLocationDownloadPhase = 1;
    commandToServer('LocationDownloadAck');
    $location::builder = %isbuilder;
    if ($location::builder)
    {
        InitializeBuilderTree();
    }
    if ($location::builder)
    {
        $Client::LightingFile = "onverse/data/live_assets/engine/terrain/tempBuilder_ter_";
    }
    else
    {
        $Client::LightingFile = "onverse/data/live_assets/engine/terrain/ter_";
    }
    $Client::LightingFile = $Client::LightingFile @ %sd_id;
}

function ClientCmdLocationDownloadPhase2()
{
    if ($currentLocationDownloadPhase != 1)
    {
        echo("Received old LocationDownloadPhase2");
    }

    $currentLocationDownloadPhase = 2;
    commandToServer('LocationDownloadPhase2Ack');
}

function ClientCmdLocationDownloadPhase3()
{
    if ($currentLocationDownloadPhase != 2)
    {
        echo("Received old LocationDownloadPhase3");
    }

    $currentLocationDownloadPhase = 3;
    InventoryManager::Create();
    InventoryGUI.Initialize();
    LocationGameConnection.SetMountPermissions();
    StartFoliageReplication();
    StartClientReplication();
    commandToServer('LocationDownloadPhase3Ack');
}

function ClientCmdLocationDownloadPhase4()
{
    if ($currentLocationDownloadPhase != 3)
    {
        echo("Recived old LocationDownloadPhase4");
    }
    $currentLocationDownloadPhase = 4;
    LocationGameConnection.setTurbo(0);
    connectToLocationDlg.StartingReplication();
    schedule(10, 0, DoPhase4);
}

function DoPhase4()
{
    if ($location::builder)
    {
        commandToServer('LocationDownloadPhase4Ack', $location::poi);
        LocationGameConnection.isInGame = 1;
    }
    else
    {
        %mode = "";
        if (isDebugBuild())
        {
            %mode = "LoadOnly";
        }

        $oldQuality = $pref::LightManager::sgLightingProfileQuality;
        $pref::LightManager::sgLightingProfileQuality = 0;
        RelightScene(0, "DoneLightingPhase4();", %mode);
    }
}

function DoneLightingPhase4()
{
    $pref::LightManager::sgLightingProfileQuality = $oldQuality;
    commandToServer('LocationDownloadPhase4Ack', $location::poi);
    LocationGameConnection.isInGame = 1;
}

function ClientCmdLocalUserLogin(%id, %username)
{
    FriendListUI.addLocalUser(%id, %username);
}

function ClientCmdLocalUserLogout(%id, %username)
{
    FriendListUI.removeLocalUser(%id, %username);
}
