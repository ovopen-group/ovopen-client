
$CurrentResolveURL[locationID] = -1;
$CurrentResolveURL[location] = "";
$CurrentResolveURL[instanceID] = -1;
$CurrentResolveURL[instance] = "";
$CurrentResolveURL[poi] = "";

function GuiOnverseURLButtonCtrl::onClickResolveFULLURL(%this, %url)
{
    resolveURL(%url);
}

function GuiOnverseURLButtonCtrl::onClickResolveLocation(%this, %location)
{
    resolveURL("ONVERSE://location/" @ %location);
}

function GuiOnverseURLButtonCtrl::onClickResolvePOI(%this, %poi)
{
    %directory = %this.getParent().getParent();
    DirectoryGUI.onInstanceClick(%directory._(InstanceList));
    $CurrentResolveURL[poi] = %poi;
    UpdateDirectory();
}

function GuiOnverseHouseButtonCtrl::onClickResolveHouse(%this, %houseName)
{
    GuiOnverseURLButtonCtrl::onClickResolvePOI(%this, %houseName);
}

function RPC_ResolveURL::onReturn(%this, %code)
{
    if (%code < 0)
    {
        error("RPC_ResolveURL returned error code: " @ %code);
    }
}

function RPC_ResolveURL::onResolvedLocation(%this, %locationID, %location, %instanceID, %instance, %poi)
{
    $CurrentResolveURL[locationID] = %locationID;
    $CurrentResolveURL[location] = %location;
    $CurrentResolveURL[instanceID] = %instanceID;
    $CurrentResolveURL[instance] = %instance;
    $CurrentResolveURL[poi] = %poi;
    UpdateDirectory();
}

function RPC_ResolveURL::onReturnURLError(%this)
{
    MessageBoxOK("URL Error", "There was an error resolving the URL", "");
}

function RPC_ResolveURL::onReturnUnknownLocation(%this)
{
    MessageBoxOK("URL Error", "The requested location is Unkown", "");
}

function RPC_ResolveURL::onReturnUnknownInstance(%this)
{
    MessageBoxOK("URL Error", "The requested instance is Unkown", "");
}

function RPC_ResolveURL::onReturnBadURLQaul(%this)
{
    MessageBoxOK("URL Error", "The URL had a bad qualifier", "");
}

function RPC_ResolveURL::onReturnInvalidURL(%this)
{
    MessageBoxOK("URL Error", "The requested url syntax is invalid", "");
}

function RPC_ResolveURL::onReturnInvalidUsername(%this)
{
    MessageBoxOK("URL Error", "That username is invalid.", "");
}

function RPC_ResolveURL::onReturnUserOffline(%this)
{
    MessageBoxOK("URL Error", "That user is currently offline.", "");
}

function RPC_ResolveURL::onReturnUserNoLoc(%this)
{
    MessageBoxOK("URL Error", "That user is not currently in a location.", "");
}

function RPC_ResolveURL::onReturnUserNoHouse(%this)
{
    MessageBoxOK("URL Error", "That user does not own a home.", "");
}

function RPC_ResolveURL::onReturnTeleportDenied(%this)
{
    MessageBoxOK("URL Error", "That user is not accepting teleports.", "");
}

function ConfResolveURL(%url)
{
    MessageBoxYesNo("URL", "Would you like to go to:" @ %url, "ResolveURL(\"" @ %url @ "\");", "");
}

function resolveURL(%url)
{
    $CurrentResolveURL[locationID] = -1;
    $CurrentResolveURL[location] = "";
    $CurrentResolveURL[instanceID] = -1;
    $CurrentResolveURL[instance] = "";
    $CurrentResolveURL[poi] = "";
    %rpcObject = new RPC_ResolveURL() {
    };
    %rpcObject.resolveURL(%url);
    %rpcObject.fireRPC(MasterServerConnection);
}

function TeleportToPlayer(%playerName, %confirm)
{
    if (%confirm)
    {
        resolveURL("onverse://player/" @ %playerName);
    }
    else
    {
        MessageBoxYesNo("Teleport", "Would you like to teleport to user \'" @ %playerName @ "\'?", "TeleportToPlayer(\"" @ %playerName @ "\",true);", "");
    }
}

function TeleportToHome(%playerName)
{
    showHomeList(%playerName);
}
