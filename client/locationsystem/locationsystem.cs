
$location::running = 0;
$location::builder = 0;
$location::locationID = -1;
$location::location = "";
$location::instanceID = -1;
$location::instance = "";
$location::poi = "";

function LocationSystem::includeFiles()
{
    echo("Initializing location system component.");
    exec("./locationconnection.cs");
    exec("./permissions.cs");
    exec("./ui/goToLocation.cs");
    exec("./ui/connectToLocationDlg.cs");
    exec("./resolveURL.cs");
    exec("./ui/homeList.cs");
    exec("./ui/directory/directoryGUI.cs");
}

function LocationSystem::ConnectToLocation(%locationID, %location, %instanceID, %instance, %poi)
{
    if (MasterServerConnection.isConnected())
    {
        if ((%locationID != -1) && (%locationID != 0))
        {
            if ((%locationID != $location::locationID) || (%instanceID != $location::instanceID))
            {
                LocationSystem::requestInstance(%locationID, %location, %instanceID, %instance, %poi);
            }
            else
            {
                if (isObject(LocationGameConnection))
                {
                    $location::poi = %poi;
                    commandToServer('GotoPOI', $location::poi);
                }
                else
                {
                    if ($location::pending)
                    {
                        $location::poi = %poi;
                    }
                    else
                    {
                        LocationSystem::requestInstance(%locationID, %location, %instanceID, %instance, %poi);
                    }
                }
            }
        }
        else
        {
            MessageBoxOK("Error", "Cannot connect to unkown location", "");
        }
    }
    else
    {
        MessageBoxOK("Error", "Must be connected to Onverse first.", "");
    }
}

function LocationSystem::onEnterLocation()
{
    gameMode::begin();
}

function LocationSystem::disconnect(%delete)
{
    gameMode::end();

    if (%delete)
    {
        if (isObject(LocationGameConnection))
        {
            LocationGameConnection.delete();
        }
    }

    FriendListUI.clearLocalUsers();
    GameStatUI.disable(1);
    
    $location::pending = 0;
    $location::running = 0;
    $location::locationID = -1;
    $location::location = "";
    $location::instanceID = -1;
    $location::instance = "";
    $location::poi = "";
    $MusicTrigger::CallCount = 0;

    deleteDataBlocks();
    LoadCommonDatablocks();
    purgeResources();
    MainMenuGui.startMainMenu();
}

function LocationSystem::onServerError(%code)
{
    Canvas.popDialog(connectToLocationDlg);

    if (%code == 4)
    {
        MessageBoxOK("Attention", "Security error, check username/password");
    }
    else
    {
        MessageBoxOK("Attention", "Master server returned error while connecting to location:" @ %code);
    }
}

function LocationSystem::onLocationReady(%location, %instance, %address, %port)
{
    if (!$location::pending)
    {
        echo("Got location ready but is no longer pending.");
        return;
    }
    if ((%location == $location::locationID) && (%instance == $location::instanceID))
    {
        $location::pending = 0;
        $location::multiRequest = 0;
        LocationConnection::MakeConnection(%address, %port);
    }
    else
    {
        if ($location::multiRequest > 9)
        {
            error("Server has sent us the wrong data 10 times in a row, taking it anyway.");
            $location::pending = 0;
            $location::multiRequest = 0;
            LocationConnection::MakeConnection(%address, %port);
        }
        else
        {
            $location::multiRequest++;
            warn("Got a ready for a different instance. Re-requesting.");
            LocationSystem::requestInstance($location::locationID, $location::location, $location::instanceID, $location::instance, $location::poi);
        }
    }
}

function LocationSystem::requestInstance(%locationID, %location, %instanceID, %instance, %poi)
{
    LocationSystem::disconnect(1);
    if (MasterServerConnection.isConnected())
    {
        %rpcObject = new RPC_RequestInstance() {
        };

        %rpcObject.locationID = %locationID;
        %rpcObject.instanceID = %instanceID;
        %rpcObject.fireRPC(MasterServerConnection);
        $location::pending = 1;
        $location::locationID = %locationID;
        $location::location = %location;
        $location::instanceID = %instanceID;
        $location::instance = %instance;
        $location::poi = %poi;
        connectToLocationDlg.Connecting();
    }
    else
    {
        MessageBoxOK("Error", "Master server must be connected first.", "");
    }
}

function RPC_RequestInstance::onReturnUnknownLocation(%unused)
{
    connectToLocationDlg.close();
    LocationSystem::disconnect(1);

    MessageBoxOK("Attention", "The requested location is unknown.", "");
    error("(RPC_RequestInstance) The requested location is unknown.");
}

function RPC_RequestInstance::onReturnUnknownInstance(%unused)
{
    connectToLocationDlg.close();
    LocationSystem::disconnect(1);

    MessageBoxOK("Attention", "The requested instance is unknown.", "");
    error("(RPC_RequestInstance) The requested instance is unknown.");
}

function RPC_RequestInstance::onReturnCannotConnect(%unused)
{
    connectToLocationDlg.close();
    LocationSystem::disconnect(1);
    MessageBoxOK("Attention", "Error while connecting.", "");
    error("(RPC_RequestInstance) Error while connecting.");
}
