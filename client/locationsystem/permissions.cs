
function permissions::OnRequest(%queryID, %type, %username, %obj, %arg0, %arg1, %arg2, %arg3, %arg4)
{
    if (%type == $Permission::Mounts)
    {
        if (($pref::Permissions::Mounts & $Permission::Mounts::Mask) == $Permission::Mounts::Private)
        {
            permissions::Respond(%queryID, 0);
        }
        else
        {
            if (($pref::Permissions::Mounts & $Permission::Mounts::Mask) == $Permission::Mounts::Public)
            {
                permissions::Respond(%queryID, 1);
            }
            else
            {
                if (($pref::Permissions::Mounts & $Permission::Mounts::Mask) == $Permission::Mounts::Friends)
                {
                    permissions::Respond(%queryID, _GFriendDatabase.isFriend(%username));
                }
                else
                {
                    error("(Permissions::OnRequest) Invalid state.");
                    permissions::Respond(%queryID, 0);
                }
            }
        }
    }
    else
    {
        error("(Permissions::OnRequest) Unkown type.");
        permissions::Respond(%queryID, 0);
    }
}

function permissions::Respond(%queryID, %response)
{
    commandToServer('perm_response', %queryID, %response);
}

function ClientCmdPerm_request(%queryID, %type, %username, %obj, %arg0, %arg1, %arg2, %arg3, %arg4)
{
    if (%obj > 0)
    {
        %obj = LocationGameConnection.resolveGhostID(%obj);
    }
    echo("Perm Request");
    permissions::OnRequest(%queryID, %type, %username, %obj, %arg0, %arg1, %arg2, %arg3, %arg4);
}

function ClientCmdPerm_active(%queryID, %type, %obj, %arg0, %arg1, %arg2, %arg3, %arg4)
{
    if (%obj > 0)
    {
        %obj = LocationGameConnection.resolveGhostID(%obj);
    }
    echo("Perm Active");
}

function ClientCmdPerm_cancel(%queryID, %type, %obj, %arg0, %arg1, %arg2, %arg3, %arg4)
{
    if (%obj > 0)
    {
        %obj = LocationGameConnection.resolveGhostID(%obj);
    }
    warn("Perm Cancel");
}

function ClientCmdPerm_response(%queryID, %type, %response, %obj, %arg0, %arg1, %arg2, %arg3, %arg4)
{
    if (%obj > 0)
    {
        %obj = LocationGameConnection.resolveGhostID(%obj);
    }
    if (!%response)
    {
        error("Perm denied");
    }
    else
    {
        echo("Perm accepted");
    }
}
