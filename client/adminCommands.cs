
function RPC_CustomerService::onReturnGeneralError()
{
    MessageBoxOK("Error", "An error occured while running the command.");
}

function RPC_CustomerService::onNoePermission()
{
    MessageBoxOK("Error", "You do not have permission to run that customer service command");
}

function RPC_CustomerService::onReturnInvalidUsername()
{
    MessageBoxOK("Error", "Invalid username for customer service command");
}

function RPC_CustomerService::onReturnUserOffline()
{
    MessageBoxOK("Error", "User offline for customer service command");
}

function RPC_CustomerService::onReturnInvalidCommand()
{
    MessageBoxOK("Error", "Invalid customer service command");
}

function RPC_CustomerService::onReturnNumberArguments()
{
    MessageBoxOK("Error", "Incorrect number of arguments for customer service command");
}

function RPC_CustomerService::onReturnInvalidArgument()
{
    MessageBoxOK("Error", "Invalid argument for customer service command");
}

function InitializeAdminCommands()
{
    if (MasterServerConnection.hasGuideLevel())
    {
        _GCommandParser.addCommand("GReply", 2, 255, CC_GuideReply, "[username] [message] (Reply as guide to user)");
        _GCommandParser.addCommand("GuideHall", 0, 0, AC_GuideHall, " (Open Guide Hall map.)");
        _GCommandParser.addCommand("Kick", 1, 1, AC_Kick, "[username] (Kick the given player.)");
        _GCommandParser.addCommand("Suspend", 2, 2, AC_Suspend, "[username] [hours] (Suspend the given player for the given number of hours.)");
        _GCommandParser.addCommand("Announce", 1, 255, AC_ServerAnnouncement, "[Message] (Send announcement to all users.)");
        _GCommandParser.addCommand("Warn", 2, 255, AC_ServerMessageTell, "[username] [Message] (Send server message to username.)");
        _GCommandParser.addCommand("Invis", 0, 0, AC_Invis, " (Toggle hidding of your avatar in the world.)");
        _GCommandParser.addCommand("Relocate", 2, 255, AC_Relocate, "[username] [message] (Relocate this user to the default POI and show the warning message.)");
        MainChatHUD.onServerMessage("Activated guide commands.");
    }

    if (MasterServerConnection.hasSuperGuideLevel())
    {
        MainChatHUD.onServerMessage("Activated senior guide commands.");
    }

    if (MasterServerConnection.hasCommManagerLevel())
    {
        MainChatHUD.onServerMessage("Activated community manager commands.");
        _GCommandParser.addCommand("Ban", 1, 1, AC_Ban, "[username] (Ban the given player.)");
        _GCommandParser.addCommand("UnBan", 1, 1, AC_UnBan, "[username] (Un-Ban the given player.)");
        _GCommandParser.addCommand("AddCC", 2, 2, AC_AddCC, "[username] [cc] (Award the user CCs.)");
        _GCommandParser.addCommand("AddPP", 2, 2, AC_AddPP, "[username] [pp] (Award the user PPs.)");
        _GCommandParser.addCommand("Fly", 0, 0, AC_Fly, " (Go into and out of flying mode.)");
        _GCommandParser.addCommand("DropCam", 0, 0, AC_DropCam, " (Drop the flying camera at the current avater position.)");
        _GCommandParser.addCommand("DropAvatar", 0, 0, AC_DropAvatar, " (Drop the avatar at the current camera position.)");
        _GCommandParser.addCommand("CamSpeed", 1, 1, AC_CamSpeed, "[speed] (1-320, Set the speed of the camera.)");
        _GCommandParser.addCommand("DropPet", 0, 0, AC_DropPet, " (Drop your pet at the current camera position.)");
    }

    if (MasterServerConnection.hasAdminLevel())
    {
        _GCommandParser.addCommand("SvrMsg", 1, 255, AC_ServerMessage, "[Message] (Send server message to all users if no name given.)");
        _GCommandParser.addCommand("PlayAnim", 1, 1, AC_PlayAnim, "[animationName] (Play the requested animation on your character.)");
        MainChatHUD.onServerMessage("Activated moderator commands.");
    }

    if (MasterServerConnection.hasDeveloperLevel())
    {
        MainChatHUD.onServerMessage("Activated developer commands.");
        _GCommandParser.addCommand("HighRes", 0, 1, AC_HighRes, " (Set number of images to take for screen shots, 1 = normal screen shot)");
    }
}

function AC_GuideHall()
{
    if (!MasterServerConnection.hasGuideLevel())
    {
        return "You do not have permision to go to the guide hall.";
    }

    resolveURL("onverse://location/guide_hall/");
    return "Opening Map";
}

function AC_Kick(%username)
{
    if (!MasterServerConnection.hasGuideLevel())
    {
        return "You do not have permision to kick players.";
    }

    %rpcObject = new RPC_CustomerService() {
    };
    %rpcObject.Command = COMMAND_KICK;
    %rpcObject.arguments = %username;
    %rpcObject.fireRPC(MasterServerConnection);

    return "Kicking " @ %username;
}

function AC_Suspend(%username, %hours)
{
    if (!MasterServerConnection.hasGuideLevel())
    {
        return "You do not have permision to suspend players.";
    }

    %rpcObject = new RPC_CustomerService() {
    };
    %rpcObject.Command = COMMAND_SUSPEND;
    %rpcObject.arguments = %username;
    %rpcObject.arguments = %hours;
    %rpcObject.fireRPC(MasterServerConnection);

    return "Suspending " @ %username;
}

function AC_ServerMessage(%message, %username)
{
    if (!MasterServerConnection.hasGuideLevel())
    {
        return "You do not have permision to send warning messages.";
    }

    if ((%username $= "") && !MasterServerConnection.hasAdminLevel())
    {
        return "You do not have permision to send server messages.";
    }

    %rpcObject = new RPC_CustomerService() {
    };
    %rpcObject.Command = COMMAND_SRV_MSG;
    if (!(%username $= ""))
    {
        %rpcObject.arguments = %username;
    }
    %rpcObject.arguments = %message;
    %rpcObject.fireRPC(MasterServerConnection);

    return "Sending message.";
}

function AC_ServerMessageTell(%username, %message)
{
    return AC_ServerMessage(%message, %username);
}

function AC_ServerAnnouncement(%message)
{
    if (!MasterServerConnection.hasGuideLevel())
    {
        return "You do not have permision to send announcements.";
    }

    %rpcObject = new RPC_CustomerService() {
    };
    %rpcObject.Command = COMMAND_SRV_ANNOUNCE;
    %rpcObject.arguments = %message;
    %rpcObject.fireRPC(MasterServerConnection);

    return "Sending message.";
}

function AC_Invis()
{
    if (!isObject(LocationGameConnection))
    {
        if (!MasterServerConnection.hasGuideLevel())
        {
            return "You do not have permision to be invisible.";
        }
        return "Must be in an instance to be invisable.";
    }

    if (!LocationGameConnection.hasGuideLevel())
    {
        return "You do not have permision to be invisible.";
    }

    commandToServer('ToggleInvisible');
    return "Alakazam";
}

function AC_Relocate(%username, %message)
{
    if (!isObject(LocationGameConnection))
    {
        if (!MasterServerConnection.hasGuideLevel())
        {
            return "You do not have permision to relocate a user.";
        }
        return "Must be in an instance to relocate a user.";
    }

    if (!LocationGameConnection.hasGuideLevel())
    {
        return "You do not have permision to relocate a user.";
    }

    commandToServer('ForceMovePlayer', %username, %message);
    return "Relocating" SPC %username;
}

function AC_Ban(%username)
{
    if (!MasterServerConnection.hasCommManagerLevel())
    {
        return "You do not have permision to ban players.";
    }

    %rpcObject = new RPC_CustomerService() {
    };
    %rpcObject.Command = COMMAND_BAN;
    %rpcObject.arguments = %username;
    %rpcObject.arguments = 1;
    %rpcObject.fireRPC(MasterServerConnection);

    return "Banning " @ %username;
}

function AC_UnBan(%username)
{
    if (!MasterServerConnection.hasCommManagerLevel())
    {
        return "You do not have permision to ban players.";
    }

    %rpcObject = new RPC_CustomerService() {
    };
    %rpcObject.Command = COMMAND_BAN;
    %rpcObject.arguments = %username;
    %rpcObject.arguments = 0;
    %rpcObject.fireRPC(MasterServerConnection);

    return "Un-Banning " @ %username;
}

function AC_Fly()
{
    if (!isObject(LocationGameConnection))
    {
        if (!MasterServerConnection.hasCommManagerLevel())
        {
            return "You do not have permision to fly.";
        }
        return "Must be in an instance to fly.";
    }

    if (!LocationGameConnection.hasCommManagerLevel())
    {
        return "You do not have permision to fly.";
    }

    commandToServer('ToggleCamera');
    return "Flying";
}

function AC_DropCam()
{
    if (!isObject(LocationGameConnection))
    {
        if (!MasterServerConnection.hasCommManagerLevel())
        {
            return "You do not have permision to fly.";
        }
        return "Must be in an instance to fly.";
    }

    if (!LocationGameConnection.hasCommManagerLevel())
    {
        return "You do not have permision to fly.";
    }

    commandToServer('DropCamAtAva');
    return "Dropping Camera";
}

function AC_DropPet()
{
    if (!isObject(LocationGameConnection))
    {
        if (!MasterServerConnection.hasCommManagerLevel())
        {
            return "You do not have permision to fly.";
        }
        return "Must be in an instance to fly.";
    }

    if (!LocationGameConnection.hasCommManagerLevel())
    {
        return "You do not have permision to fly.";
    }

    commandToServer('DropPetAtCam');
    return "Dropping Pet";
}

function AC_DropAvatar()
{
    if (!isObject(LocationGameConnection))
    {
        if (!MasterServerConnection.hasCommManagerLevel())
        {
            return "You do not have permision to fly.";
        }
        return "Must be in an instance to fly.";
    }

    if (!LocationGameConnection.hasCommManagerLevel())
    {
        return "You do not have permision to fly.";
    }

    commandToServer('DropAvaAtCam');
    return "Dropping Avatar";
}

function AC_CamSpeed(%speed)
{
    if (!isObject(LocationGameConnection))
    {
        if (!MasterServerConnection.hasCommManagerLevel())
        {
            return "You do not have permision to fly.";
        }
        return "Must be in an instance to fly.";
    }

    if (!LocationGameConnection.hasCommManagerLevel())
    {
        return "You do not have permision to fly.";
    }

    commandToServer('SetCameraMoveSpeed', %speed);
    return "Speed changed";
}

function AC_AddCC(%username, %CC)
{
    if (!MasterServerConnection.hasCommManagerLevel())
    {
        return "You do not have permision to add currency.";
    }

    %rpcObject = new RPC_CustomerService() {
    };
    %rpcObject.Command = COMMAND_ADD_CURRENCY;
    %rpcObject.arguments = %username;
    %rpcObject.arguments = %CC;
    %rpcObject.arguments = 0;
    %rpcObject.fireRPC(MasterServerConnection);

    return "Giving" SPC %CC SPC "CC to" SPC %username;
}

function AC_AddPP(%username, %PP)
{
    if (!MasterServerConnection.hasCommManagerLevel())
    {
        return "You do not have permision to add currency.";
    }

    %rpcObject = new RPC_CustomerService() {
    };
    %rpcObject.Command = COMMAND_ADD_CURRENCY;
    %rpcObject.arguments = %username;
    %rpcObject.arguments = 0;
    %rpcObject.arguments = %PP;
    %rpcObject.fireRPC(MasterServerConnection);

    return "Giving" SPC %PP SPC "PP to" SPC %username;
}

function AC_HighRes(%num)
{
    if (%num > 1)
    {
        $pref::Video::screenShotHighRes = %num;
        return "HighRes -" SPC %num @ "x" @ %num;
    }
    else
    {
        $pref::Video::screenShotHighRes = 1;
        return "Standard";
    }
}

function AC_PlayAnim(%animation)
{
    if (!MasterServerConnection.hasAdminLevel())
    {
        return "You do not have permision to play animations.";
    }

    commandToServer('AvatarAnimation', %animation);
    return "Playing " @ %animation;
}
