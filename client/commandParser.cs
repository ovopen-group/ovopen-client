
new CommandParser(_GCommandParser) {
};

function CommandParser::handleCommand(%this, %commandText)
{
    %commandText = ltrim(%commandText);
    if (getSubStr(%commandText, 0, 1) $= "/")
    {
        %returnValue = %this.executeCommand(%commandText);

        if (!(%returnValue $= ""))
        {
            MainChatHUD.onCommandReturn(%returnValue);
        }
    }
    else
    {
        if (%commandText $= "")
        {
            return;
        }
        
        Messaging::SendLocalMessage(%commandText);
    }
}

function CommandParser::tabCompleteCommand(%this, %commandText)
{
    %commandText = ltrim(%commandText);
    if (getSubStr(%commandText, 0, 1) $= "/")
    {
        %finished = _GCommandParser.finishCommand(getSubStr(%commandText, 1, strlen(%commandText) - 1));
        return "/" @ %finished;
    }

    return "";
}

function CommandParser::onCommandError(%this, %errID, %helpString)
{
    if (%errID == 0)
    {
        %errString = "Too few arguments.";
    }
    else
    {
        if (%errID == 1)
        {
            %errString = "Too many arguments.";
        }
        else
        {
            if (%errID == 2)
            {
                %errString = "Invalid command.";
            }
            else
            {
                %errString = "Unkown error.";
            }
        }
    }

    MainChatHUD.onCommandError(%errString, %helpString);
}

function CommandParser::onAmbiguousCommand(%this, %command, %help)
{
    MainChatHUD.onAmbiguousCommand(%command, %help);
}
