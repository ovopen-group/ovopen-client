$VMPlayerFadeIntervalMS = 100;
$VMPlayerCheckSongPosIntervalMS = 100;

new AudioDescription(VMPlayerStreamingAudioDescription6) {
   isStreaming = "1";
   is3D = "0";
   type = "6";
   isLooping = "0";
};

new AudioDescription(VMPlayerStreamingAudioDescription7) {
   isStreaming = "1";
   is3D = "0";
   type = "7";
   isLooping = "0";
};

new AudioDescription(VMPlayerAudioDescription6) {
   isStreaming = "0";
   is3D = "0";
   type = "6";
   isLooping = "0";
};

new AudioDescription(VMPlayerAudioDescription7) {
   isStreaming = "0";
   is3D = "0";
   type = "7";
   isLooping = "0";
};

new AudioProfile(VMPlayerStreamingAudioProfile6) {
   description = "VMPlayerStreamingAudioDescription6";
   preload = "0";
   fileName = "";
};

new AudioProfile(VMPlayerStreamingAudioProfile7) {
   description = "VMPlayerStreamingAudioDescription7";
   preload = "0";
   fileName = "";
};

new AudioProfile(VMPlayerAudioProfile6) {
   description = "VMPlayerAudioDescription6";
   preload = "0";
   fileName = "";
};

new AudioProfile(VMPlayerAudioProfile7) {
   description = "VMPlayerAudioDescription7";
   preload = "0";
   fileName = "";
};


function VMPlayer::onAdd(%this)
{
    %this.PrimaryAudioChannel = 6;
    %this.SecondaryAudioChannel = 7;
    %this.AudioProfiles = new ScriptObject() {
       class = VMPArray;
    };

    for (%x=1; %x <= 8; %x++)
    {
        %this.AudioProfiles.InsertBack(VMPlayerStreamingAudioProfile @ %x);
    }

    %this.PrimaryMusicChannel = new ScriptObject() {
        AudioProfile = %this.AudioProfiles.GetAt(%this.PrimaryAudioChannel - 1);
        AudioChannel = %this.PrimaryAudioChannel;
        AudioHandle = 0;
    };

    %this.SecondaryMusicChannel = new ScriptObject("") {
        AudioProfile = %this.AudioProfiles.GetAt(%this.SecondaryAudioChannel - 1);
        AudioChannel = %this.SecondaryAudioChannel;
        AudioHandle = 0;
    };

    %this.Streaming = 1;

    %this.Playlists = new ScriptObject() {
        class = VMPArray;
    };
    
    %this.NextTracks = new ScriptObject() {
        class = VMPArray;
    };
    
    %this.CurrentMusicChannel = %this.PrimaryMusicChannel;
    %this.CurrentVolume = $pref::Audio::channelVolume[%this.CurrentMusicChannel.AudioChannel];
    %this.Random = 0;
    %this.Repeat = 0;
    %this.fade = 0;
    %this.CrossFade = 0;
    %this.debug = 0;
    %this.CurrentPlaylist = 0;
    %this.CurrentSong = 0;
    %this.CurrentTrackIndex = -1;
    %this.FadeDuration = 2;
    %this.CheckSongPosHandle = 0;
    %this.NextTrackHandle = 0;
    %this.NextTrackDelayMS = 0;
    %this.NextTrackPause = 0;
    %this.PreviousPosition = -1;
    %this.FadeOutHandle = 0;
    %this.FadeInHandle = 0;
    %this.CrossFadeOutHandle = 0;
    %this.CrossFadeInHandle = 0;
    %this.NextTrackIndex = 0;
}

function VMPlayer::onRemove(%this)
{
    if (%this.IsPlaying() || %this.IsPaused())
    {
        %this.stop();
    }

    if (isObject(%this.Playlists))
    {
        %this.RemoveAllPlaylists();
        %this.Playlists.delete();
    }

    if (isObject(%this.NextTracks))
    {
        %this.NextTracks.RemoveAll();
        %this.NextTracks.delete();
    }

    if (isObject(%this.AudioProfiles))
    {
        %this.AudioProfiles.delete();
    }

    if (isObject(%this.PrimaryMusicChannel))
    {
        %this.PrimaryMusicChannel.delete();
    }

    if (isObject(%this.SecondaryMusicChannel))
    {
        %this.SecondaryMusicChannel.delete();
    }
}

function VMPlayer::SetAudioChannels(%this, %primary, %secondary)
{
    if ((((%primary < 0) || (%secondary < 0)) || (%primary > 8)) || (%secondary > 8))
    {
        VMPlayerPrintDebugMessage(%this, "Unable to set audio channels, channel number \'" @ %primary @ "\' or \'" @ %secondary @ "\' out of range");
        return;
    }

    if (%this.IsPlaying() || %this.IsPaused())
    {
        %this.stop();
    }

    %this.PrimaryAudioChannel = %primary;
    %this.SecondaryAudioChannel = %secondary;
    %this.PrimaryMusicChannel.AudioProfile = %this.AudioProfiles.GetAt(%this.PrimaryAudioChannel - 1);
    %this.PrimaryMusicChannel.AudioChannel = %this.PrimaryAudioChannel;
    %this.SecondaryMusicChannel.AudioProfile = %this.AudioProfiles.GetAt(%this.SecondaryAudioChannel - 1);
    %this.SecondaryMusicChannel.AudioChannel = %this.SecondaryAudioChannel;
}

function VMPlayer::SetStreaming(%this, %streaming)
{
    %this.AudioProfiles.RemoveAll();

    if ((%streaming == 0) && (%this.Streaming == 1))
    {
        for (%x=1; %x <= 8; %x++)
        {
            %this.AudioProfiles.InsertBack(VMPlayerAudioProfile @ %x);
        }
    }
    else
    {
        if ((%streaming == 1) && (%this.Streaming == 0))
        {
            for (%x=1; %x <= 8; %x++)
            {
                %this.AudioProfiles.InsertBack(VMPlayerStreamingAudioProfile @ %x);
            }
        }
    }

    %this.PrimaryMusicChannel.AudioProfile = %this.AudioProfiles.GetAt(%this.PrimaryMusicChannel.AudioChannel - 1);
    %this.SecondaryMusicChannel.AudioProfile = %this.AudioProfiles.GetAt(%this.SecondaryAudioChannel.AudioChannel - 1);
    %this.Streaming = %streaming;
}

function VMPlayer::OpenPlaylistsFromDisk(%this, %filename)
{
    if (!isFile(%filename))
    {
        VMPlayerPrintDebugMessage(%this, "Unable to open playlist, file \'" @ %filename @ "\' not found");
        return 0;
    }

    %this.RemoveAllPlaylists();
    $VMPlayerObjTmp = %this;
    return exec(%filename);
}

function VMPlayer::SavePlaylistsToDisk(%this, %filename)
{
    %file = new FileObject("") {
    };

    if (!%file.openForWrite(%filename))
    {
        VMPlayerPrintDebugMessage(%this, "Unable to save playlist, filename not valid or filename is read-only");
        return 0;
    }

    for (%x=0; %x < %this.Playlists.getCount(); %x++)
    {
        %playlist = %this.Playlists.GetAt(%x);
        %file.writeLine("%tracks = new ScriptObject() { class=VMPArray; };");

        for (%y=0; %y < %playlist.Tracks.getCount(); %y++)
        {
            %track = %playlist.Tracks.GetAt(%y);
            %file.writeLine("%events = new ScriptObject() { class=VMPArray; };");

            for (%z=0; %z < %track.Events.getCount(); %z++)
            {
                %event = %track.Events.GetAt(%z);
                %file.writeLine("%event = new ScriptObject()");
                %file.writeLine("{");
                %file.writeLine("   class=VMPlayerEvent;");
                %file.writeLine("   Position=" @ %event.position @ ";");
                %file.writeLine("   EventFunction=" @ %event.EventFunction @ ";");
                %file.writeLine("};");
                %file.writeLine("%events.InsertBack(%event);");
            }

            %file.writeLine("%track = new ScriptObject()");
            %file.writeLine("{");
            %file.writeLine("   class = VMPlayerTrack;");
            %file.writeLine("   Filename = \"" @ %track.fileName @ "\";");
            %file.writeLine("   Title = \"" @ %track.title @ "\";");
            %file.writeLine("   Artist = \"" @ %track.Artist @ "\";");
            %file.writeLine("   StartPosition = " @ %track.StartPosition @ ";");
            %file.writeLine("   EndPosition = " @ %track.EndPosition @ ";");
            %file.writeLine("   Events = %events;");
            %file.writeLine("};");
            %file.writeLine("%tracks.InsertBack(%track);");
        }

        %file.writeLine("%playlist = new ScriptObject()");
        %file.writeLine("{");
        %file.writeLine("   class = VMPlayerPlaylist;");
        %file.writeLine("   Name = " @ %playlist.name @ ";");
        %file.writeLine("   Tracks = %tracks;");
        %file.writeLine("};");
        %file.writeLine("$VMPlayerObjTmp.Playlists.InsertBack(%playlist);");
    }

    %file.close();
    %file.delete();
    VMPlayerPrintDebugMessage(%this, "Playlist " @ %filename @ " saved");

    return 1;
}

function VMPlayer::AddPlaylist(%this, %playlistname, %song1, %song2, %song3, %song4, %song5, %song6, %song7, %song8, %song9, %song10, %song11, %song12, %song13, %song14, %song15, %song16)
{
    if (%playlistname $= "")
    {
        VMPlayerPrintDebugMessage(%this, "Unable to add playlist, a playlist name must be specified");
        return 0;
    }

    if (isObject(VMPlayerGetPlaylist(%this, %playlistname)))
    {
        VMPlayerPrintDebugMessage(%this, "Unable to add playlist \'" @ %playlistname @ "\', playlist already exists");
        return 0;
    }

    %tracks = new ScriptObject("") {
        class = VMPArray;
    };

    %playlist = new ScriptObject("") {
        class = VMPlayerPlaylist;
        name = %playlistname;
        Tracks = %tracks;
    };

    %this.Playlists.InsertBack(%playlist);
    VMPlayerPrintDebugMessage(%this, "Playlist \'" @ %playlistname @ "\' added");
    
    if (strlen(%song1) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song1);
    }
    if (strlen(%song2) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song2);
    }
    if (strlen(%song3) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song3);
    }
    if (strlen(%song4) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song4);
    }
    if (strlen(%song5) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song5);
    }
    if (strlen(%song6) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song6);
    }
    if (strlen(%song7) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song7);
    }
    if (strlen(%song8) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song8);
    }
    if (strlen(%song9) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song9);
    }
    if (strlen(%song10) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song10);
    }
    if (strlen(%song11) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song11);
    }
    if (strlen(%song12) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song12);
    }
    if (strlen(%song13) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song13);
    }
    if (strlen(%song14) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song14);
    }
    if (strlen(%song15) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song15);
    }
    if (strlen(%song16) > 0)
    {
        %this.AddSongToPlaylist(%playlistname, %song16);
    }

    return %this.Playlists.getCount();
}

function VMPlayer::RemovePlaylist(%this, %playlistname)
{
    %playlist = VMPlayerGetPlaylist(%this, %playlistname);
    if (!isObject(%playlist))
    {
        VMPlayerPrintDebugMessage(%this, "Unable to remove playlist \'" @ %playlistname @ "\', playlist not found");
        return 0;
    }

    if (%this.CurrentPlaylist == %playlist)
    {
        %this.UnloadPlaylist();
    }

    %this.Playlists.RemoveAt(%this.Playlists.find(%playlist));
    VMPlayerPrintDebugMessage(%this, "Playlist \'" @ %playlistname @ "\' removed");
    return 1;
}

function VMPlayer::RemoveAllPlaylists(%this)
{
    if (%this.IsPlaylistLoaded())
    {
        %this.UnloadPlaylist();
    }

    if (!isObject(%this.Playlists))
    {
        return;
    }

    for (%x=0; %x < %this.Playlists.getCount(); %x++)
    {
        %playlist = %this.Playlists.GetAt(%x);
        for (%y=0; %y < %playlist.Tracks.getCount(); %y++)
        {
            %track = %playlist.Tracks.GetAt(%y);
            for (%z=0; %z < %track.Events.getCount(); %z++)
            {
                %track.Events.GetAt(%z).delete();
            }
            %track.Events.delete();
            %track.delete();
        }
        %playlist.Tracks.delete();
        %playlist.delete();
    }

    VMPlayerPrintDebugMessage(%this, "All playlists removed");
    %this.Playlists.RemoveAll();
}

function VMPlayer::LoadPlaylist(%this, %playlistname)
{
    %playlist = VMPlayerGetPlaylist(%this, %playlistname);
    if (!isObject(%playlist))
    {
        VMPlayerPrintDebugMessage(%this, "Unable to load playlist \'" @ %playlistname @ "\', playlist not found");
        return 0;
    }

    if (isObject(%this.CurrentPlaylist))
    {
        if (%this.CurrentPlaylist.name $= %playlist.name)
        {
            VMPlayerPrintDebugMessage(%this, "Playlist \'" @ %playlistname @ "\' already loaded");
            return 1;
        }
    }

    if (%this.IsPlaying() || %this.IsPaused())
    {
        %this.stop();
    }

    %this.CurrentPlaylist = %playlist;

    if (%playlist.Tracks.getCount() > 0)
    {
        %this.CurrentTrackIndex = 0;
    }
    else
    {
        %this.CurrentTrackIndex = -1;
    }

    VMPlayerPrintDebugMessage(%this, "Playlist \'" @ %playlistname @ "\' loaded");
    return 1;
}

function VMPlayer::UnloadPlaylist(%this)
{
    if (!%this.IsPlaylistLoaded())
    {
        VMPlayerPrintDebugMessage(%this, "Unable to unload current playlist, not playlist previously loaded");
        return;
    }

    if (%this.IsPlaying() || %this.IsPaused())
    {
        %this.stop();
    }

    %this.CurrentPlaylist = 0;
    %this.CurrentTrackIndex = -1;

    VMPlayerPrintDebugMessage(%this, "Current playlist unloaded");
}

function VMPlayer::IsPlaylistLoaded(%this)
{
    return isObject(%this.CurrentPlaylist);
}

function VMPlayer::GetCurrentPlaylistName(%this)
{
    if (!isObject(%this.CurrentPlaylist))
    {
        return "";
    }

    return %this.CurrentPlaylist.name;
}

function VMPlayer::GetPlaylistCount(%this)
{
    return %this.Playlists.getCount();
}

function VMPlayer::GetPlaylistName(%this, %index)
{
    if ((%index < 1) || (%index > %this.Playlists.getCount()))
    {
        return "";
    }

    return %this.Playlists.GetAt(%index - 1).name;
}

function VMPlayer::AddSongToPlaylist(%this, %playlistname, %song, %title, %artist, %start_pos, %end_pos)
{
    %playlist = VMPlayerGetPlaylist(%this, %playlistname);
    if (!isObject(%playlist))
    {
        VMPlayerPrintDebugMessage(%this, "Unable to add song \'" @ %song @ "\' to playlist \'" @ %playlistname @ "\', playlist does not exist");
        return 0;
    }

    if (%song $= "")
    {
        VMPlayerPrintDebugMessage(%this, "Unable to add song \'" @ %song @ "\' to playlist \'" @ %playlistname @ "\', song name invalid");
        return 0;
    }

    if (!isFile(%song))
    {
        VMPlayerPrintDebugMessage(%this, "Unable to add song \'" @ %song @ "\' to playlist \'" @ %playlistname @ "\', song file does not exist");
        return 0;
    }

    %filename = %this.CurrentMusicChannel.AudioProfile.fileName;
    %this.CurrentMusicChannel.AudioProfile.fileName = %song;
    %handle = alxPlay(%this.CurrentMusicChannel.AudioProfile);
    if (%handle == 0)
    {
        VMPlayerPrintDebugMessage(%this, "Unable to add song \'" @ %song @ "\' to playlist \'" @ %playlistname @ "\', song file does not appear to be a valid audio file");
        %this.CurrentMusicChannel.AudioProfile.fileName = %filename;
        return 0;
    }

    %song_duration = alxGetStreamDuration(%handle);
    alxStop(%handle);
    %playlist.duration += %song_duration;
    if ((%end_pos $= "") || (%end_pos < %start_pos))
    {
        %end_pos = %song_duration;
    }

    // DSODIFF: double check this
    %track = VMPlayerGenerateTrackInfo(%song, %title, %artist, %start_pos, %end_pos);
    %playlist.Tracks.InsertBack(%track);
    if (%playlist.Tracks.getCount() == 1)
    {
        %this.SetCurrentTrackNumber(1);
    }

    VMPlayerPrintDebugMessage(%this, "Song \'" @ %song @ "\' added to playlist \'" @ %playlistname @ "\'");
    return %playlist.Tracks.getCount();
}

function VMPlayer::RemoveTrack(%this, %playlistname, %tracknumber)
{
    %playlist = VMPlayerGetPlaylist(%this, %playlistname);
    if (!isObject(%playlist))
    {
        VMPlayerPrintDebugMessage(%this, "Unable to remove track \'" @ %tracknumber @ "\' from playlist \'" @ %playlistname @ "\', playlist does not exist");
        return 0;
    }

    if ((%tracknumber <= 0) || (%tracknumber > %playlist.Tracks.getCount()))
    {
        VMPlayerPrintDebugMessage(%this, "Unable to remove track \'" @ %tracknumber @ "\' from playlist \'" @ %playlistname @ "\', track number does not exist");
        return 0;
    }

    if ((%this.CurrentPlaylist == %playlist) && (%tracknumber == %this.GetCurrentTrackNumber()))
    {
        if (%this.IsPlaying() || %this.IsPaused())
        {
            %this.stop();
        }
        if (%tracknumber == %playlist.Tracks.getCount())
        {
            %this.CurrentTrackIndex--;
        }
    }

    %playlist.Tracks.RemoveAt(%tracknumber - 1);
    VMPlayerPrintDebugMessage(%this, "Track \'" @ %tracknumber @ "\' removed from playlist \'" @ %playlistname @ "\'");
    return 1;
}

function VMPlayer::RemoveAllTracks(%this, %playlistname)
{
    %playlist = VMPlayerGetPlaylist(%this, %playlistname);
    if (!isObject(%playlist))
    {
        VMPlayerPrintDebugMessage(%this, "Unable to remove all tracks from playlist \'" @ %playlistname @ "\', playlist does not exist");
        return 0;
    }

    if (%this.CurrentPlaylist == %playlist)
    {
        if (%this.IsPlaying() || %this.IsPaused())
        {
            %this.stop();
        }
        %this.CurrentTrackIndex = -1;
    }

    %playlist.Tracks.RemoveAll();
    VMPlayerPrintDebugMessage(%this, "All tracks removed from playlist \'" @ %tracknumber @ "\'");
    return 1;
}

function VMPlayer::GetTrackCount(%this, %playlistname)
{
    %playlist = VMPlayerGetPlaylist(%this, %playlistname);
    if (!isObject(%playlist))
    {
        return -1;
    }
    return %playlist.Tracks.getCount();
}

function VMPlayer::GetCurrentTrackNumber(%this)
{
    if (!%this.IsPlaylistLoaded() || (%this.CurrentPlaylist.Tracks.getCount() == 0))
    {
        return 0;
    }

    return %this.CurrentTrackIndex + 1;
}

function VMPlayer::SetCurrentTrackNumber(%this, %tracknumber)
{
    if ((!%this.IsPlaylistLoaded() || (%tracknumber < 1)) || (%tracknumber > %this.CurrentPlaylist.Tracks.getCount()))
    {
        return 0;
    }
    if (%this.IsPlaying() || %this.IsPaused())
    {
        %this.stop();
    }

    %this.CurrentTrackIndex = %tracknumber - 1;
    return 1;
}

function VMPlayer::GetTrackFilename(%this, %playlistname, %tracknumber)
{
    %track = VMPlayerGetTrack(%this, %playlistname, %tracknumber);
    if (isObject(%track))
    {
        return %track.fileName;
    }

    return "";
}

function VMPlayer::GetTrackTitle(%this, %playlistname, %tracknumber)
{
    %track = VMPlayerGetTrack(%this, %playlistname, %tracknumber);
    if (isObject(%track))
    {
        return %track.title;
    }

    return "";
}

function VMPlayer::SetTrackTitle(%this, %playlistname, %tracknumber, %title)
{
    if (%title $= "")
    {
        return 0;
    }

    %track = VMPlayerGetTrack(%this, %playlistname, %tracknumber);
    if (!isObject(%track))
    {
        return 0;
    }

    %track.title = %title;
    return 1;
}

function VMPlayer::GetTrackArtist(%this, %playlistname, %tracknumber)
{
    %track = VMPlayerGetTrack(%this, %playlistname, %tracknumber);
    if (isObject(%track))
    {
        return %track.Artist;
    }

    return "";
}

function VMPlayer::SetTrackArtist(%this, %playlistname, %tracknumber, %artist)
{
    if (%artist $= "")
    {
        %artist = "Unknown";
    }

    %track = VMPlayerGetTrack(%this, %playlistname, %tracknumber);
    if (!isObject(%track))
    {
        return 0;
    }

    %track.Artist = %artist;
    return 1;
}

function VMPlayer::GetTrackStartPosition(%this, %playlistname, %tracknumber)
{
    %track = VMPlayerGetTrack(%this, %playlistname, %tracknumber);
    if (isObject(%track))
    {
        return %track.StartPosition;
    }
    return -1;
}

function VMPlayer::SetTrackStartPosition(%this, %playlistname, %tracknumber, %start_pos)
{
    %track = VMPlayerGetTrack(%this, %playlistname, %tracknumber);
    if (!isObject(%track))
    {
        return 0;
    }

    if (%start_pos > %track.EndPosition)
    {
        return 0;
    }

    %track.StartPosition = %start_pos;
    return 1;
}

function VMPlayer::GetTrackEndPosition(%this, %playlistname, %tracknumber)
{
    %track = VMPlayerGetTrack(%this, %playlistname, %tracknumber);
    if (isObject(%track))
    {
        return %track.EndPosition;
    }

    return -1;
}

function VMPlayer::SetTrackEndPosition(%this, %playlistname, %tracknumber, %end_pos)
{
    %track = VMPlayerGetTrack(%this, %playlistname, %tracknumber);
    if (!isObject(%track))
    {
        return 0;
    }

    if (%end_pos < %track.StartPosition)
    {
        return 0;
    }

    %track.EndPosition = %end_pos;
    return 1;
}

function VMPlayer::GetTrackDuration(%this, %playlistname, %tracknumber)
{
    %track = VMPlayerGetTrack(%this, %playlistname, %tracknumber);
    if (isObject(%track))
    {
        return %track.EndPosition - %track.StartPosition;
    }

    return -1;
}

function VMPlayer::GetSongFilename(%this)
{
    if (isObject(%this.CurrentSong))
    {
        return %this.CurrentSong.fileName;
    }

    return "";
}

function VMPlayer::GetSongTitle(%this)
{
    if (isObject(%this.CurrentSong))
    {
        return %this.CurrentSong.title;
    }

    return "";
}

function VMPlayer::SetSongTitle(%this, %title)
{
    if ((%title $= "") || !isObject(%this.CurrentSong))
    {
        return 0;
    }

    %this.CurrentSong.title = %title;
    return 1;
}

function VMPlayer::GetSongArtist(%this)
{
    if (isObject(%this.CurrentSong))
    {
        return %this.CurrentSong.Artist;
    }

    return "";
}

function VMPlayer::SetSongArtist(%this, %artist)
{
    if (!isObject(%this.CurrentSong))
    {
        return 0;
    }

    if (%artist $= "")
    {
        %artist = "Unknown";
    }

    %this.CurrentSong.Artist = %artist;
    return 1;
}

function VMPlayer::GetSongStartPosition(%this)
{
    if (isObject(%this.CurrentSong))
    {
        return %this.CurrentSong.StartPosition;
    }

    return -1;
}

function VMPlayer::SetSongStartPosition(%this, %start_pos)
{
    if (!isObject(%this.CurrentSong))
    {
        return 0;
    }

    if (%start_pos > %this.CurrentSong.EndPosition)
    {
        return 0;
    }

    %this.CurrentSong.StartPosition = %start_pos;
    return 1;
}

function VMPlayer::GetSongEndPosition(%this)
{
    if (isObject(%this.CurrentSong))
    {
        return %this.CurrentSong.EndPosition;
    }

    return -1;
}

function VMPlayer::SetSongEndPosition(%this, %end_pos)
{
    if (!isObject(%this.CurrentSong))
    {
        return 0;
    }

    if (%end_pos < %this.CurrentSong.StartPosition)
    {
        return 0;
    }

    %this.CurrentSong.EndPosition = %end_pos;
    return 1;
}

function VMPlayer::GetSongDuration(%this)
{
    if (isObject(%this.CurrentSong))
    {
        return %this.CurrentSong.EndPosition - %this.CurrentSong.StartPosition;
    }

    return -1;
}

function VMPlayer::AddEventToTrack(%this, %playlistname, %tracknumber, %eventfunc, %pos)
{
    %track = VMPlayerGetTrack(%this, %playlistname, %tracknumber);
    if (!isObject(%track))
    {
        VMPlayerPrintDebugMessage(%this, "Unable to add event to track \'" @ %tracknumber @ "\' of playlist \'" @ %playlistname @ "\', track or playlist does not exist");
        return 0;
    }

    %this.RemoveEventFromTrack(%playlistname, %tracknumber, %pos);
    %event = new ScriptObject() {
        class = VMPlayerEvent;
        position = %pos;
        EventFunction = %eventfunc;
    };

    %track.Events.InsertBack(%event);
    VMPlayerPrintDebugMessage(%this, "Event added to track \'" @ %tracknumber @ "\' of playlist \'" @ %playlistname @ "\'");
    return 1;
}

function VMPlayer::RemoveEventFromTrack(%this, %playlistname, %tracknumber, %pos)
{
    %track = VMPlayerGetTrack(%this, %playlistname, %tracknumber);
    if (!isObject(%track))
    {
        return 0;
    }

    for (%x=0; %x < %track.Events.getCount(); %x++)
    {
        if (%track.Events.GetAt(%x).position == %pos)
        {
            %track.Events.RemoveAt(%x);
            return 1;
        }
    }

    return 0;
}

function VMPlayer::RemoveAllEventsFromTrack(%this, %playlistname, %tracknumber)
{
    %track = VMPlayerGetTrack(%this, %playlistname, %tracknumber);
    if (!isObject(%track))
    {
        return 0;
    }

    %track.Events.RemoveAll();
    return 1;
}

function VMPlayer::AddEventToSong(%this, %eventfunc, %pos)
{
    if (!isObject(%this.CurrentSong))
    {
        return 0;
    }

    %this.RemoveEventFromSong(%pos);

    %event = new ScriptObject() {
        class = VMPlayerEvent;
        position = %pos;
        EventFunction = %eventfunc;
    };

    %this.CurrentSong.Events.InsertBack(%event);
    return 1;
}

function VMPlayer::RemoveEventFromSong(%this, %pos)
{
    if (!isObject(%this.CurrentSong))
    {
        return 0;
    }

    for (%x=0; %x < %this.CurrentSong.Events.getCount(); %x++)
    {
        if (%this.CurrentSong.Events.GetAt(%x).position == %pos)
        {
            %this.CurrentSong.Events.RemoveAt(%x);
            return 1;
        }
    }

    return 0;
}

function VMPlayer::RemoveAllEventsFromSong(%this)
{
    if (!isObject(%this.CurrentSong))
    {
        return 0;
    }

    %this.CurrentSong.Events.RemoveAll();
    return 1;
}

function VMPlayer::SetVolume(%this, %volume)
{
   if (%volume > 1.0)
      %volume = 1.0;
   if (%volume < 0.0)
      %volume = 0.0;

    %this.CurrentVolume = %volume;
    
    if ((%this.FadeOutHandle == 0) && (%this.CrossFadeOutHandle == 0))
    {
        alxSetChannelVolume(VMPlayerGetOtherMusicChannel(%this, %this.CurrentMusicChannel).AudioChannel, %volume);
    }

    if ((%this.FadeInHandle == 0) && (%this.CrossFadeInHandle == 0))
    {
        alxSetChannelVolume(%this.CurrentMusicChannel.AudioChannel, %volume);
    }
}

function VMPlayer::GetVolume(%this)
{
    return %this.CurrentVolume;
}

function VMPlayer::GetNextTrackDelay(%this)
{
    return %this.NextTrackDelayMS / 1000;
}

function VMPlayer::SetNextTrackDelay(%this, %seconds)
{
    %this.NextTrackDelayMS = %seconds * 1000;
}

function VMPlayer::EnableFade(%this, %use_fade)
{
    %this.fade = %use_fade;
}

function VMPlayer::IsFadeEnabled(%this)
{
    return %this.fade;
}

function VMPlayer::EnableCrossFade(%this, %use_crossfade)
{
    %this.CrossFade = %use_crossfade;
}

function VMPlayer::IsCrossFadeEnabled(%this)
{
    return %this.CrossFade;
}

function VMPlayer::SetFadeDuration(%this, %use_fade)
{
    %this.FadeDuration = %use_fade;
}

function VMPlayer::GetFadeDuration(%this)
{
    return %this.FadeDuration;
}

function VMPlayer::EnableRandom(%this, %use_random)
{
    %this.Random = %use_random;
}

function VMPlayer::IsRandomEnabled(%this)
{
    return %this.Random;
}

function VMPlayer::EnableRepeat(%this, %use_repeat)
{
    %this.Repeat = %use_repeat;
}

function VMPlayer::IsRepeatEnabled(%this)
{
    return %this.Repeat;
}

function VMPlayer::EnableDebug(%this, %enable_debug)
{
    %this.debug = %enable_debug;
}

function VMPlayer::IsDebugEnabled(%this)
{
    return %this.debug;
}

function VMPlayer::DebugDisplayAll(%this)
{
    echo("");

    if (%this.Playlists.getCount() == 0)
    {
        echo("VMPlayer: No playlists added");
        return;
    }

    for (%x=0; %x < %this.Playlists.getCount(); %x++)
    {
        echo(%this.Playlists.GetAt(%x).name);
        echo("------------------");
        %this.DebugPrintTracks(%this.Playlists.GetAt(%x).name);
        echo("");
    }
}

function VMPlayer::DebugDisplayPlaylists(%this)
{
    if (%this.Playlists.getCount() == 0)
    {
        echo("No playlists added");
        return;
    }

    for (%x=0; %x < %this.Playlists.getCount(); %x++)
    {
        echo(%this.Playlists.GetAt(%x).name);
    }
}

function VMPlayer::DebugDisplayTracks(%this, %playlistname)
{
    if (%this.Playlists.getCount() == 0)
    {
        echo("VMPlayer: No playlists added");
        return;
    }

    %playlist = VMPlayerGetPlaylist(%this, %playlistname);
    if (!isObject(%playlist))
    {
        echo("VMPlayer: Playlist not found");
        return;
    }

    if (%playlist.Tracks.getCount() == 0)
    {
        echo("VMPlayer: No tracks added");
        return;
    }

    for (%x=0; %x < %playlist.Tracks.getCount(); %x++)
    {
        echo(%playlist.Tracks.GetAt(%x).title);
    }
}

function VMPlayer::PlaySong(%this, %song, %title, %artist, %start_pos, %end_pos)
{
    if (!isFile(%song))
    {
        VMPlayerPrintDebugMessage(%this, "Unable to play song \'" @ %song @ "\', song file does not exist");
        return 0;
    }

    if (%this.IsPlaylistLoaded())
    {
        %this.UnloadPlaylist();
    }

    if (%this.IsPlaying() || %this.IsPaused())
    {
        %CurrentFilename = %this.CurrentMusicChannel.AudioProfile.fileName;
        %this.CurrentMusicChannel.AudioProfile.fileName = %song;
        if (%CurrentFilename $= %this.CurrentMusicChannel.AudioProfile.fileName)
        {
            if (%this.IsPaused())
            {
                %this.Unpause();
            }
            VMPlayerPrintDebugMessage(%this, "Song \'" @ %song @ "\' already playing");
            return 1;
        }
    }

    if (%this.IsCrossFadeEnabled() && (%this.IsPlaying() || %this.IsPaused()))
    {
        VMPlayerCrossFadeOut(%this);
    }
    else
    {
        if (%this.IsPlaying() || %this.IsPaused())
        {
            %this.stop();
        }
    }

    %song = VMPlayerGenerateTrackInfo(%song, %title, %artist, %start_pos, %end_pos);
    VMPlayerPlay(%this, %song);
    if (!%this.IsPlaying())
    {
        %song.delete();
        return 0;
    }

    %song_duration = alxGetStreamDuration(%this.CurrentMusicChannel.AudioHandle);
    if (%song.EndPosition == -1)
    {
        %end_pos = %song_duration;
    }

    return 1;
}

function VMPlayer::PlayTrack(%this, %tracknumber)
{
    if (!%this.IsPlaylistLoaded())
    {
        VMPlayerPrintDebugMessage(%this, "Unable to play track \'" @ %tracknumber @ "\', no playlist loaded");
        return 0;
    }

    if ((%tracknumber < 0) || (%tracknumber > %this.CurrentPlaylist.Tracks.getCount()))
    {
        VMPlayerPrintDebugMessage(%this, "Unable to play track \'" @ %tracknumber @ "\', track is invalid");
        return 0;
    }

    if (%this.IsPlaying() && (%this.CurrentTrackIndex == (%tracknumber - 1)))
    {
        VMPlayerPrintDebugMessage(%this, "Track \'" @ %tracknumber @ "\' already playing");
        return 1;
    }

    if (%this.IsPaused() && (%this.CurrentTrackIndex == (%tracknumber - 1)))
    {
        %this.Unpause();
        VMPlayerPrintDebugMessage(%this, "Track \'" @ %tracknumber @ "\' already playing");
        return 1;
    }

    if (%this.IsCrossFadeEnabled() && (%this.IsPlaying() || %this.IsPaused()) )
    {
        VMPlayerCrossFadeOut(%this);
    }
    else
    {
        if (%this.IsPlaying() || %this.IsPaused())
        {
            %this.stop();
        }
    }

    %this.NextTracks.RemoveAll();
    %this.NextTracks.InsertBack(%tracknumber - 1);
    %this.NextTrackIndex = 0;
    %this.CurrentTrackIndex = %this.NextTracks.GetAt(%this.NextTrackIndex);
    %this.CurrentTrackIndex = %tracknumber - 1;

    return VMPlayerPlay(%this, %this.CurrentPlaylist.Tracks.GetAt(%this.CurrentTrackIndex));
}

function VMPlayer::Play(%this)
{
    if (!%this.IsPlaylistLoaded())
    {
        VMPlayerPrintDebugMessage(%this, "Unable to play, no playlist loaded");
        return 0;
    }

    if (%this.CurrentPlaylist.Tracks.getCount() == 0)
    {
        VMPlayerPrintDebugMessage(%this, "Unable to play, no tracks added");
        return 0;
    }

    if (%this.IsPlaying())
    {
        VMPlayerPrintDebugMessage(%this, "Song already playing");
        return 1;
    }

    if (%this.IsPaused())
    {
        %this.Unpause();
        VMPlayerPrintDebugMessage(%this, "Song already playing");
        return 1;
    }

    if (%this.IsCrossFadeEnabled() && (%this.IsPlaying() || %this.IsPaused()) )
    {
        VMPlayerCrossFadeOut();
    }
    else
    {
        if (%this.IsPlaying() || %this.IsPaused())
        {
            %this.stop();
        }
    }

    VMPlayerGenerateNextTrackArray(%this);
    %this.NextTrackIndex = 0;
    %this.CurrentTrackIndex = %this.NextTracks.GetAt(%this.NextTrackIndex);

    return VMPlayerPlay(%this, %this.CurrentPlaylist.Tracks.GetAt(%this.CurrentTrackIndex), 1);
}

function VMPlayer::IsPlaying(%this)
{
    if ((%this.FadeOutHandle != 0) && alxIsPaused(%this.CurrentMusicChannel.AudioHandle))
    {
        return 1;
    }

    return alxIsPlaying(%this.CurrentMusicChannel.AudioHandle);
}

function VMPlayer::IsPaused(%this)
{
    if ((%this.FadeOutHandle != 0) && !alxIsPaused(VMPlayerGetOtherMusicChannel(%this, %this.CurrentMusicChannel).AudioHandle))
    {
        return 0;
    }

    return alxIsPaused(%this.CurrentMusicChannel.AudioHandle);
}

function VMPlayer::Pause(%this)
{
    if (%this.IsPlaying())
    {
        alxPause(%this.CurrentMusicChannel.AudioHandle);
        if (VMPlayerGetOtherMusicChannel(%this, %this.CurrentMusicChannel).AudioHandle != 0)
        {
            alxPause(VMPlayerGetOtherMusicChannel(%this, %this.CurrentMusicChannel).AudioHandle);
        }
    }
    else
    {
        if (%this.NextTrackHandle != 0)
        {
            %this.NextTrackPause = 1;
        }
    }
}

function VMPlayer::Unpause(%this)
{
    if (%this.IsPaused())
    {
        if ((%this.FadeOutHandle != 0) || (%this.CrossFadeOutHandle != 0))
        {
            alxUnpause(VMPlayerGetOtherMusicChannel(%this, %this.CurrentMusicChannel).AudioHandle);
        }
        if (%this.FadeOutHandle == 0)
        {
            alxUnpause(%this.CurrentMusicChannel.AudioHandle);
        }
    }

    %this.NextTrackPause = 0;
}

function VMPlayer::stop(%this)
{
    if (%this.IsFadeEnabled())
    {
        if (%this.FadeOutHandle == 0)
        {
            VMPlayerFadeOut(%this, %this.CurrentMusicChannel);
        }
        else
        {
            if (%this.FadeInHandle != 0)
            {
                cancel(%this.FadeInHandle);
                %this.FadeInHandle = 0;
                VMPlayerStopChannel(%this, %this.CurrentMusicChannel);
            }
        }
        return;
    }

    cancel(%this.FadeInHandle);
    %this.FadeInHandle = 0;
    cancel(%this.CrossFadeInHandle);
    cancel(%this.CrossFadeOutHandle);
    %this.CrossFadeInHandle = 0;
    %this.CrossFadeOutHandle = 0;

    VMPlayerStop(%this);
}

function VMPlayer::GetSongPosition(%this)
{
    if (%this.IsPlaying() || %this.IsPaused())
    {
        return alxGetStreamPosition(%this.CurrentMusicChannel.AudioHandle) - %this.CurrentSong.StartPosition;
    }

    return -1;
}

function VMPlayer::SetSongPosition(%this, %seconds)
{
    if (%this.IsPlaying() || %this.IsPaused())
    {
        %seconds = %this.CurrentSong.StartPosition + %seconds;
        %ret = alxSeekToStreamPosition(%this.CurrentMusicChannel.AudioHandle, %seconds);
        %this.PreviousPosition = %seconds - 0.001;
        VMPlayerCheckSongPosition(%this);

        if (%ret == 1)
        {
            %time_left = %this.GetSongDuration() - %this.GetSongPosition();
            if ((%time_left <= %this.FadeDuration) && %this.IsFadeEnabled())
            {
                VMPlayerFadeOut(%this, %this.CurrentMusicChannel);
            }
        }
        return %ret;
    }

    return 0;
}

function VMPlayer::JumpBackward(%this, %seconds)
{
    if (%this.IsPlaying() || %this.IsPaused())
    {
        %new_pos = %this.GetSongPosition() - %seconds;
        if (%new_pos < 0)
        {
            %new_pos = 0;
        }

        %this.SetSongPosition(%new_pos);
        return 1;
    }

    return 0;
}

function VMPlayer::JumpForward(%this, %seconds)
{
    if (%this.IsPlaying() || %this.IsPaused())
    {
        %new_pos = %this.GetSongPosition() + %seconds;
        if (%new_pos > %this.GetSongDuration())
        {
            %new_pos = %this.GetSongDuration();
        }

        %this.SetSongPosition(%new_pos);
        return 1;
    }

    return 0;
}

function VMPlayer::SkipBackward(%this)
{
    if (%this.IsPlaying() || %this.IsPaused())
    {
        if (!%this.IsPlaylistLoaded())
        {
            %song = VMPlayerCopyTrackInfo(%this, %this.CurrentSong);
        }

        %was_paused = %this.IsPaused();
        %pos = %this.GetSongPosition();

        if (%this.IsCrossFadeEnabled())
        {
            VMPlayerCrossFadeOut(%this);
        }
        else if (%this.IsFadeEnabled())
        {
            VMPlayerFadeOut(%this);
        }
        else
        {
            %this.stop();
        }

        if (%this.IsPlaylistLoaded() && %pos <= 0.0 && %this.NextTrackIndex > 0)
        {
            %this.NextTrackIndex--;
            %this.CurrentTrackIndex = %this.NextTracks.GetAt(%this.NextTrackIndex);
        }

        if (%this.IsPlaylistLoaded())
        {
            VMPlayerPlay(%this, %this.CurrentPlaylist.Tracks.GetAt(%this.CurrentTrackIndex));
        }
        else
        {
            VMPlayerPlay(%this, %song);
        }

        if (%was_paused)
        {
            %this.Pause();
        }

        return 1;
    }

    return 0;
}

function VMPlayer::SkipForward(%this)
{
    if (%this.IsPlaying() || %this.IsPaused())
    {
        if (%this.IsPlaylistLoaded())
        {
            %musicplayer.NextTrackPause = 0;
            VMPlayerScheduleNextTrack(%this);
            return 1;
        }
    }
    return 0;
}

function VMPlayerScheduleNextTrack(%musicplayer)
{
    if (%musicplayer.IsPlaylistLoaded())
    {
        if ((%musicplayer.NextTrackIndex + 1) < %musicplayer.NextTracks.getCount())
        {
            %musicplayer.NextTrackIndex++;
        }
        else
        {
            %musicplayer.NextTrackIndex = 0;
        }
        if ((%musicplayer.NextTrackIndex == 0) && !%musicplayer.IsRepeatEnabled())
        {
            %musicplayer.stop();
            %musicplayer.CurrentTrackIndex = %musicplayer.NextTracks.GetAt(0);
            %musicplayer.NextTrackPause = 0;
            return;
        }
        %musicplayer.CurrentTrackIndex = %musicplayer.NextTracks.GetAt(%musicplayer.NextTrackIndex);
    }

    %filename = %musicplayer.CurrentSong.fileName;
    %title = %musicplayer.CurrentSong.title;
    %artist = %musicplayer.CurrentSong.Artist;
    %start_pos = %musicplayer.CurrentSong.StartPosition;
    %end_pos = %musicplayer.CurrentSong.EndPosition;
    %was_paused = %musicplayer.NextTrackPause || %musicplayer.IsPaused();
    
    if (%musicplayer.IsCrossFadeEnabled() && (%musicplayer.IsPlaying() || %musicplayer.IsPaused()))
    {
        VMPlayerCrossFadeOut(%musicplayer);
    }
    else
    {
        %musicplayer.stop();
    }

    if (%musicplayer.IsPlaylistLoaded())
    {
        VMPlayerPlay(%musicplayer, %musicplayer.CurrentPlaylist.Tracks.GetAt(%musicplayer.NextTracks.GetAt(%musicplayer.NextTrackIndex)));
    }
    else
    {
        if (%musicplayer.IsRepeatEnabled())
        {
            %musicplayer.PlaySong(%filename, %title, %artist, %start_pos, %end_pos);
        }
    }

    if (%was_paused)
    {
        %musicplayer.Pause();
    }

    %musicplayer.NextTrackPause = 0;
    %musicplayer.NextTrackHandle = 0;
}

function VMPlayerNextTrack(%musicplayer)
{
    %musicplayer.NextTrackPause = 0;
    %musicplayer.NextTrackHandle = schedule(%musicplayer.NextTrackDelayMS, 0, VMPlayerScheduleNextTrack, %musicplayer);
}

function VMPlayerCheckSongPosition(%musicplayer)
{
    if (!isObject(%musicplayer.CurrentSong))
    {
        return;
    }

    if (alxIsPaused(%musicplayer.CurrentMusicChannel.AudioHandle))
    {
        %musicplayer.CheckSongPosHandle = schedule($VMPlayerCheckSongPosIntervalMS, 0, VMPlayerCheckSongPosition, %musicplayer);
        return;
    }

    %pos = %musicplayer.GetSongPosition();
    if (%musicplayer.Streaming == 1)
    {
        if (%pos < 0)
        {
            %musicplayer.PreviousPosition = -1;
            %musicplayer.SetSongPosition(0);
            %pos = 0;
        }
        if (%pos != %musicplayer.PreviousPosition)
        {
            for (%x=0; %x < %musicplayer.CurrentSong.Events.getCount(); %x++)
            {
                if (%musicplayer.CurrentSong.Events.GetAt(%x).Position > (%musicplayer.CurrentSong.StartPosition + %musicplayer.PreviousPosition) && %musicplayer.CurrentSong.Events.GetAt(%x).Position <= (%musicplayer.CurrentSong.StartPosition + %pos) && %musicplayer.CurrentSong.StartPosition <= %musicplayer.CurrentSong.Events.GetAt(%x).Position)
                {
                    VMPlayerPrintDebugMessage(%musicplayer, "Firing event function");
                    schedule(1, 0, %musicplayer.CurrentSong.Events.GetAt(%x).EventFunction);
                }
            }
        }
        %musicplayer.PreviousPosition = %pos;
    }

    if (alxIsPlaying(%musicplayer.CurrentMusicChannel.AudioHandle))
    {
        %end_pos = %musicplayer.GetSongDuration();
        if (%musicplayer.Streaming == 1)
        {
            if (%pos >= %musicplayer.GetSongDuration())
            {
                %musicplayer.stop();
                VMPlayerNextTrack(%musicplayer);
                return;
            }
            if (%musicplayer.IsPlaylistLoaded() && %musicplayer.IsCrossFadeEnabled() && (%musicplayer.NextTrackIndex + 1 < %musicplayer.NextTracks.GetCount() || %musicplayer.IsRepeatEnabled()) && %end_pos - %pos <= %musicplayer.FadeDuration)
            {
                %musicplayer.NextTrackPause = 0;
                VMPlayerScheduleNextTrack(%musicplayer);
                return;
            }
            else
            {
                if (%musicplayer.IsFadeEnabled() && %end_pos - %pos <= %musicplayer.FadeDuration)
                {
                    %musicplayer.NextTrackPause = 0;
                    VMPlayerScheduleNextTrack(%musicplayer);
                    return;
                }
            }
        }
        %musicplayer.CheckSongPosHandle = schedule($VMPlayerCheckSongPosIntervalMS, 0, VMPlayerCheckSongPosition, %musicplayer);
    }
    else
    {
        %musicplayer.stop();
        %musicplayer.NextTrackPause = 0;
        VMPlayerNextTrack(%musicplayer);
    }
}

function VMPlayerDeleteTrackInfo(%musicplayer, %track_info)
{
    if (isObject(%track_info))
    {
        if (!%musicplayer.IsPlaylistLoaded())
        {
            %track_info.Events.delete();
            %track_info.delete();
        }
    }
}

function VMPlayerCopyTrackInfo(%musicplayer, %track_info)
{
    if (isObject(%track_info))
    {
        %events = new ScriptObject() {
            class = VMPArray;
        };

        for (%x=0; %x < %track_info.Events.getCount(); %x++)
        {
            %event = new ScriptObject() {
                class = VMPlayerEvent;
                position = %track_info.Events.GetAt(%x).position;
                EventFunction = %track_info.Events.GetAt(%x).EventFunction;
            };
            %events.InsertBack(%event);
        }

        %track = new ScriptObject() {
            class = VMPlayerTrack;
            fileName = %track_info.fileName;
            title = %track_info.title;
            Artist = %track_info.Artist;
            StartPosition = %track_info.StartPosition;
            EndPosition = %track_info.EndPosition;
            Events = %events;
        };

        return %track;
    }
    return 0;
}

function VMPlayerGenerateTrackInfo(%filename, %title, %artist, %start_pos, %end_pos)
{
    if (%title $= "")
    {
        %index = 0;
        while (strpos(%filename, "\\", %index) != -1)
        {
            %index = strpos(%filename, "\\", %index) + 1;
        }
        %title = getSubStr(%filename, %index, strlen(%filename));
        %index = 0;
        while (strpos(%title, "/", %index) != -1)
        {
            %index = strpos(%title, "/", %index) + 1;
        }
        %title = getSubStr(%title, %index, strlen(%title));
        %offset = 0;
        %index = strlen(%title);
        while (strpos(%title, ".", %offset) != -1)
        {
            %index = strpos(%title, ".", %offset);
            %offset = %index + 1;
        }
        %title = getSubStr(%title, 0, %index);
    }

    if (%artist $= "")
    {
        %artist = "Unknown";
    }

    if ((%start_pos $= "") || (%start_pos < 0.0))
    {
        %start_pos = 0.0;
    }

    if ((%end_pos $= "") || (%end_pos < %start_pos))
    {
        %end_pos = -1;
    }

    %events = new ScriptObject() {
        class = VMPArray;
    };

    %track = new ScriptObject() {
        class = VMPlayerTrack;
        fileName = %filename;
        title = %title;
        Artist = %artist;
        StartPosition = %start_pos;
        EndPosition = %end_pos;
        Events = %events;
    };

    return %track;
}

function VMPlayerGetPlaylist(%musicplayer, %playlistname)
{
    if ((%musicplayer.Playlists.getCount() == 0) || (%playlistname $= ""))
    {
        return 0;
    }

    for (%x=0; %x < %musicplayer.Playlists.getCount(); %x++)
    {
        if (%musicplayer.Playlists.GetAt(%x).name $= %playlistname)
        {
            return %musicplayer.Playlists.GetAt(%x);
        }
    }

    return 0;
}

function VMPlayerGetTrack(%musicplayer, %playlistname, %tracknumber)
{
    %playlist = VMPlayerGetPlaylist(%musicplayer, %playlistname);
    if (!isObject(%playlist))
    {
        return 0;
    }

    if ((%tracknumber < 1) || (%tracknumber > %playlist.Tracks.getCount()))
    {
        return 0;
    }

    return %playlist.Tracks.GetAt(%tracknumber - 1);
}

function VMPlayerGenerateNextTrackArray(%musicplayer)
{
    %musicplayer.NextTracks.RemoveAll();
    for (%x = %musicplayer.GetCurrentTrackNumber() - 1; %x < %musicplayer.GetTrackCount(%musicplayer.GetCurrentPlaylistName()); %x++)
    {
        %musicplayer.NextTracks.InsertBack(%x);
    }

    for (%x=0; %x < (%musicplayer.GetCurrentTrackNumber() - 1); %x++)
    {
        %musicplayer.NextTracks.InsertBack(%x);
    }

    if (%musicplayer.IsRandomEnabled())
    {
        for (%x=0; %x < %musicplayer.NextTracks.getCount(); %x++)
        {
            %rnd = getRandom(0, %musicplayer.NextTracks.getCount() - 1);
            %musicplayer.NextTracks.Swap(%x, %rnd);
        }
    }
}

$VMPlayer_FadeInVolume = 0.1;

function VMPlayerPlay(%musicplayer, %song)
{
    alxSetChannelVolume(%musicplayer.CurrentMusicChannel.AudioChannel, %musicplayer.CurrentVolume);
    %musicplayer.CurrentMusicChannel.AudioProfile.fileName = %song.fileName;
    %musicplayer.CurrentMusicChannel.AudioHandle = alxPlay(%musicplayer.CurrentMusicChannel.AudioProfile);
    
    if (%song.StartPosition > 0.0)
    {
        alxSeekToStreamPosition(%musicplayer.CurrentMusicChannel.AudioHandle, %song.StartPosition);
    }

    if (alxIsPlaying(%musicplayer.CurrentMusicChannel.AudioHandle))
    {
        if (%song.EndPosition == -1)
        {
            %song.EndPosition = alxGetStreamDuration(%musicplayer.CurrentMusicChannel.AudioHandle);
        }

        %musicplayer.PreviousPosition = -1;
        
        if (%musicplayer.IsFadeEnabled())
        {
            alxSetChannelVolume(%musicplayer.CurrentMusicChannel.AudioChannel, $VMPlayer_FadeInVolume);
            %volume_delta = (%musicplayer.CurrentVolume - $VMPlayer_FadeInVolume) / ((%musicplayer.FadeDuration * 1000) / $VMPlayerFadeIntervalMS);
            VMPlayerScheduleMusicFadeIn(%musicplayer, %volume_delta, %musicplayer.CurrentMusicChannel);
        }
        else
        {
            if (%musicplayer.IsCrossFadeEnabled() && (%musicplayer.CrossFadeOutHandle != 0))
            {
                alxSetChannelVolume(%musicplayer.CurrentMusicChannel.AudioChannel, $VMPlayer_FadeInVolume);
                %volume_delta = (%musicplayer.CurrentVolume - $VMPlayer_FadeInVolume) / ((%musicplayer.FadeDuration * 1000) / $VMPlayerFadeIntervalMS);
                VMPlayerScheduleMusicCrossFadeIn(%musicplayer, %volume_delta, %musicplayer.CurrentMusicChannel);
            }
            else
            {
                alxSetChannelVolume(%musicplayer.CurrentMusicChannel.AudioChannel, %musicplayer.CurrentVolume);
            }
        }
        
        if (%musicplayer.FadeOutHandle != 0)
        {
            alxPause(%musicplayer.CurrentMusicChannel.AudioHandle);
        }
        
        %musicplayer.CurrentSong = %song;
        %musicplayer.CheckSongPosHandle = schedule($VMPlayerCheckSongPosIntervalMS, 0, VMPlayerCheckSongPosition, %musicplayer);
        VMPlayerPrintDebugMessage(%musicplayer, "Playing song \'" @ %song.fileName @ "\'");
        return 1;
    }

    VMPlayerPrintDebugMessage(%musicplayer, "Unable to play song \'" @ %song.fileName @ "\', filename does not exist or audio file is not valid");
    return 0;
}

function VMPlayerStop(%musicplayer)
{
    cancel(%musicplayer.CheckSongPosHandle);
    cancel(%musicplayer.NextTrackHandle);
    VMPlayerStopChannel(%musicplayer, %musicplayer.PrimaryMusicChannel);
    VMPlayerStopChannel(%musicplayer, %musicplayer.SecondaryMusicChannel);
    VMPlayerDeleteTrackInfo(%musicplayer, %musicplayer.CurrentSong);
    %musicplayer.CurrentSong = 0;
    %musicplayer.PreviousPosition = -1;
}

function VMPlayerStopChannel(%musicplayer, %musicchannel)
{
    if (%musicchannel.AudioHandle != 0)
    {
        alxStop(%musicchannel.AudioHandle);
    }

    %musicchannel.AudioHandle = 0;
    alxSetChannelVolume(%musicchannel.AudioChannel, %musicplayer.CurrentVolume);
}

function VMPlayerGetOtherMusicChannel(%musicplayer, %musicchannel)
{
    if (%musicchannel == %musicplayer.PrimaryMusicChannel)
    {
        return %musicplayer.SecondaryMusicChannel;
    }

    return %musicplayer.PrimaryMusicChannel;
}

function VMPlayerSwitchAudioChannels(%musicplayer)
{
    if (%musicplayer.CurrentMusicChannel == %musicplayer.PrimaryMusicChannel)
    {
        %musicplayer.CurrentMusicChannel = %musicplayer.SecondaryMusicChannel;
    }
    else
    {
        %musicplayer.CurrentMusicChannel = %musicplayer.PrimaryMusicChannel;
    }
}

function VMPlayerScheduleMusicFadeIn(%musicplayer, %volume_delta, %musicchannel)
{
    if (!VMPlayerDoMusicFade(%musicplayer, %volume_delta, %musicchannel))
    {
        %musicplayer.FadeInHandle = schedule($VMPlayerFadeIntervalMS, 0, VMPlayerScheduleMusicFadeIn, %musicplayer, %volume_delta, %musicchannel);
        return;
    }

    alxSetChannelVolume(%musicchannel.AudioChannel, %musicplayer.CurrentVolume);
    %musicplayer.FadeInHandle = 0;
}

function VMPlayerFadeOut(%musicplayer, %musicchannel)
{
    VMPlayerDeleteTrackInfo(%musicplayer, %musicplayer.CurrentSong);
    %musicplayer.CurrentSong = 0;
    %curr_channel = %musicplayer.CurrentMusicChannel;
    %other_channel = VMPlayerGetOtherMusicChannel(%musicplayer, %musicplayer.CurrentMusicChannel);
    %curr_volume = alxGetChannelVolume(%curr_channel.AudioChannel);
    %other_volume = alxGetChannelVolume(%other_channel.AudioChannel);

    if (%other_channel.AudioHandle != 0)
    {
        if (%other_volume > %curr_volume)
        {
            VMPlayerStopChannel(%musicplayer, %curr_channel);
            VMPlayerSwitchAudioChannels(%musicplayer);
            %curr_volume = %other_volume;
        }
        else
        {
            VMPlayerStopChannel(%musicplayer, %other_channel);
        }
    }

    cancel(%musicplayer.CheckSongPosHandle);
    cancel(%musicplayer.NextTrackHandle);
    cancel(%musicplayer.FadeInHandle);
    cancel(%musicplayer.FadeOutHandle);
    cancel(%musicplayer.CrossFadeOutHandle);
    cancel(%musicplayer.CrossFadeInHandle);
    %musicplayer.CheckSongPosHandle = 0;
    %musicplayer.NextTrackHandle = 0;
    %musicplayer.FadeInHandle = 0;
    %musicplayer.FadeOutHandle = 0;
    %musicplayer.CrossFadeOutHandle = 0;
    %musicplayer.CrossFadeInHandle = 0;
    %volume_delta = (%musicplayer.CurrentVolume / ((%musicplayer.FadeDuration * 1000) / $VMPlayerFadeIntervalMS)) * -1;
    VMPlayerScheduleMusicFadeOut(%musicplayer, %volume_delta, %musicplayer.CurrentMusicChannel);
    VMPlayerSwitchAudioChannels(%musicplayer);
}

function VMPlayerScheduleMusicFadeOut(%musicplayer, %volume_delta, %musicchannel)
{
    if (!VMPlayerDoMusicFade(%musicplayer, %volume_delta, %musicchannel))
    {
        %musicplayer.FadeOutHandle = schedule($VMPlayerFadeIntervalMS, 0, VMPlayerScheduleMusicFadeOut, %musicplayer, %volume_delta, %musicchannel);
        return;
    }

    if (%musicchannel.AudioHandle != 0)
    {
        alxStop(%musicchannel.AudioHandle);
    }

    %musicchannel.AudioHandle = 0;
    alxSetChannelVolume(%musicchannel.AudioChannel, %musicplayer.CurrentVolume);
    %musicplayer.FadeOutHandle = 0;
    
    if (alxIsPaused(VMPlayerGetOtherMusicChannel(%musicplayer, %musicchannel).AudioHandle))
    {
        alxUnpause(VMPlayerGetOtherMusicChannel(%musicplayer, %musicchannel).AudioHandle);
    }
}

function VMPlayerScheduleMusicCrossFadeIn(%musicplayer, %volume_delta, %musicchannel)
{
    if (!VMPlayerDoMusicFade(%musicplayer, %volume_delta, %musicchannel))
    {
        %musicplayer.CrossFadeInHandle = schedule($VMPlayerFadeIntervalMS, 0, VMPlayerScheduleMusicCrossFadeIn, %musicplayer, %volume_delta, %musicchannel);
        return;
    }

    alxSetChannelVolume(%musicchannel.AudioChannel, %musicplayer.CurrentVolume);
    %musicplayer.CrossFadeInHandle = 0;
}

function VMPlayerCrossFadeOut(%musicplayer)
{
    VMPlayerDeleteTrackInfo(%musicplayer, %musicplayer.CurrentSong);
    %musicplayer.CurrentSong = 0;
    %curr_channel = %musicplayer.CurrentMusicChannel;
    %other_channel = VMPlayerGetOtherMusicChannel(%musicplayer, %musicplayer.CurrentMusicChannel);
    %curr_volume = alxGetChannelVolume(%curr_channel.AudioChannel);
    %other_volume = alxGetChannelVolume(%other_channel.AudioChannel);

    if (%other_channel.AudioHandle != 0)
    {
        if (%other_volume > %curr_volume)
        {
            cancel(%musicplayer.CheckSongPosHandle);
            cancel(%musicplayer.NextTrackHandle);
            VMPlayerStopChannel(%musicplayer, %curr_channel);
            VMPlayerSwitchAudioChannels(%musicplayer);
            %curr_volume = %other_volume;
        }
        else
        {
            VMPlayerStopChannel(%musicplayer, %other_channel);
        }
    }

    cancel(%musicplayer.CheckSongPosHandle);
    cancel(%musicplayer.NextTrackHandle);
    cancel(%musicplayer.FadeInHandle);
    cancel(%musicplayer.FadeOutHandle);
    cancel(%musicplayer.CrossFadeOutHandle);
    cancel(%musicplayer.CrossFadeInHandle);
    %musicplayer.CheckSongPosHandle = 0;
    %musicplayer.NextTrackHandle = 0;
    %musicplayer.FadeInHandle = 0;
    %musicplayer.FadeOutHandle = 0;
    %musicplayer.CrossFadeOutHandle = 0;
    %musicplayer.CrossFadeInHandle = 0;
    %volume_delta = (%curr_volume / ((%musicplayer.FadeDuration * 1000) / $VMPlayerFadeIntervalMS)) * -1;
    VMPlayerScheduleMusicCrossFadeOut(%musicplayer, %volume_delta, %musicplayer.CurrentMusicChannel);
    VMPlayerSwitchAudioChannels(%musicplayer);
}

function VMPlayerScheduleMusicCrossFadeOut(%musicplayer, %volume_delta, %musicchannel)
{
    if (!VMPlayerDoMusicFade(%musicplayer, %volume_delta, %musicchannel))
    {
        %musicplayer.CrossFadeOutHandle = schedule($VMPlayerFadeIntervalMS, 0, VMPlayerScheduleMusicCrossFadeOut, %musicplayer, %volume_delta, %musicchannel);
        return;
    }

    if (%musicchannel.AudioHandle != 0)
    {
        alxStop(%musicchannel.AudioHandle);
    }

    %musicchannel.AudioHandle = 0;
    alxSetChannelVolume(%musicchannel.AudioChannel, %musicplayer.CurrentVolume);
    %musicplayer.CrossFadeOutHandle = 0;
}

function VMPlayerDoMusicFade(%musicplayer, %volume_delta, %musicchannel)
{
    if (!alxIsPlaying(%musicchannel.AudioHandle) && !alxIsPaused(%musicchannel.AudioHandle))
    {
        alxSetChannelVolume(%musicchannel.AudioChannel, %musicplayer.CurrentVolume);
        return 1;
    }

    if (alxIsPlaying(%musicchannel.AudioHandle))
    {
        %fade_volume = alxGetChannelVolume(%musicchannel.AudioChannel);

        if (%volume_delta < 0.0 && %fade_volume + %volume_delta <= 0)
        {
            return 1;
        }
        else
        {
            if (%volume_delta > 0.0 && %fade_volume + %volume_delta >= %musicplayer.CurrentVolume)
            {
                return 1;
            }
        }

        alxSetChannelVolume(%musicchannel.AudioChannel, %fade_volume + %volume_delta);
    }

    return 0;
}

function VMPlayerPrintDebugMessage(%musicplayer, %msg)
{
    if (%musicplayer.debug == 1)
    {
        echo("VMPlayer: " @ %msg);
    }
}

function VMPArray::getCount(%this)
{
    if (%this.count $= "")
    {
        %this.count = 0;
    }
    return %this.count;
}

function VMPArray::InsertAt(%this, %index, %value)
{
    if ((%index < 0) || (%index > %this.getCount()))
    {
        return;
    }

    for (%x=%this.getCount(); %x > %index; %x--)
    {
        %this.Content[%x] = %this.Content[%x - 1];
    }

    %this.Content[%index] = %value;
    %this.count++;
}

function VMPArray::InsertBack(%this, %value)
{
    %index = %this.getCount();
    %this.Content[%index] = %value;
    %this.count++;
}

function VMPArray::RemoveAt(%this, %index)
{
    if ((%index < 0) || (%index >= %this.getCount()))
    {
        return;
    }

    for (%x=%index; %x < %this.getCount(); %x++)
    {
        if ((%x + 1) < %this.getCount())
        {
            %this.Content[%x] = %this.Content[%x + 1];
        }
    }

    %this.Content[%this.count - 1] = "";
    %this.count--;
}

function VMPArray::RemoveBack(%this)
{
    if (%this.getCount() > 0)
    {
        %this.count--;
        %this.Content[%this.count] = "";
    }
}

function VMPArray::RemoveAll(%this)
{
    for (%x=0; %x < %this.getCount(); %x++)
    {
        %this.Content[%x] = "";
    }

    %this.count = 0;
}

function VMPArray::GetAt(%this, %index)
{
    if ((%index >= 0) && (%index < %this.getCount()))
    {
        return %this.Content[%index];
    }

    return "";
}

function VMPArray::IsEmpty(%this)
{
    return %this.getCount() == 0;
}

function VMPArray::find(%this, %value)
{
    for (%x=0; %x < %this.getCount(); %x++)
    {
        if (%this.Content[%x] == %value)
        {
            return %x;
        }
    }

    return -1;
}

function VMPArray::findString(%this, %value)
{
    for (%x=0; %x < %this.getCount(); %x++)
    {
        if (%this.Content[%x] $= %value)
        {
            return %x;
        }
    }
    return -1;
}

function VMPArray::Swap(%this, %index1, %index2)
{
    if (%index1 == %index2)
    {
        return;
    }
    
    if ((((%index1 < 0) || (%index2 < 0)) || (%index1 >= %this.getCount())) || (%index2 >= %this.getCount()))
    {
        return;
    }

    %tmp = %this.Content[%index2];
    %this.Content[%index2] = %this.Content[%index1];
    %this.Content[%index1] = %tmp;
}
