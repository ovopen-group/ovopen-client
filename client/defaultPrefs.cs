
package OnverseClient
{
    function setDefaults()
    {
        $pref::defaultsNum = 7;
        $pref::MasterServer::default_address = "onverseworld.onverse.com";
        $pref::MasterServer::default_port = 65355;
        $pref::MasterServer::address = "onverseworld.onverse.com";
        $pref::MasterServer::port = $pref::MasterServer::default_port;
        $pref::I18N::charset = ANSI;
        $pref::Permissions::Mounts = 0;
        $pref::Player::Name = "";
        $pref::Player::defaultFov = 90;
        $pref::Player::zoomSpeed = 0;
        $Pref::Net::LagThreshold = 800;
        $pref::HudMessageLogSize = 40;
        $pref::ChatHudLength = 1;
        $pref::Input::LinkMouseSensitivity = 0.75;
        $pref::Input::MouseEnabled = 0;
        $pref::Input::JoystickEnabled = 0;
        $pref::Input::KeyboardTurnSpeed = 0.1;
        $Pref::Input::KeyboardTurnSpeedCoeff = 0.2;
        $pref::sceneLighting::cacheSize = 20000;
        $pref::sceneLighting::purgeMethod = "lastCreated";
        $pref::sceneLighting::cacheLighting = 1;
        $pref::sceneLighting::terrainGenerateLevel = 1;
        $pref::Video::displayDevice = "OpenGL";
        $pref::Video::allowOpenGL = 1;
        $pref::Video::allowD3D = 1;
        $pref::Video::preferOpenGL = 1;
        $pref::Video::appliedPref = 0;
        $pref::Video::disableVerticalSync = 1;
        $pref::Video::monitorNum = 0;
        $pref::Video::windowedRes = "800 600";
        $pref::Video::screenShotFormat = "PNG";
        $pref::OpenGL::force16BitTexture = "0";
        $pref::OpenGL::forcePalettedTexture = "0";
        $pref::OpenGL::maxHardwareLights = 3;
        $pref::visibleDistanceMod = 1.0;
        $pref::Audio::driver = "OpenAL";
        $pref::Audio::forceMaxDistanceUpdate = 0;
        $pref::Audio::environmentEnabled = 0;
        $pref::Audio::masterVolume = 0.8;
        $pref::Audio::channelVolume1 = 0.8;
        $pref::Audio::channelVolume2 = 0.8;
        $pref::Audio::channelVolume3 = 0.8;
        $pref::Audio::channelVolume4 = 0.8;
        $pref::Audio::channelVolume5 = 0.8;
        $pref::Audio::channelVolume6 = 0.8;
        $pref::Audio::channelVolume7 = 0.8;
        $pref::Audio::channelVolume8 = 0.8;
        $pref::Video::quality = 2;
        $pref::visibleDistanceMod = 1;
        $pref::TS::detailAdjust = 1;
        $pref::OpenGL::mipReduction = 0;
        $pref::OpenGL::interiorMipReduction = 0;
        $pref::OpenGL::skyMipReduction = 0;
        $pref::OpenGL::compressionHint = GL_NICEST;
        $pref::OpenGL::allowCompression = 0;
        $pref::OpenGL::multisample = 0;
        $pref::OpenGL::textureAnisotropy = 0;
        $pref::OpenGL::textureTrilinear = 1;
        $pref::Interior::DynamicLights = 1;
        $pref::Interior::detailAdjust = 1;
        $pref::Interior::TexturedFog = 1;
        $pref::Terrain::texDetail = 0;
        $pref::Terrain::enableDetails = 1;
        $pref::Terrain::dynamicLights = 1;
        $pref::decalsOn = 1;
        $pref::Decal::maxNumDecals = 128;
        $pref::Decal::decalTimeout = 5000;
        $pref::Shadows = 0.5;
        $pref::LightManager::sgUseDynamicShadows = 0;
        $pref::LightManager::sgDynamicShadowQuality = 2;
        $pref::LightManager::sgDynamicLightingOcclusionQuality = 2;
        $pref::environmentMaps = 1;
        $pref::chatColors[defaul,font] = "FFFFFF";
        $pref::chatColors[defaul,title] = "FFFFFF";
        $pref::chatColors[cmdret,font] = $pref::chatColors[defaul,font];
        $pref::chatColors[cmdret,title] = "FFFF00";
        $pref::chatColors[cmderror,font] = $pref::chatColors[defaul,font];
        $pref::chatColors[cmderror,title] = "FF0000";
        $pref::chatColors[cmdmatch,font] = $pref::chatColors[defaul,font];
        $pref::chatColors[cmdmatch,title] = $pref::chatColors[defaul,title];
        $pref::chatColors[local,font] = $pref::chatColors[defaul,font];
        $pref::chatColors[local,title] = $pref::chatColors[defaul,title];
        $pref::chatColors[shout,font] = $pref::chatColors[defaul,font];
        $pref::chatColors[shout,title] = "FF8080";
        $pref::chatColors[pm,font] = $pref::chatColors[defaul,font];
        $pref::chatColors[pm,title] = "80ABFF";
        $pref::chatColors[fm,font] = $pref::chatColors[defaul,font];
        $pref::chatColors[fm,title] = "80FF80";
        $pref::chatColors[gm,font] = $pref::chatColors[defaul,font];
        $pref::chatColors[gm,title] = "FFBE80";
        $pref::chatColors[ugm,font] = "FFBE80";
        $pref::chatColors[ugm,title] = "FFBE80";
        $pref::chatColors[sntlocal,font] = $pref::chatColors[local,font];
        $pref::chatColors[sntlocal,title] = $pref::chatColors[local,title];
        $pref::chatColors[sntshout,font] = $pref::chatColors[shout,font];
        $pref::chatColors[sntshout,title] = $pref::chatColors[shout,title];
        $pref::chatColors[sntpm,font] = $pref::chatColors[pm,font];
        $pref::chatColors[sntpm,title] = $pref::chatColors[pm,title];
        $pref::chatColors[sntfm,font] = $pref::chatColors[fm,font];
        $pref::chatColors[sntfm,title] = $pref::chatColors[fm,title];
        $pref::chatColors[sntgm,font] = $pref::chatColors[gm,font];
        $pref::chatColors[sntgm,title] = $pref::chatColors[gm,title];
        $pref::chatColors[sntugm,font] = $pref::chatColors[ugm,font];
        $pref::chatColors[sntugm,title] = $pref::chatColors[ugm,title];
        $pref::chatColors[fndon,font] = $pref::chatColors[defaul,font];
        $pref::chatColors[fndon,title] = "80FF80";
        $pref::chatColors[fndoff,font] = $pref::chatColors[defaul,font];
        $pref::chatColors[fndoff,title] = "80FF80";
        $pref::chatColors[fndacc,font] = $pref::chatColors[defaul,font];
        $pref::chatColors[fndacc,title] = "80FF80";
        $pref::chatColors[svrmsg,font] = $pref::chatColors[defaul,font];
        $pref::chatColors[svrmsg,title] = "FFFF00";
        $pref::chatColors[announ,font] = "FFFF00";
        $pref::chatColors[announ,title] = "FFFF00";
        $pref::chatColors[action,font] = $pref::chatColors[defaul,font];
        $pref::chatColors[action,title] = "FFFF00";
        $pref::chatColors[game,font] = $pref::chatColors[defaul,font];
        $pref::chatColors[game,title] = "D280FF";
    }

    function savePreferences()
    {
        if (!$journal::Reading)
        {
            echo("Exporting client preferences");
            export("$pref::*", "cfg/clientprefs.cfg", False);
        }
        EquipBar.save();
    }

    function loadPreferences()
    {
        echo("Loading client preferences");
        %file = new FileObject() {
        };

        %open = 0;
        if ($journal::Reading)
        {
            echo("Attempting to open journal\'s matching client config file.");
            echo($journal::File);
            %open = %file.openForRead($journal::File @ "_clientprefs.cfg");
            if (!%open)
            {
                echo("Using default client config file.");
                %open = %file.openForRead("cfg/clientprefs.cfg");
            }
        }
        else
        {
            %open = %file.openForRead("cfg/clientprefs.cfg");
        }

        %infoObject = "";
        if (%open)
        {
            %inInfoBlock = 0;
            while (!%file.isEOF())
            {
                %line = %file.readLine();
                %line = trim(%line);
                %infoObject = %infoObject @ %line @ " ";
            }
            %file.close();
        }
        
        eval(%infoObject);
        %file.delete();
    }

    function setModuleDefaults()
    {
    }

    function saveModulePreferences()
    {
    }

    function loadModulePreferences()
    {
    }

};


