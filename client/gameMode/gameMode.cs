
function gameMode::includeFiles()
{
    exec("./ui/profiles.cs");
    exec("./ui/inGameUI.cs");
    exec("./ui/chatHud.cs");
    exec("./ui/friendList.cs");
    exec("./ui/animationDlg.cs");
    exec("./ui/windowBar.cs");
    exec("./ui/relightStatus.cs");
    exec("./ui/inventory.cs");
    exec("./ui/inventoryManager.cs");
    exec("./ui/equipBar.cs");
    exec("./ui/fadingText.cs");
    exec("./triggers/triggers.cs");
    exec("./gameBase/gameBase.cs");
    exec("./gameBase/mounting.cs");
    exec("./gameBase/player.cs");
    exec("./gameBase/sceneObject.cs");
    exec("./homeArea/homePoint.cs");
    exec("./gameArea/include.cs");
    exec("./interactiveObjects/include.cs");
    exec("./NPCs/pets.cs");
    exec("./NPCs/npcs.cs");
    exec("./storeArea/purchaseClothingObject.cs");
    exec("./storeArea/purchaseInteractiveObject.cs");
    exec("./storeArea/purchaseMountablePlayerObject.cs");
    exec("./storeArea/purchasePetObject.cs");
    exec("./storeArea/purchaseToolObject.cs");
    exec("./storeArea/purchaseWheeledVehicleObject.cs");
    exec("./transactions/purchase.cs");
    exec("./transactions/inspect.cs");
    exec("./furniturePlacement/furniturePlacement.cs");
    exec("./gameMode.bind.cs");
}

function gameMode::begin()
{
    if (Canvas.getFirstResponder() == chatInput.getId())
    {
        %overrideFirst = 1;
        %blankFirst = chatInput.getText() $= "";
    }

    setCurrentInGameUI(inGameUI, 1);
    gameModeActionMap.push();

    if (WindowBar.isAwake())
    {
        windowBarActionMap.push();
    }

    $mvMounted = 0;
    driveModeActionMap.pop();
    moveforward(0);
    inGameUI.setFirstResponder();

    if ($isInSaveMode == 1)
    {
        echo("World is in the middle of a save, joining wait.");
        Canvas.pushDialog(saveBuilderDlg);
    }

    if (%overrideFirst)
    {
        chatInput.forceFirstResponder();
        if (%blankFirst)
        {
            chatInput.setText("");
        }
    }
}

function gameMode::end()
{
    gameModeActionMap.pop();
    driveModeActionMap.pop();
}
