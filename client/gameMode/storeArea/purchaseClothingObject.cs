
function PurchaseClothingObject::onRightClick(%this)
{
    if (!%this.verifyPlayerInRange())
    {
        error("Cannot interact with object. Too far away.");
        return;
    }

    PurchaseClothingObjectContext.construct();
    PurchaseClothingObjectContext.var[0] = %this;
    PurchaseClothingObjectContext.showContextMenu("default");
}

function PurchaseClothingObject::doDefaultAction(%this)
{
    if (!%this.verifyPlayerInRange())
    {
        error("Cannot interact with object. Too far away.");
        return;
    }

    PurchaseClothingObjectContext.construct();
    PurchaseClothingObjectContext.var[0] = %this;
    PurchaseClothingObjectContext.doDefault();
}

function PurchaseClothingObject::onPurchase(%this)
{
    PurchaseItem(%this);
}

function PurchaseClothingObject::info(%this)
{
    InspectItem(%this);
}

new GuiContextMenu(PurchaseClothingObjectContext) {
   Profile = "GuiMenuContextProfile";
   position = "0 0";
   Extent = "640 480";
};

function PurchaseClothingObjectContext::construct(%this)
{
    if (%this.constructed)
    {
        return;
    }

    %menuId = 0;
    %itemId = 0;
    %this.addMenu("default", %menuId++);
    %this.addMenuItem("default", "Purchase", %itemId++, "$tempVar[0].onPurchase();");
    %this.addMenuItem("default", "Info", %itemId++, "$tempVar[0].info();");
    %this.constructed = 1;
}

function PurchaseClothingObjectContext::doDefault(%this)
{
    Parent::doDefault(%this, "default");
}

function PurchaseClothingObject::getCurrentDatablock(%this)
{
    %clothing[Male] = %this.maleClothing;
    %clothing[Female] = %this.femaleClothing;

    if (%clothing[Male] && %clothing[Female])
    {
        return %clothing[$currentPlayerSex];
    }
    else
    {
        if (%clothing[Female])
        {
            return %clothing[Female];
        }
    }
    return %clothing[Male];
}

function PurchaseClothingObject::logInteraction(%this)
{
    return %this.getCurrentDatablock().logInteraction(%this);
}

function PurchaseClothingObject::GetDisplayName(%this, %colors)
{
    return %this.getCurrentDatablock().GetDisplayName(%colors, %this);
}

function PurchaseClothingObject::GetDescription(%this)
{
    return %this.getCurrentDatablock().GetDescription(%this);
}

function PurchaseClothingObject::GetStats(%this)
{
    %stats = %this.getCurrentDatablock().GetStats(%this);

    if (%this.discount != 1)
    {
        %stats = %stats @ "<spush><color:FFFF00>" @ mFloor(((1 - %this.discount) * 100) + 0.5) @ "% Discount<spop><br>";
    }

    return %stats;
}

function PurchaseClothingObject::GetCCValue(%this)
{
    %CC = %this.getCurrentDatablock().GetCCValue(%this);

    if (((%CC !$= "")) && (%this.discount != 1))
    {
        %CC = mFloor((%CC * %this.discount) + 0.5);
    }

    return %CC;
}

function PurchaseClothingObject::GetCCValueVIP(%this)
{
    if (%this.discount != 1)
    {
        return %this.GetCCValue();
    }
    else
    {
        return %this.getCurrentDatablock().GetCCValueVIP(%this);
    }
}

function PurchaseClothingObject::GetAdjCCValue(%this)
{
    return %this.getCurrentDatablock().GetAdjCCValue(%this);
}

function PurchaseClothingObject::GetPPValue(%this)
{
    %PP = %this.getCurrentDatablock().GetPPValue(%this);
    if (((%PP !$= "")) && (%this.discount != 1))
    {
        %PP = mFloor((%PP * %this.discount) + 0.5);
    }
    return %PP;
}

function PurchaseClothingObject::GetPPValueVIP(%this)
{
    if (%this.discount != 1)
    {
        return %this.GetPPValue();
    }
    else
    {
        return %this.getCurrentDatablock().GetPPValueVIP(%this);
    }
}

function PurchaseClothingObject::GetAdjPPValue(%this)
{
    return %this.getCurrentDatablock().GetAdjPPValue(%this);
}

function PurchaseClothingObject::GetDepreciation(%this)
{
    return %this.getCurrentDatablock().GetDepreciation(%this);
}

function PurchaseClothingObject::GetVIPDiscount(%this)
{
    return %this.getCurrentDatablock().GetVIPDiscount(%this);
}

function PurchaseClothingObject::GetPreview(%this, %objView, %bitView)
{
    return %this.getCurrentDatablock().GetPreview(%objView, %bitView, %this);
}

function ClothingData::logInteraction(%this)
{
    commandToServer('UpInter', %this.getId());
}

function ClothingData::GetDisplayName(%this, %colors, %obj)
{
    if (!(%colors $= ""))
    {
        %color = getWord(%colors, %this.tier);
        if (%color $= "")
        {
            %color = getWord(%colors, 0);
        }
        return "<spush><color:" @ %color @ ">" @ %this.displayName @ "<spop>";
    }
    return %this.displayName;
}

function ClothingData::GetDescription(%this, %obj)
{
    return ParseDescriptionText(%this.desc);
}

function ClothingData::GetStats(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    %CC = %obj.GetCCValue();
    %PP = %obj.GetPPValue();
    %CCVip = %obj.GetCCValueVIP();
    %PPVip = %obj.GetPPValueVIP();

    if (%CC)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/cc.png>" SPC %CC;
        if (%CC != %CCVip)
        {
            %stats = %stats @ " (" @ %CCVip @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)";
        }
        %stats = %stats @ "<br>";
    }

    if (%PP)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/pp.png>" SPC %PP;
        if (%PP != %PPVip)
        {
            %stats = %stats @ " (" @ %PPVip @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)";
        }
        %stats = %stats @ "<br>";
    }

    if (%this.dualSex)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/male.png><bitmap:onverse/data/live_assets/engine/ui/images/icons/female.png><br>";
    }
    else
    {
        if (%this.sex $= "Male")
        {
            %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/male.png><bitmap:onverse/data/live_assets/engine/ui/images/icons/female_off.png>";
        }
        else
        {
            %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/male_off.png><bitmap:onverse/data/live_assets/engine/ui/images/icons/female.png>";
        }
        %text = "For " @ %this.sex @ " avatars only.";
        if (!(%this.sex $= $currentPlayerSex))
        {
            %stats = %stats @ "<spush><color:FF0000>" @ %text @ "<spop>" @ "<br>";
        }
        else
        {
            %stats = %stats @ %text @ "<br>";
        }
    }
    return %stats;
}

function ClothingData::GetCCValue(%this, %obj)
{
    return %this.CC;
}

function ClothingData::GetCCValueVIP(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    return mFloor(%obj.GetCCValue() * %obj.GetVIPDiscount());
}

function ClothingData::GetAdjCCValue(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    if (LocationGameConnection.getIsSubscriber())
    {
        return %obj.GetCCValueVIP();
    }
    else
    {
        return %obj.GetCCValue();
    }
}

function ClothingData::GetPPValue(%this, %obj)
{
    return %this.PP;
}

function ClothingData::GetPPValueVIP(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    return mFloor(%obj.GetPPValue() * %obj.GetVIPDiscount());
}

function ClothingData::GetAdjPPValue(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }
    
    if (LocationGameConnection.getIsSubscriber())
    {
        return %obj.GetPPValueVIP();
    }
    else
    {
        return %obj.GetPPValue();
    }
}

function ClothingData::GetDepreciation(%this, %obj)
{
    if (%this.tier == 1)
    {
        return 0.5;
    }
    else
    {
        if (%this.tier == 2)
        {
            return 0.5;
        }
        else
        {
            if (%this.tier == 3)
            {
                return 0.5;
            }
            else
            {
                if (%this.tier == 4)
                {
                    return 0.5;
                }
            }
        }
    }
    return 0;
}

function ClothingData::GetVIPDiscount(%this, %obj)
{
    return 0.80000001;
}

function ClothingData::GetPreview(%this, %objView, %bitView, %obj)
{
    %bitView.setBitmap(%this.iconFile);
}
