
function PurchaseInteractiveObject::constructContextMenu(%this, %datablock)
{
    %datablock.addContextItem(%this, "Purchase", "$tempVar[1].onPurchase($tempVar[0]);");
    %datablock.addContextItem(%this, "-", "");
}

function PurchaseInteractiveObject::onPurchase(%this, %datablock)
{
    PurchaseItem(%this);
}

function PurchaseInteractiveObject::GetCCValue(%this)
{
    %CC = %this.getDataBlock().GetCCValue(%this);
    if (((%CC !$= "")) && (%this.discount != 1))
    {
        %CC = mFloor((%CC * %this.discount) + 0.5);
    }
    return %CC;
}

function PurchaseInteractiveObject::GetPPValue(%this)
{
    %PP = %this.getDataBlock().GetPPValue(%this);
    if (((%PP !$= "")) && (%this.discount != 1))
    {
        %PP = mFloor((%PP * %this.discount) + 0.5);
    }
    return %PP;
}

function PurchaseInteractiveObject::GetCCValueVIP(%this)
{
    if (%this.discount != 1)
    {
        return %this.GetCCValue();
    }
    else
    {
        return %this.getDataBlock().GetCCValueVIP(%this);
    }
}

function PurchaseInteractiveObject::GetPPValueVIP(%this)
{
    if (%this.discount != 1)
    {
        return %this.GetPPValue();
    }
    else
    {
        return %this.getDataBlock().GetPPValueVIP(%this);
    }
}

function PurchaseInteractiveObject::GetStats(%this)
{
    %stats = %this.getDataBlock().GetStats(%this);
    if (%this.discount != 1)
    {
        %stats = %stats @ "<spush><color:FFFF00>" @ mFloor(((1 - %this.discount) * 100) + 0.5) @ "% Discount<spop><br>";
    }
    return %stats;
}
