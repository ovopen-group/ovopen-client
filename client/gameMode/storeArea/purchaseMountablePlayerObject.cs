
function PurchaseMountablePlayer::constructContextMenu(%this, %datablock)
{
    %datablock.addContextItem(%this, "Purchase", "$tempVar[1].onPurchase($tempVar[0]);");
    %datablock.addContextItem(%this, "-", "");
}

function PurchaseMountablePlayer::onPurchase(%this, %datablock)
{
    PurchaseItem(%this);
}

function MountablePlayerData::showContextMenu(%datablock, %obj)
{
    if (%obj.getClassName() $= "PurchaseMountablePlayer")
    {
        GameBaseData::showContextMenu(%datablock, %obj);
    }
    else
    {
        PlayerData::showContextMenu(%datablock, %obj);
    }
}

function MountablePlayerData::doDefaultAction(%datablock, %obj, %menuText)
{
    if (%obj.getClassName() $= "PurchaseMountablePlayer")
    {
        GameBaseData::doDefaultAction(%datablock, %obj, %menuText);
    }
}

function MountablePlayerData::constructContextMenu(%datablock, %obj)
{
    if (%obj.getClassName() $= "PurchaseMountablePlayer")
    {
        %obj.constructContextMenu(%datablock);
    }
    %menu = %datablock.getContextMenu(%obj);
    %menu.addContextItem("Info", "$tempVar[0].info($tempVar[1]);");
}

function MountablePlayerData::info(%datablock, %obj)
{
    InspectItem(%obj);
}

function MountablePlayerData::GetDisplayName(%this, %colors)
{
    if (!(%colors $= ""))
    {
        %color = getWord(%colors, %this.tier);
        if (%color $= "")
        {
            %color = getWord(%colors, 0);
        }
        return "<spush><color:" @ %color @ ">" @ %this.displayName @ "<spop>";
    }
    return %this.displayName;
}

function MountablePlayerData::GetDescription(%this)
{
    return ParseDescriptionText(%this.desc);
}

function MountablePlayerData::GetStats(%this, %obj)
{
    if (!(%name $= ""))
    {
        %stats = "Name:" SPC %name @ "<br>";
    }
    
    if (%obj $= "")
    {
        %obj = %this;
    }

    %CC = %obj.GetCCValue();
    %PP = %obj.GetPPValue();
    %CCVip = %obj.GetCCValueVIP();
    %PPVip = %obj.GetPPValueVIP();

    if (%CC)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/cc.png>" SPC %CC;
        if (%CC != %CCVip)
        {
            %stats = %stats @ " (" @ %CCVip @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)";
        }
        %stats = %stats @ "<br>";
    }

    if (%PP)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/pp.png>" SPC %PP;
        if (%PP != %PPVip)
        {
            %stats = %stats @ " (" @ %PPVip @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)";
        }
        %stats = %stats @ "<br>";
    }

    %stats = %stats @ %this.getFeatureList("");
    return %stats;
}

function MountablePlayerData::getFeatureList(%this, %currentList)
{
    %speedBoost = ((%this.maxForwardSpeed / 6) - 1) * 100;
    %speedBoost = mFloor(%speedBoost + 0.5);
    %currentList = %currentList @ %speedBoost @ "% Speed Boost<br>";
    if (%this.getNumMounts() > 1)
    {
        %currentList = %currentList @ "Sit points:" SPC %this.getNumMounts() - 1 @ "<br>";
    }
    return %currentList;
}

function MountablePlayerData::GetCCValue(%this)
{
    return %this.CC;
}

function MountablePlayerData::GetCCValueVIP(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }
    return mFloor(%obj.GetCCValue() * %obj.GetVIPDiscount());
}

function MountablePlayerData::GetAdjCCValue(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }
    if (LocationGameConnection.getIsSubscriber())
    {
        return %obj.GetCCValueVIP();
    }
    else
    {
        return %obj.GetCCValue();
    }
}

function MountablePlayerData::GetPPValue(%this)
{
    return %this.PP;
}

function MountablePlayerData::GetPPValueVIP(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }
    return mFloor(%obj.GetPPValue() * %obj.GetVIPDiscount());
}

function MountablePlayerData::GetAdjPPValue(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }
    if (LocationGameConnection.getIsSubscriber())
    {
        return %obj.GetPPValueVIP();
    }
    else
    {
        return %obj.GetPPValue();
    }
}

function MountablePlayerData::GetDepreciation(%this)
{
    if (%this.tier == 1)
    {
        return 0.5;
    }
    else
    {
        if (%this.tier == 2)
        {
            return 0.5;
        }
        else
        {
            if (%this.tier == 3)
            {
                return 0.5;
            }
            else
            {
                if (%this.tier == 4)
                {
                    return 0.5;
                }
            }
        }
    }
    return 0;
}

function MountablePlayerData::GetVIPDiscount(%this, %obj)
{
    return 0.80000001;
}

function MountablePlayerData::GetPreview(%this, %objView, %bitView)
{
    %objView.setObject("obj", %this.shapeFile, %this.textureFile, "");
}

function PurchaseMountablePlayer::GetDisplayName(%this, %colors)
{
    return %this.getDataBlock().GetDisplayName(%colors);
}

function PurchaseMountablePlayer::GetDescription(%this)
{
    return %this.getDataBlock().GetDescription();
}

function PurchaseMountablePlayer::GetStats(%this)
{
    %stats = %this.getDataBlock().GetStats(%this);
    if (%this.discount != 1)
    {
        %stats = %stats @ "<spush><color:FFFF00>" @ mFloor(((1 - %this.discount) * 100) + 0.5) @ "% Discount<spop><br>";
    }
    return %stats;
}

function PurchaseMountablePlayer::GetCCValue(%this)
{
    %CC = %this.getDataBlock().GetCCValue(%this);
    if (((%CC !$= "")) && (%this.discount != 1))
    {
        %CC = mFloor((%CC * %this.discount) + 0.5);
    }
    return %CC;
}

function PurchaseMountablePlayer::GetCCValueVIP(%this)
{
    if (%this.discount != 1)
    {
        return %this.GetCCValue();
    }
    else
    {
        return %this.getDataBlock().GetCCValueVIP(%this);
    }
}

function PurchaseMountablePlayer::GetAdjCCValue(%this)
{
    return %this.getDataBlock().GetAdjCCValue(%this);
}

function PurchaseMountablePlayer::GetPPValue(%this)
{
    %PP = %this.getDataBlock().GetPPValue(%this);
    if (((%PP !$= "")) && (%this.discount != 1))
    {
        %PP = mFloor((%PP * %this.discount) + 0.5);
    }
    return %PP;
}

function PurchaseMountablePlayer::GetPPValueVIP(%this)
{
    if (%this.discount != 1)
    {
        return %this.GetPPValue();
    }
    else
    {
        return %this.getDataBlock().GetPPValueVIP(%this);
    }
}

function PurchaseMountablePlayer::GetAdjPPValue(%this)
{
    return %this.getDataBlock().GetAdjPPValue(%this);
}

function PurchaseMountablePlayer::GetDepreciation(%this)
{
    return %this.getDataBlock().GetDepreciation();
}

function PurchaseMountablePlayer::GetVIPDiscount(%this)
{
    return %this.getDataBlock().GetVIPDiscount(%this);
}

function PurchaseMountablePlayer::GetPreview(%this, %objView, %bitView)
{
    return %this.getDataBlock().GetPreview(%objView, %bitView);
}
