function PurchaseToolObject::onRightClick(%this)
{
    if (%this.getMenu())
    {
        PurchaseToolObjectContext.showContextMenu("default");
    }
}

function PurchaseToolObject::doDefaultAction(%this)
{
    if (%this.getMenu())
    {
        PurchaseToolObjectContext.doDefault();
    }
}

function PurchaseToolObject::getMenu(%this)
{
    if (!%this.verifyPlayerInRange())
    {
        error("Cannot interact with object. Too far away.");
        return 0;
    }

    PurchaseToolObjectContext.construct();
    PurchaseToolObjectContext.var[0] = %this;
    return 1;
}

function PurchaseToolObject::onPurchase(%this)
{
    PurchaseItem(%this);
}

function PurchaseToolObject::info(%this)
{
    InspectItem(%this);
}

function PurchaseToolObject::GetDisplayName(%this, %colors)
{
    return %this.toolDatablock.GetDisplayName(%colors);
}

function PurchaseToolObject::GetDescription(%this)
{
    return %this.toolDatablock.GetDescription();
}

function PurchaseToolObject::GetStats(%this)
{
    %stats = %this.toolDatablock.GetStats(%this);

    if (%this.discount != 1)
    {
        %stats = %stats @ "<spush><color:FFFF00>" @ mFloor(((1 - %this.discount) * 100) + 0.5) @ "% Discount<spop><br>";
    }

    return %stats;
}

function PurchaseToolObject::GetCCValue(%this)
{
    %CC = %this.toolDatablock.GetCCValue(%this);

    if (((%CC !$= "")) && (%this.discount != 1))
    {
        %CC = mFloor((%CC * %this.discount) + 0.5);
    }

    return %CC;
}

function PurchaseToolObject::GetCCValueVIP(%this)
{
    if (%this.discount != 1)
    {
        return %this.GetCCValue();
    }
    else
    {
        return %this.toolDatablock.GetCCValueVIP(%this);
    }
}

function PurchaseToolObject::GetAdjCCValue(%this)
{
    return %this.toolDatablock.GetAdjCCValue(%this);
}

function PurchaseToolObject::GetPPValue(%this)
{
    %PP = %this.toolDatablock.GetPPValue(%this);

    if (((%PP !$= "")) && (%this.discount != 1))
    {
        %PP = mFloor((%PP * %this.discount) + 0.5);
    }

    return %PP;
}

function PurchaseToolObject::GetPPValueVIP(%this)
{
    if (%this.discount != 1)
    {
        return %this.GetPPValue();
    }
    else
    {
        return %this.toolDatablock.GetPPValueVIP(%this);
    }
}

function PurchaseToolObject::GetAdjPPValue(%this)
{
    return %this.toolDatablock.GetAdjPPValue(%this);
}

function PurchaseToolObject::GetDepreciation(%this)
{
    return %this.toolDatablock.GetDepreciation();
}

function PurchaseToolObject::GetVIPDiscount(%this)
{
    return %this.toolDatablock.GetVIPDiscount(%this);
}

function PurchaseToolObject::GetPreview(%this, %objView, %bitView)
{
    return %this.toolDatablock.GetPreview(%objView, %bitView);
}

new GuiContextMenu(PurchaseToolObjectContext) {
   Profile = "GuiMenuContextProfile";
   position = "0 0";
   Extent = "640 480";
};

function PurchaseToolObjectContext::construct(%this)
{
    if (%this.constructed)
    {
        return;
    }

    %menuId = 0;
    %itemId = 0;
    %this.addMenu("default", %menuId++);
    %this.addMenuItem("default", "Purchase", %itemId++, "$tempVar[0].onPurchase();");
    %this.addMenuItem("default", "Info", %itemId++, "$tempVar[0].info();");
    %this.constructed = 1;
}

function PurchaseToolObjectContext::doDefault(%this)
{
    Parent::doDefault(%this, "default");
}

function ToolImageData::GetDisplayName(%this, %colors, %obj)
{
    if (!(%colors $= ""))
    {
        %color = getWord(%colors, %this.tier);
        if (%color $= "")
        {
            %color = getWord(%colors, 0);
        }
        return "<spush><color:" @ %color @ ">" @ %this.displayName @ "<spop>";
    }

    return %this.displayName;
}

function ToolImageData::GetDescription(%this, %obj)
{
    return ParseDescriptionText(%this.desc);
}

function ToolImageData::GetStats(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    %CC = %obj.GetCCValue();
    %PP = %obj.GetPPValue();
    %CCVip = %obj.GetCCValueVIP();
    %PPVip = %obj.GetPPValueVIP();

    if (%CC)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/cc.png>" SPC %CC;
        if (%CC != %CCVip)
        {
            %stats = %stats @ " (" @ %CCVip @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)";
        }
        %stats = %stats @ "<br>";
    }

    if (%PP)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/pp.png>" SPC %PP;
        if (%PP != %PPVip)
        {
            %stats = %stats @ " (" @ %PPVip @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)";
        }
        %stats = %stats @ "<br>";
    }

    %stats = %stats @ %this.getFeatureList();
    return %stats;
}

function ToolImageData::GetCCValue(%this, %obj)
{
    return %this.CC;
}

function ToolImageData::GetCCValueVIP(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    return mFloor(%obj.GetCCValue() * %obj.GetVIPDiscount());
}

function ToolImageData::GetAdjCCValue(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    if (LocationGameConnection.getIsSubscriber())
    {
        return %obj.GetCCValueVIP();
    }
    else
    {
        return %obj.GetCCValue();
    }
}

function ToolImageData::GetPPValue(%this, %obj)
{
    return %this.PP;
}

function ToolImageData::GetPPValueVIP(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    return mFloor(%obj.GetPPValue() * %obj.GetVIPDiscount());
}

function ToolImageData::GetAdjPPValue(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    if (LocationGameConnection.getIsSubscriber())
    {
        return %obj.GetPPValueVIP();
    }
    else
    {
        return %obj.GetPPValue();
    }
}

function ToolImageData::GetDepreciation(%this, %obj)
{
    if (%this.tier == 1)
    {
        return 0.5;
    }
    else
    {
        if (%this.tier == 2)
        {
            return 0.5;
        }
        else
        {
            if (%this.tier == 3)
            {
                return 0.5;
            }
            else
            {
                if (%this.tier == 4)
                {
                    return 0.5;
                }
            }
        }
    }
    return 0;
}

function ToolImageData::GetVIPDiscount(%this, %obj)
{
    return 0.80000001;
}

function ToolImageData::GetPreview(%this, %objView, %bitView, %obj)
{
    %shape = %this.shapeFile;
    
    if (!(%this.thumbShape $= ""))
    {
        %shape = %this.thumbShape;
    }

    %objView.setObject("obj", %shape, %this.textureFile, "");
}

