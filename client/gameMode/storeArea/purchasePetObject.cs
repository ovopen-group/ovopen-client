function PurchaseAIPet::constructContextMenu(%this, %datablock)
{
    %datablock.addContextItem(%this, "Purchase", "$tempVar[1].onPurchase($tempVar[0]);");
    %datablock.addContextItem(%this, "-", "");
}

function PurchaseAIPet::onPurchase(%this, %datablock)
{
    PurchaseItem(%this);
}

function PurchaseAIPet::GetCCValue(%this)
{
    %CC = %this.getDataBlock().GetCCValue(%this);
    if (((%CC !$= "")) && (%this.discount != 1))
    {
        %CC = mFloor((%CC * %this.discount) + 0.5);
    }
    return %CC;
}

function PurchaseAIPet::GetPPValue(%this)
{
    %PP = %this.getDataBlock().GetPPValue(%this);
    if (((%PP !$= "")) && (%this.discount != 1))
    {
        %PP = mFloor((%PP * %this.discount) + 0.5);
    }
    return %PP;
}

function PurchaseAIPet::GetCCValueVIP(%this)
{
    if (%this.discount != 1)
    {
        return %this.GetCCValue();
    }
    else
    {
        return %this.getDataBlock().GetCCValueVIP(%this);
    }
}

function PurchaseAIPet::GetPPValueVIP(%this)
{
    if (%this.discount != 1)
    {
        return %this.GetPPValue();
    }
    else
    {
        return %this.getDataBlock().GetPPValueVIP(%this);
    }
}

function PurchaseAIPet::GetStats(%this)
{
    %stats = %this.getDataBlock().GetStats(%this.getShapeName(), %this);
    if (%this.discount != 1)
    {
        %stats = %stats @ "<spush><color:FFFF00>" @ mFloor(((1 - %this.discount) * 100) + 0.5) @ "% Discount<spop><br>";
    }
    return %stats;
}

function AIPetData::GetDisplayName(%this, %colors, %obj)
{
    if (!(%colors $= ""))
    {
        %color = getWord(%colors, %this.tier);
        if (%color $= "")
        {
            %color = getWord(%colors, 0);
        }
        return "<spush><color:" @ %color @ ">" @ %this.displayName @ "<spop>";
    }
    return %this.displayName;
}

function AIPetData::GetDescription(%this, %obj)
{
    return ParseDescriptionText(%this.desc);
}

function AIPetData::GetStats(%this, %name, %obj)
{
    if (!(%name $= ""))
    {
        %stats = "Name:" SPC %name @ "<br>";
    }

    if (%obj $= "")
    {
        %obj = %this;
    }

    %CC = %obj.GetCCValue();
    %PP = %obj.GetPPValue();
    %CCVip = %obj.GetCCValueVIP();
    %PPVip = %obj.GetPPValueVIP();

    if (%CC)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/cc.png>" SPC %CC;
        if (%CC != %CCVip)
        {
            %stats = %stats @ " (" @ %CCVip @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)";
        }
        %stats = %stats @ "<br>";
    }

    if (%PP)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/pp.png>" SPC %PP;
        if (%PP != %PPVip)
        {
            %stats = %stats @ " (" @ %PPVip @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)";
        }
        %stats = %stats @ "<br>";
    }
    return %stats;
}

function AIPetData::GetCCValue(%this, %obj)
{
    return %this.CC;
}

function AIPetData::GetCCValueVIP(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }
    return mFloor(%obj.GetCCValue() * %obj.GetVIPDiscount());
}

function AIPetData::GetAdjCCValue(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }
    if (LocationGameConnection.getIsSubscriber())
    {
        return %obj.GetCCValueVIP();
    }
    else
    {
        return %obj.GetCCValue();
    }
}

function AIPetData::GetPPValue(%this, %obj)
{
    return %this.PP;
}

function AIPetData::GetPPValueVIP(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    return mFloor(%obj.GetPPValue() * %obj.GetVIPDiscount());
}

function AIPetData::GetAdjPPValue(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    if (LocationGameConnection.getIsSubscriber())
    {
        return %obj.GetPPValueVIP();
    }
    else
    {
        return %obj.GetPPValue();
    }
}

function AIPetData::GetDepreciation(%this, %obj)
{
    if (%this.tier == 1)
    {
        return 0.5;
    }
    else
    {
        if (%this.tier == 2)
        {
            return 0.5;
        }
        else
        {
            if (%this.tier == 3)
            {
                return 0.5;
            }
            else
            {
                if (%this.tier == 4)
                {
                    return 0.5;
                }
            }
        }
    }
    return 0;
}

function AIPetData::GetVIPDiscount(%this, %obj)
{
    return 0.80000001;
}

function AIPetData::GetPreview(%this, %objView, %bitView, %obj)
{
    %objView.setObject("obj", %this.shapeFile, %this.textureFile, "");
}

function AIPet::GetDisplayName(%this, %colors)
{
    return %this.getDataBlock().GetDisplayName(%colors);
}

function AIPet::GetDescription(%this)
{
    return %this.getDataBlock().GetDescription(%this);
}

function AIPet::GetStats(%this)
{
    return %this.getDataBlock().GetStats(%this.getShapeName(), %this);
}

function AIPet::GetCCValue(%this)
{
    return %this.getDataBlock().GetCCValue(%this);
}

function AIPet::GetCCValueVIP(%this)
{
    return %this.getDataBlock().GetCCValueVIP(%this);
}

function AIPet::GetAdjCCValue(%this)
{
    return %this.getDataBlock().GetAdjCCValue(%this);
}

function AIPet::GetPPValue(%this)
{
    return %this.getDataBlock().GetPPValue(%this);
}

function AIPet::GetPPValueVIP(%this)
{
    return %this.getDataBlock().GetPPValueVIP(%this);
}

function AIPet::GetAdjPPValue(%this)
{
    return %this.getDataBlock().GetAdjPPValue(%this);
}

function AIPet::GetDepreciation(%this)
{
    return %this.getDataBlock().GetDepreciation(%this);
}

function AIPet::GetVIPDiscount(%this)
{
    return %this.getDataBlock().GetVIPDiscount(%this);
}

function AIPet::GetPreview(%this, %objView, %bitView)
{
    return %this.getDataBlock().GetPreview(%objView, %bitView, %this);
}

function PurchaseShoulderPet::onRightClick(%this)
{
    %this.getDataBlock().handleRightClick(%this);
}

function ShoulderPetData::constructContextMenu(%datablock, %obj)
{
    %menu = %datablock.getContextMenu(%obj);
    %menu.addContextItem("Purchase", "$tempVar[1].onPurchase($tempVar[0]);");
    %menu.addContextItem("-", "");
    %menu.addContextItem("Info", "$tempVar[0].info($tempVar[1]);");
}

function PurchaseShoulderPet::onPurchase(%this, %datablock)
{
    PurchaseItem(%this);
}

function ShoulderPetData::info(%datablock, %obj)
{
    InspectItem(%obj);
}

function ShoulderPetData::GetDisplayName(%this, %colors)
{
    if (!(%colors $= ""))
    {
        %color = getWord(%colors, %this.tier);
        if (%color $= "")
        {
            %color = getWord(%colors, 0);
        }
        return "<spush><color:" @ %color @ ">" @ %this.displayName @ "<spop>";
    }
    return %this.displayName;
}

function ShoulderPetData::GetDescription(%this)
{
    return ParseDescriptionText(%this.desc);
}

function ShoulderPetData::GetStats(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    %CC = %obj.GetCCValue();
    %PP = %obj.GetPPValue();
    %CCVip = %obj.GetCCValueVIP();
    %PPVip = %obj.GetPPValueVIP();

    if (%CC)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/cc.png>" SPC %CC;
        if (%CC != %CCVip)
        {
            %stats = %stats @ " (" @ %CCVip @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)";
        }
        %stats = %stats @ "<br>";
    }

    if (%PP)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/pp.png>" SPC %PP;
        if (%PP != %PPVip)
        {
            %stats = %stats @ " (" @ %PPVip @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)";
        }
        %stats = %stats @ "<br>";
    }

    return %stats;
}

function ShoulderPetData::GetCCValue(%this, %obj)
{
    return %this.CC;
}

function ShoulderPetData::GetCCValueVIP(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    return mFloor(%obj.GetCCValue() * %obj.GetVIPDiscount());
}

function ShoulderPetData::GetAdjCCValue(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    if (LocationGameConnection.getIsSubscriber())
    {
        return %obj.GetCCValueVIP();
    }
    else
    {
        return %obj.GetCCValue();
    }
}

function ShoulderPetData::GetPPValue(%this, %obj)
{
    return %this.PP;
}

function ShoulderPetData::GetPPValueVIP(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    return mFloor(%obj.GetPPValue() * %obj.GetVIPDiscount());
}

function ShoulderPetData::GetAdjPPValue(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    if (LocationGameConnection.getIsSubscriber())
    {
        return %obj.GetPPValueVIP();
    }
    else
    {
        return %obj.GetPPValue();
    }
}

function ShoulderPetData::GetDepreciation(%this, %obj)
{
    if (%this.tier == 1)
    {
        return 0.5;
    }
    else
    {
        if (%this.tier == 2)
        {
            return 0.5;
        }
        else
        {
            if (%this.tier == 3)
            {
                return 0.5;
            }
            else
            {
                if (%this.tier == 4)
                {
                    return 0.5;
                }
            }
        }
    }
    return 0;
}

function ShoulderPetData::GetVIPDiscount(%this, %obj)
{
    return 0.80000001;
}

function ShoulderPetData::GetPreview(%this, %objView, %bitView)
{
    %objView.setObject("obj", %this.shapeFile, %this.textureFile, "");
}

function PurchaseShoulderPet::GetDisplayName(%this, %colors)
{
    return %this.getDataBlock().GetDisplayName(%colors, %this);
}

function PurchaseShoulderPet::GetDescription(%this)
{
    return %this.getDataBlock().GetDescription(%this);
}

function PurchaseShoulderPet::GetStats(%this)
{
    %stats = %this.getDataBlock().GetStats(%this);

    if (%this.discount != 1)
    {
        %stats = %stats @ "<spush><color:FFFF00>" @ mFloor(((1 - %this.discount) * 100) + 0.5) @ "% Discount<spop><br>";
    }

    return %stats;
}

function PurchaseShoulderPet::GetCCValue(%this)
{
    %CC = %this.getDataBlock().GetCCValue(%this);

    if (((%CC !$= "")) && (%this.discount != 1))
    {
        %CC = mFloor((%CC * %this.discount) + 0.5);
    }

    return %CC;
}

function PurchaseShoulderPet::GetCCValueVIP(%this)
{
    if (%this.discount != 1)
    {
        return %this.GetCCValue();
    }
    else
    {
        return %this.getDataBlock().GetCCValueVIP(%this);
    }
}

function PurchaseShoulderPet::GetAdjCCValue(%this)
{
    return %this.getDataBlock().GetAdjCCValue(%this);
}

function PurchaseShoulderPet::GetPPValue(%this)
{
    %PP = %this.getDataBlock().GetPPValue(%this);
    
    if (((%PP !$= "")) && (%this.discount != 1))
    {
        %PP = mFloor((%PP * %this.discount) + 0.5);
    }

    return %PP;
}

function PurchaseShoulderPet::GetPPValueVIP(%this)
{
    if (%this.discount != 1)
    {
        return %this.GetPPValue();
    }
    else
    {
        return %this.getDataBlock().GetPPValueVIP(%this);
    }
}

function PurchaseShoulderPet::GetAdjPPValue(%this)
{
    return %this.getDataBlock().GetAdjPPValue(%this);
}

function PurchaseShoulderPet::GetDepreciation(%this)
{
    return %this.getDataBlock().GetDepreciation(%this);
}

function PurchaseShoulderPet::GetVIPDiscount(%this)
{
    return %this.getDataBlock().GetVIPDiscount(%this);
}

function PurchaseShoulderPet::GetPreview(%this, %objView, %bitView)
{
    return %this.getDataBlock().GetPreview(%objView, %bitView, %this);
}
