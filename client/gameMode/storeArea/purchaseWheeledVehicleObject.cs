
function PurchaseWheeledVehicle::constructContextMenu(%this, %datablock)
{
    %datablock.addContextItem(%this, "Purchase", "$tempVar[1].onPurchase($tempVar[0]);");
    %datablock.addContextItem(%this, "-", "");
}

function PurchaseWheeledVehicle::onPurchase(%this, %datablock)
{
    PurchaseItem(%this);
}

function WheeledVehicleData::onRightClick(%datablock, %obj)
{
    %datablock.handleRightClick(%obj);
}

function WheeledVehicleData::showContextMenu(%datablock, %obj)
{
    if (%obj.getClassName() $= "PurchaseWheeledVehicle")
    {
        GameBaseData::showContextMenu(%datablock, %obj);
    }
    else
    {
        PlayerData::showContextMenu(%datablock, %obj);
    }
}

function WheeledVehicleData::doDefaultAction(%datablock, %obj, %menuText)
{
    if (%obj.getClassName() $= "PurchaseWheeledVehicle")
    {
        GameBaseData::doDefaultAction(%datablock, %obj, %menuText);
    }
}

function WheeledVehicleData::constructContextMenu(%datablock, %obj)
{
    if (%obj.getClassName() $= "PurchaseWheeledVehicle")
    {
        %obj.constructContextMenu(%datablock);
    }
    %menu = %datablock.getContextMenu(%obj);
    %menu.addContextItem("Info", "$tempVar[0].info($tempVar[1]);");
}

function WheeledVehicleData::info(%datablock, %obj)
{
    InspectItem(%obj);
}

function WheeledVehicleData::GetDisplayName(%this, %colors)
{
    if (!(%colors $= ""))
    {
        %color = getWord(%colors, %this.tier);
        if (%color $= "")
        {
            %color = getWord(%colors, 0);
        }
        return "<spush><color:" @ %color @ ">" @ %this.displayName @ "<spop>";
    }

    return %this.displayName;
}

function WheeledVehicleData::GetDescription(%this)
{
    return ParseDescriptionText(%this.desc);
}

function WheeledVehicleData::GetStats(%this, %obj)
{
    if (!(%name $= ""))
    {
        %stats = "Name:" SPC %name @ "<br>";
    }

    if (%obj $= "")
    {
        %obj = %this;
    }

    %CC = %obj.GetCCValue();
    %PP = %obj.GetPPValue();
    %CCVip = %obj.GetCCValueVIP();
    %PPVip = %obj.GetPPValueVIP();

    if (%CC)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/cc.png>" SPC %CC;
        if (%CC != %CCVip)
        {
            %stats = %stats @ " (" @ %CCVip @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)";
        }
        %stats = %stats @ "<br>";
    }

    if (%PP)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/pp.png>" SPC %PP;
        if (%PP != %PPVip)
        {
            %stats = %stats @ " (" @ %PPVip @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)";
        }
        %stats = %stats @ "<br>";
    }

    %stats = %stats @ %this.getFeatureList("");
    return %stats;
}

function WheeledVehicleData::getFeatureList(%this, %currentList)
{
    %speedBoost = ((%this.maxWheelSpeed / 6) - 1) * 100;
    %speedBoost = mFloor(%speedBoost + 0.5);
    %jetBoost = (%this.jetForce / 2900) * 100;
    %jetBoost = mFloor(%jetBoost + 0.5);
    %currentList = %currentList @ %speedBoost @ "% Speed Boost<br>";
    %currentList = %currentList @ %jetBoost @ "% Jet Power<br>";

    if (%this.getNumMounts() > 1)
    {
        %currentList = %currentList @ "Sit points:" SPC %this.getNumMounts() - 1 @ "<br>";
    }

    return %currentList;
}

function WheeledVehicleData::GetCCValue(%this)
{
    return %this.CC;
}

function WheeledVehicleData::GetCCValueVIP(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    return mFloor(%obj.GetCCValue() * %obj.GetVIPDiscount());
}

function WheeledVehicleData::GetAdjCCValue(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    if (LocationGameConnection.getIsSubscriber())
    {
        return %obj.GetCCValueVIP();
    }
    else
    {
        return %obj.GetCCValue();
    }
}

function WheeledVehicleData::GetPPValue(%this)
{
    return %this.PP;
}

function WheeledVehicleData::GetPPValueVIP(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    return mFloor(%obj.GetPPValue() * %obj.GetVIPDiscount());
}

function WheeledVehicleData::GetAdjPPValue(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }

    if (LocationGameConnection.getIsSubscriber())
    {
        return %obj.GetPPValueVIP();
    }
    else
    {
        return %obj.GetPPValue();
    }
}

function WheeledVehicleData::GetDepreciation(%this)
{
    if (%this.tier == 1)
    {
        return 0.5;
    }
    else
    {
        if (%this.tier == 2)
        {
            return 0.5;
        }
        else
        {
            if (%this.tier == 3)
            {
                return 0.5;
            }
            else
            {
                if (%this.tier == 4)
                {
                    return 0.5;
                }
            }
        }
    }
    return 0;
}

function WheeledVehicleData::GetVIPDiscount(%this, %obj)
{
    return 0.80000001;
}

function WheeledVehicleData::GetPreview(%this, %objView, %bitView)
{
    %objView.setObject("obj", %this.shapeFile, %this.textureFile, "");
}

function PurchaseWheeledVehicle::GetDisplayName(%this, %colors)
{
    return %this.getDataBlock().GetDisplayName(%colors);
}

function PurchaseWheeledVehicle::GetDescription(%this)
{
    return %this.getDataBlock().GetDescription();
}

function PurchaseWheeledVehicle::GetStats(%this)
{
    %stats = %this.getDataBlock().GetStats(%this);
    if (%this.discount != 1)
    {
        %stats = %stats @ "<spush><color:FFFF00>" @ mFloor(((1 - %this.discount) * 100) + 0.5) @ "% Discount<spop><br>";
    }

    return %stats;
}

function PurchaseWheeledVehicle::GetCCValue(%this)
{
    %CC = %this.getDataBlock().GetCCValue(%this);
    if (((%CC !$= "")) && (%this.discount != 1))
    {
        %CC = mFloor((%CC * %this.discount) + 0.5);
    }

    return %CC;
}

function PurchaseWheeledVehicle::GetCCValueVIP(%this)
{
    if (%this.discount != 1)
    {
        return %this.GetCCValue();
    }
    else
    {
        return %this.getDataBlock().GetCCValueVIP(%this);
    }
}

function PurchaseWheeledVehicle::GetAdjCCValue(%this)
{
    return %this.getDataBlock().GetAdjCCValue(%this);
}

function PurchaseWheeledVehicle::GetPPValue(%this)
{
    %PP = %this.getDataBlock().GetPPValue(%this);
    if (((%PP !$= "")) && (%this.discount != 1))
    {
        %PP = mFloor((%PP * %this.discount) + 0.5);
    }
    
    return %PP;
}

function PurchaseWheeledVehicle::GetPPValueVIP(%this)
{
    if (%this.discount != 1)
    {
        return %this.GetPPValue();
    }
    else
    {
        return %this.getDataBlock().GetPPValueVIP(%this);
    }
}

function PurchaseWheeledVehicle::GetAdjPPValue(%this)
{
    return %this.getDataBlock().GetAdjPPValue(%this);
}

function PurchaseWheeledVehicle::GetDepreciation(%this)
{
    return %this.getDataBlock().GetDepreciation();
}

function PurchaseWheeledVehicle::GetVIPDiscount(%this)
{
    return %this.getDataBlock().GetVIPDiscount(%this);
}

function PurchaseWheeledVehicle::GetPreview(%this, %objView, %bitView)
{
    return %this.getDataBlock().GetPreview(%objView, %bitView);
}
