
function StaticBenchObject::constructContextMenu(%datablock, %obj)
{
    Parent::constructContextMenu(%datablock, %obj);
    InteractiveMountable::constructContextMenu(%datablock, %obj, %datablock.verb $= "" ? "Sit" : %datablock.verb);
}

function StaticBenchObject::showContextMenu(%datablock, %obj, %menuText)
{
    InteractiveMountable::showContextMenu(%datablock, %obj, %menuText);
    Parent::showContextMenu(%datablock, %obj, %menuText);
}

function StaticBenchObject::getFeatureList(%datablock, %currentList)
{
    %currentList = Parent::getFeatureList(%datablock, %currentList);
    %currentList = InteractiveMountable::getFeatureList(%datablock, %currentList, %datablock.verb $= "" ? "Sit" : %datablock.verb);
    return %currentList;
}

function StaticBenchObject::onMountNodeClick(%datablock, %obj, %mountPoint)
{
    InteractiveMountable::onMountNodeClick(%datablock, %obj, %mountPoint);
}
