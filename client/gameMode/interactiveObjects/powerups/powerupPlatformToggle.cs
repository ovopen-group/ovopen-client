
function PowerupPlatformToggle::constructContextMenu(%datablock, %obj)
{
    if (%obj.getClassName() $= "PurchaseInteractiveObject")
    {
        %obj.constructContextMenu(%datablock);
    }
    
    if ((%obj.ownerID != 0) && (%obj.ownerID == $currentPlayerID))
    {
        %datablock.addContextItem(%obj, "Move", "$tempVar[0].moveFurniture($tempVar[1]);");
        %datablock.addContextItem(%obj, "Pickup", "$tempVar[0].pickupFurniture($tempVar[1]);");
        %datablock.addContextItem(%obj, "-", "");
    }

    InteractiveToggleable::constructContextMenu(%datablock, %obj, %obj.toggled);
    %datablock.addContextItem(%obj, "-", "");
    %datablock.addContextItem(%obj, "Info", "$tempVar[0].info($tempVar[1]);");
}

function PowerupPlatformToggle::showContextMenu(%datablock, %obj, %menuText)
{
    InteractiveToggleable::showContextMenu(%datablock, %obj, %menuText, %obj.toggled);
    Parent::showContextMenu(%datablock, %obj, %menuText);
}

function PowerupPlatformToggle::getFeatureList(%datablock, %currentList)
{
    %currentList = Parent::getFeatureList(%datablock, %currentList);
    %currentList = InteractiveToggleable::getFeatureList(%datablock, %currentList);
    return %currentList;
}
