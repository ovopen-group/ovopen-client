
function SeymourTheDuck::constructContextMenu(%datablock, %obj)
{
    %datablock.addContextItem(%obj, "Buy Player Points", "$tempVar[0].onPurchase($tempVar[1]);");
}

function SeymourTheDuck::onPurchase(%datablock, %obj)
{
    PurchaseItem(%obj);
}

function SeymourTheDuck::GetStats(%this)
{
    %CC = %this.GetAdjCCValue();
    %PP = %this.PPAmt;
    %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/cc.png>" SPC %CC SPC "=" SPC "<bitmap:onverse/data/live_assets/engine/ui/images/icons/pp.png>" SPC %PP @ "<br>";
    return %stats;
}

function SeymourTheDuck::GetAdjCCValue(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }
    return %obj.GetCCValue();
}

function SeymourTheDuck::GetDisplayName(%this, %colors)
{
    if (!(%colors $= ""))
    {
        return Parent::GetDisplayName(%this, %colors);
    }
    return %this.PPAmt SPC "PP";
}

function SeymourTheDuck::GetDescription(%this)
{
    %text = ParseDescriptionText(%this.desc);
    %text = strreplace(%text, "%PP", %this.PPAmt);
    %text = strreplace(%text, "%CC", %this.GetAdjCCValue());
    return %text;
}
