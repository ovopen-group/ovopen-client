
function StaticToggleObject::constructContextMenu(%datablock, %obj)
{
    if (%obj.getClassName() $= "PurchaseInteractiveObject")
    {
        %obj.constructContextMenu(%datablock);
    }

    if ((%obj.ownerID != 0) && (%obj.ownerID == $currentPlayerID))
    {
        %datablock.addContextItem(%obj, "Move", "$tempVar[0].moveFurniture($tempVar[1]);");
        %datablock.addContextItem(%obj, "Pickup", "$tempVar[0].pickupFurniture($tempVar[1]);");
        %datablock.addContextItem(%obj, "-", "");
    }
    
    InteractiveToggleable::constructContextMenu(%datablock, %obj, %obj.toggled);
    InteractiveMountable::constructContextMenu(%datablock, %obj, "Sit");
    %datablock.addContextItem(%obj, "-", "");
    %datablock.addContextItem(%obj, "Info", "$tempVar[0].info($tempVar[1]);");
}

function StaticToggleObject::showContextMenu(%datablock, %obj, %menuText)
{
    InteractiveToggleable::showContextMenu(%datablock, %obj, %menuText, %obj.toggled);
    InteractiveMountable::showContextMenu(%datablock, %obj, %menuText);
    Parent::showContextMenu(%datablock, %obj, %menuText);
}

function StaticToggleObject::getFeatureList(%datablock, %currentList)
{
    %currentList = Parent::getFeatureList(%datablock, %currentList);
    %currentList = InteractiveToggleable::getFeatureList(%datablock, %currentList);
    %currentList = InteractiveMountable::getFeatureList(%datablock, %currentList, "Sit");
    return %currentList;
}

function StaticToggleObject::onMountNodeClick(%datablock, %obj, %mountPoint)
{
    InteractiveMountable::onMountNodeClick(%datablock, %obj, %mountPoint);
}
