
function StaticSwitchObject::constructContextMenu(%datablock, %obj)
{
    Parent::constructContextMenu(%datablock, %obj);
    %datablock.addContextItem(%obj, "-", "");
    %datablock.addContextItem(%obj, "Turn On", "$tempVar[0].toggle($tempVar[1],1);");
    %datablock.addContextItem(%obj, "Turn Off", "$tempVar[0].toggle($tempVar[1],0);");
}

function StaticSwitchObject::getFeatureList(%datablock, %currentList)
{
    %currentList = Parent::getFeatureList(%datablock, %currentList);
    %currentList = InteractiveToggleable::getFeatureList(%datablock, %currentList);
    return %currentList;
}
