
function StaticCloseObject::constructContextMenu(%datablock, %obj)
{
    if (%obj.getClassName() $= "PurchaseInteractiveObject")
    {
        %obj.constructContextMenu(%datablock);
    }

    if ((%obj.ownerID != 0) && (%obj.ownerID == $currentPlayerID))
    {
        %datablock.addContextItem(%obj, "Move", "$tempVar[0].moveFurniture($tempVar[1]);");
        %datablock.addContextItem(%obj, "Pickup", "$tempVar[0].pickupFurniture($tempVar[1]);");
        %datablock.addContextItem(%obj, "-", "");
    }
    
    InteractiveCloseable::constructContextMenu(%datablock, %obj, %obj.toggled);
    %datablock.addContextItem(%obj, "-", "");
    %datablock.addContextItem(%obj, "Info", "$tempVar[0].info($tempVar[1]);");
}

function StaticCloseObject::showContextMenu(%datablock, %obj, %menuText)
{
    InteractiveCloseable::showContextMenu(%datablock, %obj, %menuText, %obj.toggled);
    Parent::showContextMenu(%datablock, %obj, %menuText);
}

function StaticCloseObject::getFeatureList(%datablock, %currentList)
{
    %currentList = Parent::getFeatureList(%datablock, %currentList);
    %currentList = InteractiveCloseable::getFeatureList(%datablock, %currentList);
    return %currentList;
}
