
function PetFeederObject::constructContextMenu(%datablock, %obj)
{
    %datablock.addContextItem(%obj, "Feed Pet", "$tempVar[0].onPurchase($tempVar[1]);");
    %datablock.addContextItem(%obj, "-", "");
    Parent::constructContextMenu(%datablock, %obj);
}

function PetFeederObject::onPurchase(%datablock, %obj)
{
    PurchaseItem(%obj);
}
