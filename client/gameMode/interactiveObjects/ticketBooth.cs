
function TicketBoothObject::constructContextMenu(%datablock, %obj)
{
    %datablock.addContextItem(%obj, "Play Game", "$tempVar[0].onPurchase($tempVar[1]);");
    %datablock.addContextItem(%obj, "-", "");
    Parent::constructContextMenu(%datablock, %obj);
}

function TicketBoothObject::onPurchase(%datablock, %obj)
{
    PurchaseItem(%obj);
}

function TicketBoothObject::GetCCValue(%this, %obj)
{
    if (%obj.CC)
    {
        return %obj.CC;
    }
    
    return %this.CC;
}

function TicketBoothObject::GetPPValue(%this, %obj)
{
    if (%obj.PP)
    {
        return %obj.PP;
    }

    return %this.PP;
}

function TicketBoothObject::GetVIPDiscount(%this, %obj)
{
    return 1;
}
