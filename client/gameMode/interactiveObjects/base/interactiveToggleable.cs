function InteractiveObjectData::toggle(%datablock, %obj, %toggle)
{
    %datablock.sendCommand(%obj, "toggle", %toggle);
}

function InteractiveToggleable::constructContextMenu(%datablock, %obj, %toggled)
{
    %menu = %datablock.getContextMenu(%obj);
    %menu.addContextItem("Turn On", "$tempVar[0].toggle($tempVar[1],1);");
    %menu.addContextItem("Turn Off", "$tempVar[0].toggle($tempVar[1],0);");
    
    if (%toggled)
    {
        %menu.setMenuItemVisible("default", "Turn On", 0);
    }
    else
    {
        %menu.setMenuItemVisible("default", "Turn Off", 0);
    }
}

function InteractiveToggleable::showContextMenu(%datablock, %obj, %unused, %toggled)
{
    %menu = %datablock.getContextMenu(%obj);

    if (%toggled)
    {
        %menu.setMenuItemVisible("default", "Turn Off", 1);
        %menu.setMenuItemVisible("default", "Turn On", 0);
    }
    else
    {
        %menu.setMenuItemVisible("default", "Turn Off", 0);
        %menu.setMenuItemVisible("default", "Turn On", 1);
    }
}

function InteractiveToggleable::getFeatureList(%datablock, %currentList, %unused)
{
    return %currentList @ "Toggleable<br>";
}
