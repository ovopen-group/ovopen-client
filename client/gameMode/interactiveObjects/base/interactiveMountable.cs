
function InteractiveObjectData::mount(%datablock, %obj, %mountPoint)
{
    %datablock.sendCommand(%obj, "mount", %mountPoint);
}

function InteractiveMountable::onMountNodeClick(%datablock, %obj, %mountPoint)
{
    if (%mountPoint == %datablock.particleMountPoint)
    {
        error("(onMountNodeClick) Particle Mount Point");
        return;
    }
    
    if (%mountPoint == %datablock.lightMountPoint)
    {
        error("(onMountNodeClick) Light Mount Point");
        return;
    }

    %datablock.mount(%obj, %mountPoint);
}

function InteractiveMountable::constructContextMenu(%datablock, %obj, %text)
{
    %menu = %datablock.getContextMenu(%obj);
    %count = %obj.getNumMounts();
    %countMod = 0;

    if (%datablock.particleMountPoint != -1)
    {
        %countMod++;
    }
    if (%datablock.lightMountPoint != -1)
    {
        %countMod++;
    }
    if (%count > %countMod)
    {
        %menu.addContextItem("-", "");
    }

    %datablock.mountText = %text;
    %numEntries = 0;

    for (%i=0; %i < %count; %i++)
    {
        if ((%i != %datablock.particleMountPoint) && (%i != %datablock.lightMountPoint))
        {
            if (%count > (%countMod + 1))
            {
                %menu.IM_mountNodeName[%numEntries] = %text SPC %numEntries + 1;
            }
            else
            {
                %menu.IM_mountNodeName[%numEntries] = %text;
            }

            %menu.addContextItem(%menu.IM_mountNodeName[%numEntries], "$tempVar[0].mount($tempVar[1]," @ %i @ ");");
            %numEntries++;
        }
    }
}

function InteractiveMountable::showContextMenu(%datablock, %obj, %unused)
{
    %menu = %datablock.getContextMenu(%obj);
    %count = %obj.getNumMounts();
    %numEntries = 0;

    for (%i=0; %i < %count; %i++)
    {
        if ((%i != %datablock.particleMountPoint) && (%i != %datablock.lightMountPoint))
        {
            if (%obj.getMountNodeObject(%i) != 0)
            {
                %menu.setMenuItemEnable("default", %menu.IM_mountNodeName[%numEntries], 0);
            }
            else
            {
                %menu.setMenuItemEnable("default", %menu.IM_mountNodeName[%numEntries], 1);
            }
            %numEntries++;
        }
    }
}

function InteractiveMountable::getFeatureList(%datablock, %currentList, %text)
{
    %count = %datablock.getNumMounts();
    %numEntries = 0;

    for (%i=0; %i < %count; %i++)
    {
        if ((%i != %datablock.particleMountPoint) && (%i != %datablock.lightMountPoint))
        {
            %numEntries++;
        }
    }

    if (%numEntries > 0)
    {
        return %currentList @ %text SPC "points:" SPC %numEntries @ ", right click object to" SPC strlwr(%text) @ "<br>";
    }
    else
    {
        return %currentList;
    }
}
