
function InteractiveObjectData::canShowRightClick(%datablock, %obj)
{
    return %datablock.showRightClick;
}

function InteractiveObjectData::onRightClick(%datablock, %obj)
{
    %datablock.handleRightClick(%obj);
}

function InteractiveObjectData::constructContextMenu(%datablock, %obj)
{
    if (%obj.getClassName() $= "PurchaseInteractiveObject")
    {
        %obj.constructContextMenu(%datablock);
    }
    
    if ((%obj.ownerID != 0) && (%obj.ownerID == $currentPlayerID))
    {
        %datablock.addContextItem(%obj, "Move", "$tempVar[0].moveFurniture($tempVar[1]);");
        %datablock.addContextItem(%obj, "Pickup", "$tempVar[0].pickupFurniture($tempVar[1]);");
        %datablock.addContextItem(%obj, "-", "");
    }
    %datablock.addContextItem(%obj, "Info", "$tempVar[0].info($tempVar[1]);");
}

function InteractiveObjectData::MoveFurniture(%datablock, %obj)
{
    FurniturePlacement::MoveFurnitureObject(%obj);
}

function InteractiveObjectData::PickupFurniture(%datablock, %obj)
{
    FurniturePlacement::PickupFurnitureObject(%obj);
}

function InteractiveObjectData::info(%datablock, %obj)
{
    InspectItem(%obj);
}

function InteractiveObjectData::getFeatureList(%datablock, %currentList)
{
    return %currentList;
}

function InteractiveObjectData::logInteraction(%this)
{
    commandToServer('UpInter', %this.getId());
}

function InteractiveObjectData::GetDisplayName(%datablock, %colors, %this)
{
    if (!(%colors $= ""))
    {
        %color = getWord(%colors, %datablock.tier);
        if (%color $= "")
        {
            %color = getWord(%colors, 0);
        }
        return "<spush><color:" @ %color @ ">" @ %datablock.displayName @ "<spop>";
    }
    return %datablock.displayName;
}

function InteractiveObjectData::GetDescription(%datablock, %this)
{
    return ParseDescriptionText(%datablock.desc);
}

function InteractiveObjectData::GetStats(%datablock, %this)
{
    if (%this $= "")
    {
        %this = %datablock;
    }
    %CC = %this.GetCCValue();
    %PP = %this.GetPPValue();
    %CCVip = %this.GetCCValueVIP();
    %PPVip = %this.GetPPValueVIP();
    if (%CC)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/cc.png>" SPC %CC;
        if (%CC != %CCVip)
        {
            %stats = %stats @ " (" @ %CCVip @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)";
        }
        %stats = %stats @ "<br>";
    }
    if (%PP)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/pp.png>" SPC %PP;
        if (%PP != %PPVip)
        {
            %stats = %stats @ " (" @ %PPVip @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)";
        }
        %stats = %stats @ "<br>";
    }
    %stats = %stats @ %datablock.getFeatureList("");
    return %stats;
}

function InteractiveObjectData::GetCCValue(%this, %obj)
{
    return %this.CC;
}

function InteractiveObjectData::GetCCValueVIP(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }
    return mFloor(%obj.GetCCValue() * %obj.GetVIPDiscount());
}

function InteractiveObjectData::GetAdjCCValue(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }
    if (LocationGameConnection.getIsSubscriber())
    {
        return %obj.GetCCValueVIP();
    }
    else
    {
        return %obj.GetCCValue();
    }
}

function InteractiveObjectData::GetPPValue(%this, %obj)
{
    return %this.PP;
}

function InteractiveObjectData::GetPPValueVIP(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }
    return mFloor(%obj.GetPPValue() * %obj.GetVIPDiscount());
}

function InteractiveObjectData::GetAdjPPValue(%this, %obj)
{
    if (%obj $= "")
    {
        %obj = %this;
    }
    if (LocationGameConnection.getIsSubscriber())
    {
        return %obj.GetPPValueVIP();
    }
    else
    {
        return %obj.GetPPValue();
    }
}

function InteractiveObjectData::GetDepreciation(%this)
{
    if (%this.tier == 1)
    {
        return 0.5;
    }
    else
    {
        if (%this.tier == 2)
        {
            return 0.5;
        }
        else
        {
            if (%this.tier == 3)
            {
                return 0.5;
            }
            else
            {
                if (%this.tier == 4)
                {
                    return 0.5;
                }
            }
        }
    }
    return 0;
}

function InteractiveObjectData::GetVIPDiscount(%this, %obj)
{
    return 0.80000001;
}

function InteractiveObjectData::GetPreview(%this, %objView, %bitView)
{
    %objView.setObject("obj", %this.shapeFile, %this.textureFile, "");
}

function InteractiveObject::logInteraction(%this)
{
    return %this.getDataBlock().logInteraction();
}

function InteractiveObject::GetDisplayName(%this, %colors)
{
    return %this.getDataBlock().GetDisplayName(%colors, %this);
}

function InteractiveObject::GetDescription(%this)
{
    return %this.getDataBlock().GetDescription(%this);
}

function InteractiveObject::GetStats(%this)
{
    return %this.getDataBlock().GetStats(%this);
}

function InteractiveObject::GetCCValue(%this)
{
    return %this.getDataBlock().GetCCValue(%this);
}

function InteractiveObject::GetCCValueVIP(%this)
{
    return %this.getDataBlock().GetCCValueVIP(%this);
}

function InteractiveObject::GetAdjCCValue(%this)
{
    return %this.getDataBlock().GetAdjCCValue(%this);
}

function InteractiveObject::GetPPValue(%this)
{
    return %this.getDataBlock().GetPPValue(%this);
}

function InteractiveObject::GetPPValueVIP(%this)
{
    return %this.getDataBlock().GetPPValueVIP(%this);
}

function InteractiveObject::GetAdjPPValue(%this)
{
    return %this.getDataBlock().GetAdjPPValue(%this);
}

function InteractiveObject::GetDepreciation(%this)
{
    return %this.getDataBlock().GetDepreciation();
}

function InteractiveObject::GetVIPDiscount(%this)
{
    return %this.getDataBlock().GetVIPDiscount(%this);
}

function InteractiveObject::GetPreview(%this, %objView, %bitView)
{
    return %this.getDataBlock().GetPreview(%objView, %bitView);
}
