
function InteractiveCloseable::constructContextMenu(%datablock, %obj, %toggled)
{
    %menu = %datablock.getContextMenu(%obj);
    %menu.addContextItem("Open", "$tempVar[0].toggle($tempVar[1],1);");
    %menu.addContextItem("Close", "$tempVar[0].toggle($tempVar[1],0);");
    if (%toggled)
    {
        %menu.setMenuItemVisible("default", "Open", 0);
    }
    else
    {
        %menu.setMenuItemVisible("default", "Close", 0);
    }
}

function InteractiveCloseable::showContextMenu(%datablock, %obj, %unused, %toggled)
{
    %menu = %datablock.getContextMenu(%obj);
    if (%toggled)
    {
        %menu.setMenuItemVisible("default", "Close", 1);
        %menu.setMenuItemVisible("default", "Open", 0);
    }
    else
    {
        %menu.setMenuItemVisible("default", "Close", 0);
        %menu.setMenuItemVisible("default", "Open", 1);
    }
}

function InteractiveCloseable::getFeatureList(%datablock, %currentList, %unused)
{
    return %currentList @ "Open/Close<br>";
}
