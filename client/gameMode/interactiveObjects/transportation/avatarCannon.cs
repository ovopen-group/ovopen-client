
function AvatarCannonObject::constructContextMenu(%datablock, %obj)
{
    %datablock.addContextItem(%obj, "Ride", "$tempVar[0].ride($tempVar[1]);");
    %datablock.addContextItem(%obj, "-", "");
    Parent::constructContextMenu(%datablock, %obj);
}

function AvatarCannonObject::ride(%datablock, %obj)
{
    %datablock.sendCommand(%obj, "ride");
}
