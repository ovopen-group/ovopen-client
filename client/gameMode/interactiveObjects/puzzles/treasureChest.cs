
function TreasureChestObject::constructContextMenu(%datablock, %obj)
{
    %datablock.addContextItem(%obj, "Open", "$tempVar[0].open($tempVar[1]);");
    %datablock.addContextItem(%obj, "-", "");
    %datablock.addContextItem(%obj, "Info", "$tempVar[0].info($tempVar[1]);");
}

function TreasureChestObject::open(%datablock, %obj)
{
    %datablock.sendCommand(%obj, "open");
}
