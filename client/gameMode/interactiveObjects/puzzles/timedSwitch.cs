
function TimedSwitchObject::constructContextMenu(%datablock, %obj)
{
    if ((%obj.dispMode == 0) || (%obj.dispMode & 1))
    {
        %datablock.addContextItem(%obj, "Turn On", "$tempVar[0].toggle($tempVar[1],1);");
    }
    
    if ((%obj.dispMode == 0) || (%obj.dispMode & 2))
    {
        %datablock.addContextItem(%obj, "Turn Off", "$tempVar[0].toggle($tempVar[1],0);");
    }

    %datablock.addContextItem(%obj, "-", "");
    Parent::constructContextMenu(%datablock, %obj);
}

function TimedSwitchObject::showContextMenu(%datablock, %obj, %menuText)
{
    %menu = %datablock.getContextMenu(%obj);
    if ((%obj.dispMode == 3) || (%obj.dispMode == 0))
    {
        if (%obj.toggled)
        {
            %menu.setMenuItemVisible("default", "Turn Off", 1);
            %menu.setMenuItemVisible("default", "Turn On", 0);
        }
        else
        {
            %menu.setMenuItemVisible("default", "Turn Off", 0);
            %menu.setMenuItemVisible("default", "Turn On", 1);
        }
    }
    else
    {
        if (%obj.dispMode == 1)
        {
            %menu.setMenuItemEnable("default", "Turn On", !%obj.toggled);
        }
        else
        {
            %menu.setMenuItemEnable("default", "Turn Off", %obj.toggled);
        }
    }
    Parent::showContextMenu(%datablock, %obj, %menuText);
}

function TimedSwitchObject::getFeatureList(%datablock, %currentList)
{
    %currentList = Parent::getFeatureList(%datablock, %currentList);
    %currentList = InteractiveToggleable::getFeatureList(%datablock, %currentList);
    return %currentList;
}
