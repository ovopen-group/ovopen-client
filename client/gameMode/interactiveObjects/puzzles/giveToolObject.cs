
function GiveToolObject::constructContextMenu(%datablock, %obj)
{
    %datablock.addContextItem(%obj, "Take", "$tempVar[0].open($tempVar[1]);");
    %datablock.addContextItem(%obj, "-", "");
    %datablock.addContextItem(%obj, "Info", "$tempVar[0].info($tempVar[1]);");
}

function GiveToolObject::open(%datablock, %obj)
{
    %datablock.sendCommand(%obj, "open");
}
