
$TOOL_MAX = 10000;
%id = -1;
$ToolActions[%id++] = "smash" TAB "onverse/data/live_assets/engine/ui/images/icons/hammer_i.png";
$ToolActions[%id++] = "dig" TAB "onverse/data/live_assets/engine/ui/images/icons/shovel_i.png";
$ToolActions[%id++] = "catch" TAB "onverse/data/live_assets/engine/ui/images/icons/net_i.png";
$ToolActions[%id++] = "pick" TAB "onverse/data/live_assets/engine/ui/images/icons/pick_i.png";
$ToolActions[%id++] = "scan" TAB "onverse/data/live_assets/engine/ui/images/icons/scan_i.png";
$ToolActions[%id++] = "egg" TAB "onverse/data/live_assets/engine/ui/images/icons/basket_i.png";
$ToolActions[%id++] = "spark" TAB "onverse/data/live_assets/engine/ui/images/icons/sparkler_i.png";
$ToolActions = %id++;

function ToolTarget::getFeatureList(%datablock)
{
    for (%i=0; %i < $ToolActions; %i++)
    {
        %action[%i] = $TOOL_MAX;
    }

    %i = 0;
    while (%datablock.action[%i] !$= "")
    {
        %curaction = %datablock.action[%i];

        for (%j=0; %j < $ToolActions; %j++)
        {
            %toolAction = getField($ToolActions[%j], 0);
            if (strstr(%curaction, %toolAction) == 0)
            {
                %num = getSubStr(%curaction, strlen(%toolAction), 1000);
                if (%num < %action[%j])
                {
                    %action[%j] = %num;
                }
            }
        }

        %i++;
    }

    %s = "Requires";
    for (%i=0; %i < $ToolActions; %i++)
    {
        if (%action[%i] < $TOOL_MAX)
        {
            %s = %s SPC "<bitmap:" @ getField($ToolActions[%i], 1) @ ">" @ %action[%i];
        }
    }

    return %s;
}

function ToolImageData::getFeatureList(%this)
{
    for (%i=0; %i < $ToolActions; %i++)
    {
        %action[%i] = $TOOL_MAX;
    }

    %i = 0;
    while ((%curaction = getField(%this.actionList, %i)) !$= "")
    {
        for (%j=0; %j < $ToolActions; %j++)
        {
            %toolAction = getField($ToolActions[%j], 0);
            if (strstr(%curaction, %toolAction) == 0)
            {
                %num = getSubStr(%curaction, strlen(%toolAction), 1000);
                if (%num < %action[%j])
                {
                    %action[%j] = %num;
                }
            }
        }
        %i++;
    }

    %s = "Actions";
    for (%i=0; %i < $ToolActions; %i++)
    {
        if (%action[%i] < $TOOL_MAX)
        {
            %s = %s SPC "<bitmap:" @ getField($ToolActions[%i], 1) @ ">" @ %action[%i];
        }
    }
    return %s @ "<br>";
}

function ToolTarget::doDefaultAction(%datablock, %obj, %menuText)
{
    if (LocationGameConnection.Player.getMountedImage(0) == 0)
    {
        Parent::doDefaultAction(%datablock, %obj, %menuText);
    }
}
