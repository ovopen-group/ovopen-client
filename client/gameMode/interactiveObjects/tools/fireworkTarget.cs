
function FireworkTarget::getFeatureList(%datablock)
{
    return ToolTarget::getFeatureList(%datablock);
}

function FireworkTarget::doDefaultAction(%datablock, %obj, %menuText)
{
    if (LocationGameConnection.Player.getMountedImage(0) == 0)
    {
        Parent::doDefaultAction(%datablock, %obj, %menuText);
    }
}
