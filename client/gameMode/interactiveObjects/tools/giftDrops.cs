
function GiftItem::onRightClick(%obj)
{
    %obj.getDataBlock().handleRightClick(%obj);
}

function GiftObjectItem::constructContextMenu(%datablock, %obj)
{
    %datablock.addContextItem(%obj, "Pickup", "$tempVar[0].onPurchase($tempVar[1]);");
}

function GiftObjectItem::onPurchase(%datablock, %this)
{
    GiftItem(%this);
}

function GiftItem::getCurrentDatablock(%this)
{
    %clothing[Male] = %this.itemMaleDatablockID;
    %clothing[Female] = %this.itemFemaleDatablockID;

    if (%clothing[Male] && %clothing[Female])
    {
        return %clothing[$currentPlayerSex];
    }
    else
    {
        if (%clothing[Female])
        {
            return %clothing[Female];
        }
    }
    return %clothing[Male];
}

function GiftItem::GetDisplayName(%this, %colors)
{
    return %this.getCurrentDatablock().GetDisplayName(%colors);
}

function GiftItem::GetDescription(%this)
{
    return %this.getCurrentDatablock().GetDescription();
}

function GiftItem::GetStats(%this)
{
    return %this.getCurrentDatablock().GetStats();
}

function GiftItem::GetCCValue(%this)
{
    return %this.getCurrentDatablock().GetCCValue(%this);
}

function GiftItem::GetCCValueVIP(%this)
{
    return %this.getCurrentDatablock().GetCCValueVIP(%this);
}

function GiftItem::GetAdjCCValue(%this)
{
    return %this.getCurrentDatablock().GetAdjCCValue(%this);
}

function GiftItem::GetPPValue(%this)
{
    return %this.getCurrentDatablock().GetPPValue(%this);
}

function GiftItem::GetPPValueVIP(%this)
{
    return %this.getCurrentDatablock().GetPPValueVIP(%this);
}

function GiftItem::GetAdjPPValue(%this)
{
    return %this.getCurrentDatablock().GetAdjPPValue(%this);
}

function GiftItem::GetDepreciation(%this)
{
    return %this.getCurrentDatablock().GetDepreciation();
}

function GiftItem::GetVIPDiscount(%this)
{
    return %this.getCurrentDatablock().GetVIPDiscount(%this);
}

function GiftItem::GetPreview(%this, %objView, %bitView)
{
    return %this.getCurrentDatablock().GetPreview(%objView, %bitView);
}
