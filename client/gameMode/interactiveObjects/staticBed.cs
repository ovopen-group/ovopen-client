
function StaticBedObject::constructContextMenu(%datablock, %obj)
{
    Parent::constructContextMenu(%datablock, %obj);
    InteractiveMountable::constructContextMenu(%datablock, %obj, "Sleep");
}

function StaticBedObject::showContextMenu(%datablock, %obj, %menuText)
{
    InteractiveMountable::showContextMenu(%datablock, %obj, %menuText);
    Parent::showContextMenu(%datablock, %obj, %menuText);
}

function StaticBedObject::getFeatureList(%datablock, %currentList)
{
    %currentList = Parent::getFeatureList(%datablock, %currentList);
    %currentList = InteractiveMountable::getFeatureList(%datablock, %currentList, "Sleep");
    return %currentList;
}

function StaticBedObject::onMountNodeClick(%datablock, %obj, %mountPoint)
{
    InteractiveMountable::onMountNodeClick(%datablock, %obj, %mountPoint);
}
