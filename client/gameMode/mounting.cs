// DSONOTE: not actually executed
function clientCmdOnMountedObject(%ghostId, %mounted, %isTravel, %driving)
{
    %obj = LocationGameConnection.resolveGhostID(%ghostId);
    if (%isTravel && %driving)
    {
        if (%mounted)
        {
            driveModeActionMap.push();
        }
        else
        {
            driveModeActionMap.pop();
        }
        $mvMounted = 0;
    }
    else
    {
        if (%isTravel)
        {
            $mvMounted = 0;
        }
        else
        {
            $mvMounted = %mounted;
        }
    }
}

function VehicleData::board(%datablock, %obj, %mount)
{
    commandToServer('EnterVehicle', %obj.getGhostID(), %mount);
}

function VehicleData::onRightClick(%datablock, %obj)
{
    %datablock.handleRightClick(%obj);
}

function VehicleData::constructContextMenu(%datablock, %obj)
{
    %menu = %datablock.getContextMenu(%datablock, %obj);
    %count = %obj.getNumMounts();
    %menu.addContextItem("Drive", "$tempVar[0].board($tempVar[1],0);");
    if (%count > 1)
    {
        %menu.addContextItem("-", "");
    }

    for (%i = 1; %i < %count; %i++)
    {
        %menu.addContextItem("Mount" SPC %i, "$tempVar[0].board($tempVar[1]," @ %i @ ");");
    }
}

function VehicleData::showContextMenu(%datablock, %obj, %menuText)
{
    %menu = %datablock.getContextMenu(%obj);
    %count = %obj.getNumMounts();
    if (%count > 0)
    {
        if (%obj.getMountNodeObject(0) != 0)
        {
            %menu.setMenuItemEnable("default", "Drive", 0);
        }
        else
        {
            %menu.setMenuItemEnable("default", "Drive", 1);
        }

        for (%i=1; %i < %count; %i++)
        {
            if (%obj.getMountNodeObject(%i) != 0)
            {
                %menu.setMenuItemEnable("default", "Mount" SPC %i, 0);
            }
            else
            {
                %menu.setMenuItemEnable("default", "Mount" SPC %i, 1);
            }
        }
    }

    Parent::showContextMenu(%datablock, %obj, %menuText);
}
