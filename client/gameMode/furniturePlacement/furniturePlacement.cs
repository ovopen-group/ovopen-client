
function FurniturePlacement::PlaceFurnitureObject(%datablock, %uniqueID, %position)
{
    if (%position $= "")
    {
        getCurrentInGameUI().setPlacePos("0 0");
    }
    else
    {
        getCurrentInGameUI().setPlacePos(%position);
    }

    commandToServer('PlaceFurnitureObject', %datablock, %uniqueID);
}

function FurniturePlacement::MoveFurnitureObject(%obj)
{
    commandToServer('MoveFurnitureObject', %obj.getGhostID());
}

function FurniturePlacement::PickupFurnitureObject(%obj)
{
    commandToServer('PickupFurnitureObject', %obj.getGhostID());
}

function ClientCmdFurniturePlacementError(%message)
{
    MessageBoxOK("Error", %message);
    getCurrentInGameUI().selectObject();
}

function ClientCmdFurniturePlacementBegin(%ghostId)
{
    %obj = LocationGameConnection.resolveGhostID(%ghostId);
    %ret = getCurrentInGameUI().placeObject(%obj, %obj.getDataBlock().furnType);
    
    if (%ret < 0)
    {
        ClientCmdFurniturePlacementError("Unknown error placing furniture.");
    }
    else
    {
        if (%ret > 0)
        {
            if (%obj.getDataBlock().furnType == 1)
            {
                MessageBoxOK("Placement Error", "You must look at the ceiling in your home to place ceiling objects.");
            }
            else
            {
                if (%obj.getDataBlock().furnType == 2)
                {
                    MessageBoxOK("Placement Error", "You must look at the wall in your home to place wall objects.");
                }
                else
                {
                    MessageBoxOK("Placement Error", "You must look at the floor in your home to place floor objects.");
                }
            }
            commandToServer('PickupFurnitureObject', %obj.getGhostID());
        }
    }
}

function ClientCmdFurnitureMovementBegin(%ghostId)
{
    %obj = LocationGameConnection.resolveGhostID(%ghostId);
    getCurrentInGameUI().selectObject(%obj);
}
