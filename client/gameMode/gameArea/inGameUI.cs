
function ClientCmdSwitchToGameUI(%ui)
{
    if (%ui $= "paintball")
    {
        setCurrentInGameUI(inGameUI_Paintball);
        PushGameCameraOpts(0);
    }
    else
    {
        setCurrentInGameUI(inGameUI);
        PopGameCameraOpts();
    }
}

$gameCamera::set = 0;
$gameCamera::zoom = 0;

function PushGameCameraOpts(%zoom)
{
    if (!$gameCamera::set)
    {
        $gameCamera::set = 1;
        $gameCamera::zoom = LocationGameConnection.getTargetZoom();
    }
    LocationGameConnection.setTargetZoom(%zoom);
}

function PopGameCameraOpts()
{
    if ($gameCamera::set)
    {
        $gameCamera::set = 0;
        LocationGameConnection.setTargetZoom($gameCamera::zoom);
    }
}
