
new GuiControlProfile(GuiStatHeaderProfile : GuiWindowProfile) {
   border = "0";
   borderColor = "0 0 0";
   opaque = "0";
   gradient = "1";
   fillColors[0] = "147 184 244 255";
   fillColors[1] = "147 184 244 255";
   fillColors[2] = "60 101 181 255";
   fillColors[3] = "60 101 181 255";
};

new GuiControlProfile(GuiStatEntryProfile : GuiStatHeaderProfile) {
   opaque = "1";
   gradient = "0";
   fillColors[0] = "20 50 100 255";
   fillColors[1] = "20 50 100 255";
   fillColors[2] = "20 50 100 255";
   fillColors[3] = "20 50 100 255";
};

new GuiControlProfile(GuiStatDisabledProfile : GuiStatHeaderProfile) {
   opaque = "1";
   gradient = "0";
   fillColors[0] = "70 70 70 255";
   fillColors[1] = "70 70 70 255";
   fillColors[2] = "70 70 70 255";
   fillColors[3] = "70 70 70 255";
};

new GuiControlProfile(GuiStatMinPlayerProfile : GuiStatHeaderProfile) {
   opaque = "0";
   gradient = "1";
   fillColors[0] = "70 0 0 255";
   fillColors[1] = "70 0 0 255";
   fillColors[2] = "90 30 30 255";
   fillColors[3] = "90 30 30 255";
};

new GuiControlProfile(GuiStatWinPlayerProfile : GuiStatHeaderProfile) {
   opaque = "0";
   gradient = "1";
   fillColors[0] = "171 152 0 255";
   fillColors[1] = "171 152 0 255";
   fillColors[2] = "230 200 50 255";
   fillColors[3] = "230 200 50 255";
};

new GuiControlProfile(GuiStatEntryHeaderTextProfile : GuiBaseTextProfile) {
   fontType = "Arial Bold";
   fontColor = "255 255 255";
   fontSize = "16";
};

new GuiControlProfile(GuiStatEntryTextProfile : GuiBaseTextProfile) {
   fontType = "Arial Bold";
   fontColor = "255 255 255";
   fontSize = "14";
};

new GuiControlProfile(GuiStatEntryScoreHeaderTextProfile : GuiBaseTextProfile) {
   fontType = "Arial Bold";
   fontColor = "255 255 255";
   justify = right;
   fontSize = "16";
   autoSizeWidth = "0";
};

new GuiControlProfile(GuiStatEntryScoreTextProfile : GuiStatEntryScoreHeaderTextProfile) {
   fontSize = "14";
};

new GuiControlProfile(GuiStatEntryDisabledTextProfile : GuiStatEntryTextProfile) {
   fontColor = "150 150 150";
};

new GuiControlProfile(GuiStatEntryWinTextProfile : GuiStatEntryTextProfile) {
   fontColor = "150 150 150";
};


if (!isObject(GameStatUI))
{
    exec("./gameStatUI.gui");
}

function GameStatUI::Enable(%this)
{
    %this.enableCount++;
    %this.enabled = 1;
    Canvas.pushDialog(%this);
    WindowBar._(gameStatButton).setVisible(1);
}

function GameStatUI::disable(%this, %force)
{
    %this.enableCount--;
    if ((%this.enableCount <= 0) || %force)
    {
        %this.enabled = 0;
        Canvas.popDialog(%this);
        WindowBar._(gameStatButton).setVisible(0);
        %this.enableCount = 0;
    }
}

function GameStatUI::show(%this)
{
    if (!%this.isAwake() && %this.enabled)
    {
        Canvas.pushDialog(%this);
    }
    else
    {
        Canvas.popDialog(%this);
    }
}

function GameStatUI::getListFromState(%this, %state)
{
    if (%state == 4)
    {
        return %this._(gamePlayersStack);
    }
    else
    {
        if (%state == 3)
        {
            return %this._(gamePlayersStack);
        }
        else
        {
            if (%state == 2)
            {
                return %this._(gamePlayersStack);
            }
            else
            {
                if (%state == 1)
                {
                    return %this._(waitPlayersStack);
                }
            }
        }
    }
    return 0;
}

function GameStatUI::getPlayerCountTextFromState(%this, %state)
{
    if (%state == 4)
    {
        return GameStatUI._(gamePlayerCount);
    }
    else
    {
        if (%state == 3)
        {
            return GameStatUI._(gamePlayerCount);
        }
        else
        {
            if (%state == 2)
            {
                return GameStatUI._(gamePlayerCount);
            }
            else
            {
                if (%state == 1)
                {
                    return GameStatUI._(waitPlayerCount);
                }
            }
        }
    }
    
    return 0;
}

function GameStatUI::CheckWaitList(%this)
{
    %count = GameStatUI._(waitPlayerCount).text;
    if (%count < GameStatUI.minPlayerCount)
    {
        %i = 0;
        while (%i < GameStatUI._(waitPlayersStack).getCount())
        {
            %entry = GameStatUI._(waitPlayersStack).getObject(%i);
            if (%entry.UID == 0)
            {
                %entry.delete();
            }
            else
            {
                %i++;
            }
        }

        for (%i=GameStatUI._(waitPlayersStack).getCount(); %i < GameStatUI.minPlayerCount; %i++)
        {
            GameStatUI._(waitPlayersStack).AddEntry(0, "Waiting For Player" SPC %i + 1, "", -1);
        }
    }
}

function testGameAreaStats()
{
    GameStatUI.Enable();
    ClientCmdGameAreaStats_WipeBoard(3);
    ClientCmdGameAreaStats_FullStats(1, 2, "Player 1", 10);
    ClientCmdGameAreaStats_FullStats(2, 3, "Player 2", 10);
    ClientCmdGameAreaStats_FullStats(3, 3, "Player 3", 1000);
    ClientCmdGameAreaStats_FullStats(4, 1, "Waiting User 1", "");
    ClientCmdGameAreaStats_FullStats(5, 1, "Waiting User 2", "");
}

function ClientCmdGameAreaStats_WipeBoard(%minPlayerCount)
{
    GameStatUI._(gamePlayersStack).clear();
    GameStatUI._(waitPlayersStack).clear();
    GameStatUI._(gamePlayerCount).setText(0);
    GameStatUI._(waitPlayerCount).setText(0);
    GameStatUI.minPlayerCount = %minPlayerCount;
    GameStatUI.CheckWaitList();
}

function ClientCmdGameAreaStats_ClearPlayers()
{
    GameStatUI._(gamePlayersStack).clear();
    GameStatUI._(gamePlayerCount).setText(0);
}

function ClientCmdGameAreaStats_FullStats(%UID, %state, %username, %score)
{
    %list = GameStatUI.getListFromState(%state);
    if (%list)
    {
        %entry = %list.AddEntry(%UID, %username, %score, %state);
        %countText = GameStatUI.getPlayerCountTextFromState(%state);
        %countText.setText(%countText.text + 1);
        if (%list == GameStatUI._(gamePlayersStack))
        {
            %list.sortEntry(%entry);
        }
    }
}

function ClientCmdGameAreaStats_UpdateStats(%UID, %state, %score)
{
    %waitList = 0;
    if (%state == 4)
    {
        %list = GameStatUI._(gamePlayersStack);
        %entry = %list.FindEntry(%UID);
        if (%entry)
        {
            %entry.setProfile(GuiStatWinPlayerProfile);
            %entry._(username).setProfile(GuiStatEntryTextProfile);
            %entry._(Score).setText(%score);
            %list.sortEntry(%entry);
        }
    }

    if (%state == 3)
    {
        %list = GameStatUI._(gamePlayersStack);
        %entry = %list.FindEntry(%UID);
        if (%entry)
        {
            %entry.setProfile(GuiStatDisabledProfile);
            %entry._(username).setProfile(GuiStatEntryDisabledTextProfile);
            %entry._(Score).setText(%score);
            %list.sortEntry(%entry);
        }
    }
    else
    {
        if (%state == 0)
        {
            %list = GameStatUI._(gamePlayersStack);
            %entry = %list.FindEntry(%UID);
            if (%entry)
            {
                %list.deleteEntry(%entry);
                GameStatUI._(gamePlayerCount).setText(GameStatUI._(gamePlayerCount).text - 1);
            }
            %list = GameStatUI._(waitPlayersStack);
            %entry = %list.FindEntry(%UID);
            if (%entry)
            {
                %list.deleteEntry(%entry);
                GameStatUI._(waitPlayerCount).setText(GameStatUI._(waitPlayerCount).text - 1);
                %waitList = 1;
            }
        }
        else
        {
            %list = GameStatUI.getListFromState(%state);
            %entry = %list.FindEntry(%UID);
            if (%entry)
            {
                %entry._(Score).setText(%score);
                %list.sortEntry(%entry);
            }
            else
            {
                %list = GameStatUI._(gamePlayersStack);
                %entry = %list.FindEntry(%UID);
                %count = GameStatUI._(gamePlayerCount);
                if (!%entry)
                {
                    %list = GameStatUI._(waitPlayersStack);
                    %entry = %list.FindEntry(%UID);
                    %count = GameStatUI._(waitPlayerCount);
                    %waitList = 1;
                }
                if (%entry)
                {
                    GameStatUI.getListFromState(%state).AddEntry(%entry.UID, %entry._(username).text, %score, %state);
                    %list.deleteEntry(%entry);
                    %count.setText(%count.text - 1);
                    %count = GameStatUI.getPlayerCountTextFromState(%state);
                    %count.setText(%count.text + 1);
                }
            }
        }
    }

    if (%waitList)
    {
        GameStatUI.CheckWaitList();
    }
}

function GameStatEntry::FindEntry(%this, %UID)
{
    %count = %this.getCount();
    for (%i=0; %i < %count; %i++)
    {
        %entry = %this.getObject(%i);
        if (%entry.UID == %UID)
        {
            return %entry;
        }
    }
    return 0;
}

function GameStatEntry::deleteEntry(%this, %entry)
{
    %entry.delete();
    %this.updateStack();
    %this.getParent().updateStack();
}

function GameStatEntry::AddEntry(%this, %UID, %username, %score, %state)
{
    %profileBar = GuiStatEntryProfile;
    %profileText = GuiStatEntryTextProfile;

    if (%state == 4)
    {
        %profileBar = GuiStatWinPlayerProfile;
        %profileText = GuiStatEntryTextProfile;
    }
    else
    {
        if (%state == 3)
        {
            %profileBar = GuiStatDisabledProfile;
            %profileText = GuiStatEntryDisabledTextProfile;
        }
        else
        {
            if (%state == -1)
            {
                %profileBar = GuiStatMinPlayerProfile;
                %profileText = GuiStatEntryDisabledTextProfile;
            }
        }
    }

    return %this.AddEntry_Profile(%UID, %username, %score, %profileBar, %profileText);
}

function GameStatEntry::AddEntry_Profile(%this, %UID, %username, %score, %profileBar, %profileText)
{
    %found = 0;
    if (%UID != 0)
    {
        for (%i=0; %i < %this.getCount(); %i++)
        {
            %entry = %this.getObject(%i);
            if (%entry.UID == 0)
            {
                %entry.UID = %UID;
                %entry.setProfile(%profileBar);
                %found = 1;
                break;
            }
        }
    }

    if (!%found)
    {
        %entry = new GuiControl() {
           Profile = %profileBar;
           position = "0 0";
           Extent = "266 16";
           UID = %UID;
        };
    }
    %this.add(%entry);
    
    if (%found)
    {
        %text = %entry._(username);
        %text.setProfile(%profileText);
    }
    else
    {
        %text = new GuiTextCtrl() {
           Profile = %profileText;
           position = "8 -1";
           Extent = "0 16";
           internalName = "username";
           VertSizing = "center";
        };
        %entry.add(%text);
    }

    %text.setText(%username);
    if (%found)
    {
        %text = %entry._(Score);
    }
    else
    {
        %text = new GuiTextCtrl() {
           Profile = GuiStatEntryScoreTextProfile;
           position = "190 -1";
           Extent = "70 16";
           internalName = "score";
           VertSizing = "center";
        };
        %entry.add(%text);
    }

    %text.setText(%score);
    return %entry;
}

function GameStatEntry::sortEntry(%this, %entry)
{
    %this.pushToBack(%entry);
    %count = %this.getCount() - 1;

    for (%i=0; %i < %count; %i++)
    {
        %testObj = %this.getObject(%i);
        if (%testObj._(Score).text < %entry._(Score).text)
        {
            %this.reorder(%entry, %testObj);
            break;
        }
        else
        {
            if (%testObj._(Score).text == %entry._(Score).text)
            {
                if (%entry.Profile $= %testObj.Profile)
                {
                    if (%testObj.UID > %entry.UID)
                    {
                        %this.reorder(%entry, %testObj);
                        break;
                    }
                }
                else
                {
                    if (%testObj.Profile $= GuiStatDisabledProfile)
                    {
                        %this.reorder(%entry, %testObj);
                        break;
                    }
                }
            }
        }
    }
}

