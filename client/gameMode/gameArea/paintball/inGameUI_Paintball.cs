
exec("./inGameUI_Paintball.gui");

function inGameUI_Paintball::onWake(%this)
{
    Parent::onWake(%this);
}

function inGameUI_Paintball::selectTool(%this, %toolID)
{
    commandToServer('GameInventory_EquipTool', %toolID);
}

function inGameUI_Paintball::GameInventory_Added(%this)
{
    %this.equippedTool = 0;
    
    for (%i=1; %i <= 5; %i++)
    {
        %this._("tool" @ %i).setBitmap("onverse/data/live_assets/engine/ui/images/icons/equipBar/paintball/paintball_slot_" @ %i @ "_inactive.png");
        %this._("tool" @ %i @ "_text").setText("");
    }
}

function inGameUI_Paintball::GameInventory_Removed(%this)
{
    for (%i=1; %i <= 5; %i++)
    {
        %this._("tool" @ %i).setBitmap("onverse/data/live_assets/engine/ui/images/icons/equipBar/paintball/paintball_slot_" @ %i @ "_inactive.png");
    }

    %this.equippedTool = "";
}

function inGameUI_Paintball::GameInventory_UpdateAmmo(%this, %toolID, %ammo)
{
    if (%this.equippedTool != %toolID.toolIdx)
    {
        %this._("tool" @ %toolID.toolIdx).setBitmap("onverse/data/live_assets/engine/ui/images/icons/equipBar/paintball/paintball_slot_" @ %toolID.toolIdx @ "_active.png");
    }
    else
    {
        %this._("tool" @ %toolID.toolIdx).setBitmap("onverse/data/live_assets/engine/ui/images/icons/equipBar/paintball/paintball_slot_" @ %toolID.toolIdx @ "_equiped.png");
    }

    %this._("tool" @ %toolID.toolIdx @ "_text").setText(%ammo >= 0 ? %ammo : "");
}

function inGameUI_Paintball::GameInventory_RemoveTool(%this, %toolID)
{
    %this._("tool" @ %toolID.toolIdx).setBitmap("onverse/data/live_assets/engine/ui/images/icons/equipBar/paintball/paintball_slot_" @ %toolID.toolIdx @ "_inactive.png");
}

function inGameUI_Paintball::GameInventory_ClearAll(%this)
{
    for (%i=1; %i <= 5; %i++)
    {
        %this._("tool" @ %i).setBitmap("onverse/data/live_assets/engine/ui/images/icons/equipBar/paintball/paintball_slot_" @ %i @ "_inactive.png");
        %this._("tool" @ %i @ "_text").setText("");
    }
    %this.equippedTool = "";
}

function inGameUI_Paintball::GameInventory_EquippedTool(%this, %toolID)
{
    if (!(%this.equippedTool $= ""))
    {
        %this._("tool" @ %this.equippedTool).setBitmap("onverse/data/live_assets/engine/ui/images/icons/equipBar/paintball/paintball_slot_" @ %this.equippedTool @ "_active.png");
    }
    %this.equippedTool = %toolID.toolIdx;
    %this._("tool" @ %this.equippedTool).setBitmap("onverse/data/live_assets/engine/ui/images/icons/equipBar/paintball/paintball_slot_" @ %this.equippedTool @ "_equiped.png");
}
