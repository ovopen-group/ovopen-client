
function ClientCmdGameInventory_Added()
{
    getCurrentInGameUI().GameInventory_Added();
}

function ClientCmdGameInventory_Removed()
{
    getCurrentInGameUI().GameInventory_Removed();
}

function ClientCmdGameInventory_UpdateAmmo(%toolID, %ammo)
{
    getCurrentInGameUI().GameInventory_UpdateAmmo(%toolID, %ammo);
}

function ClientCmdGameInventory_RemoveTool(%toolID)
{
    getCurrentInGameUI().GameInventory_RemoveTool(%toolID);
}

function ClientCmdGameInventory_ClearAll()
{
    getCurrentInGameUI().GameInventory_ClearAll();
}

function ClientCmdGameInventory_EquippedTool(%toolID)
{
    getCurrentInGameUI().GameInventory_EquippedTool(%toolID);
}
