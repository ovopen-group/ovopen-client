
function GameBaseData::sendCommand(%datablock, %obj, %cmd, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9, %arg10)
{
    commandToServer('IO_RPC_Command', %datablock, %obj.getGhostID(), %cmd, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9, %arg10);
}

function GameBaseData::handleRightClick(%datablock, %obj)
{
    if (!%obj.verifyPlayerInRange())
    {
        error("Cannot interact with object. Too far away.");
        return;
    }
    %datablock.showContextMenu(%obj);
}

function GameBaseData::doDefaultAction(%datablock, %obj, %menuText)
{
    if (!%obj.verifyPlayerInRange())
    {
        error("Cannot interact with object. Too far away.");
        return;
    }

    %menu = %datablock.getContextMenu(%obj);
    if (!isObject(%menu))
    {
        return;
    }

    %menu.var[0] = %datablock;
    %menu.var[1] = %obj;
    if (%menuText $= "")
    {
        %menuText = "default";
    }
    %menu.doDefault(%menuText);
}

function GameBaseData::showContextMenu(%datablock, %obj, %menuText)
{
    %menu = %datablock.getContextMenu(%obj);
    %menu.var[0] = %datablock;
    %menu.var[1] = %obj;
    if (%menuText $= "")
    {
        %menuText = "default";
    }
    %menu.showContextMenu(%menuText);
}

function GameBaseData::getContextMenu(%datablock, %obj)
{
    if (isObject(%datablock.contextMenu))
    {
        return %datablock.contextMenu;
    }

    %datablock.contextMenu = new GuiContextMenu() {
        Profile = "GuiMenuContextProfile";
        position = "0 0";
        Extent = "640 480";
        superClass = "GameBaseContext";
    };

    %datablock.constructContextMenu(%obj);
    return %datablock.contextMenu;
}

function GameBaseData::addContextItem(%datablock, %obj, %name, %cmd, %menuText)
{
    %menu = %datablock.getContextMenu(%datablock, %obj);
    return %menu.addContextItem(%name, %cmd, %menuText);
}

function GameBaseData::constructContextMenu(%datablock, %obj)
{
}

function GameBaseContext::addContextItem(%this, %name, %cmd, %menuText)
{
    return %this.addContextItemM(%menuText, %name, %cmd);
}

function GameBaseContext::addContextItemM(%this, %menuText, %name, %cmd)
{
    if (%menuText $= "")
    {
        %menuText = "default";
    }
    
    if (%this.itemid[%menuText] == 0)
    {
        %this.addMenu(%menuText, %menu.menuid++);
    }

    %this.addMenuItem(%menuText, %name, %this.itemid[%menuText]++, %cmd);
    return %this.itemid[%menuText];
}

function GameBaseContext::MenuClosed(%this)
{
    %this.Destroy();
}

function GameBaseData::logInteraction()
{
    return;
}

function ShapeBase::getTopControlObject(%this)
{
    %obj = %this;
    while (%obj.getControllingObject())
    {
        %obj = %obj.getControllingObject();
    }
    return %obj;
}

function ShapeBase::getTopObjectMount(%this)
{
    %obj = %this;
    while (%obj.getObjectMount())
    {
        %obj = %obj.getObjectMount();
    }
    return %obj;
}

function ShapeBase::getTopMountNodeObject(%this)
{
    %obj = %this;
    while (%obj.getMountNodeObject(0))
    {
        %obj = %obj.getMountNodeObject(0);
    }
    return %obj;
}
