// DSODIFF: double check
function clientCmdOnMountedObject(%ghostId, %mounted, %isTravel, %driving)
{
    %obj = LocationGameConnection.resolveGhostID(%ghostId);
    if (%driving)
    {
        if (%mounted)
        {
            driveModeActionMap.push();
        }
        else
        {
            driveModeActionMap.pop();
        }
    }
    
    $mvMounted = %mounted;
    if ((%isTravel && %obj) && (%obj.getControllingClient() != 0))
    {
        $mvMounted = 0;
    }
}

function MountablePlayerData::onMountNodeClick(%unused, %obj, %node)
{
    commandToServer('BoardTravel', %obj.getGhostID(), %node);
}

function MountableVehicleData::onMountNodeClick(%unused, %obj, %node)
{
    commandToServer('BoardTravel', %obj.getGhostID(), %node);
}

function VehicleData::showContextMenu(%unused, %obj, %unused)
{
    %obj = %obj.getTopControlObject();
    if (%obj != LocationGameConnection.GetPlayerObject())
    {
        %obj = %obj.getTopMountNodeObject();
        FriendList.ShowLocalContext(%obj.getShapeName());
    }
}
