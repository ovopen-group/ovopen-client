
function PlayerData::onRightClick(%datablock, %obj)
{
    %datablock.handleRightClick(%obj);
}

function PlayerData::showContextMenu(%datablock, %obj)
{
    %obj = %obj.getTopControlObject();
    if (%obj != LocationGameConnection.GetPlayerObject())
    {
        %obj = %obj.getTopMountNodeObject();
        FriendList.ShowLocalContext(%obj.getShapeName());
    }
}
