
$interactiveRange = 20;

function SceneObject::verifyPlayerInRange(%this)
{
    %vector = VectorSub(%this.getPosition(), LocationGameConnection.GetPlayerObject().getPosition());
    return VectorLen(%vector) < $interactiveRange;
}

function SceneObject::logInteraction()
{
	return;
}
