
function HomePointData::onPurchase(%datablock, %obj)
{
    if ($location::builder)
    {
        return MessageBoxOK("Builder", "Cannot purchase homes in a builder instance.");
    }

    if (!(%obj.ownerID) == 0)
    {
        return MessageBoxOK("Purchase", "Cannot purchase a home that is already owned by another user.");
    }
    PurchaseItem(%obj);
}

function HomePointData::onSell(%datablock, %obj)
{
    if ($location::builder)
    {
        return MessageBoxOK("Builder", "Cannot sell homes in a builder instance.");
    }

    if (%obj.ownerID != $currentPlayerID)
    {
        if ((%obj.ownerID == 0) || !MasterServerConnection.hasDeveloperLevel())
        {
            return MessageBoxOK("Purchase", "Cannot sell a home that is owned by another user.");
        }
    }
    SellItem(%obj);
}

function HomePointData::onToggle(%datablock, %obj, %toggle)
{
    if (%obj.ownerID != $currentPlayerID)
    {
        return MessageBoxOK("Error", "Cannot change lights in a home you do not own.");
    }
    %datablock.sendCommand(%obj, "toggle", %toggle);
}

function HomePointData::onPickupAllFurn(%datablock, %obj, %conf)
{
    if (%obj.ownerID != $currentPlayerID)
    {
        return MessageBoxOK("Error", "Cannot pickup furniture in a home you do not own.");
    }

    if (%conf $= "")
    {
        MessageBoxYesNo("You Sure?", "Are you sure you want to pickup all your furniture in this home?", %datablock @ ".onPickupAllFurn(" @ %obj @ ",1);", "");
    }
    else
    {
        %datablock.sendCommand(%obj, "pickupAllFurn");
    }
}

function HomePointData::getRightClickMenuText(%datablock, %obj)
{
    if (%obj.ownerID == 0)
    {
        return "open";
    }
    else
    {
        if (%obj.ownerID == $currentPlayerID)
        {
            return "owned_self";
        }
        else
        {
            return "owned_other";
        }
    }
}

function HomePointData::onRightClick(%datablock, %obj)
{
    if (!%obj.verifyPlayerInRange())
    {
        return error("Cannot interact with object. Too far away.");
    }
    
    %datablock.showContextMenu(%obj, %datablock.getRightClickMenuText(%obj));
}

function HomePointData::doDefaultAction(%datablock, %obj)
{
    Parent::doDefaultAction(%datablock, %obj, %datablock.getRightClickMenuText(%obj));
}

function HomePointData::constructContextMenu(%datablock, %obj)
{
    %menu = %datablock.getContextMenu(%datablock, %obj);
    %menu.addContextItemM("owned_other", "Info", "$tempVar[0].info($tempVar[1]);");
    if (MasterServerConnection.hasDeveloperLevel())
    {
        %menu.addContextItemM("owned_other", "ForceSell", "$tempVar[0].onSell($tempVar[1]);");
    }
    %menu.addContextItemM("owned_self", "Info", "$tempVar[0].info($tempVar[1]);");
    %menu.addContextItemM("owned_self", "-", "");
    %menu.addContextItemM("owned_self", "Sell", "$tempVar[0].onSell($tempVar[1]);");
    %menu.addContextItemM("owned_self", "Pickup Furniture", "$tempVar[0].onPickupAllFurn($tempVar[1]);");
    %menu.addContextItemM("owned_self", "-", "");
    %menu.addContextItemM("owned_self", "Power On", "$tempVar[0].onToggle($tempVar[1],true);");
    %menu.addContextItemM("owned_self", "Power Off", "$tempVar[0].onToggle($tempVar[1],false);");
    %menu.addContextItemM("open", "Purchase", "$tempVar[0].onPurchase($tempVar[1]);");
    %menu.addContextItemM("open", "-", "");
    %menu.addContextItemM("open", "Info", "$tempVar[0].info($tempVar[1]);");
}

function HomePointData::info(%datablock, %obj)
{
    InspectItem(%obj);
}

function HomePoint::GetDisplayName(%this, %unused)
{
    return %this.displayName;
}

function HomePoint::GetDescription(%this)
{
    return ParseDescriptionText(%this.desc);
}

function HomePoint::useCustomServerArgument(%this)
{
    return 1;
}

function HomePoint::GetStats(%this, %argument)
{
    %stats = "Home Name: " @ %this.homeName @ "<br>";
    %CC = %this.GetCCValue();
    %PP = %this.GetPPValue();
    
    if (%CC)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/cc.png>" SPC %CC @ " (" @ %this.GetCCValueVIP() @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)<br>";
    }
    
    if (%PP)
    {
        %stats = %stats @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/pp.png>" SPC %PP @ " (" @ %this.GetPPValueVIP() @ " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)<br>";
    }
    
    if (((%argument !$= "")) && (getWord(%argument, 2) > 0))
    {
        %stats = %stats @ "Multiple Housing Fee:" @ "<bitmap:onverse/data/live_assets/engine/ui/images/icons/cc.png>" SPC getWord(%argument, 1) @ "X" @ getWord(%argument, 0) @ "(" @ getWord(%argument, 2) @ ")<br>";
    }
    %stats = %stats @ "Max Furniture:" SPC %this.maxFurniture @ " (" @ mFloor(%this.maxFurniture * 1.2) SPC " <a:www.onverse.com/cashcoins/cashcoins.php>VIP</a>)<br>";
    
    if (%this.ownerID == 0)
    {
        %stats = %stats @ "Not owned";
    }
    else
    {
        if (%this.ownerID == $currentPlayerID)
        {
            %stats = %stats @ "This is your home!";
        }
        else
        {
            %stats = %stats @ "Owned by \'<a:www.onverse.com/profile/profile.php?id=" @ %this.ownerID @ ">" @ %this.ownerName @ "</a>\'";
        }
    }
    return %stats;
}

function HomePoint::PostPurchaseInspect(%this, %guiItem, %argument)
{
    if (((%argument !$= "")) && (getWord(%argument, 2) > 0))
    {
        %guiItem._("PP").setText(%guiItem._("PP").getText() SPC "+" SPC getWord(%argument, 2) @ "CC");
        %guiItem.PopText[PP] = %guiItem.PopText[PP] SPC "+" SPC getWord(%argument, 2) @ "CC";
        if (%guiItem.CC == 0)
        {
            %guiItem._("CC").setText(%guiItem._("CC").getText() SPC "+" SPC getWord(%argument, 2) @ "CC");
            %guiItem.PopText[CC] = %guiItem.PopText[CC] SPC "+" SPC getWord(%argument, 2) @ "CC";
        }
        else
        {
            %guiItem._("CC").setText("Purchase for" SPC %guiItem.CC + getWord(%argument, 2) @ "CC");
            %guiItem.PopText[CC] = %guiItem.CC + getWord(%argument, 2) @ "CC";
        }
    }
}

function HomePoint::GetCCValue(%this)
{
    return %this.CC;
}

function HomePoint::GetCCValueVIP(%this)
{
    return mFloor(%this.GetCCValue() * %this.GetVIPDiscount());
}

function HomePoint::GetAdjCCValue(%this)
{
    if (LocationGameConnection.getIsSubscriber())
    {
        return %this.GetCCValueVIP();
    }
    else
    {
        return %this.GetCCValue();
    }
}

function HomePoint::GetPPValue(%this)
{
    return %this.PP;
}

function HomePoint::GetPPValueVIP(%this)
{
    return mFloor(%this.GetPPValue() * %this.GetVIPDiscount());
}

function HomePoint::GetAdjPPValue(%this)
{
    if (LocationGameConnection.getIsSubscriber())
    {
        return %this.GetPPValueVIP();
    }
    else
    {
        return %this.GetPPValue();
    }
}

function HomePoint::GetDepreciation(%this)
{
    return 0.5;
}

function HomePoint::GetVIPDiscount(%this)
{
    return 0.80000001;
}

function HomePoint::GetPreview(%this, %unused, %bitView)
{
    %bitView.setBitmap(GetHousingIcon(%this.iconID));
}
