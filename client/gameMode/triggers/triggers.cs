
exec("./tutorialTextTrigger.cs");

function ClientCmdResolveURL(%url)
{
    resolveURL(%url);
}

$AreaTrigger_LastAreaName = "";
$StoreAreaTrigger_LastAreaName = "";

function ClientCmdonTriggerEvent(%cmd, %arg0, %arg1, %arg2, %unused, %unused)
{
    if (%cmd == 1)
    {
        if ($AreaTrigger_LastAreaName $= %arg0)
        {
        }
        else
        {
            $AreaTrigger_LastAreaName = %arg0;
            %ml = "<lmargin%:5><br>" @ "<font:Arial:15>Now Entering<br>" @ "<font:Arial:50>" @ %arg0 @ "<br>" @ "<font:Arial:18>" @ %arg1 @ "<br>";
            FadingText::start(AreaTriggerFadingTextGui, %ml);
        }
    }
    else
    {
        if (%cmd == 2)
        {
            if ($MusicTrigger::CallCount++ == 1)
            {
                GameMusic.FadeIn(%arg0);
            }
        }
        else
        {
            if (%cmd == 3)
            {
                %musicPlaylist = %arg2;
                if (strlen(%musicPlaylist) == 0)
                {
                    %musicPlaylist = "stores";
                }
                if ($MusicTrigger::CallCount++ == 1)
                {
                    GameMusic.FadeIn(%musicPlaylist);
                }
                if ($StoreAreaTrigger_LastAreaName $= %arg0)
                {
                }
                else
                {
                    $StoreAreaTrigger_LastAreaName = %arg0;
                    %ml = "<lmargin%:5><br>" @ "<font:Arial:15>Now Entering<br>" @ "<font:Arial:50>" @ %arg0 @ "<br>" @ "<font:Arial:18>" @ %arg1 @ "<br>";
                    FadingText::start(AreaTriggerFadingTextGui, %ml);
                }
            }
            else
            {
                if (%cmd == 4)
                {
                    showTutorialPopup(%arg0);
                }
            }
        }
    }
}

function ClientCmdoffTriggerEvent(%cmd, %arg0, %arg1, %arg2, %unused, %unused)
{
    if (%cmd == 2)
    {
        $MusicTrigger::CallCount--;
        if ($MusicTrigger::CallCount == 0)
        {
            GameMusic.FadeOut();
        }
    }
    else
    {
        if (%cmd == 3)
        {
            $MusicTrigger::CallCount--;
            if ($MusicTrigger::CallCount == 0)
            {
                GameMusic.FadeOut();
            }
        }
    }
}
