
exec("./tutorialTextBoxGUI.gui");

function showTutorialPopup(%textfile)
{
    %text = ParseDescriptionText(%textfile);
    tutorialTextBoxGUI._(MLTextCtrl).setText(%text);
    Canvas.pushDialog(tutorialTextBoxGUI);
}
