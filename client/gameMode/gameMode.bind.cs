
if (isObject(gameModeActionMap))
{
    gameModeActionMap.delete();
}

new ActionMap(gameModeActionMap) {
};

if (isObject(playerModeActionMap))
{
    playerModeActionMap.delete();
}

new ActionMap(playerModeActionMap) {
};

function escapeFromGame()
{
    MessageBoxYesNo("Disconnect", "Leave to the Main Menu?", "LocationSystem::disconnect(true);", "");
}

function toggleTool()
{
    getCurrentInGameUI().toggleActionMode();
}

function selectTool(%toolID)
{
    getCurrentInGameUI().selectTool(%toolID);
}

function toggleScope()
{
    $mvScopeWeapon = !$mvScopeWeapon;
}

gameModeActionMap.bindCmd(keyboard, tab, "toggleTool();", "");
gameModeActionMap.bindCmd(keyboard, 1, "selectTool(1);", "");
gameModeActionMap.bindCmd(keyboard, 2, "selectTool(2);", "");
gameModeActionMap.bindCmd(keyboard, 3, "selectTool(3);", "");
gameModeActionMap.bindCmd(keyboard, 4, "selectTool(4);", "");
gameModeActionMap.bindCmd(keyboard, 5, "selectTool(5);", "");
gameModeActionMap.bindCmd(keyboard, 6, "selectTool(6);", "");
gameModeActionMap.bindCmd(keyboard, 7, "selectTool(7);", "");
gameModeActionMap.bindCmd(keyboard, 8, "selectTool(8);", "");
gameModeActionMap.bindCmd(keyboard, 9, "selectTool(9);", "");
gameModeActionMap.bindCmd(keyboard, 0, "selectTool(0);", "");
gameModeActionMap.bindCmd(keyboard, numpad1, "selectTool(1);", "");
gameModeActionMap.bindCmd(keyboard, numpad2, "selectTool(2);", "");
gameModeActionMap.bindCmd(keyboard, numpad3, "selectTool(3);", "");
gameModeActionMap.bindCmd(keyboard, numpad4, "selectTool(4);", "");
gameModeActionMap.bindCmd(keyboard, numpad5, "selectTool(5);", "");
gameModeActionMap.bindCmd(keyboard, numpad6, "selectTool(6);", "");
gameModeActionMap.bindCmd(keyboard, numpad7, "selectTool(7);", "");
gameModeActionMap.bindCmd(keyboard, numpad8, "selectTool(8);", "");
gameModeActionMap.bindCmd(keyboard, numpad9, "selectTool(9);", "");
gameModeActionMap.bindCmd(keyboard, numpad0, "selectTool(0);", "");
gameModeActionMap.bindCmd(keyboard, Z, "toggleScope();", "");

function toggleFirstPerson(%val)
{
    if (%val)
    {
        %firstPerson = LocationGameConnection.isFirstPerson();
        LocationGameConnection.setFirstPerson(!%firstPerson);
        if (%firstPerson && (LocationGameConnection.getTargetZoom() == 0))
        {
            LocationGameConnection.setTargetZoom(0.25);
        }
    }
}

function fullzoomOutCamera(%val)
{
    if (%val)
    {
        zoomCam(-1);
        LocationGameConnection.setTargetZoom(1);
    }
}

function zoomInCamera(%val)
{
    if (%val)
    {
        zoomCam(1);
    }
}

function zoomOutCamera(%val)
{
    if (%val)
    {
        zoomCam(-1);
    }
}

gameModeActionMap.bind(keyboard, home, toggleFirstPerson);
gameModeActionMap.bind(keyboard, end, fullzoomOutCamera);
gameModeActionMap.bind(keyboard, pageup, zoomInCamera);
gameModeActionMap.bind(keyboard, pagedown, zoomOutCamera);

function unmount_look(%val)
{
    if (%val)
    {
        $mvFreeLook = 0;
        unmount(%val);
    }
}

function unmount(%val)
{
    if (%val)
    {
        if ($mvMounted)
        {
            commandToServer('DismountObject');
        }
    }
}

function ToggleAutoMove(%val)
{
    if (%val)
    {
        if ($mvForwardAction != 0)
        {
            $mvForwardAction = 0;
        }
        else
        {
            $mvFreeLook = 0;
            $mvForwardAction = $movementSpeed;
        }
    }
}

function walkMode(%val)
{
    walkModeToggle(1);
}

function walkModeToggle(%val)
{
    if (%val)
    {
        if ($movementSpeed > 0.9)
        {
            $movementSpeed = 0.5;
            $mvLeftAction *= 0.5;
            $mvRightAction *= 0.5;
            $mvForwardAction *= 0.5;
            $mvBackwardAction *= 0.5;
        }
        else
        {
            $movementSpeed = 1;
            $mvLeftAction *= 2;
            $mvRightAction *= 2;
            $mvForwardAction *= 2;
            $mvBackwardAction *= 2;
        }
    }
}

$movementSpeed = 1;

function setSpeed(%speed)
{
    if (%speed)
    {
        $movementSpeed = %speed;
    }
}

function nullmove(%val)
{
}

function moveleft(%val)
{
    unmount_look(%val);
    $mvLeftAction = %val * $movementSpeed;
}

function moveright(%val)
{
    unmount_look(%val);
    $mvRightAction = %val * $movementSpeed;
}

function moveforward(%val)
{
    unmount_look(%val);
    $mvForwardAction = %val * $movementSpeed;
}

function zoomCam(%val)
{
    if (((LocationGameConnection.getControlObject().getClassName() !$= "Camera")) && ((LocationGameConnection.getControlObject().getClassName() !$= "ClientEditCamera")))
    {
        if (%val > 0)
        {
            LocationGameConnection.setTargetZoom(LocationGameConnection.getTargetZoom() / 2);
            if (LocationGameConnection.getTargetZoom() < 0.03124)
            {
                LocationGameConnection.setTargetZoom(0);
            }
        }
        else
        {
            if (LocationGameConnection.isFirstPerson())
            {
                LocationGameConnection.setFirstPerson(0);
                LocationGameConnection.setTargetZoom(0.03125);
            }
            else
            {
                LocationGameConnection.setTargetZoom(LocationGameConnection.getTargetZoom() * 2);
            }
        }
        return;
    }
    $mvZoomAction = -1 * %val;
}

function movebackward(%val)
{
    unmount_look(%val);
    $mvForwardAction = 0;
    $mvBackwardAction = %val * $movementSpeed;
}

function moveup(%val)
{
    unmount_look(%val);
    $mvUpAction = %val * $movementSpeed;
}

function movedown(%val)
{
    unmount_look(%val);
    $mvDownAction = %val * $movementSpeed;
}

function turnLeft(%val)
{
    unmount_look(%val);
    $mvYawRightSpeed = %val ? $pref::Input::KeyboardTurnSpeed : 0;
    $mvYawRightCoefficient = 0;
}

function turnRight(%val)
{
    unmount_look(%val);
    $mvYawLeftSpeed = %val ? $pref::Input::KeyboardTurnSpeed : 0;
    $mvYawLeftCoefficient = 0;
}

function panUp(%val)
{
    unmount_look(%val);
    $mvPitchDownSpeed = %val ? $pref::Input::KeyboardTurnSpeed : 0;
    $mvPitchDownCoefficient = 0;
}

function panDown(%val)
{
    unmount_look(%val);
    $mvPitchUpSpeed = %val ? $pref::Input::KeyboardTurnSpeed : 0;
    $mvPitchUpCoefficient = 0;
}

function getMouseAdjustAmount(%val)
{
    return ((%val * ($cameraFov / 90)) * 0.01) * $pref::Input::LinkMouseSensitivity;
}

function yaw(%val)
{
    $mvYaw += getMouseAdjustAmount(%val);
}

function pitch(%val)
{
    $mvPitch += getMouseAdjustAmount(%val);
}

function jump(%val)
{
    unmount(%val);
    $mvTriggerCount2++;
}

function trigger3(%val)
{
    $mvTriggerCount3++;
}

function trigger4(%val)
{
    $mvTriggerCount4++;
}

function trigger5(%val)
{
    $mvTriggerCount5++;
}

function trigger6(%val)
{
    $mvTriggerCount6++;
}

function joyForward(%val)
{
    unmount_look(%val);
    if (mAbs(%val) < 0.25)
    {
        %val = 0;
    }
    $mvForwardAction = 0;
    $mvBackwardAction = %val * $movementSpeed;
}

function joyTurn(%val)
{
    unmount_look(%val);
    $mvYawRightSpeed = 0;
    $mvYawRightCoefficient = 0;
    $mvYawLeftSpeed = $pref::Input::KeyboardTurnSpeed * %val;
    $mvYawLeftCoefficient = 0;
}

function LockedMoveLeft(%val)
{
    if (mouseLocked())
    {
        moveleft(%val);
        turnLeft(0);
    }
    else
    {
        turnLeft(%val);
        moveleft(0);
    }
}

function LockedMoveRight(%val)
{
    if (mouseLocked())
    {
        moveright(%val);
        turnRight(0);
    }
    else
    {
        turnRight(%val);
        moveright(0);
    }
}

function hideUI(%val)
{
    if (%val)
    {
        if (getCurrentInGameUI().isAwake())
        {
            getCurrentInGameUI().toggleUI();
        }
        else
        {
            if (WindowBar.isAwake())
            {
                Canvas.popDialog(WindowBar);
            }
            else
            {
                Canvas.pushDialog(WindowBar);
            }
        }
    }
}

function hideNames(%val)
{
    if (%val)
    {
        if (getCurrentInGameUI().isAwake())
        {
            getCurrentInGameUI().toggleNames();
        }
    }
}

gameModeActionMap.bind(keyboard, F3, hideUI);
gameModeActionMap.bind(keyboard, F4, hideNames);
gameModeActionMap.bind(keyboard, a, LockedMoveLeft);
gameModeActionMap.bind(keyboard, d, LockedMoveRight);
gameModeActionMap.bind(keyboard, q, moveleft);
gameModeActionMap.bind(keyboard, e, moveright);
gameModeActionMap.bind(keyboard, w, moveforward);
gameModeActionMap.bind(keyboard, s, movebackward);
gameModeActionMap.bind(keyboard, r, walkModeToggle);
gameModeActionMap.bind(keyboard, rshift, walkMode);
gameModeActionMap.bind(keyboard, lshift, walkMode);
gameModeActionMap.bind(keyboard, space, jump);
gameModeActionMap.bind(mouse, xaxis, yaw);
gameModeActionMap.bind(mouse, yaxis, pitch);
gameModeActionMap.bind(keyboard, left, LockedMoveLeft);
gameModeActionMap.bind(keyboard, right, LockedMoveRight);
gameModeActionMap.bind(keyboard, up, moveforward);
gameModeActionMap.bind(keyboard, down, movebackward);
gameModeActionMap.bind(keyboard, numlock, ToggleAutoMove);
gameModeActionMap.bindCmd(keyboard, "escape", "", "escapeFromGame();");
playerModeActionMap.bind(mouse, zaxis, zoomCam);

function mouseFire(%val)
{
    $mvTriggerCount0++;
}

function altTrigger(%val)
{
    $mvTriggerCount1++;
}

gameModeActionMap.bind(mouse, button0, mouseFire);
gameModeActionMap.bindCmd(keyboard, t, "", "commandToServer(\'ToggleTravel\');");

if (isObject(driveModeActionMap))
{
    driveModeActionMap.delete();
}

new ActionMap(driveModeActionMap) {
};

$turnspeed = 6;

function incVehicleTurn(%type, %dir)
{
    %maxSteering = 1;
    %turn = (%dir * $turnspeed) * 0.01;
    $mv[%type] += %turn;
    $vechTurn::steer[%type] += %turn;
    
    if ($vechTurn::steer[%type] > 1)
    {
        $vechTurn::steer[%type] = 1;
    }

    if ($vechTurn::steer[%type] < -1)
    {
        $vechTurn::steer[%type] = -1;
    }

    $vechTurn::sched[%type] = schedule(32, 0, "incVehicleTurn", %type, %dir);
}

function VehicleTurn(%val, %type, %dir)
{
    if (%val)
    {
        $mv[%type] = 0;
        cancel($vechTurn::sched[%type]);
        $vechTurn::sched[%type] = schedule(32, 0, "incVehicleTurn", %type, %dir);
    }
    else
    {
        cancel($vechTurn::sched[%type]);
        if ($vechTurn::steer[%type] != 0)
        {
            $mv[%type] = -$vechTurn::steer[%type];
            $vechTurn::steer[%type] = 0;
        }
    }
}

function VehicleTurnLeft(%val)
{
    $mvYawRightSpeed = %val ? $pref::Input::KeyboardTurnSpeed : 0;
    $mvYawRightCoefficient = %val ? $Pref::Input::KeyboardTurnSpeedCoeff : 0;
}

function VehicleTurnRight(%val)
{
    $mvYawLeftSpeed = %val ? $pref::Input::KeyboardTurnSpeed : 0;
    $mvYawLeftCoefficient = %val ? $Pref::Input::KeyboardTurnSpeedCoeff : 0;
}

function VehiclePitchUp(%val)
{
    VehicleTurn(%val, pitch, 1);
}

function VehiclePitchDown(%val)
{
    VehicleTurn(%val, pitch, -1);
}

function CarTurnJoy(%val)
{
    if (%val < -0.25)
    {
        VehicleTurnLeft(%val);
    }
    else
    {
        if (%val > 0.25)
        {
            VehicleTurnRight(%val);
        }
        else
        {
            VehicleTurnLeft(0);
            VehicleTurnRight(0);
        }
    }
}

function yaw_driving(%val)
{
    $mvYaw += (getMouseAdjustAmount(%val) / 16);
}

driveModeActionMap.bind(mouse, xaxis, nullmove);
driveModeActionMap.bind(mouse, yaxis, nullmove);
driveModeActionMap.bind(keyboard, space, trigger3);
driveModeActionMap.bind(keyboard, left, VehicleTurnLeft);
driveModeActionMap.bind(keyboard, right, VehicleTurnRight);
driveModeActionMap.bind(keyboard, a, VehicleTurnLeft);
driveModeActionMap.bind(keyboard, d, VehicleTurnRight);
driveModeActionMap.bind(joystick, xaxis, CarTurnJoy);

if (isObject(windowBarActionMap))
{
    windowBarActionMap.delete();
}

new ActionMap(windowBarActionMap) {
};

windowBarActionMap.bindCmd(keyboard, g, "", "ShowAnimationDialog();");
windowBarActionMap.bindCmd(keyboard, m, "", "ShowDirectory();");
windowBarActionMap.bindCmd(keyboard, o, "", "ShowOptions();");
windowBarActionMap.bindCmd(keyboard, f, "", "ShowFriendsList();");
windowBarActionMap.bindCmd(keyboard, i, "", "ShowInventoryDialog();");
windowBarActionMap.bindCmd(keyboard, c, "", "ShowEquipmentBar();");
windowBarActionMap.bindCmd(keyboard, h, "", "showHomeList();");
windowBarActionMap.bindCmd(keyboard, "ctrl enter", "", "MainChatHUD.show();");
windowBarActionMap.bindCmd(keyboard, enter, "", "MainChatHUD.focus();");
windowBarActionMap.bindCmd(keyboard, slash, "", "MainChatHUD.setCommand(\"/\");");
windowBarActionMap.bindCmd(keyboard, backspace, "", "MainChatHUD.reply();");

function selectChatTab(%tabID)
{
    MainChatHUD.selectTab(%tabID);
}

windowBarActionMap.bindCmd(keyboard, "ctrl 1", "selectChatTab(0);", "");
windowBarActionMap.bindCmd(keyboard, "ctrl 2", "selectChatTab(1);", "");
windowBarActionMap.bindCmd(keyboard, "ctrl 3", "selectChatTab(2);", "");
windowBarActionMap.bindCmd(keyboard, "ctrl 4", "selectChatTab(3);", "");
windowBarActionMap.bindCmd(keyboard, "ctrl 5", "selectChatTab(4);", "");
windowBarActionMap.bindCmd(keyboard, "ctrl 6", "selectChatTab(5);", "");
windowBarActionMap.bindCmd(keyboard, "ctrl 7", "selectChatTab(6);", "");
windowBarActionMap.bindCmd(keyboard, "ctrl numpad1", "selectChatTab(0);", "");
windowBarActionMap.bindCmd(keyboard, "ctrl numpad2", "selectChatTab(1);", "");
windowBarActionMap.bindCmd(keyboard, "ctrl numpad3", "selectChatTab(2);", "");
windowBarActionMap.bindCmd(keyboard, "ctrl numpad4", "selectChatTab(3);", "");
windowBarActionMap.bindCmd(keyboard, "ctrl numpad5", "selectChatTab(4);", "");
windowBarActionMap.bindCmd(keyboard, "ctrl numpad6", "selectChatTab(5);", "");
windowBarActionMap.bindCmd(keyboard, "ctrl numpad7", "selectChatTab(6);", "");

