
exec("./inspectGUI.gui");

function inspectGUI::onWake(%this)
{
    %this._(inspectHost).add(inspectContentGUI);
    inspectContentGUI.inspect(%this.target, %this.argument);
}

function inspectContentGUI::inspect(%this, %target, %argument)
{
    %this._(objView).setEmpty();
    %this._(bitView).setBitmap("");
    %target.GetPreview(%this._(objView), %this._(bitView));
    %this._(descText).setText(%target.GetDescription());
    %colors = "FFFFFF" SPC "2A00FE" SPC "FEB300" SPC "9c0000" SPC "FFF600";
    %stats = "<font:Arial Bold:16>";
    %stats = %stats @ "<spush><font:Arial Bold:22>" @ %target.GetDisplayName(%colors) @ "<spop><br>";
    %stats = %stats @ %target.GetStats(%argument);
    %this._(statText).setText(%stats);
    %target.logInteraction();
}

function inspectContentGUI::loading(%this, %target)
{
    %this._(objView).setEmpty();
    %this._(bitView).setBitmap("");
    %target.GetPreview(%this._(objView), %this._(bitView));
    %this._(descText).setText(%target.GetDescription());
    %stats = "<font:Arial Bold:16>";
    %stats = %stats @ "<spush><font:Arial Bold:22>" @ %target.GetDisplayName(%colors) @ "<spop><br>";
    %stats = %stats @ "Loading...";
    %this._(statText).setText(%stats);
}

function InspectItem(%itemId, %argument)
{
    inspectGUI.target = %itemId;
    inspectGUI.argument = %argument;
    Canvas.pushDialog(inspectGUI);
}

function ParseDescriptionText(%filename)
{
    if (%filename $= "")
    {
        return "";
    }
    
    %desc = "";

    %file = new FileObject() {
    };
    
    if (%file.openForRead(%filename))
    {
        while (!%file.isEOF())
        {
            %desc = %desc @ %file.readLine() @ "<br>";
        }
        %file.close();
    }
    %file.delete();

    return %desc;
}
