
if (!isObject(purchaseGUI))
{
    exec("./purchaseGUI.gui");
}

new AudioProfile(SuccessPurchaseAudio) {
   description = "AudioMessage";
   preload = "1";
   fileName = "onverse/data/live_assets/sounds/objects/coin_grab.ogg";
};

function purchaseGUI::onWake(%this)
{
    %this._(inspectHost).add(inspectContentGUI);
    if (%this.CustomArgSeqID $= "")
    {
        %this.CustomArgSeqID = 0;
    }

    %this.CustomArgSeqID++;
    if (((%this.mode == 1) && (%this.target.isMethod(useCustomServerArgument)) && %this.target.useCustomServerArgument()))
    {
        commandToServer('GetCustomPurchaseArgument', %this.target.getGhostID(), %this.CustomArgSeqID);
        inspectContentGUI.loading(%this.target);
        %this._("CC").setVisible(0);
        %this._("PP").setVisible(0);
    }
    else
    {
        %this.inspect(%this.target, %this.mode, %this.argument);
    }
}

function purchaseGUI::inspect(%this, %target, %mode, %argument)
{
    inspectContentGUI.inspect(%target, %argument);
    if (%mode == 3)
    {
        %CC = 0;
        %PP = 0;
    }
    else
    {
        if (%mode == 2)
        {
            %cverb = Sell;
            %CC = mFloor(%target.GetCCValue() * %target.GetDepreciation());
            %PP = mFloor(%target.GetPPValue() * %target.GetDepreciation());
        }
        else
        {
            %cverb = Purchase;
            %CC = %target.GetAdjCCValue();
            %PP = %target.GetAdjPPValue();
        }
    }

    %this._("CC").setVisible(1);
    %this._("PP").setVisible(1);
    %this._("CC").setText(%cverb SPC "for" SPC %CC SPC "CC");
    %this._("PP").setText(%cverb SPC "for" SPC %PP SPC "PP");

    if (%PP == 0)
    {
        %this._("PP").setVisible(0);
        if (%CC == 0)
        {
            if (%mode == 2)
            {
                %this._("CC").setText(%cverb SPC "for NOTHING");
            }
            else
            {
                %this._("CC").setText(%cverb SPC "for FREE");
            }
        }
        else
        {
            %this._("CC").setText(%cverb SPC "for " @ %CC @ "CC");
        }
    }
    else
    {
        if (%CC == 0)
        {
            %this._("CC").setVisible(0);
        }
    }

    if (%mode == 3)
    {
        %this.verb = "Pickup";
        %this.amtWord = "FREE";
        %this.cmd = %this @ ".ProcessPurchase();";
        %this._("CC").setText("Pickup");
    }
    else
    {
        if (%mode == 2)
        {
            %this.verb = "sell";
            %this.amtWord = "NOTHING";
            %this.cmd = %this @ ".ProcessSell();";
        }
        else
        {
            %this.verb = "purchase";
            %this.amtWord = "FREE";
            %this.cmd = %this @ ".ProcessPurchase();";
        }
    }

    %this.Item = %target;
    %this.name = %target.GetDisplayName("");
    %this.CC = %CC;
    %this.PP = %PP;

    if (%this.CC != 0)
    {
        %this.PopText[CC] = %this.CC SPC "CC";
    }
    else
    {
        %this.PopText[CC] = %this.amtWord;
    }

    if (%this.PP != 0)
    {
        %this.PopText[PP] = %this.PP SPC "PP";
    }
    else
    {
        %this.PopText[PP] = %this.amtWord;
    }

    if (%mode == 1)
    {
        if (%this.Item.isMethod(PostPurchaseInspect))
        {
            %this.Item.PostPurchaseInspect(%this, %argument);
        }
    }
}

function purchaseGUI::ProcessPurchase(%this)
{
    echo("Purchasing \'" @ %this.Item @ "\' for" SPC %this.PopText[%this.currency]);
    Canvas.popDialog(%this);
    if (!isObject(%this.Item))
    {
        error("Item no longer exists.");
        return;
    }
    %type = 0;
    if (%this.currency $= "CC")
    {
        %type = 1;
    }
    if (%this.currency $= "PP")
    {
        %type = 2;
    }
    if (!%type)
    {
        error("Currency type invalid.");
        return;
    }

    commandToServer('PurchaseItem', %this.Item.getGhostID(), %type);
}

function purchaseGUI::ProcessSell(%this)
{
    echo("Selling \'" @ %this.Item @ "\' for" SPC %this.PopText[%this.currency]);
    Canvas.popDialog(%this);
    %type = 0;

    if (%this.currency $= "CC")
    {
        %type = 1;
    }
    if (%this.currency $= "PP")
    {
        %type = 2;
    }
    if (!%type)
    {
        error("Currency type invalid.");
        return;
    }

    if (%this.uniqueid == 0)
    {
        commandToServer('SellItem', %this.Item.getGhostID(), %type);
    }
    else
    {
        commandToServer('SellInvItem', %this.Item.getId(), %type, %this.uniqueid);
    }
}

function PurchaseItem(%itemId)
{
    purchaseGUI._("window").setText("Purchase");
    purchaseGUI.target = %itemId;
    purchaseGUI.mode = 1;
    purchaseGUI.uniqueid = 0;
    purchaseGUI.argument = "";
    Canvas.pushDialog(purchaseGUI);
}

function SellItem(%itemId)
{
    purchaseGUI._("window").setText("Sell");
    purchaseGUI.target = %itemId;
    purchaseGUI.mode = 2;
    purchaseGUI.uniqueid = 0;
    purchaseGUI.argument = "";
    Canvas.pushDialog(purchaseGUI);
}

function GiftItem(%itemId)
{
    purchaseGUI._("window").setText("You\'ve found an item!");
    purchaseGUI.target = %itemId;
    purchaseGUI.mode = 3;
    purchaseGUI.uniqueid = 0;
    purchaseGUI.argument = "";
    Canvas.pushDialog(purchaseGUI);
}

function SellInvItem(%itemId, %uniqueID, %argument)
{
    purchaseGUI._("window").setText("Sell");
    purchaseGUI.target = %itemId;
    purchaseGUI.mode = 2;
    purchaseGUI.uniqueid = %uniqueID;
    purchaseGUI.argument = %argument;
    Canvas.pushDialog(purchaseGUI);
}

function ConfirmPurchase(%currency)
{
    if ((%currency !$= "CC") && (%currency !$= "PP"))
    {
        return;
    }
    
    purchaseGUI.currency = %currency;
    %val = purchaseGUI.getFieldValue(%currency);
    
    if (purchaseGUI.mode == 3)
    {
        eval(purchaseGUI.cmd @ "(CC);");
        return;
    }

    %message = "Are you sure you would like to" SPC purchaseGUI.verb SPC "\'" @ purchaseGUI.name @ "\' for" SPC purchaseGUI.PopText[%currency] @ "?";
    MessageBoxYesNo("Confirm", %message, purchaseGUI.cmd, "");
}

function ClientCmdReturnCustomPurchaseArgument(%retVal, %text, %seqid)
{
    if (%retVal == -1)
    {
        error("ClientCmdReturnCustomPurchaseArgument: -1");
        return;
    }

    if (%seqid == purchaseGUI.CustomArgSeqID)
    {
        %argument = %text;
        if (!(purchaseGUI.argument $= ""))
        {
            %argument = purchaseGUI.argument SPC %argument;
        }
        purchaseGUI.inspect(purchaseGUI.target, purchaseGUI.mode, %argument);
    }
    else
    {
        error("ClientCmdReturnCustomPurchaseArgument: Invalid seqID");
    }
    return;
}

function ClientCmdTransactionError(%code, %message)
{
    error("Transaction Error:" @ %code @ " - \'" @ %message @ "\'");

    if ((%code == 4) || (%code == 8))
    {
        if (%code == 4)
        {
            %message = "<just:left><bitmap:onverse/data/live_assets/engine/ui/images/icons/pp.png><just:center>You need more player points!<br>" @ "To find out how to get more visit<br><a:www.onverse.com/help/Game/Shopping.php>Game Help</a>";
        }
        else
        {
            %message = "<just:left><bitmap:onverse/data/live_assets/engine/ui/images/icons/cc.png><just:center>You need more cash coins!<br>" @ "To get more visit the <a:www.onverse.com/cashcoins/cashcoins.php>Cash Coins Page</a>";
        }
    }
    else
    {
        %message = "Error Code:" @ %code NL %message;
    }

    MessageBoxOK("Transaction Error", %message);
    alxPlay(wrongToolSound);
}

function ClientCmdTransactionComplete(%message, %textfile)
{
    echo("Transaction Complete - " @ %message);
    MainChatHUD.onServerMessage(%message);

    if (!(%textfile $= ""))
    {
        showTutorialPopup(%textfile);
    }

    alxPlay(SuccessPurchaseAudio);
}
