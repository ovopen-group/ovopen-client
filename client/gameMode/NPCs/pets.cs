
function AIPetData::canShowRightClick(%datablock, %this)
{
    return 1;
}

function AIPetData::doDefaultAction(%datablock, %obj, %menuText)
{
    GameBaseData::doDefaultAction(%datablock, %obj, %menuText);
}

function AIPetData::onRightClick(%datablock, %obj)
{
    %datablock.handleRightClick(%obj);
}

function AIPetData::showContextMenu(%datablock, %obj)
{
    GameBaseData::showContextMenu(%datablock, %obj);
}

function AIPetData::constructContextMenu(%datablock, %obj)
{
    if (%obj.getClassName() $= "PurchaseAIPet")
    {
        %obj.constructContextMenu(%datablock);
    }

    %menu = %datablock.getContextMenu(%obj);
    %menu.addContextItem("Info", "$tempVar[0].info($tempVar[1]);");
    %menu.addContextItem("-", "");
    %menu.addContextItem("Pet", "$tempVar[0].pet($tempVar[1]);");
    
    if (%datablock.numTrickAnimations > 0)
    {
        %menu.addContextItem("-", "");
        for (%i=0; %i < %datablock.numTrickAnimations; %i += 1)
        {
            %menu.addContextItem("Trick" SPC %i + 1, "$tempVar[0].trick($tempVar[1]," @ %i @ ");");
        }
    }
    
    if (%obj.ownerID == $currentPlayerID)
    {
        %menu.addContextItem("-", "");
        %menu.addContextItem("Pickup", "$tempVar[0].pickup($tempVar[1]);");
        %menu.addContextItem("Rename", "$tempVar[0].rename($tempVar[1]);");
        if (MasterServerConnection.hasAdminLevel())
        {
            %menu.addContextItem("Mount", "$tempVar[0].mount($tempVar[1]);");
        }
    }
}

function AIPetData::pet(%datablock, %obj)
{
    %datablock.sendCommand(%obj, "pet");
}

function AIPetData::trick(%datablock, %obj, %i)
{
    %datablock.sendCommand(%obj, "trick", %i);
}

function AIPetData::pickup(%datablock, %obj)
{
    commandToServer('EquipPet', 0);
}

function AIPetData::mount(%datablock, %obj)
{
    commandToServer('MountPet');
}

function AIPetData::rename(%datablock, %obj)
{
    $renameObjID = %obj;
    MessageBoxEntryOk("Rename Pet", "Enter a new name for your pet", %datablock @ "._finishRename", %obj.getShapeName());
}

function AIPetData::_finishRename(%datablock, %objText)
{
    %objText = getSubStr(%objText, 0, 31);
    %datablock.sendCommand($renameObjID, "rename", %objText);
}

function AIPetData::info(%datablock, %obj)
{
    InspectItem(%obj);
}

function AIPet::updateFriendState(%this)
{
    %this.friendState = 126;
}
