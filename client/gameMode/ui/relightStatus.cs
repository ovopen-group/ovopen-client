
exec("./relightStatusDlg.gui");

function RelightScene(%filtered, %callback, %mode)
{
    RelightStatusDlg.callback = %callback;
    Canvas.pushDialog(RelightStatusDlg);
    runLighting(%filtered, %mode);
}

function runLighting(%filtered, %mode)
{
    $lightingSch = 0;
    $SceneLighting::sgFilterRelight = %filtered;
    
    if (lightScene("sceneLightingComplete", %mode))
    {
        Canvas.pushDialog(RelightStatusDlg);
        $lightingSch = schedule(32, 0, "updateLightingProgress");
    }
}

function updateLightingProgress()
{
    RelightStatusDlg._(progress).setValue($SceneLighting::lightingProgress);
    $lightingSch = schedule(32, 0, "updateLightingProgress");
}

function sceneLightingComplete()
{
    if ($lightingSch != 0)
    {
        cancel($lightingSch);
    }

    Canvas.popDialog(RelightStatusDlg);
    if (!(RelightStatusDlg.callback $= ""))
    {
        eval(RelightStatusDlg.callback);
    }

    if (!MasterServerConnection.active)
    {
        MasterServerConnection.schedule(32, onDropped);
    }
}

function RelightWithAmbientOcclusion()
{
    $oldQuality = $pref::LightManager::sgLightingProfileQuality;
    $pref::LightManager::sgLightingProfileQuality = 0;
    $SceneLighting::sgRelightAmbientOcclusion = 1;
    RelightScene(0, "DoneWithAmbientOcclusion();", "forceAlways");
}

function DoneWithAmbientOcclusion()
{
    $pref::LightManager::sgLightingProfileQuality = $oldQuality;
    $SceneLighting::sgRelightAmbientOcclusion = 0;
}
