
exec("./friendList.gui");

new GuiContextMenu(friendsListContextMenu) {
   Profile = "GuiMenuContextProfile";
   position = "0 0";
   Extent = "640 480";
};

function friendsListContextMenu::Initialize(%this)
{
    if (!%this.initialized)
    {
        %menuId = 0;
        %itemId = 0;
        %this.addMenu("online", %menuId++);
        %this.addMenuItem("online", "Message", %itemId++, "MainChatHUD.setCommand(\"/Tell \\\"\" @ $tempVar[0] @ \"\\\" \");");
        %itemId = %this.addGuideMenu("online", %itemId);
        %this.addMenuItem("online", "Profile (Web)", %itemId++, "gotoWebProfile($tempVar[0]);");
        %this.addMenuItem("online", "Teleport", %itemId++, "TeleportToPlayer($tempVar[0]);");
        %this.addMenuItem("online", "Home", %itemId++, "TeleportToHome($tempVar[0]);");
        %this.addMenuItem("online", "Ignore", %itemId++, "IgnoreUser($tempVar[0]);");
        %this.addMenuItem("online", "-", 0);
        %this.addMenuItem("online", "Remove", %itemId++, "DeleteFriend($tempVar[0]);");
        %itemId = 0;
        %this.addMenu("offline", %menuId++);
        %itemId = %this.addGuideMenuOffline("offline", %itemId);
        %this.addMenuItem("offline", "Profile (Web)", %itemId++, "gotoWebProfile($tempVar[0]);");
        %this.addMenuItem("offline", "Home", %itemId++, "TeleportToHome($tempVar[0]);");
        %this.addMenuItem("offline", "Ignore", %itemId++, "IgnoreUser($tempVar[0]);");
        %this.addMenuItem("offline", "-", 0);
        %this.addMenuItem("offline", "Remove", %itemId++, "DeleteFriend($tempVar[0]);");
        %itemId = 0;
        %this.addMenu("accepting", %menuId++);
        %this.addMenuItem("accepting", "Message", %itemId++, "MainChatHUD.setCommand(\"/Tell \\\"\" @ $tempVar[0] @ \"\\\" \");");
        %itemId = %this.addGuideMenu("accepting", %itemId);
        %this.addMenuItem("accepting", "Profile (Web)", %itemId++, "gotoWebProfile($tempVar[0]);");
        %this.addMenuItem("accepting", "Home", %itemId++, "TeleportToHome($tempVar[0]);");
        %this.addMenuItem("accepting", "Ignore", %itemId++, "IgnoreUser($tempVar[0]);");
        %this.addMenuItem("accepting", "-", 0);
        %this.addMenuItem("accepting", "Accept", %itemId++, "AcceptFriend($tempVar[0]);");
        %this.addMenuItem("accepting", "Remove", %itemId++, "DeleteFriend($tempVar[0]);");
        %itemId = 0;
        %this.addMenu("pending", %menuId++);
        %this.addMenuItem("pending", "Message", %itemId++, "MainChatHUD.setCommand(\"/Tell \\\"\" @ $tempVar[0] @ \"\\\" \");");
        %itemId = %this.addGuideMenu("pending", %itemId);
        %this.addMenuItem("pending", "Profile (Web)", %itemId++, "gotoWebProfile($tempVar[0]);");
        %this.addMenuItem("pending", "Home", %itemId++, "TeleportToHome($tempVar[0]);");
        %this.addMenuItem("pending", "Ignore", %itemId++, "IgnoreUser($tempVar[0]);");
        %this.addMenuItem("pending", "-", 0);
        %this.addMenuItem("pending", "Remove", %itemId++, "DeleteFriend($tempVar[0]);");
        %itemId = 0;
        %this.addMenu("ignored", %menuId++);
        %this.addMenuItem("ignored", "Remove", %itemId++, "DeleteFriend($tempVar[0]);");
        %itemId = 0;
        %this.addMenu("local", %menuId++);
        %this.addMenuItem("local", "Message", %itemId++, "MainChatHUD.setCommand(\"/Tell \\\"\" @ $tempVar[0] @ \"\\\" \");");
        %itemId = %this.addGuideMenu("local", %itemId);
        %this.addMenuItem("local", "Profile (Web)", %itemId++, "gotoWebProfile($tempVar[0]);");
        %this.addMenuItem("local", "Teleport", %itemId++, "TeleportToPlayer($tempVar[0]);");
        %this.addMenuItem("local", "Home", %itemId++, "TeleportToHome($tempVar[0]);");
        %this.addMenuItem("local", "Ignore", %itemId++, "IgnoreUser($tempVar[0]);");
        %this.addMenuItem("local", "-", 0);
        %this.addMenuItem("local", "Add", %itemId++, "AddFriend($tempVar[0]);");
        %itemId = 0;
        %this.addMenu("acceptFriend", %menuId++);
        %this.addMenuItem("acceptFriend", "Accept", %itemId++, "RPC_FriendRequest(ACCEPT_FRIEND,$tempVar[0]);");
        %this.addMenuItem("acceptFriend", "Remove", %itemId++, "RPC_FriendRequest(DELETE_FRIEND,$tempVar[0]);");
    }
    %this.initialized = 1;
}

function friendsListContextMenu::addGuideMenu(%this, %menu, %itemId)
{
    if (((MasterServerConnection.hasGuideLevel() || MasterServerConnection.hasSuperGuideLevel()) || MasterServerConnection.hasCommManagerLevel()) || MasterServerConnection.hasAdminLevel())
    {
        %this.addMenuItem(%menu, "-", 0);
        if (MasterServerConnection.hasGuideLevel())
        {
            %this.addMenuItem(%menu, "Guide Reply", %itemId++, "MainChatHUD.setCommand(\"/GReply \\\"\" @ $tempVar[0] @ \"\\\" \");");
            %this.addMenuItem(%menu, "Warn", %itemId++, "MainChatHUD.setCommand(\"/Warn \\\"\" @ $tempVar[0] @ \"\\\" \");");
            %this.addMenuItem(%menu, "Relocate", %itemId++, "MainChatHUD.setCommand(\"/Relocate \\\"\" @ $tempVar[0] @ \"\\\" \");");
            %this.addMenuItem(%menu, "Kick", %itemId++, "MainChatHUD.setCommand(\"/Kick \\\"\" @ $tempVar[0] @ \"\\\" \");");
            %this.addMenuItem(%menu, "Suspend", %itemId++, "MainChatHUD.setCommand(\"/Suspend \\\"\" @ $tempVar[0] @ \"\\\" \");");
        }
        if (MasterServerConnection.hasCommManagerLevel())
        {
            %this.addMenuItem(%menu, "Ban", %itemId++, "MainChatHUD.setCommand(\"/Ban \\\"\" @ $tempVar[0] @ \"\\\" \");");
            %this.addMenuItem(%menu, "UnBan", %itemId++, "MainChatHUD.setCommand(\"/UnBan \\\"\" @ $tempVar[0] @ \"\\\" \");");
            %this.addMenuItem(%menu, "AddCC", %itemId++, "MainChatHUD.setCommand(\"/AddCC \\\"\" @ $tempVar[0] @ \"\\\" \");");
            %this.addMenuItem(%menu, "AddPP", %itemId++, "MainChatHUD.setCommand(\"/AddPP \\\"\" @ $tempVar[0] @ \"\\\" \");");
        }
        %this.addMenuItem(%menu, "-", 0);
    }
    return %itemId;
}

function friendsListContextMenu::addGuideMenuOffline(%this, %menu, %itemId)
{
    if (((MasterServerConnection.hasGuideLevel() || MasterServerConnection.hasSuperGuideLevel()) || MasterServerConnection.hasCommManagerLevel()) || MasterServerConnection.hasAdminLevel())
    {
        if (MasterServerConnection.hasGuideLevel())
        {
            %this.addMenuItem(%menu, "Suspend", %itemId++, "MainChatHUD.setCommand(\"/Suspend \\\"\" @ $tempVar[0] @ \"\\\" \");");
        }
        if (MasterServerConnection.hasCommManagerLevel())
        {
            %this.addMenuItem(%menu, "Ban", %itemId++, "MainChatHUD.setCommand(\"/Ban \\\"\" @ $tempVar[0] @ \"\\\" \");");
            %this.addMenuItem(%menu, "UnBan", %itemId++, "MainChatHUD.setCommand(\"/UnBan \\\"\" @ $tempVar[0] @ \"\\\" \");");
        }
        %this.addMenuItem(%menu, "-", 0);
    }
    return %itemId;
}

function friendsListContextMenu::showContextMenu(%this, %menu, %friendName, %layer)
{
    %this.Initialize();
    if (MasterServerConnection.hasGuideLevel())
    {
        if ((((%menu $= "online") || (%menu $= "accepting")) || (%menu $= "pending")) || (%menu $= "local"))
        {
            %this.setMenuItemVisible(%menu, "Relocate", FriendListUI.isLocalUser(%friendName));
        }
    }
    %this.var[0] = %friendName;
    Parent::showContextMenu(%this, %menu, %layer);
}

function RPC_FriendRequest::onReturnInvalidUsername(%this)
{
    MessageBoxOK("Error", "Invalid Friend Username");
}

function RPC_FriendRequest::onReturnTooManyFriends(%this)
{
    MessageBoxOK("Error", "You have too many friends");
}

function RPC_FriendRequest::onReturnIgnored(%this)
{
    MessageBoxOK("Error", "That user is currently in your ignore list.");
}

function RPC_FriendRequest::onReturnGeneralError(%this)
{
    MessageBoxOK("Error", "An error occured while adding friend");
}

function RPC_FriendRequest(%type, %username)
{
    %rpcObject = new RPC_FriendRequest() {
    };
    %rpcObject.requestType = %type;
    %rpcObject.username = %username;
    %rpcObject.fireRPC(MasterServerConnection);
}

if (!isObject(_GFriendDatabase))
{
    new ScriptObject(_GFriendDatabase) {
       superClass = "FriendDatabase";
    };
}

MasterServerConnection.RegisterForBroadcast(_GFriendDatabase);

function FriendDatabase::onEntireFriendList(%this, %numFriends)
{
    %this.clearFriendsList();
}

function FriendDatabase::onAddEntireFriend(%this, %friendID, %uname, %state)
{
    %this.AddFriend(%friendID, %uname, %state);
}

function FriendDatabase::onFriendUpdate(%this, %friendID, %uname, %state)
{
    %this.updateFriend(%friendID, %uname, %state);
}

function FriendDatabase::onDisconnect(%this)
{
    %this.clearFriendsList();
}

function FriendDatabase::getNumFriends(%this)
{
    return %this.numFriends;
}

function FriendDatabase::GetFriendIndex(%this, %friendID)
{
    %idx = %this.friendMap[%friendID];
    return %idx - 1;
}

function FriendDatabase::GetFriendID(%this, %idx)
{
    %friendData = %this.FriendList[%idx + 1];
    if (%friendData $= "")
    {
        error("Could not find idx:" @ %friendID SPC "to return friendID.");
        return 0;
    }
    return getField(%friendData, 1);
}

function FriendDatabase::GetFriendState(%this, %idx)
{
    %friendData = %this.FriendList[%idx + 1];
    if (%friendData $= "")
    {
        error("Could not find idx:" @ %friendID SPC "to return state.");
        return 255;
    }
    return getField(%friendData, 0);
}

function FriendDatabase::GetFriendName(%this, %idx)
{
    %friendData = %this.FriendList[%idx + 1];
    if (%friendData $= "")
    {
        error("Could not find idx:" @ %friendID SPC "to return state.");
        return 0;
    }
    return getField(%friendData, 2);
}

function FriendDatabase::clearFriendsList(%this)
{
    for (%idx=0; %idx < %this.numFriends; %idx++)
    {
        %friendID = %this.GetFriendID(%idx);
        %this.friendMap[%friendID] = "";
        %this.FriendList[%idx + 1] = "";
    }
    %this.numFriends = 0;
}

function FriendDatabase::AddFriend(%this, %friendID, %uname, %state)
{
    %this.friendMap[%friendID] = %this.numFriends++;
    %this.updateFriend(%friendID, %uname, %state);
}

function FriendDatabase::updateFriend(%this, %friendID, %uname, %state)
{
    %idx = %this.GetFriendIndex(%friendID);
    if (%idx == -1)
    {
        %this.AddFriend(%friendID, %uname, %state);
        return;
    }

    %oldstate = getWord(%this.FriendList[%idx + 1], 0);
    %this.FriendList[%idx + 1] = %state TAB %friendID TAB %uname;

    if ((%oldstate == 1) && (%state == 0 || %state == 4))
    {
        MainChatHUD.onFriendOffline(%uname);
    }
    else
    {
        if (%state == 1)
        {
            MainChatHUD.onFriendOnline(%uname);
        }
        else
        {
            if (%state == 2)
            {
                MainChatHUD.onFriendAccept(%uname);
            }
        }
    }
}

function FriendDatabase::resolveFriend(%this, %uname)
{
    for (%i=0; %i < %this.numFriends; %i++)
    {
        if (%this.GetFriendName(%i) $= %uname)
        {
            return %this.GetFriendID(%i);
        }
    }

    return 0;
}

function FriendDatabase::isFriend(%this, %uname)
{
    for (%i=0; %i < %this.numFriends; %i++)
    {
        if (%this.GetFriendName(%i) $= %uname)
        {
            %state = %this.GetFriendState(%i);
            return (%state == 0) || (%state == 1);
        }
    }

    return 0;
}

function FriendDatabase::isFriendOnline(%this, %uname)
{
    for (%i=0; %i < %this.numFriends; %i++)
    {
        if (%this.GetFriendName(%i) $= %uname)
        {
            %state = %this.GetFriendState(%i);
            return %state == 1;
        }
    }

    return 0;
}

function FriendDatabase::isFriendOffline(%this, %uname)
{
    for (%i=0; %i < %this.numFriends; %i++)
    {
        if (%this.GetFriendName(%i) $= %uname)
        {
            %state = %this.GetFriendState(%i);
            return %state == 0;
        }
    }

    return 0;
}

function FriendDatabase::isPending(%this, %uname)
{
    for (%i=0; %i < %this.numFriends; %i++)
    {
        if (%this.GetFriendName(%i) $= %uname)
        {
            %state = %this.GetFriendState(%i);
            return %state == 3;
        }
    }

    return 0;
}

function FriendDatabase::isAccepting(%this, %uname)
{
    for (%i=0; %i < %this.numFriends; %i++)
    {
        if (%this.GetFriendName(%i) $= %uname)
        {
            %state = %this.GetFriendState(%i);
            return %state == 2;
        }
    }

    return 0;
}

function FriendDatabase::isIgnored(%this, %uname)
{
    for (%i=0; %i < %this.numFriends; %i++)
    {
        if (%this.GetFriendName(%i) $= %uname)
        {
            %state = %this.GetFriendState(%i);
            return %state == 4;
        }
    }

    return 0;
}

MasterServerConnection.RegisterForBroadcast(FriendListUI);

function FriendListUI::onEntireFriendList(%this, %numFriends)
{
    %this.flushAll(%numFriends);
}

function FriendListUI::onAddEntireFriend(%this, %friendID, %uname, %state)
{
    %this.numFriends++;
    if (%this.numFriends >= %this.target)
    {
        FriendList.RebuildList();
    }
}

function FriendListUI::onDisconnect(%this)
{
    %this.flushAll(0);
    FriendList.RebuildList();
}

function FriendListUI::onFriendUpdate(%this, %friendID, %uname, %state)
{
    %idx = _GFriendDatabase.GetFriendIndex(%friendID);
    FriendList.RemoveFriend(%idx);
    FriendList.AddFriend(%idx);
    FriendList.UpdateState();
    if (isObject(LocationGameConnection) && LocationGameConnection.isInGame)
    {
        %shapeID = findByShapeName(%uname);
        if (%shapeID)
        {
            if (%shapeID.isMethod(updateFriendState))
            {
                %shapeID.updateFriendState();
            }
        }
    }
    FriendListUI.updateLocalUser(%friendID, %uname);
}

function FriendListUI::clearLocalUsers(%this)
{
    for (%i=0; %i < %this.localUserCount; %i++)
    {
        %this.localUser[%i] = "";
    }

    %this.localUserCount = 0;
    FriendList.clearLocalUsers();
    FriendList.UpdateState();
}

function FriendListUI::addLocalUser(%this, %id, %name)
{
    %this.localUser[%this.localUserCount] = %id TAB %name;
    %this.localUserCount++;
    FriendList.addLocalUser(%id, %name);
    FriendList.UpdateState();
}

function FriendListUI::removeLocalUser(%this, %id, %name)
{
    for (%i=0; %i < %this.localUserCount; %i++)
    {
        %user_id = getField(%this.localUser[%i], 0);
        if (%user_id == %id)
        {
            %i = %i + 1;
            while (%i < %this.localUserCount)
            {
                %this.localUser[%i - 1] = %this.localUser[%i];
                %i++;
            }
            
            %this.localUserCount--;
            break;
        }
    }
    FriendList.removeLocalUser(%id, %name);
    FriendList.UpdateState();
}

function FriendListUI::updateLocalUser(%this, %id, %name)
{
    for (%i=0; %i < %this.localUserCount; %i++)
    {
        %user_id = getField(%this.localUser[%i], 0);
        if (%user_id == %id)
        {
            FriendList.updateLocalUser(%id, %name);
            FriendList.UpdateState();
            break;
        }
    }
}

function FriendListUI::resolveLocalUser(%this, %name)
{
    for (%i=0; %i < %this.localUserCount; %i++)
    {
        %user_name = getField(%this.localUser[%i], 1);
        if (%user_name $= %name)
        {
            return getField(%this.localUser[%i], 0);
        }
    }
    return 0;
}

function FriendListUI::isLocalUser(%this, %name)
{
    return %this.resolveLocalUser(%name) != 0;
}

function FriendListUI::flushAll(%this, %target)
{
    %this.numFriends = 0;
    %this.target = %target;
}

function Player::updateFriendState(%this)
{
    %username = %this.getShapeName();
    if (_GFriendDatabase.isFriend(%username))
    {
        %this.friendState = 1;
    }
    else
    {
        if (_GFriendDatabase.isAccepting(%username))
        {
            %this.friendState = 2;
        }
        else
        {
            if (_GFriendDatabase.isPending(%username))
            {
                %this.friendState = 3;
            }
            else
            {
                if (_GFriendDatabase.isIgnored(%username))
                {
                    %this.friendState = 4;
                }
                else
                {
                    %this.friendState = 0;
                }
            }
        }
    }
}

function FriendList::onWake(%this)
{
    %this.clear();
    %this.buildIconTable();
    %this.RebuildList();
}

function FriendList::setFriendIdxToListID(%this, %friendIdx, %listid)
{
    %this.friendIdxToID[%friendIdx] = %listid;
}

function FriendList::getFriendIdxToListID(%this, %friendIdx)
{
    return %this.friendIdxToID[%friendIdx];
}

function FriendList::UpdateState(%this)
{
    if (%this.isAwake())
    {
        %this.editItem(%this.onlineList, "Online (" @ %this.getNumChildren(%this.onlineList) @ ")", "");
        %this.editItem(%this.offlineList, "Offline (" @ %this.getNumChildren(%this.offlineList) @ ")", "");
        %this.editItem(%this.acceptList, "Accept Requests (" @ %this.getNumChildren(%this.acceptList) @ ")", "");
        %this.editItem(%this.pendingList, "Pending Requests (" @ %this.getNumChildren(%this.pendingList) @ ")", "");
        %this.editItem(%this.ignoreList, "Ignored Users (" @ %this.getNumChildren(%this.ignoreList) @ ")", "");
        %this.editItem(%this.localList, "Local Users (" @ %this.getNumChildren(%this.localList) @ ")", "");
    }
}

function FriendList::RebuildList(%this)
{
    %this.clear();
    %this.onlineList = %this.insertItem(0, "Online (0)", "", "", 2, 1);
    %this.offlineList = %this.insertItem(0, "Offline (0)", "", "", 2, 1);
    %this.acceptList = %this.insertItem(0, "Accept Requests  (0)", "", "", 2, 1);
    %this.pendingList = %this.insertItem(0, "Pending Requests (0)", "", "", 2, 1);
    %this.ignoreList = %this.insertItem(0, "Ignored Users (0)", "", "", 2, 1);
    %this.localList = %this.insertItem(0, "Local Users (0)", "", "", 2, 1);
    
    for (%i=0; %i < FriendListUI.localUserCount; %i++)
    {
        %this.addLocalUser(getField(FriendListUI.localUser[%i], 0), getField(FriendListUI.localUser[%i], 1));
    }

    %count = _GFriendDatabase.getNumFriends();
    for (%i=0; %i < %count; %i++)
    {
        %this.AddFriend(%i);
    }

    %this.UpdateState();
}

function FriendList::RemoveFriend(%this, %friendIdx)
{
    %item = %this.getFriendIdxToListID(%friendIdx);
    if (%item != 0)
    {
        %this.removeItem(%item);
        %this.setFriendIdxToListID(%friendIdx, 0);
    }
}

function FriendList::AddFriend(%this, %friendIdx)
{
    if (%this.isAwake())
    {
        %item = 0;
        %state = _GFriendDatabase.GetFriendState(%friendIdx);
        %uname = _GFriendDatabase.GetFriendName(%friendIdx);
        if (%state == 0)
        {
            %item = %this.insertItem(%this.offlineList, %uname, %friendIdx, "", 31, 31);
        }
        else
        {
            if (%state == 1)
            {
                %item = %this.insertItem(%this.onlineList, %uname, %friendIdx, "", 30, 30);
            }
            else
            {
                if (%state == 2)
                {
                    %item = %this.insertItem(%this.acceptList, %uname, %friendIdx, "", 33, 33);
                }
                else
                {
                    if (%state == 3)
                    {
                        %item = %this.insertItem(%this.pendingList, %uname, %friendIdx, "", 32, 32);
                    }
                    else
                    {
                        if (%state == 4)
                        {
                            %item = %this.insertItem(%this.ignoreList, %uname, %friendIdx, "", 34, 34);
                        }
                        else
                        {
                            if (%state == 255)
                            {
                                echo("Friend deleted");
                            }
                            else
                            {
                                error("Unkown friend state. " @ %state);
                            }
                        }
                    }
                }
            }
        }
        %this.setFriendIdxToListID(%friendIdx, %item);
    }
}

function FriendList::clearLocalUsers(%this)
{
    %this.localCount = 0;
    %this.removeAllChildren(%this.localList);
}

function FriendList::getLocalFriendIcon(%this, %id, %username)
{
    %icon = 32;
    if (_GFriendDatabase.isFriend(%username))
    {
        %icon = 30;
    }
    else
    {
        if (_GFriendDatabase.isIgnored(%username))
        {
            %icon = 34;
        }
        else
        {
            if (_GFriendDatabase.isAccepting(%username))
            {
                %icon = 33;
            }
        }
    }
    return %icon;
}

function FriendList::addLocalUser(%this, %id, %username)
{
    %this.localCount++;
    %icon = %this.getLocalFriendIcon(%id, %username);
    %this.insertItem(%this.localList, %username, %id, "", %icon, %icon);
}

function FriendList::getNumChildren(%this, %item)
{
    %count = 0;
    for (%curentTreeItem = %this.getChild(%item); %curentTreeItem != 0; %curentTreeItem = %this.getNextSibling(%curentTreeItem))
    {
        %count++;
    }
    return %count;
}

function FriendList::removeLocalUser(%this, %id, %username)
{
    if (%this.localList == 0)
    {
        return;
    }

    for (%curentTreeItem = %this.getChild(%this.localList); %curentTreeItem != 0; %curentTreeItem = %this.getNextSibling(%curentTreeItem))
    {
        if (%this.getItemValue(%curentTreeItem) == %id)
        {
            %this.removeItem(%curentTreeItem);
            %this.localCount--;
            break;
        }
    }
}

function FriendList::updateLocalUser(%this, %id, %username)
{
    if (%this.localList == 0)
    {
        return;
    }

    for (%curentTreeItem = %this.getChild(%this.localList); %curentTreeItem != 0; %curentTreeItem = %this.getNextSibling(%curentTreeItem))
    {
        if (%this.getItemValue(%curentTreeItem) == %id)
        {
            %icon = %this.getLocalFriendIcon(%id, %username);
            %this.editItemIcon(%curentTreeItem, %icon, %icon);
            break;
        }
    }
}

function FriendList::ShowLocalContext(%this, %username)
{
    if (_GFriendDatabase.isFriendOnline(%username))
    {
        friendsListContextMenu.showContextMenu("online", %username);
    }
    else
    {
        if (_GFriendDatabase.isFriendOffline(%username))
        {
            friendsListContextMenu.showContextMenu("offline", %username);
        }
        else
        {
            if (_GFriendDatabase.isIgnored(%username))
            {
                friendsListContextMenu.showContextMenu("ignored", %username);
            }
            else
            {
                if (_GFriendDatabase.isAccepting(%username))
                {
                    friendsListContextMenu.showContextMenu("accepting", %username);
                }
                else
                {
                    if (_GFriendDatabase.isPending(%username))
                    {
                        friendsListContextMenu.showContextMenu("pending", %username);
                    }
                    else
                    {
                        friendsListContextMenu.showContextMenu("local", %username);
                    }
                }
            }
        }
    }
}

function FriendList::onRightMouseDown(%this, %item, %unused)
{
    %this.clearSelection();

    if (%this.getParent(%item) == %this.localList)
    {
        %this.ShowLocalContext(%this.getItemText(%item));
        return;
    }

    if (%this.getParent(%item) == %this.acceptList)
    {
        %this.selectItem(%item);
        friendsListContextMenu.showContextMenu("accepting", %this.getItemText(%item));
        return;
    }

    if (%this.getParent(%item) == %this.pendingList)
    {
        %this.selectItem(%item);
        friendsListContextMenu.showContextMenu("pending", %this.getItemText(%item));
        return;
    }

    if (%this.getParent(%item) == %this.ignoreList)
    {
        %this.selectItem(%item);
        friendsListContextMenu.showContextMenu("ignored", %this.getItemText(%item));
        return;
    }

    if (%this.getParent(%item) == %this.offlineList)
    {
        %this.selectItem(%item);
        friendsListContextMenu.showContextMenu("offline", %this.getItemText(%item));
        return;
    }

    if (%this.getParent(%item) == %this.onlineList)
    {
        %this.selectItem(%item);
        friendsListContextMenu.showContextMenu("online", %this.getItemText(%item));
        return;
    }
}

function AddFriend(%username)
{
    MessageBoxYesNo("Request Friend", "Would you like to add " @ %username @ " as a friend?", "RPC_FriendRequest(ADD_FRIEND,\"" @ %username @ "\");", "");
}

function AcceptFriend(%username)
{
    MessageBoxYesNo("Accept Friend", "Would you like to accept " @ %username @ " as a friend?", "RPC_FriendRequest(ACCEPT_FRIEND,\"" @ %username @ "\");", "RPC_FriendRequest(DELETE_FRIEND,\"" @ %username @ "\");", "");
}

function DeleteFriend(%username)
{
    %noun = "friend";
    if (_GFriendDatabase.isIgnored(%username))
    {
        %noun = "ignored user";
    }
    MessageBoxYesNo("Delete Friend", "Would you like to delete" SPC %noun SPC %username @ "?", "RPC_FriendRequest(DELETE_FRIEND,\"" @ %username @ "\");", "");
}

function IgnoreUser(%username)
{
    MessageBoxYesNo("Ignore User", "Would you like to ignore user " @ %username @ "?", "RPC_FriendRequest(IGNORE_USER,\"" @ %username @ "\");", "");
}

function gotoWebProfile(%username)
{
    %userID = _GFriendDatabase.resolveFriend(%username);

    if (%userID == 0)
    {
        %userID = FriendListUI.resolveLocalUser(%username);
    }
    
    if (%userID != 0)
    {
        gotoWebPage("http://www.onverse.com/profile/profile.php?id=" @ %userID);
    }
    else
    {
        error("(gotoWebProfile) Could not resolve username:" @ %username);
    }
}

function ShowFriendsList()
{
    if (!FriendListUI.isAwake())
    {
        Canvas.pushDialog(FriendListUI);
    }
    else
    {
        Canvas.popDialog(FriendListUI);
    }
}
