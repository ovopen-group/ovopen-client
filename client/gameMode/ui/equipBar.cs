
if (!isObject(EquipBar))
{
    exec("./equipBar.gui");
}

function ShowEquipmentBar()
{
    if (isObject(LocationGameConnection) && LocationGameConnection.isInGame)
    {
        if (!EquipBar.isAwake())
        {
            Canvas.pushDialog(EquipBar);
        }
        else
        {
            Canvas.popDialog(EquipBar);
        }
    }
    else
    {
        Canvas.popDialog(EquipBar);
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
}

function EquipBar::onWake(%this)
{
    %this.refresh();
}

function EquipBar::Load(%this)
{
    %this.loadedData = 1;
    %file = new FileObject() {
    };

    %open = 0;
    if ($journal::Reading)
    {
        echo("Attempting to open journal\'s matching equip config file.");
        %open = %file.openForRead($journal::File @ "_equipBar.cfg");
        if (!%open)
        {
            echo("Using default equip file.");
            %open = %file.openForRead("cfg/equipBar.cfg");
        }
    }
    else
    {
        %open = %file.openForRead("cfg/equipBar.cfg");
    }
    if (%open)
    {
        %this.otherEquipBars = 0;
        %this.readVersion = 1;
        if (!%file.isEOF())
        {
            %skipLine = 0;
            %line = %file.readLine();
            if (getFieldCount(%line) == 1)
            {
                %this.readVersion = %line;
            }
            if (%this.readVersion == 1)
            {
                %skipLine = 1;
            }
            if (%this.readVersion < 1)
            {
                error("Equip bar version too low.");
            }
            else
            {
                if (%this.readVersion > 2)
                {
                    error("Equip bar version too high.");
                }
                else
                {
                    while (%skipLine || !%file.isEOF())
                    {
                        if (!%skipLine)
                        {
                            %line = %file.readLine();
                        }
                        else
                        {
                            %skipLine = 0;
                        }
                        %this._parseLine(%line);
                    }
                }/* 4 | 495 */
            }/* 4 | 495 */
        }/* 4 | 495 */
    }/* 4 | 495 */
    %file.delete();
}

function EquipBar::_parseField(%this, %line, %idx)
{
    if (%this.readVersion > 1)
    {
        %field = getField(%line, %idx + 1);
        return getWord(%field, 0) TAB getWord(%field, 1);
    }
    else
    {
        %field = getField(%line, %idx + 1);
        if (%field == 0)
        {
            return 0 TAB 0;
        }
        else
        {
            return 1 TAB getField(%line, %idx + 1);
        }
    }
}

function EquipBar::_parseLine(%this, %line)
{
    %line = trim(%line);
    if (getFieldCount(%line))
    {
        %name = getField(%line, 0);
        if (%name $= $currentPlayerName)
        {
            for (%i=0; %i < 10; %i++)
            {
                %this.equipmentSlot[%i] = %this._parseField(%line, %i);
            }
        }
        else
        {
            if (!(%name $= ""))
            {
                %this.otherEquipBarNames[%this.otherEquipBars] = %name;
                for (%i=0; %i < 10; %i++)
                {
                    %this.otherEquipBars[%this.otherEquipBars,%i] = %this._parseField(%line, %i);
                }
                %this.otherEquipBars++;
            }
        }
    }
}

function EquipBar::save(%this)
{
    if (!(%this.loadedData) || $journal::Reading)
    {
        return;
    }
    %writeVersion = 2;
    
    %file = new FileObject() {
    };

    if (%file.openForWrite("cfg/equipBar.cfg"))
    {
        if (%writeVersion > 1)
        {
            %file.writeLine(%writeVersion);
        }
        
        %line = $currentPlayerName;

        for (%i=0; %i < 10; %i++)
        {
            %type = %this.equipmentSlot[%i] ? getField(%this.equipmentSlot[%i], 0) : 0;
            %uniqueID = %this.equipmentSlot[%i] ? getField(%this.equipmentSlot[%i], 1) : 0;
            if (%writeVersion > 1)
            {
                %line = %line TAB %type SPC %uniqueID;
            }
            else
            {
                if (%type == 1)
                {
                    %line = %line TAB %uniqueID;
                }
                else
                {
                    %line = %line TAB 0;
                }
            }
        }

        %file.writeLine(%line);

        for (%n=0; %n < %this.otherEquipBars; %n++)
        {
            %line = %this.otherEquipBarNames[%n];

            for (%i=0; %i < 10; %i++)
            {
                %type = %this.otherEquipBars[%n,%i] ? getField(%this.otherEquipBars[%n,%i], 0) : 0;
                %uniqueID = %this.otherEquipBars[%n,%i] ? getField(%this.otherEquipBars[%n,%i], 1) : 0;
                if (%writeVersion > 1)
                {
                    %line = %line TAB %type SPC %uniqueID;
                }
                else
                {
                    if (%type == 1)
                    {
                        %line = %line TAB %uniqueID;
                    }
                    else
                    {
                        %line = %line TAB 0;
                    }
                }
            }

            %file.writeLine(%line);
        }
    }

    %file.delete();
}

function EquipBar::setEquipmentSlot(%this, %guiDragItem, %slot)
{
    %type = 0;
    if (%guiDragItem.dragType $= EquipBarItemDrag)
    {
        %type = -1;
    }
    else
    {
        if (%guiDragItem.dragType $= EquipmentDrag)
        {
            %type = 1;
        }
        else
        {
            if (%guiDragItem.dragType $= PetDrag)
            {
                %type = 2;
            }
            else
            {
                if (%guiDragItem.dragType $= TravelDrag)
                {
                    %type = 3;
                }
            }
        }
    }

    if (%type == -1)
    {
        %uniqueID = getField(%guiDragItem.info, 0);
        %type = %guiDragItem.getTypeFromInfo();
        %parent = %this._(slot @ %slot);
        if (%parent.dragItem && ((%parent.dragItem.info !$= "")))
        {
            %otherSlot = %guiDragItem.slot;
            %this.equipmentSlot[%slot] = %type TAB %uniqueID;
            %uniqueID = getField(%parent.dragItem.info, 0);
            %type = %parent.dragItem.getTypeFromInfo();
            %this.equipmentSlot[%otherSlot] = %type TAB %uniqueID;
            %this.refresh();
            return;
        }
    }
    else
    {
        if (%type > 0)
        {
            %uniqueID = getField(%guiDragItem.info, 0);
        }
    }

    for (%i=0; %i < 10; %i++)
    {
        if ((%type == getField(%this.equipmentSlot[%i], 0)) && (%uniqueID == getField(%this.equipmentSlot[%i], 1)))
        {
            %this.equipmentSlot[%i] = 0 TAB 0;
        }
    }

    %this.equipmentSlot[%slot] = %type TAB %uniqueID;
    %this.refresh();
}

function EquipBar::emptyEquipmentSlot(%this, %type, %uniqueID)
{
    for (%i=0; %i < 10; %i++)
    {
        if ((%type == getField(%this.equipmentSlot[%i], 0)) && (%uniqueID == getField(%this.equipmentSlot[%i], 1)))
        {
            %this.equipmentSlot[%i] = 0 TAB 0;
        }
    }

    %this.refresh();
}

function EquipBar::sendEquipSlot(%this, %slot)
{
    %parent = %this._(slot @ %slot);
    if (!%parent.dragItem)
    {
        return;
    }

    if (%parent.dragItem.info $= "")
    {
        return;
    }

    %type = %parent.dragItem.getTypeFromInfo();
    if (%type == 1)
    {
        InventoryGUI.equipEquipment(%parent.dragItem);
    }
    else
    {
        if (%type == 2)
        {
            InventoryGUI.equipPet(%parent.dragItem);
        }
        else
        {
            if (%type == 3)
            {
                InventoryGUI.equipTravel(%parent.dragItem);
            }
        }
    }
}

function EquipBar::refresh(%this)
{
    for (%i=0; %i < 10; %i++)
    {
        %this._refreshSlot(%i);
    }
}

function EquipBar::_refreshSlot(%this, %slot)
{
    %type = getField(%this.equipmentSlot[%slot], 0);
    %uniqueID = getField(%this.equipmentSlot[%slot], 1);
    %type = %type ? %type : 0;
    %uniqueID = %uniqueID ? %uniqueID : 0;

    if (%this._isSameSlot(%slot))
    {
        return;
    }

    %this._setSlotInfo(%slot, %this._genSlotInfo(%slot));
}

function EquipBar::_genSlotInfo(%this, %slot)
{
    %type = getField(%this.equipmentSlot[%slot], 0);
    %uniqueID = getField(%this.equipmentSlot[%slot], 1);
    %info = "";

    if (%type == 1)
    {
        %info = InventoryManager.getToolInfo(%uniqueID);
    }
    else
    {
        if (%type == 2)
        {
            %info = InventoryManager.getPetInfo(%uniqueID);
        }
        else
        {
            if (%type == 3)
            {
                %info = InventoryManager.getTravelInfo(%uniqueID);
            }
        }
    }

    if (%info $= "")
    {
        return "";
    }

    return %info TAB %type;
}

function EquipBar::_isSameSlot(%this, %slot)
{
    %type = getField(%this.equipmentSlot[%slot], 0);
    %uniqueID = getField(%this.equipmentSlot[%slot], 1);
    %parent = %this._(slot @ %slot);

    if (!%parent.dragItem)
    {
        return 0;
    }

    if ((%parent.dragItem.info $= "") && (%type != 0))
    {
        return 0;
    }

    if (!((%parent.dragItem.info $= "")) && (%type == 0))
    {
        return 0;
    }

    if (%type != getField(%parent.dragItem.info, 0))
    {
        return 0;
    }
    
    if (%uniqueID != getField(%parent.dragItem.info, 1))
    {
        return 0;
    }

    return 1;
}

function EquipBar::_setSlotInfo(%this, %slot, %info)
{
    %parent = %this._(slot @ %slot);
    if (!%parent.dragItem)
    {
        %newItem = new GuiDragDropCtrl() {
           Extent = "32 32";
           bitmap = "";
           info = "";
           RightMouseCommand = "";
           tooltipprofile = GuiToolTipProfile;
           enableDrag = "0";
           dragType = EquipBarItemDrag;
           class = EquipBarItemDrag;
           slot = %slot;
        };
        %parent.add(%newItem);
        %parent.dragItem = %newItem;
    }

    if (%parent.dragItem.info $= %info)
    {
        return;
    }

    if (%info $= "")
    {
        %parent.dragItem.info = "";
        %parent.dragItem.bitmap = "";
        %parent.dragItem.RightMouseCommand = "";
        %parent.dragItem.ToolTip = "";
        %parent.dragItem.enableDrag = 0;
        return;
    }

    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 1);
    %iconfile = %datablock.iconFile;
    if (!isFile(%iconfile))
    {
        %iconfile = $icons::Inventory::Invalid;
    }

    %parent.dragItem.info = %info;
    %type = %parent.dragItem.getTypeFromInfo();
    %parent.dragItem.bitmap = %iconfile;
    %parent.dragItem.enableDrag = 1;
    if ((%type == 2) && ((%petName = getField(%info, 2)) !$= "") )
    {
        %parent.dragItem.ToolTip = %petName SPC "(" @ %slot @ ")";
    }
    else
    {
        %parent.dragItem.ToolTip = %datablock.GetDisplayName() SPC "(" @ %slot @ ")";
    }
    
    %rightCommand = "";
    if (%type == 1)
    {
        %rightCommand = "inventoryContextMenu.showContextMenu(equipmentItem,$ThisControl);";
    }
    else
    {
        if (%type == 2)
        {
            %rightCommand = "inventoryContextMenu.showContextMenu(petItem,$ThisControl);";
        }
        else
        {
            if (%type == 3)
            {
                %rightCommand = "inventoryContextMenu.showContextMenu(travelItem,$ThisControl);";
            }
        }
    }
    %parent.dragItem.RightMouseCommand = %rightCommand;
}

function EquipBar::_getTypeFromInfo(%this, %info)
{
    %type = getField(%info, 3);
    if (%type $= "")
    {
        %type = getField(%info, 2);
    }

    return %type;
}

function EquipBarItemDrag::canDragDrop(%this, %dragCtrl, %position)
{
    %doit = 0;
    if (%dragCtrl.dragType $= EquipmentDrag)
    {
        %doit = 1;
    }

    if (%dragCtrl.dragType $= PetDrag)
    {
        %doit = 1;
    }

    if (%dragCtrl.dragType $= TravelDrag)
    {
        %doit = 1;
    }

    if (%dragCtrl.dragType $= EquipBarItemDrag)
    {
        %doit = 1;
    }

    if (%doit)
    {
        %dragCtrl.resize(getWord(%position, 0) - 16, getWord(%position, 1) - 16, 32, 32);
    }

    return %doit;
}

function EquipBarItemDrag::onDragDrop(%this, %dragCtrl, %position)
{
    EquipBar.setEquipmentSlot(%dragCtrl, %this.slot);
}

function EquipBarItemDrag::onDoubleClick(%this)
{
    if (%this.getTypeFromInfo() == 1)
    {
        EquipmentDrag::onDoubleClick(%this);
    }
    else
    {
        if (%this.getTypeFromInfo() == 2)
        {
            PetDrag::onDoubleClick(%this);
        }
        else
        {
            if (%this.getTypeFromInfo() == 3)
            {
                TravelDrag::onDoubleClick(%this);
            }
        }
    }
}

function EquipBarItemDrag::getTypeFromInfo(%this)
{
    return EquipBar._getTypeFromInfo(%this.info);
}
