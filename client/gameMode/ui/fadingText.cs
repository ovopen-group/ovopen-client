exec("./fadingText.gui");

function FadingText::start(%gui, %text)
{
    Canvas.pushDialog(%gui, 5);
    %gui._("text").setText(%text);
    %gui._("text").start();
}

function FadingTextML::onFadeComplete(%this)
{
    if (Canvas.getFirstResponder() == chatInput.getId())
    {
        %overrideFirst = 1;
        %blankFirst = chatInput.getText() $= "";
    }
    
    Canvas.popDialog(%this.getParent());

    if (%overrideFirst)
    {
        chatInput.forceFirstResponder();
        if (%blankFirst)
        {
            chatInput.setText("");
        }
    }
}
