
if (!isObject(InventoryGUI))
{
    exec("./inventory.gui");
}

$icons::Inventory::Slot = "~/data/live_assets/engine/ui/images/icons/inventory/inventory_slot";
$icons::Inventory::Slot_Equip = "~/data/live_assets/engine/ui/images/icons/inventory/inventory_slot_equipped";
$icons::Inventory::EquipEmpty[chest] = "~/data/live_assets/engine/ui/images/icons/inventory/icon_shirt_blank";
$icons::Inventory::EquipEmpty[legs] = "~/data/live_assets/engine/ui/images/icons/inventory/icon_pants_blank";
$icons::Inventory::EquipEmpty[feet] = "~/data/live_assets/engine/ui/images/icons/inventory/icon_shoes_blank";
$icons::Inventory::EquipEmpty[hands] = "~/data/live_assets/engine/ui/images/icons/inventory/icon_hands_blank";
$icons::Inventory::EquipEmpty[over] = "~/data/live_assets/engine/ui/images/icons/inventory/icon_over_blank";
$icons::Inventory::EquipEmpty[hats] = "~/data/live_assets/engine/ui/images/icons/inventory/icon_hats_blank";
$icons::Inventory::EquipEmpty[face] = "~/data/live_assets/engine/ui/images/icons/inventory/icon_face_blank";
$icons::Inventory::EquipEmpty[belt] = "~/data/live_assets/engine/ui/images/icons/inventory/icon_belt_blank";
$icons::Inventory::EquipEmpty[ear] = "~/data/live_assets/engine/ui/images/icons/inventory/icon_ear_blank";
$icons::Inventory::EquipEmpty[neck] = "~/data/live_assets/engine/ui/images/icons/inventory/icon_neck_blank";
$icons::Inventory::Invalid = "~/data/live_assets/engine/ui/images/icons/inventory/inventory_empty";
$icons::Inventory::noEquip = "~/data/live_assets/engine/ui/images/icons/inventory/inventory_slot_noequip";

function ShowInventoryDialog()
{
    if (isObject(LocationGameConnection) && LocationGameConnection.isInGame)
    {
        if (!InventoryGUI.isAwake())
        {
            Canvas.pushDialog(InventoryGUI);
        }
        else
        {
            Canvas.popDialog(InventoryGUI);
        }
    }
    else
    {
        Canvas.popDialog(InventoryGUI);
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
}

function InventoryGUI::onWake(%this)
{
    if (isObject(LocationGameConnection) && LocationGameConnection.isInGame)
    {
        %this.updateClothingDisplay();
        %this.updateReservedDisplay();
        %this.updateFurnitureDisplay();
        %this.updateEquipmentDisplay();
        %this.updatePetDisplay();
        %this.updateTravelDisplay();
    }
}

function InventoryGUI::Initialize(%this)
{
    if (!InventoryManager.isValid)
    {
        %this.InitializeClothing();
        %this.InitializeReserved();
        %this.InitializeFurniture();
        %this.InitializeEquipment();
        %this.InitializePets();
        %this.InitializeTravel();
        InventoryManager.download();
    }
}

function InventoryGUI::InitializePets(%this)
{
    %this.pet_idx[Pets] = 0;
    %petsTab = %this._(petsTab);
    %petsTab._(upArrow).setVisible(0);
    %petsTab._(downArrow).setVisible(0);
}

function InventoryGUI::ChangePetsPage(%this, %amt, %type)
{
    %this.pet_idx[%type] += (%amt * 5);
    InventoryGUI.updatePetDisplay();
}

function InventoryGUI::updatePetDisplay(%this)
{
    %petsTab = %this._(petsTab);
    if (%petsTab.isAwake())
    {
        for (%y=0; %y < 5; %y++)
        {
            %this.DisplayPetRow(%y, %petsTab, Pets);
        }
        %petsTab._(upArrow).setVisible(%this.pet_idx[Pets] > 0);
        %petsTab._(downArrow).setVisible(InventoryManager.pet_num[Pets] > ((%this.pet_idx[Pets] * 5) + 25));
    }
}

function InventoryGUI::DisplayPetRow(%this, %y, %tab, %type)
{
    %offset = (%this.pet_idx[%type] + %y) * 5;

    for (%x=1; %x <= 5; %x++)
    {
        %idx = %offset + %x;
        %currentParent = %tab._("Slot" @ %y @ %x);
        if (%idx > InventoryManager.pet_num[%type])
        {
            %this.DisplayPetItem(%currentParent, "");
        }
        else
        {
            %this.DisplayPetItem(%currentParent, InventoryManager.pet_list[%type,%idx]);
        }
    }
}

function InventoryGUI::DisplayPetItem(%this, %parent, %info)
{
    if (!isObject(%parent))
    {
        return;
    }

    if (!%parent.dragItem)
    {
        %newItem = new GuiDragDropCtrl() {
           Extent = "64 64";
           bitmap = "";
           info = "";
           RightMouseCommand = "";
           tooltipprofile = GuiToolTipProfile;
           enableDrag = "0";
           dragType = PetDrag;
           class = PetDrag;
        };

        %parent.add(%newItem);
        %parent.dragItem = %newItem;
    }
    
    if ((%parent.dragItem.info $= "") && (%info $= ""))
    {
        return;
    }
    
    if (%info $= "")
    {
        %parent.bitmap = $icons::Inventory::Slot;
        %parent.dragItem.info = "";
        %parent.dragItem.bitmap = "";
        %parent.dragItem.RightMouseCommand = "";
        %parent.dragItem.ToolTip = "";
        %parent.dragItem.enableDrag = 0;
        return;
    }
    
    %uniqueID = getField(%info, 0);
    %parent.bitmap = $icons::Inventory::Slot;
    
    if (%parent.dragItem.info $= %info)
    {
        return;
    }

    %datablock = getField(%info, 1);
    %petName = getField(%info, 2);
    %iconfile = %datablock.iconFile;
    if (!isFile(%iconfile))
    {
        %iconfile = $icons::Inventory::Invalid;
    }
    
    %displayName = %datablock.GetDisplayName();
    if (!(%petName $= ""))
    {
        %displayName = %petName SPC "(" @ %displayName @ ")";
    }
    
    %parent.dragItem.info = %info;
    %parent.dragItem.bitmap = %iconfile;
    %parent.dragItem.ToolTip = %displayName;
    %parent.dragItem.enableDrag = 1;
    %parent.dragItem.RightMouseCommand = "inventoryContextMenu.showContextMenu(petItem,$ThisControl);";
}

function PetDrag::onDoubleClick(%this)
{
    InventoryGUI.equipPet(%this);
}

function PetDrag::onPickedUp(%this, %position)
{
    %this.showingBar = 0;
    if (!EquipBar.isAwake())
    {
        %this.showingBar = 1;
    }
    else
    {
        Canvas.popDialog(EquipBar);
    }
    Canvas.pushDialog(EquipBar);
}

function PetDrag::onDropped(%this, %onCtrl, %position)
{
    if (%this.showingBar)
    {
        %this.showingBar = 0;
        if (!(%onCtrl.class $= EquipBarItemDrag))
        {
            Canvas.popDialog(EquipBar);
        }
    }
}

function PetDrag::canDragDrop(%this, %dragCtrl, %position)
{
    if (%dragCtrl.dragType $= EquipBarItemDrag)
    {
        if (%dragCtrl.getTypeFromInfo() != 2)
        {
            return 0;
        }
 
        %dragCtrl.resize(getWord(%position, 0) - 32, getWord(%position, 1) - 32, 64, 64);
        return 1;
    }

    return 0;
}

function PetDrag::onDragDrop(%this, %dragCtrl, %position)
{
    %uniqueID = getField(%dragCtrl.info, 0);
    EquipBar.emptyEquipmentSlot(2, %uniqueID);
}

function InventoryGUI::equipPet(%this, %petGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %uniqueID = getField(%petGuiItem.info, 0);
        commandToServer('EquipPet', %uniqueID);
    }
}

function InventoryGUI::infoPet(%this, %petGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %datablock = getField(%petGuiItem.info, 1);
        %petName = getField(%petGuiItem.info, 2);
        if (!isDatablock(%datablock))
        {
            return;
        }
        InspectItem(%datablock, %petName);
    }
}

function InventoryGUI::sellPet(%this, %petGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %uniqueID = getField(%petGuiItem.info, 0);
        %datablock = getField(%petGuiItem.info, 1);
        %petName = getField(%petGuiItem.info, 2);
        
        if (!isDatablock(%datablock))
        {
            return;
        }

        SellInvItem(%datablock, %uniqueID, %petName);
    }
}

function InventoryGUI::InitializeTravel(%this)
{
    %this.travel_idx[mounts] = 0;
    %travelTab = %this._(travelTab);
    %travelTab._(upArrow).setVisible(0);
    %travelTab._(downArrow).setVisible(0);
}

function InventoryGUI::ChangeTravelPage(%this, %amt, %type)
{
    %this.travel_idx[%type] += (%amt * 5);
    InventoryGUI.updateTravelDisplay();
}

function InventoryGUI::updateTravelDisplay(%this)
{
    %travelTab = %this._(travelTab);
    if (%travelTab.isAwake())
    {
        for (%y=0; %y < 5; %y++)
        {
            %this.DisplayTravelRow(%y, %travelTab, mounts);
        }

        %travelTab._(upArrow).setVisible(%this.travel_idx[mounts] > 0);
        %travelTab._(downArrow).setVisible(InventoryManager.travel_num[mounts] > ((%this.travel_idx[mounts] * 5) + 25));
    }
}

function InventoryGUI::DisplayTravelRow(%this, %y, %tab, %type)
{
    %offset = (%this.travel_idx[%type] + %y) * 5;
    for (%x=1; %x <= 5; %x++)
    {
        %idx = %offset + %x;
        %currentParent = %tab._("Slot" @ %y @ %x);
        if (%idx > InventoryManager.travel_num[%type])
        {
            %this.DisplayTravelItem(%currentParent, "");
        }
        else
        {
            %this.DisplayTravelItem(%currentParent, InventoryManager.travel_list[%type,%idx]);
        }
    }
}

function InventoryGUI::DisplayTravelItem(%this, %parent, %info)
{
    if (!isObject(%parent))
    {
        return;
    }

    if (!%parent.dragItem)
    {
        %newItem = new GuiDragDropCtrl() {
           Extent = "64 64";
           bitmap = "";
           info = "";
           RightMouseCommand = "";
           tooltipprofile = GuiToolTipProfile;
           enableDrag = "0";
           dragType = TravelDrag;
           class = TravelDrag;
        };
        %parent.add(%newItem);
        %parent.dragItem = %newItem;
    }

    if ((%parent.dragItem.info $= "") && (%info $= ""))
    {
        return;
    }

    if (%info $= "")
    {
        %parent.bitmap = $icons::Inventory::Slot;
        %parent.dragItem.info = "";
        %parent.dragItem.bitmap = "";
        %parent.dragItem.RightMouseCommand = "";
        %parent.dragItem.ToolTip = "";
        %parent.dragItem.enableDrag = 0;
        return;
    }

    %uniqueID = getField(%info, 0);
    %parent.bitmap = $icons::Inventory::Slot;
    if (isObject(LocationGameConnection) && isObject(LocationGameConnection.travel))
    {
        if (%uniqueID == LocationGameConnection.travel.uniqueid)
        {
            %parent.bitmap = $icons::Inventory::Slot_Equip;
        }
    }
    
    if (%parent.dragItem.info $= %info)
    {
        return;
    }
    
    %datablock = getField(%info, 1);
    %iconfile = %datablock.iconFile;
    if (!isFile(%iconfile))
    {
        %iconfile = $icons::Inventory::Invalid;
    }
    
    %parent.dragItem.info = %info;
    %parent.dragItem.bitmap = %iconfile;
    %parent.dragItem.ToolTip = %datablock.GetDisplayName();
    %parent.dragItem.enableDrag = 1;
    %parent.dragItem.RightMouseCommand = "inventoryContextMenu.showContextMenu(travelItem,$ThisControl);";
}

function TravelDrag::onDoubleClick(%this)
{
    InventoryGUI.equipTravel(%this);
}

function TravelDrag::onPickedUp(%this, %position)
{
    %this.showingBar = 0;

    if (!EquipBar.isAwake())
    {
        %this.showingBar = 1;
    }
    else
    {
        Canvas.popDialog(EquipBar);
    }
    
    Canvas.pushDialog(EquipBar);
}

function TravelDrag::onDropped(%this, %onCtrl, %position)
{
    if (%this.showingBar)
    {
        %this.showingBar = 0;
        
        if (!(%onCtrl.class $= EquipBarItemDrag))
        {
            Canvas.popDialog(EquipBar);
        }
    }
}

function TravelDrag::canDragDrop(%this, %dragCtrl, %position)
{
    if (%dragCtrl.dragType $= EquipBarItemDrag)
    {
        if (%dragCtrl.getTypeFromInfo() != 3)
        {
            return 0;
        }
        
        %dragCtrl.resize(getWord(%position, 0) - 32, getWord(%position, 1) - 32, 64, 64);
        return 1;
    }

    return 0;
}

function TravelDrag::onDragDrop(%this, %dragCtrl, %position)
{
    %uniqueID = getField(%dragCtrl.info, 0);
    EquipBar.emptyEquipmentSlot(3, %uniqueID);
}

function InventoryGUI::equipTravel(%this, %travelGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %uniqueID = getField(%travelGuiItem.info, 0);
        commandToServer('EquipTravel', %uniqueID);
    }
}

function InventoryGUI::infoTravel(%this, %travelGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %datablock = getField(%travelGuiItem.info, 1);
        if (!isDatablock(%datablock))
        {
            return;
        }
        InspectItem(%datablock);
    }
}

function InventoryGUI::sellTravel(%this, %travelGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %uniqueID = getField(%travelGuiItem.info, 0);
        %datablock = getField(%travelGuiItem.info, 1);
        if (!isDatablock(%datablock))
        {
            return;
        }
        SellInvItem(%datablock, %uniqueID);
    }
}

function InventoryGUI::InitializeEquipment(%this)
{
    %this.equipment_idx[tools] = 0;
    %this.equipment_idx[weapons] = 0;
    %equipmentTab = %this._(equipTab);
    %equipmentTab._(toolsUp).setVisible(0);
    %equipmentTab._(toolsDown).setVisible(0);
}

function InventoryGUI::ChangeEquipmentPage(%this, %amt, %type)
{
    %this.equipment_idx[%type] += (%amt * 5);
    InventoryGUI.updateEquipmentDisplay();
}

function InventoryGUI::updateEquipmentDisplay(%this)
{
    %equipTab = %this._(equipTab);
    if (%equipTab.isAwake())
    {
        for (%y=0; %y < 5; %y++)
        {
            %this.DisplayEquipmentRow(%y, %equipTab._(toolsTab), tools);
        }

        %equipTab._(toolsUp).setVisible(%this.equipment_idx[tools] > 0);
        %equipTab._(toolsDown).setVisible(InventoryManager.equipment_num[tools] > ((%this.equipment_idx[tools] * 5) + 25));
    }
}

function InventoryGUI::DisplayEquipmentRow(%this, %y, %tab, %type)
{
    %offset = (%this.equipment_idx[%type] + %y) * 5;

    for (%x=1; %x <= 5; %x++)
    {
        %idx = %offset + %x;
        %currentParent = %tab._("Slot" @ %y @ %x);
        if (%idx > InventoryManager.equipment_num[%type])
        {
            %this.DisplayEquipmentItem(%currentParent, "");
        }
        else
        {
            %this.DisplayEquipmentItem(%currentParent, InventoryManager.equipment_list[%type,%idx]);
        }
    }
}

function InventoryGUI::DisplayEquipmentItem(%this, %parent, %info)
{
    if (!isObject(%parent))
    {
        return;
    }

    if (!%parent.dragItem)
    {
        %newItem = new GuiDragDropCtrl() {
           Extent = "64 64";
           bitmap = "";
           info = "";
           RightMouseCommand = "";
           tooltipprofile = GuiToolTipProfile;
           enableDrag = "0";
           dragType = EquipmentDrag;
           class = EquipmentDrag;
        };

        %parent.add(%newItem);
        %parent.dragItem = %newItem;
    }

    if (%parent.dragItem.info $= %info)
    {
        return;
    }

    if (%info $= "")
    {
        %parent.dragItem.info = "";
        %parent.dragItem.bitmap = "";
        %parent.dragItem.RightMouseCommand = "";
        %parent.dragItem.ToolTip = "";
        %parent.dragItem.enableDrag = 0;
        return;
    }

    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 1);
    %iconfile = %datablock.iconFile;
    
    if (!isFile(%iconfile))
    {
        %iconfile = $icons::Inventory::Invalid;
    }

    %parent.dragItem.info = %info;
    %parent.dragItem.bitmap = %iconfile;
    %parent.dragItem.ToolTip = %datablock.GetDisplayName();
    %parent.dragItem.enableDrag = 1;
    %parent.dragItem.RightMouseCommand = "inventoryContextMenu.showContextMenu(equipmentItem,$ThisControl);";
}

function EquipmentDrag::onDoubleClick(%this)
{
    InventoryGUI.equipEquipment(%this);
}

function EquipmentDrag::onPickedUp(%this, %position)
{
    %this.showingBar = 0;
    
    if (!EquipBar.isAwake())
    {
        %this.showingBar = 1;
    }
    else
    {
        Canvas.popDialog(EquipBar);
    }

    Canvas.pushDialog(EquipBar);
}

function EquipmentDrag::onDropped(%this, %onCtrl, %position)
{
    if (%this.showingBar)
    {
        %this.showingBar = 0;
        if (!(%onCtrl.class $= EquipBarItemDrag))
        {
            Canvas.popDialog(EquipBar);
        }
    }
}

function EquipmentDrag::canDragDrop(%this, %dragCtrl, %position)
{
    if (%dragCtrl.dragType $= EquipBarItemDrag)
    {
        if (%dragCtrl.getTypeFromInfo() != 1)
        {
            return 0;
        }

        %dragCtrl.resize(getWord(%position, 0) - 32, getWord(%position, 1) - 32, 64, 64);
        return 1;
    }

    return 0;
}

function EquipmentDrag::onDragDrop(%this, %dragCtrl, %position)
{
    %uniqueID = getField(%dragCtrl.info, 0);
    EquipBar.emptyEquipmentSlot(1, %uniqueID);
}

function InventoryGUI::equipEquipment(%this, %equipmentGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %uniqueID = getField(%equipmentGuiItem.info, 0);
        commandToServer('EquipTool', %uniqueID);
    }
}

function InventoryGUI::infoEquipment(%this, %equipmentGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %datablock = getField(%equipmentGuiItem.info, 1);
        
        if (!isDatablock(%datablock))
        {
            return;
        }

        InspectItem(%datablock);
    }
}

function InventoryGUI::sellEquipment(%this, %equipmentGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %uniqueID = getField(%equipmentGuiItem.info, 0);
        %datablock = getField(%equipmentGuiItem.info, 1);
        
        if (!isDatablock(%datablock))
        {
            return;
        }

        SellInvItem(%datablock, %uniqueID);
    }
}

function InventoryGUI::InitializeFurniture(%this)
{
    %this.furniture_idx[floor] = 0;
    %this.furniture_idx[wall] = 0;
    %this.furniture_idx[ceiling] = 0;

    %furnitureTab = %this._(furnitureTab);
    %furnitureTab._(floorUp).setVisible(0);
    %furnitureTab._(wallUp).setVisible(0);
    %furnitureTab._(ceilingUp).setVisible(0);
    %furnitureTab._(floorDown).setVisible(0);
    %furnitureTab._(wallDown).setVisible(0);
    %furnitureTab._(ceilingDown).setVisible(0);
}

function InventoryGUI::ChangeFurniturePage(%this, %amt, %type)
{
    %this.furniture_idx[%type] += (%amt * 5);
    InventoryGUI.updateFurnitureDisplay();
}

function InventoryGUI::updateFurnitureDisplay(%this)
{
    %furnTab = %this._(furnitureTab);
    if (!%furnTab.isAwake())
    {
        return;
    }
    
    for (%y=0; %y < 5; %y++)
    {
        %this.DisplayFurnitureRow(%y, %furnTab._(floorTab), floor);
    }
    
    for (%y=0; %y < 5; %y++)
    {
        %this.DisplayFurnitureRow(%y, %furnTab._(wallTab), wall);
    }

    for (%y=0; %y < 5; %y++)
    {
        %this.DisplayFurnitureRow(%y, %furnTab._(ceilingTab), ceiling);
    }
    
    %furnTab._(floorUp).setVisible(%this.furniture_idx[floor] > 0);
    %furnTab._(wallUp).setVisible(%this.furniture_idx[wall] > 0);
    %furnTab._(ceilingUp).setVisible(%this.furniture_idx[ceiling] > 0);
    %furnTab._(floorDown).setVisible(InventoryManager.furniture_num[floor] > ((%this.furniture_idx[floor] * 5) + 25));
    %furnTab._(wallDown).setVisible(InventoryManager.furniture_num[wall] > ((%this.furniture_idx[wall] * 5) + 25));
    %furnTab._(ceilingDown).setVisible(InventoryManager.furniture_num[ceiling] > ((%this.furniture_idx[ceiling] * 5) + 25));
}

function InventoryGUI::DisplayFurnitureRow(%this, %y, %tab, %type)
{
    %offset = (%this.furniture_idx[%type] + %y) * 5;

    for (%x=1; %x <= 5; %x++)
    {
        %idx = %offset + %x;
        %currentParent = %tab._("Slot" @ %y @ %x);
        if (%idx > InventoryManager.furniture_num[%type])
        {
            %this.DisplayFurnitureItem(%currentParent, "");
        }
        else
        {
            %this.DisplayFurnitureItem(%currentParent, InventoryManager.furniture_list[%type,%idx]);
        }
    }
}

function InventoryGUI::DisplayFurnitureItem(%this, %parent, %info)
{
    if (!isObject(%parent))
    {
        return;
    }

    if (!%parent.dragItem)
    {
        %newItem = new GuiDragDropCtrl() {
           Extent = "64 64";
           bitmap = "";
           info = "";
           RightMouseCommand = "";
           tooltipprofile = GuiToolTipProfile;
           enableDrag = "0";
           dragType = FurnitureDrag;
           superClass = FurnitureDrag;
        };
        %parent.add(%newItem);
        %parent.dragItem = %newItem;
    }
    
    if (%parent.dragItem.info $= %info)
    {
        return;
    }

    if (%info $= "")
    {
        %parent.dragItem.info = "";
        %parent.dragItem.bitmap = "";
        %parent.dragItem.RightMouseCommand = "";
        %parent.dragItem.ToolTip = "";
        %parent.dragItem.enableDrag = 0;
        return;
    }
    
    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 1);
    %iconfile = %datablock.iconFile;
    
    if (!isFile(%iconfile))
    {
        %iconfile = $icons::Inventory::Invalid;
    }

    %parent.dragItem.info = %info;
    %parent.dragItem.bitmap = %iconfile;
    %parent.dragItem.ToolTip = %datablock.GetDisplayName();
    %parent.dragItem.enableDrag = 1;
    %parent.dragItem.RightMouseCommand = "inventoryContextMenu.showContextMenu(furnitureItem,$ThisControl);";
}

function FurnitureDrag::onDoubleClick(%this)
{
    %uniqueID = getField(%this.info, 0);
    %datablock = getField(%this.info, 1);
    FurniturePlacement::PlaceFurnitureObject(%datablock, %uniqueID);
}

function InventoryGUI::dropFurniture(%this, %furnitureGuiItem)
{
}

function InventoryGUI::infoFurniture(%this, %furnitureGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %datablock = getField(%furnitureGuiItem.info, 1);
        
        if (!isDatablock(%datablock))
        {
            return;
        }

        InspectItem(%datablock);
    }
}

function InventoryGUI::sellFurniture(%this, %furnitureGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %uniqueID = getField(%furnitureGuiItem.info, 0);
        %datablock = getField(%furnitureGuiItem.info, 1);
        
        if (!isDatablock(%datablock))
        {
            return;
        }

        SellInvItem(%datablock, %uniqueID);
    }
}

function InventoryGUI::PlaceFurniture(%this, %furnitureGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %uniqueID = getField(%furnitureGuiItem.info, 0);
        %datablock = getField(%furnitureGuiItem.info, 1);
        
        if (!isDatablock(%datablock))
        {
            return;
        }

        FurniturePlacement::PlaceFurnitureObject(%datablock, %uniqueID);
    }
}

function InventoryGUI::InitializeClothing(%this)
{
    %this.clothing_idx[chest] = 0;
    %this.clothing_idx[legs] = 0;
    %this.clothing_idx[feet] = 0;
    %this.clothing_idx[hands] = 0;
    %this.clothing_idx[over] = 0;
    %this.clothing_idx[hats] = 0;
    %this.clothing_idx[face] = 0;
    %this.clothing_idx[belt] = 0;
    %this.clothing_idx[ear] = 0;
    %this.clothing_idx[neck] = 0;
    %clothingTab = %this._(clothingTab);
    %accTab = %this._(accTab);
    %clothingTab._(chestUp).setVisible(0);
    %clothingTab._(legsUp).setVisible(0);
    %clothingTab._(feetUp).setVisible(0);
    %clothingTab._(handsUp).setVisible(0);
    %clothingTab._(overUp).setVisible(0);
    %clothingTab._(chestDown).setVisible(0);
    %clothingTab._(legsDown).setVisible(0);
    %clothingTab._(feetDown).setVisible(0);
    %clothingTab._(handsDown).setVisible(0);
    %clothingTab._(overDown).setVisible(0);
    %accTab._(hatsUp).setVisible(0);
    %accTab._(faceUp).setVisible(0);
    %accTab._(beltUp).setVisible(0);
    %accTab._(earUp).setVisible(0);
    %accTab._(neckUp).setVisible(0);
    %accTab._(hatsDown).setVisible(0);
    %accTab._(faceDown).setVisible(0);
    %accTab._(beltDown).setVisible(0);
    %accTab._(earDown).setVisible(0);
    %accTab._(neckDown).setVisible(0);
}

function InventoryGUI::ChangeClothingPage(%this, %amt, %type)
{
    %this.clothing_idx[%type] += %amt;
    InventoryGUI.updateClothingDisplay();
}

function InventoryGUI::updateClothingDisplay(%this)
{
    %clothingTab = %this._(clothingTab);
    %accTab = %this._(accTab);
    
    if (!%clothingTab.isAwake())
    {
        return;
    }

    %this.DisplayClothingColumn(4, %clothingTab, chest);
    %this.DisplayClothingColumn(4, %clothingTab, legs);
    %this.DisplayClothingColumn(4, %clothingTab, feet);
    %this.DisplayClothingColumn(4, %clothingTab, hands);
    %this.DisplayClothingColumn(4, %clothingTab, over);
    %this.DisplayClothingColumn(4, %accTab, hats);
    %this.DisplayClothingColumn(4, %accTab, face);
    %this.DisplayClothingColumn(4, %accTab, belt);
    %this.DisplayClothingColumn(4, %accTab, ear);
    %this.DisplayClothingColumn(4, %accTab, neck);
}

function InventoryGUI::DisplayClothingColumn(%this, %numRows, %tab, %name)
{
    %this.DisplayEquipItem(%tab._(%name @ "Equip"), InventoryManager.clothingEquip[$clothing::type[%name]], $icons::Inventory::EquipEmpty[%name]);
    %tab._(%name @ "Up").setVisible(%this.clothing_idx[%name] > 0);
    %shift = 0;
    %downShift = 0;

    %equipIdx = InventoryManager.clothing_listEquipMap[$clothing::type[%name]];
    if (%equipIdx != 0)
    {
        if (%equipIdx <= ((%this.clothing_idx[%name] * %numRows) + %numRows))
        {
            if (%equipIdx <= (%this.clothing_idx[%name] * %numRows))
            {
                %shift = 1;
            }
        }
        else
        {
            %downShift = 1;
        }
    }

    %idx = (%this.clothing_idx[%name] * %numRows) + %shift;

    for (%i=1; %i <= %numRows; %i++)
    {
        %currentParent = %tab._(%name @ %i);
        %idx++;

        if (%idx == %equipIdx)
        {
            %idx++;
            %shift = 1;
        }

        if (%idx > InventoryManager.clothing_num[%name])
        {
            %this.DisplayClothingItem(%currentParent, "");
        }
        else
        {
            %this.DisplayClothingItem(%currentParent, InventoryManager.clothing_list[%name,%idx]);
        }
    }

    %downShift = %shift || %downShift;
    %tab._(%name @ "Down").setVisible((((%this.clothing_idx[%name] * %numRows) + %numRows) + %downShift) < InventoryManager.clothing_num[%name]);
}

function InventoryGUI::DisplayClothingItem(%this, %parent, %info)
{
    if (!isObject(%parent))
    {
        return;
    }

    if (!%parent.getCount())
    {
        %newItem = new GuiDragDropCtrl() {
           Extent = "64 64";
           bitmap = "";
           info = "";
           RightMouseCommand = "";
           tooltipprofile = GuiToolTipProfile;
           enableDrag = "0";
           dragType = ClothingDrag;
           superClass = ClothingDrop;
        };
        %parent.add(%newItem);
    }

    %parent.getObject(0).setVisible(1);
    if (%parent.getObject(0).info $= %info)
    {
        return;
    }

    if (%info $= "")
    {
        %parent.getObject(0).info = "";
        %parent.getObject(0).bitmap = "";
        %parent.getObject(0).RightMouseCommand = "";
        %parent.getObject(0).ToolTip = "";
        %parent.getObject(0).enableDrag = 0;
        
        if (%parent.getObject(0).getCount())
        {
            %parent.getObject(0).getObject(0).delete();
        }

        return;
    }

    %uniqueID = getField(%info, 0);
    %canEquip = getField(%info, 1);
    %datablock = getField(%info, 2);
    %iconfile = %datablock.iconFile;
    
    if (!isFile(%iconfile))
    {
        %iconfile = $icons::Inventory::Invalid;
    }

    %parent.getObject(0).info = %info;
    %parent.getObject(0).bitmap = %iconfile;
    if (%canEquip == 0)
    {
        if (!%parent.getObject(0).getCount())
        {
            %newItem = new GuiBitmapCtrl() {
               Extent = "64 64";
               bitmap = $icons::Inventory::noEquip;
               superClass = InvalidClothingDrop;
               tooltipprofile = GuiToolTipProfile;
            };

            %parent.getObject(0).add(%newItem);
            %newItem.RightMouseCommand = "inventoryContextMenu.showContextMenu(lockedClothingItem," @ %parent.getObject(0) @ ");";
            %newItem.ToolTip = %datablock.GetDisplayName();
        }
        %parent.getObject(0).RightMouseCommand = "";
        %parent.getObject(0).ToolTip = "";
        %parent.getObject(0).enableDrag = 0;
    }
    else
    {
        if (%parent.getObject(0).getCount())
        {
            %parent.getObject(0).getObject(0).delete();
        }

        %parent.getObject(0).RightMouseCommand = "inventoryContextMenu.showContextMenu(clothingItem,$ThisControl);";
        %parent.getObject(0).ToolTip = %datablock.GetDisplayName();
        %parent.getObject(0).enableDrag = 1;
    }
}

function InventoryGUI::DisplayEquipItem(%this, %parent, %info, %failIcon)
{
    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 2);
    if (%uniqueID == 0)
    {
        %parent.bitmap = $icons::Inventory::Slot;
        %parent.getObject(0).bitmap = %failIcon;
        %parent.getObject(0).info = "";
        %parent.getObject(0).RightMouseCommand = "";
        %parent.getObject(0).enableDrag = 0;
        %parent.getObject(0).ToolTip = "";
    }
    else
    {
        %parent.bitmap = $icons::Inventory::Slot_Equip;
        %iconfile = %datablock.iconFile;
        
        if (!isFile(%iconfile))
        {
            %iconfile = $icons::Inventory::Invalid;
        }

        %parent.getObject(0).bitmap = %iconfile;
        %parent.getObject(0).info = %info;
        %parent.getObject(0).RightMouseCommand = "inventoryContextMenu.showContextMenu(equippedClothingItem,$ThisControl);";
        %parent.getObject(0).enableDrag = 1;
        %parent.getObject(0).ToolTip = %datablock.GetDisplayName();
    }
    %parent.getObject(0).setVisible(1);
}

function ClothingDrop::canDragDrop(%this, %dragCtrl, %position)
{
    if (!(%dragCtrl.dragType $= ClothingEquipDrag))
    {
        return 0;
    }

    return 1;
}

function ClothingDrop::onDragDrop(%this, %dragCtrl, %position)
{
    if (%dragCtrl.dragType $= ClothingEquipDrag)
    {
        InventoryGUI.unequipClothing(%dragCtrl);
        %dragCtrl.setVisible(0);
    }
}

function ClothingDrop::onDoubleClick(%this)
{
    InventoryGUI.equipClothing(%this);
    %this.setVisible(0);
}

function InvalidClothingDrop::canDragDrop(%this, %dragCtrl, %position)
{
    ClothingDrop::canDragDrop(%this, %dragCtrl, %position);
}

function InvalidClothingDrop::onDragDrop(%this, %dragCtrl, %position)
{
    ClothingDrop::onDragDrop(%this, %dragCtrl, %position);
}

function InvalidClothingDrop::onDoubleClick(%this)
{
}

function EquipClothingDrop::canDragDrop(%this, %dragCtrl, %position)
{
    if (!(%dragCtrl.dragType $= ClothingDrag))
    {
        return 0;
    }

    return 1;
}

function EquipClothingDrop::onDragDrop(%this, %dragCtrl, %position)
{
    if (%dragCtrl.dragType $= ClothingDrag)
    {
        InventoryGUI.equipClothing(%dragCtrl);
        %dragCtrl.setVisible(0);
    }
}

function EquipClothingDrop::onDoubleClick(%this)
{
    InventoryGUI.unequipClothing(%this);
    %this.setVisible(0);
}

function InventoryGUI::equipClothing(%this, %clothingGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %uniqueID = getField(%clothingGuiItem.info, 0);
        %datablock = getField(%clothingGuiItem.info, 2);

        if (isDatablock(%datablock))
        {
            commandToServer('ChangeClothingSlot', %datablock.type - $clothing::numrestypes, %uniqueID);
        }
        else
        {
            error("Cannot find datablock for inventory item:" @ %uniqueID);
            MessageBoxOK("Error", "Error occured while working with this inventory item.", "");
        }
    }
}

function InventoryGUI::unequipClothing(%this, %clothingGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %uniqueID = getField(%clothingGuiItem.info, 0);
        %datablock = getField(%clothingGuiItem.info, 2);
        if (isDatablock(%datablock))
        {
            commandToServer('ChangeClothingSlot', %datablock.type - $clothing::numrestypes, 0);
        }
        else
        {
            error("Cannot find datablock for inventory item:" @ %uniqueID);
            MessageBoxOK("Error", "Error occured while working with this inventory item.", "");
        }
    }
}

function InventoryGUI::dropClothing(%this, %clothingGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %uniqueID = getField(%clothingGuiItem.info, 0);
        commandToServer('DropInventoryItem', %uniqueID);
    }
}

function InventoryGUI::infoClothing(%this, %clothingGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %datablock = getField(%clothingGuiItem.info, 2);
        if (!isDatablock(%datablock))
        {
            return;
        }
        InspectItem(%datablock);
    }
}

function InventoryGUI::sellClothing(%this, %clothingGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %uniqueID = getField(%clothingGuiItem.info, 0);
        %datablock = getField(%clothingGuiItem.info, 2);
        if (!isDatablock(%datablock))
        {
            return;
        }
        SellInvItem(%datablock, %uniqueID);
    }
}

function InventoryGUI::InitializeReserved(%this)
{
    %this.reserved_idx[hair] = 0;
    %this.reserved_idx[hairc] = 0;
    %this.reserved_idx[eyes] = 0;
    %this.reserved_idx[lips] = 0;
    %this.reserved_idx[skin] = 0;
    %headTab = %this._(headTab);
    %headTab._(hairUp).setVisible(0);
    %headTab._(haircUp).setVisible(0);
    %headTab._(eyesUp).setVisible(0);
    %headTab._(lipsUp).setVisible(0);
    %headTab._(skinUp).setVisible(0);
    %headTab._(hairDown).setVisible(0);
    %headTab._(haircDown).setVisible(0);
    %headTab._(eyesDown).setVisible(0);
    %headTab._(lipsDown).setVisible(0);
    %headTab._(skinDown).setVisible(0);
}

function InventoryGUI::ChangeReservedPage(%this, %amt, %type)
{
    %this.reserved_idx[%type] += %amt;
    InventoryGUI.updateReservedDisplay();
}

function InventoryGUI::updateReservedDisplay(%this)
{
    %headTab = %this._(headTab);
    if (!%headTab.isAwake())
    {
        return;
    }

    %this.DisplayReservedColumn(4, %headTab, hair);
    %this.DisplayReservedColumn(4, %headTab, eyes);
    %this.DisplayReservedColumn(4, %headTab, lips);
    %this.DisplaySkinTextureColumn(4, %headTab);
    %this.DisplayHairTextureColumn(4, %headTab);
}

function InventoryGUI::DisplayReservedColumn(%this, %numRows, %tab, %name)
{
    %this.DisplayReservedEquipItem(%tab._(%name @ "Equip"), InventoryManager.reservedEquip[$clothing::type[%name]]);
    
    %tab._(%name @ "Up").setVisible(%this.reserved_idx[%name] > 0);
    %shift = 0;
    %downShift = 0;

    %equipIdx = InventoryManager.reserved_listEquipMap[$clothing::type[%name]];
    if (%equipIdx != 0)
    {
        if (%equipIdx <= ((%this.reserved_idx[%name] * %numRows) + %numRows))
        {
            if (%equipIdx <= (%this.reserved_idx[%name] * %numRows))
            {
                %shift = 1;
            }
        }
        else
        {
            %downShift = 1;
        }
    }
    
    %idx = (%this.reserved_idx[%name] * %numRows) + %shift;

    for (%i=1; %i <= %numRows; %i++)
    {
        %currentParent = %tab._(%name @ %i);
        %idx++;
        
        if (%idx == %equipIdx)
        {
            %idx++;
            %shift = 1;
        }
        if (%idx > InventoryManager.reserved_num[%name])
        {
            %this.DisplayReservedItem(%currentParent, "");
        }
        else
        {
            %this.DisplayReservedItem(%currentParent, InventoryManager.reserved_list[%name,%idx]);
        }
    }

    %downShift = %shift || %downShift;
    %tab._(%name @ "Down").setVisible((((%this.reserved_idx[%name] * %numRows) + %numRows) + %downShift) < InventoryManager.reserved_num[%name]);
}

function InventoryGUI::DisplayReservedItem(%this, %parent, %info)
{
    if (!isObject(%parent))
    {
        return;
    }

    if (!%parent.getCount())
    {
        %newItem = new GuiDragDropCtrl() {
           Extent = "64 64";
           bitmap = "";
           info = "";
           RightMouseCommand = "";
           enableDrag = "0";
           dragType = ReservedClothingDrag;
           superClass = ReservedClothingDrag;
        };
        %parent.add(%newItem);
    }

    %parent.getObject(0).setVisible(1);
    if (%parent.getObject(0).info $= %info)
    {
        return;
    }

    if (%info $= "")
    {
        %parent.getObject(0).info = "";
        %parent.getObject(0).bitmap = "";
        %parent.getObject(0).RightMouseCommand = "";
        %parent.getObject(0).enableDrag = 0;
        return;
    }

    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 1);
    %iconfile = %datablock.iconFile;
    if (!isFile(%iconfile))
    {
        %iconfile = $icons::Inventory::Invalid;
    }
    
    %parent.getObject(0).info = %info;
    %parent.getObject(0).bitmap = %iconfile;
    %parent.getObject(0).RightMouseCommand = "inventoryContextMenu.showContextMenu(reservedItem,$ThisControl);";
    %parent.getObject(0).enableDrag = 1;
}

function InventoryGUI::DisplayReservedEquipItem(%this, %parent, %info)
{
    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 1);
    if (%uniqueID == 0)
    {
        %parent.bitmap = $icons::Inventory::Slot;
        %parent.getObject(0).bitmap = "";
        %parent.getObject(0).info = "";
    }
    else
    {
        %parent.bitmap = $icons::Inventory::Slot_Equip;
        %iconfile = %datablock.iconFile;
        if (!isFile(%iconfile))
        {
            %iconfile = $icons::Inventory::Invalid;
        }
        %parent.getObject(0).bitmap = %iconfile;
        %parent.getObject(0).info = %info;
    }
    %parent.getObject(0).setVisible(1);
}

function InventoryGUI::DisplaySkinTextureColumn(%this, %numRows, %tab)
{
    %sex = LocationGameConnection.GetPlayerObject().sex;
    %skinID = LocationGameConnection.GetPlayerObject().skinID;
    if (InventoryManager.OverrideSkin != 0)
    {
        %skinID = InventoryManager.OverrideSkin;
        InventoryManager.OverrideSkin = 0;
    }

    %this.DisplayTextureEquipItem(%tab._(SkinEquip), "skin" TAB %skinID, $Skins::Avatar::SkinIcon[%sex,%skinID]);
    %tab._(skinUp).setVisible(%this.reserved_idx[skin] > 0);
    %shift = 0;
    %downShift = 0;
    if (%skinID != 0)
    {
        if (%skinID <= ((%this.reserved_idx[skin] * %numRows) + %numRows))
        {
            if (%skinID <= (%this.reserved_idx[skin] * %numRows))
            {
                %shift = 1;
            }
        }
        else
        {
            %downShift = 1;
        }
    }

    %idx = (%this.reserved_idx[skin] * %numRows) + %shift;
    
    for (%i=1; %i <= %numRows; %i++)
    {
        %currentParent = %tab._("Skin" @ %i);
        %idx++;

        if (%idx == %skinID)
        {
            %idx++;
            %shift = 1;
        }
        if (%idx > $Skins::Avatar::SkinSize[%sex])
        {
            %this.DisplayTextureItem(%currentParent, "");
        }
        else
        {
            %this.DisplayTextureItem(%currentParent, "skin" TAB %idx, $Skins::Avatar::SkinIcon[%sex,%idx]);
        }
    }
    %downShift = %shift || %downShift;
    %tab._(skinDown).setVisible((((%this.reserved_idx[skin] * %numRows) + %numRows) + %downShift) < $Skins::Avatar::SkinSize[%sex]);
}

function InventoryGUI::DisplayHairTextureColumn(%this, %numRows, %tab)
{
    %sex = LocationGameConnection.GetPlayerObject().sex;
    %hairID = LocationGameConnection.GetPlayerObject().hairID;
    if (InventoryManager.OverrideHair != 0)
    {
        %hairID = InventoryManager.OverrideHair;
        InventoryManager.OverrideHair = 0;
    }

    %this.DisplayTextureEquipItem(%tab._(HairCEquip), "hair" TAB %hairID, $Skins::Avatar::HairIcon[%sex,%hairID]);
    %tab._(haircUp).setVisible(%this.reserved_idx[hairc] > 0);
    %shift = 0;
    %downShift = 0;
    if (%hairID != 0)
    {
        if (%hairID <= ((%this.reserved_idx[hairc] * %numRows) + %numRows))
        {
            if (%hairID <= (%this.reserved_idx[hairc] * %numRows))
            {
                %shift = 1;
            }
        }
        else
        {
            %downShift = 1;
        }
    }

    %idx = (%this.reserved_idx[hairc] * %numRows) + %shift;
    
    for (%i=1; %i <= %numRows; %i++)
    {
        %currentParent = %tab._("HairC" @ %i);
        %idx++;

        if (%idx == %hairID)
        {
            %idx++;
            %shift = 1;
        }
        if (%idx > $Skins::Avatar::HairSize[%sex])
        {
            %this.DisplayTextureItem(%currentParent, "");
        }
        else
        {
            %this.DisplayTextureItem(%currentParent, "hair" TAB %idx, $Skins::Avatar::HairIcon[%sex,%idx]);
        }
    }

    %downShift = %shift || %downShift;
    %tab._(haircDown).setVisible((((%this.reserved_idx[hairc] * %numRows) + %numRows) + %downShift) < $Skins::Avatar::HairSize[%sex]);
}

function InventoryGUI::DisplayTextureItem(%this, %parent, %info, %iconfile)
{
    if (!isObject(%parent))
    {
        return;
    }

    if (!%parent.getCount())
    {
        %newItem = new GuiDragDropCtrl() {
           Extent = "64 64";
           bitmap = "";
           info = "";
           RightMouseCommand = "";
           enableDrag = "0";
           dragType = ReservedTextureDrag;
           superClass = ReservedTextureDrag;
        };
        %parent.add(%newItem);
    }

    %parent.getObject(0).setVisible(1);
    if (%parent.getObject(0).info $= %info)
    {
        return;
    }

    if (%info $= "")
    {
        %parent.getObject(0).info = "";
        %parent.getObject(0).bitmap = "";
        %parent.getObject(0).RightMouseCommand = "";
        %parent.getObject(0).enableDrag = 0;
        return;
    }

    if (!isFile(%iconfile))
    {
        %iconfile = $icons::Inventory::Invalid;
    }

    %parent.getObject(0).info = %info;
    %parent.getObject(0).bitmap = %iconfile;
    %parent.getObject(0).RightMouseCommand = "inventoryContextMenu.showContextMenu(textureItem,$ThisControl);";
    %parent.getObject(0).enableDrag = 1;
}

function InventoryGUI::DisplayTextureEquipItem(%this, %parent, %info, %iconfile)
{
    %uniqueID = getField(%info, 1);
    if (%uniqueID == 0)
    {
        %parent.bitmap = $icons::Inventory::Slot;
        %parent.getObject(0).bitmap = "";
        %parent.getObject(0).info = "";
    }
    else
    {
        %parent.bitmap = $icons::Inventory::Slot_Equip;
        if (!isFile(%iconfile))
        {
            %iconfile = $icons::Inventory::Invalid;
        }
        %parent.getObject(0).bitmap = %iconfile;
        %parent.getObject(0).info = %info;
    }
    %parent.getObject(0).setVisible(1);
}

function ReservedEquipClothingDrop::canDragDrop(%this, %dragCtrl, %position)
{
    if (%dragCtrl.dragType $= ReservedClothingDrag)
    {
        return 1;
    }
    if (%dragCtrl.dragType $= ReservedTextureDrag)
    {
        return 1;
    }
    return 0;
}

function ReservedEquipClothingDrop::onDragDrop(%this, %dragCtrl, %position)
{
    if (%dragCtrl.dragType $= ReservedClothingDrag)
    {
        InventoryGUI.equipReserved(%dragCtrl);
        %dragCtrl.setVisible(0);
    }
    if (%dragCtrl.dragType $= ReservedTextureDrag)
    {
        InventoryGUI.equipTexture(%dragCtrl);
        %dragCtrl.setVisible(0);
    }
}

function ReservedEquipTextureDrop::canDragDrop(%this, %dragCtrl, %position)
{
    if (%dragCtrl.dragType $= ReservedClothingDrag)
    {
        return 1;
    }
    if (%dragCtrl.dragType $= ReservedTextureDrag)
    {
        return 1;
    }
    return 0;
}

function ReservedEquipTextureDrop::onDragDrop(%this, %dragCtrl, %position)
{
    if (%dragCtrl.dragType $= ReservedClothingDrag)
    {
        InventoryGUI.equipReserved(%dragCtrl);
        %dragCtrl.setVisible(0);
    }
    if (%dragCtrl.dragType $= ReservedTextureDrag)
    {
        InventoryGUI.equipTexture(%dragCtrl);
        %dragCtrl.setVisible(0);
    }
}

function ReservedTextureDrag::onDoubleClick(%this)
{
    InventoryGUI.equipTexture(%this);
    %this.setVisible(0);
}

function ReservedClothingDrag::onDoubleClick(%this)
{
    InventoryGUI.equipReserved(%this);
    %this.setVisible(0);
}

function InventoryGUI::equipReserved(%this, %reservedGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %uniqueID = getField(%reservedGuiItem.info, 0);
        %datablock = getField(%reservedGuiItem.info, 1);
        if (isDatablock(%datablock))
        {
            commandToServer('ChangeReservedSlot', %datablock.type, %uniqueID);
        }
        else
        {
            error("Cannot find datablock for reserved item:" @ %uniqueID);
            MessageBoxOK("Error", "Error occured while working with this inventory item.", "");
        }
    }
}

function InventoryGUI::equipTexture(%this, %textureGuiItem)
{
    if (!isObject(LocationGameConnection))
    {
        MessageBoxOK("Not in world", "You must be in the world to work with your inventory.", "");
    }
    else
    {
        %type = getField(%textureGuiItem.info, 0);
        %id = getField(%textureGuiItem.info, 1);
        commandToServer('ChangeTextureSlot', %type, %id);
    }
}

if (isObject(inventoryContextMenu))
{
    inventoryContextMenu.delete();
}

new GuiContextMenu(inventoryContextMenu) {
   Profile = "GuiMenuContextProfile";
   position = "0 0";
   Extent = "640 480";
};

function inventoryContextMenu::Initialize(%this)
{
    if (!%this.initialized)
    {
        %menuId = 0;
        %itemId = 0;
        %this.addMenu("clothingItem", %menuId++);
        %this.addMenuItem("clothingItem", "Put On", %itemId++, "InventoryGUI.equipClothing($tempVar[0]);");
        %this.addMenuItem("clothingItem", "-", %itemId++, "");
        %this.addMenuItem("clothingItem", "Info", %itemId++, "InventoryGUI.infoClothing($tempVar[0]);");
        %this.addMenuItem("clothingItem", "Sell", %itemId++, "InventoryGUI.sellClothing($tempVar[0]);");
        %itemId = 0;
        %this.addMenu("lockedClothingItem", %menuId++);
        %this.addMenuItem("lockedClothingItem", "Info", %itemId++, "InventoryGUI.infoClothing($tempVar[0]);");
        %this.addMenuItem("lockedClothingItem", "Sell", %itemId++, "InventoryGUI.sellClothing($tempVar[0]);");
        %itemId = 0;
        %this.addMenu("equippedClothingItem", %menuId++);
        %this.addMenuItem("equippedClothingItem", "Take Off", %itemId++, "InventoryGUI.unequipClothing($tempVar[0]);");
        %this.addMenuItem("equippedClothingItem", "-", %itemId++, "");
        %this.addMenuItem("equippedClothingItem", "Info", %itemId++, "InventoryGUI.infoClothing($tempVar[0]);");
        %this.addMenuItem("equippedClothingItem", "Sell", %itemId++, "InventoryGUI.sellClothing($tempVar[0]);");
        %itemId = 0;
        %this.addMenu("reservedItem", %menuId++);
        %this.addMenuItem("reservedItem", "Put On", %itemId++, "InventoryGUI.equipReserved($tempVar[0]);");
        %itemId = 0;
        %this.addMenu("textureItem", %menuId++);
        %this.addMenuItem("textureItem", "Put On", %itemId++, "InventoryGUI.equipTexture($tempVar[0]);");
        %this.addMenu("furnitureItem", %menuId++);
        %this.addMenuItem("furnitureItem", "Place", %itemId++, "InventoryGUI.placeFurniture($tempVar[0]);");
        %this.addMenuItem("furnitureItem", "-", %itemId++, "");
        %this.addMenuItem("furnitureItem", "Info", %itemId++, "InventoryGUI.infoFurniture($tempVar[0]);");
        %this.addMenuItem("furnitureItem", "Sell", %itemId++, "InventoryGUI.sellFurniture($tempVar[0]);");
        %this.addMenu("equipmentItem", %menuId++);
        %this.addMenuItem("equipmentItem", "Equip", %itemId++, "InventoryGUI.equipEquipment($tempVar[0]);");
        %this.addMenuItem("equipmentItem", "-", %itemId++, "");
        %this.addMenuItem("equipmentItem", "Info", %itemId++, "InventoryGUI.infoEquipment($tempVar[0]);");
        %this.addMenuItem("equipmentItem", "Sell", %itemId++, "InventoryGUI.sellEquipment($tempVar[0]);");
        %this.addMenu("petItem", %menuId++);
        %this.addMenuItem("petItem", "Equip", %itemId++, "InventoryGUI.equipPet($tempVar[0]);");
        %this.addMenuItem("petItem", "-", %itemId++, "");
        %this.addMenuItem("petItem", "Info", %itemId++, "InventoryGUI.infoPet($tempVar[0]);");
        %this.addMenuItem("petItem", "Sell", %itemId++, "InventoryGUI.sellPet($tempVar[0]);");
        %this.addMenu("travelItem", %menuId++);
        %this.addMenuItem("travelItem", "Equip", %itemId++, "InventoryGUI.equipTravel($tempVar[0]);");
        %this.addMenuItem("travelItem", "-", %itemId++, "");
        %this.addMenuItem("travelItem", "Info", %itemId++, "InventoryGUI.infoTravel($tempVar[0]);");
        %this.addMenuItem("travelItem", "Sell", %itemId++, "InventoryGUI.sellTravel($tempVar[0]);");
    }

    %this.initialized = 1;
}

function inventoryContextMenu::showContextMenu(%this, %menu, %item)
{
    %this.Initialize();
    %this.var[0] = %item;
    Parent::showContextMenu(%this, %menu, %layer);
}

function InventoryGUI::onCustomizeSlider(%this, %slider, %axis)
{
    commandToServer('MoveAvatarCustomAxis', %axis, %slider.getValue());
}
