
exec("./animationDlg.gui");

function AvatarAnimationTreeView::Initialize(%this)
{

    %danceFolder = %this.addFolder("Dance1");
    %this.AddAnimation(%danceFolder, "Belly Dance", "dance_belly");
    %this.AddAnimation(%danceFolder, "Breakdance", "dance_breakdance");
    %this.AddAnimation(%danceFolder, "Breakdance Flips", "dance_breakdance_flip");
    %this.AddAnimation(%danceFolder, "Breakin", "dance_breakin");
    %this.AddAnimation(%danceFolder, "Cabbage Patch", "dance_cabbage_patch");
    %this.AddAnimation(%danceFolder, "Cali Sway", "dance_cali_sway");
    %this.AddAnimation(%danceFolder, "Capoeira", "dance_capoeira");
    %this.AddAnimation(%danceFolder, "Carlton", "dance_the_carlton");
    %this.AddAnimation(%danceFolder, "Charleston", "dance_charleston_slow");
    %this.AddAnimation(%danceFolder, "Clapping", "dance_clapping");
    %this.AddAnimation(%danceFolder, "Dougie", "dance_dougie");
    %this.AddAnimation(%danceFolder, "Fist Pumping", "dance_fist_pumping");
    %this.AddAnimation(%danceFolder, "Flow", "dance_flow");
    %this.AddAnimation(%danceFolder, "Floor Kicks", "dance_floorkicks");
    %this.AddAnimation(%danceFolder, "Gangnam Style", "dance_gangnam");
    %this.AddAnimation(%danceFolder, "Groove", "dance_groove");
    %this.AddAnimation(%danceFolder, "Ground Breakin", "dance_groundbreakin");
    %this.AddAnimation(%danceFolder, "Hand Dance", "dance_handdance");
    %this.AddAnimation(%danceFolder, "Hand Weave", "dance_handweave");
    %this.AddAnimation(%danceFolder, "HipHop Kicks", "dance_hiphop_kicks");

    %danceFolder = %this.addFolder("Dance2");
    %this.AddAnimation(%danceFolder, "House", "dance_house");
    %this.AddAnimation(%danceFolder, "Knee Thrust", "dance_knee_thrust");
    %this.AddAnimation(%danceFolder, "Leg Sweeps", "dance_legsweeps");
    %this.AddAnimation(%danceFolder, "Macarena", "dance_macarena");
    %this.AddAnimation(%danceFolder, "Pulp Fiction", "dance_pulpfiction");
    %this.AddAnimation(%danceFolder, "Party Boy", "dance_partyboy");
    %this.AddAnimation(%danceFolder, "Pop & Drop", "dance_poplockanddrop");
    %this.AddAnimation(%danceFolder, "River Kicks", "dance_riverdance_kicks");
    %this.AddAnimation(%danceFolder, "River Steps", "dance_riverdance_step");
    %this.AddAnimation(%danceFolder, "Robot", "dance_robot");
    %this.AddAnimation(%danceFolder, "Running Man", "dance_running_man");

    if ($currentPlayerSex $= "Female")
    {
        %this.AddAnimation(%danceFolder, "Sexy Moves", "dance_sexy");
    }

    %this.AddAnimation(%danceFolder, "Shuffle", "dance_shuffle");
    %this.AddAnimation(%danceFolder, "Sprinkler", "dance_sprinkler");
    %this.AddAnimation(%danceFolder, "Swing", "dance_swing");
    %this.AddAnimation(%danceFolder, "Stumble", "dance_stumble");
    %this.AddAnimation(%danceFolder, "The Sammy", "dance_sammy");
    %this.AddAnimation(%danceFolder, "Thriller", "dance_thriller");
    %this.AddAnimation(%danceFolder, "Thunderclap", "dance_thunderclap");
    %this.AddAnimation(%danceFolder, "Walk It Out", "dance_walkitout");
    %this.AddAnimation(%danceFolder, "Windmill", "dance_windmill");
    
    %emoteFolder = %this.addFolder("Emote");
    %this.AddAnimation(%emoteFolder, "Back Flip", "emote_backflip");
    %this.AddAnimation(%emoteFolder, "Big Flex", "emote_flexing");
    %this.AddAnimation(%emoteFolder, "Boxing", "emote_boxing");
    %this.AddAnimation(%emoteFolder, "Changing", "changing");
    %this.AddAnimation(%emoteFolder, "Confused", "emote_confused");
    %this.AddAnimation(%emoteFolder, "Flex", "muscle_flex");
    %this.AddAnimation(%emoteFolder, "Hand Stand", "pose_statue");
    %this.AddAnimation(%emoteFolder, "Heel Click", "emote_heel_click");
    %this.AddAnimation(%emoteFolder, "Impatient", "emote_impatient");
    %this.AddAnimation(%emoteFolder, "Laugh", "laugh_hard");
    %this.AddAnimation(%emoteFolder, "Lay Down", "laydown");
    %this.AddAnimation(%emoteFolder, "Planking", "emote_planking");
    %this.AddAnimation(%emoteFolder, "Sit", "sit");

    if ($currentPlayerSex $= "Male")
    {
        %this.AddAnimation(%emoteFolder, "Sit Down", "sit_generic_male");
    }

    if ($currentPlayerSex $= "Female")
    {
        %this.AddAnimation(%emoteFolder, "Sit Down", "sit_generic_female");
    }

    %this.AddAnimation(%emoteFolder, "Snow Angel", "emote_snowangel");
    %this.AddAnimation(%emoteFolder, "Tebowing", "emote_tebowing");
    %blendFolder = %this.addFolder("Expression");
    %this.AddBlendedAnimation(%blendFolder, "Air Guitar", "blend_airguitar");
    %this.AddBlendedAnimation(%blendFolder, "Applause", "blend_applause");
    %this.AddBlendedAnimation(%blendFolder, "Beckon", "blend_beckon");
    %this.AddBlendedAnimation(%blendFolder, "Blow Kiss", "blend_blow_kiss");
    %this.AddBlendedAnimation(%blendFolder, "Bow", "blend_bow");
    %this.AddBlendedAnimation(%blendFolder, "Cry", "blend_cry");
    %this.AddBlendedAnimation(%blendFolder, "Drumming", "blend_drumming");
    %this.AddBlendedAnimation(%blendFolder, "Fan", "blend_fan");
    %this.AddBlendedAnimation(%blendFolder, "Giggle", "blend_giggle");
    %this.AddBlendedAnimation(%blendFolder, "Hand Drumming", "blend_drumming_hand");
    %this.AddBlendedAnimation(%blendFolder, "Roll Eyes", "blend_rolleyes");
    %this.AddBlendedAnimation(%blendFolder, "Rub Hands", "blend_rub_hands");
    %this.AddBlendedAnimation(%blendFolder, "Singing", "blend_singing");
    %this.AddBlendedAnimation(%blendFolder, "Singing Pause", "blend_singing_pause");
    %this.AddBlendedAnimation(%blendFolder, "Singing Point", "blend_singing_point");
    %this.AddBlendedAnimation(%blendFolder, "Singing Wave", "blend_singing_wave");
    %this.AddBlendedAnimation(%blendFolder, "Sad", "blend_sad");
    %this.AddBlendedAnimation(%blendFolder, "Talking", "blend_talking");
    %this.AddBlendedAnimation(%blendFolder, "Touch", "blend_touch");
    %this.AddBlendedAnimation(%blendFolder, "Wave", "blend_wave");
    %this.AddBlendedAnimation(%blendFolder, "Wink", "blend_wink");
    %this.AddBlendedAnimation(%blendFolder, "Writing", "blend_writing");
}

function PlayAnimationUI::onWake(%this)
{
    %this._(AnimationList).clear();
    %this._(AnimationList).buildIconTable();
    %this._(AnimationList).Initialize();
}

function ShowAnimationDialog()
{
    if (isObject(LocationGameConnection) && LocationGameConnection.isInGame)
    {
        if (!PlayAnimationUI.isAwake())
        {
            Canvas.pushDialog(PlayAnimationUI);
        }
        else
        {
            Canvas.popDialog(PlayAnimationUI);
        }
    }
    else
    {
        Canvas.popDialog(PlayAnimationUI);
        MessageBoxOK("Not in world", "You must be in the world to play animations.", "");
    }
}

function AvatarAnimationTreeView::addFolder(%this, %foldername, %parent)
{
    return %this.insertItem(%parent, %foldername, 0, "", 2, 1);
}

function AvatarAnimationTreeView::AddAnimation(%this, %parentFolder, %displayName, %animation)
{
    %this.insertItem(%parentFolder, %displayName, 1 TAB %animation TAB 0);
}

function AvatarAnimationTreeView::AddBlendedAnimation(%this, %parentFolder, %displayName, %animation)
{
    %this.insertItem(%parentFolder, %displayName, 1 TAB %animation TAB 1);
}

function AvatarAnimationTreeView::onSelect(%this, %item)
{
    %value = %this.getItemValue(%item);
    if (getField(%value, 0))
    {
        %animationName = getField(%value, 1);
        %blend = getField(%value, 2);
        commandToServer('AvatarAnimation', %animationName, %blend);
    }

    %this.clearSelection();
}

