if (!isObject(MainChatHUD))
{
    exec("./chatHud.gui");
}

new AudioProfile(pmSound) {
   description = "AudioMessage";
   preload = "1";
   fileName = "~/data/live_assets/sounds/UI/pm.ogg";
};

new AudioProfile(fmSound) {
   description = "AudioMessage";
   preload = "1";
   fileName = "~/data/live_assets/sounds/UI/fm.ogg";
};

new AudioProfile(announceSound) {
   description = "AudioMessage";
   preload = "1";
   fileName = "~/data/live_assets/sounds/UI/chimes_connect.ogg";
};

new AudioProfile(wrongToolSound) {
   description = "AudioMessage";
   preload = "1";
   fileName = "~/data/live_assets/sounds/UI/twang.ogg";
};

function MainChatHUD::show(%this)
{
    if (!%this.isAwake())
    {
        WindowBar._(chatButton).setBitmap("onverse/data/live_assets/engine/ui/images/icons/windowbar/chat");
        Canvas.pushDialog(%this);
    }
    else
    {
        Canvas.popDialog(%this);
    }
}

function MainChatHUD::focus(%this)
{
    if (!%this.isAwake())
    {
        %this.show();
    }
    chatInput.forceFirstResponder();
}

function MainChatHUD::setCommand(%this, %cmdTxt)
{
    %this.focus();
    chatInput.isAutoText = 0;
    chatInput.setText(%cmdTxt);
    chatInput.schedule(0, forceFirstResponder);
}

function MainChatHUD::onTextUpdated(%this)
{
    chatInput.isAutoText = 0;
}

function MainChatHUD::fillAutoText(%this)
{
    if ((chatInput.getText() $= "") || (chatInput.isAutoText == 1))
    {
        chatInput.setText("");
        if (%this.getSelectedTab() == %this._(friend))
        {
            chatInput.isAutoText = 1;
            chatInput.setText("/Friends ");
        }
        else
        {
            if (%this.getSelectedTab() == %this._(Guide))
            {
                chatInput.isAutoText = 1;
                chatInput.setText("/Guide ");
            }
            else
            {
                if (%this.getSelectedTab() == %this._(pm))
                {
                    chatInput.isAutoText = 1;
                    if (!(%this.replyTo $= ""))
                    {
                        chatInput.setText("/Tell \"" @ %this.replyTo @ "\" ");
                    }
                    else
                    {
                        chatInput.setText("/Tell ");
                    }
                }
            }
        }
    }
}

function MainChatHUD::getSelectedTab(%this)
{
    return %this._(tabCtrl).getObject(%this._(tabCtrl).selectedTab);
}

function MainChatHUD::selectTab(%this, %tabID)
{
    if (%tabID >= %this._(tabCtrl).getCount())
    {
        error("Invalid tab ID");
        return;
    }
    %this._(tabCtrl).setSelectedTab(%tabID);
    MainChatHUD.focus();
    MainChatHUD.fillAutoText();
}

function MainChatHUD::flashChatTab(%this, %tab)
{
    %selTab = %this.getSelectedTab();
    if ((%selTab != %this._(all)) && (%selTab != %tab))
    {
        %tab.tabIcon = %tab.flashIcon;
        %this._(tabCtrl).updateTabBitmaps();
    }
}

function chatTabCtrl::onSelectedTab(%this, %thisTab)
{
    %thisTab.tabIcon = %thisTab.normalIcon;
    if (%thisTab == MainChatHUD._(all))
    {
        for (%i=0; %i < MainChatHUD._(tabCtrl).getCount(); %i++)
        {
            %tab = MainChatHUD._(tabCtrl).getObject(%i);
            %tab.tabIcon = %tab.normalIcon;
        }
    }
    MainChatHUD._(tabCtrl).updateTabBitmaps();
    MainChatHUD.focus();
    MainChatHUD.fillAutoText();
}

function chatInput::onFirstResponder(%this)
{
    MainChatHUD.fillAutoText();
}

function MainChatHUD::showHelpInfo(%this)
{
    showTutorialPopup("onverse/data/live_assets/text/tutorials/UI_tips/chat_window/chat_help.txt");
}

function MainChatHUD::onCommandReturn(%this, %message)
{
    %this.AddMessage(%this._(all), %message, %this.GetIcon(console), "", "", $pref::chatColors[cmdret,title], $pref::chatColors[cmdret,font]);
    %selectedTab = %this._(tabCtrl).getObject(%this._(tabCtrl).selectedTab);
    if (%selectedTab != %this._(all))
    {
        %this.AddMessage(%selectedTab, %message, %this.GetIcon(console), "", "", $pref::chatColors[cmdret,title], $pref::chatColors[cmdret,font]);
    }
}

function MainChatHUD::onCommandError(%this, %message, %helpString)
{
    if (!(%helpString $= ""))
    {
        %message = %message @ "<br>" @ %helpString;
    }
    %this.AddMessage(%this._(all), %message, %this.GetIcon(console), "Error", "", $pref::chatColors[cmderror,title], $pref::chatColors[cmderror,font]);
    %selectedTab = %this._(tabCtrl).getObject(%this._(tabCtrl).selectedTab);
    if (%selectedTab != %this._(all))
    {
        %this.AddMessage(%selectedTab, %message, %this.GetIcon(console), "Error", "", $pref::chatColors[cmderror,title], $pref::chatColors[cmderror,font]);
    }
}

function MainChatHUD::onAmbiguousCommand(%this, %command, %help)
{
    %this.AddMessage(%this._(all), %help, %this.GetIcon(console), "/" @ %command, "", $pref::chatColors[cmdmatch,title], $pref::chatColors[cmdmatch,font]);
    %selectedTab = %this._(tabCtrl).getObject(%this._(tabCtrl).selectedTab);
    if (%selectedTab != %this._(all))
    {
        %this.AddMessage(%selectedTab, %help, %this.GetIcon(console), "/" @ %command, "", $pref::chatColors[cmdmatch,title], $pref::chatColors[cmdmatch,font]);
    }
}

function chatInput::onReturn(%this)
{
    _GCommandParser.handleCommand(%this.getText());
    %this.setText("");
    Canvas.checkCursor();
    %this.makeFirstResponder(0);
}

function chatInput::onTabComplete(%this, %unused)
{
    %text = _GCommandParser.tabCompleteCommand(%this.getText());
    if (!(%text $= ""))
    {
        %this.setText(%text);
    }
}

function MainChatHUD::GetIcon(%this, %icon)
{
    if (%icon $= chat)
    {
        return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/local_tab.png";
    }
    else
    {
        if (%icon $= shout)
        {
            return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/shout_tab.png";
        }
        else
        {
            if (%icon $= alert)
            {
                return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/alert_tab.png";
            }
            else
            {
                if (%icon $= console)
                {
                    return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/system_tab.png";
                }
                else
                {
                    if (%icon $= privateMessage)
                    {
                        return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/message_tab.png";
                    }
                    else
                    {
                        if (%icon $= privateMessage_reply)
                        {
                            return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/message_reply_tab.png";
                        }
                        else
                        {
                            if (%icon $= friendsMessage)
                            {
                                return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/friend_tab.png";
                            }
                            else
                            {
                                if (%icon $= guidesMessage)
                                {
                                    return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/guide_tab.png";
                                }
                                else
                                {
                                    if (%icon $= guideReply)
                                    {
                                        return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/guide_reply_tab.png";
                                    }
                                    else
                                    {
                                        if (%icon $= friendOnline)
                                        {
                                            return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/friend_online.png";
                                        }
                                        else
                                        {
                                            if (%icon $= friendRequest)
                                            {
                                                return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/friend_add_tab.png";
                                            }
                                            else
                                            {
                                                if (%icon $= friendOffline)
                                                {
                                                    return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/friend_offline.png";
                                                }
                                                else
                                                {
                                                    if (%icon $= svrmsg)
                                                    {
                                                        return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/system_tab.png";
                                                    }
                                                    else
                                                    {
                                                        if (%icon $= announce)
                                                        {
                                                            return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/announcement_tab.png";
                                                        }
                                                        else
                                                        {
                                                            if (%icon $= game)
                                                            {
                                                                return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/game_tab.png";
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return "onverse/data/live_assets/engine/ui/images/icons/chat/tabs/all_tab.png";
}

function MainChatHUD::AddMessage(%this, %tab, %message, %icon, %title, %buttonlist, %titleColor, %textColor)
{
    if (!MainChatHUD.isAwake())
    {
        WindowBar._(chatButton).setBitmap("onverse/data/live_assets/engine/ui/images/icons/windowbar/chat_orange");
    }
    while (%tab._(fieldStack).getCount() >= 100)
    {
        %tab._(fieldStack).getObject(0).delete();
    }

    %textCtrl = new GuiMLTextCtrl("") {
        superClass = "ChatTextBlock";
        Profile = "GuiChatProfile";
        position = "3 2";
        Extent = getWord(%this._(fieldStack).Extent, 0) - 4 SPC 30;
    };
    %windowCtrl = new GuiChatTransControl("") {
        Profile = "GuiChatMessageWindowProfile";
        Extent = getWord(%textCtrl.Extent, 0) + 4 SPC getWord(%textCtrl.Extent, 1) + 4;
        waitTime = 10;
        fadeoutTime = 2.5;
        buttonList = %buttonlist;
    };

    if (%titleColor $= "")
    {
        %titleColor = $pref::chatColors[defaul,title];
    }
    if (%textColor $= "")
    {
        %textColor = $pref::chatColors[defaul,font];
    }
    %linkColor = "<color:" @ %titleColor @ "><linkColor:" @ %titleColor @ ">" @ "<linkcolorhl:" @ %titleColor @ ">";
    %oldLinkColor = "<linkColor:A6A6FF><linkcolorhl:0000FF>";
    %fontText = "<font:Arial Bold:15><shadow:1:1><shadowcolor:000000FF>";
    if (!(%buttonlist $= ""))
    {
        %fontText = "<a:gamelink://messageClick/" @ %windowCtrl @ "/>" @ %fontText;
    }
    if (!(%icon $= ""))
    {
        %fontText = %fontText @ "<bitmap:" @ %icon @ ">" @ " ";
    }
    %message = "<color:" @ %textColor @ ">" @ %oldLinkColor @ %message;
    if (!(%title $= ""))
    {
        if (!(%buttonlist $= ""))
        {
            %message = %fontText @ %linkColor @ %title @ ":</a>" SPC %message;
        }
        else
        {
            %message = %fontText @ %linkColor @ %title @ ":" SPC %message;
        }
    }
    else
    {
        if (%buttonlist)
        {
            %message = %fontText @ "</a>" @ %message;
        }
        else
        {
            %message = %fontText @ %message;
        }
    }
    %textCtrl.setText(%message);
    %windowCtrl.add(%textCtrl);
    %tab._(fieldStack).add(%windowCtrl);
    %tab.showDownGlow();
    return %windowCtrl;
}

function MainChatHUD::cleanMessage(%this, %message)
{
    %cleanCount = 0;
    %dirtyMessage = "";

    while (%dirtyMessage !$= %message)
    {
        %dirtyMessage = %message;
        %message = StripMLControlChars(%dirtyMessage);
        %cleanCount++;
        if (%cleanCount > 3)
        {
            %message = strreplace(%message, "<", "");
            %message = strreplace(%message, ">", "");
            break;
        }
    }

    %message = %this.processEmotes(%message);
    %message = filterString(%message, "*");
    return %message;
}
function MainChatHUD::processEmotes(%this, %message)
{
    %parse = %message;
    %idx = 0;
    while (%parse !$= "")
    {
        %word = getWord(%parse, 0);
        %bitmap = "";
        if (%word $= ":)")
        {
            %bitmap = "smile16";
        }
        else
        {
            if (%word $= ":-)")
            {
                %bitmap = "smile16";
            }
            else
            {
                if (%word $= "=)")
                {
                    %bitmap = "smile16";
                }
                else
                {
                    if (%word $= ":]")
                    {
                        %bitmap = "smile16";
                    }
                    else
                    {
                        if (%word $= ":-D")
                        {
                            %bitmap = "smile_big16";
                        }
                        else
                        {
                            if (%word $= ":D")
                            {
                                %bitmap = "smile_big16";
                            }
                            else
                            {
                                if (%word $= "=D")
                                {
                                    %bitmap = "smile_big16";
                                }
                                else
                                {
                                    if (%word $= ":-(")
                                    {
                                        %bitmap = "frown16";
                                    }
                                    else
                                    {
                                        if (%word $= ":(")
                                        {
                                            %bitmap = "frown16";
                                        }
                                        else
                                        {
                                            if (%word $= ":[")
                                            {
                                                %bitmap = "frown16";
                                            }
                                            else
                                            {
                                                if (%word $= "=(")
                                                {
                                                    %bitmap = "frown16";
                                                }
                                                else
                                                {
                                                    if (%word $= ":-@")
                                                    {
                                                        %bitmap = "angry16";
                                                    }
                                                    else
                                                    {
                                                        if (%word $= ":@")
                                                        {
                                                            %bitmap = "angry16";
                                                        }
                                                        else
                                                        {
                                                            if (%word $= ":-P")
                                                            {
                                                                %bitmap = "raspberry16";
                                                            }
                                                            else
                                                            {
                                                                if (%word $= ":p")
                                                                {
                                                                    %bitmap = "raspberry16";
                                                                }
                                                                else
                                                                {
                                                                    if (%word $= ":-O")
                                                                    {
                                                                        %bitmap = "surprise16";
                                                                    }
                                                                    else
                                                                    {
                                                                        if (%word $= ":o")
                                                                        {
                                                                            %bitmap = "surprise16";
                                                                        }
                                                                        else
                                                                        {
                                                                            if (%word $= ";-)")
                                                                            {
                                                                                %bitmap = "wink16";
                                                                            }
                                                                            else
                                                                            {
                                                                                if (%word $= ";)")
                                                                                {
                                                                                    %bitmap = "wink16";
                                                                                }
                                                                                else
                                                                                {
                                                                                    if (%word $= "P-)")
                                                                                    {
                                                                                        %bitmap = "pirate16";
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if (%word $= "P)")
                                                                                        {
                                                                                            %bitmap = "pirate16";
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            if (%word $= "B-)")
                                                                                            {
                                                                                                %bitmap = "cool16";
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                if (%word $= "B)")
                                                                                                {
                                                                                                    %bitmap = "cool16";
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    if (%word $= "(h)")
                                                                                                    {
                                                                                                        %bitmap = "cool16";
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        if (%word $= "0:-)")
                                                                                                        {
                                                                                                            %bitmap = "angel16";
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            if (%word $= "0:)")
                                                                                                            {
                                                                                                                %bitmap = "angel16";
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                if (%word $= "(a)")
                                                                                                                {
                                                                                                                    %bitmap = "angel16";
                                                                                                                }
                                                                                                                else
                                                                                                                {
                                                                                                                    if (%word $= ">:-)")
                                                                                                                    {
                                                                                                                        %bitmap = "devil16";
                                                                                                                    }
                                                                                                                    else
                                                                                                                    {
                                                                                                                        if (%word $= ">:)")
                                                                                                                        {
                                                                                                                            %bitmap = "devil16";
                                                                                                                        }
                                                                                                                        else
                                                                                                                        {
                                                                                                                            if (%word $= "(6)")
                                                                                                                            {
                                                                                                                                %bitmap = "devil16";
                                                                                                                            }
                                                                                                                            else
                                                                                                                            {
                                                                                                                                if (%word $= ":|")
                                                                                                                                {
                                                                                                                                    %bitmap = "expressionless16";
                                                                                                                                }
                                                                                                                                else
                                                                                                                                {
                                                                                                                                    if (%word $= "<3")
                                                                                                                                    {
                                                                                                                                        %bitmap = "heart16";
                                                                                                                                    }
                                                                                                                                    else
                                                                                                                                    {
                                                                                                                                        if (%word $= "</3")
                                                                                                                                        {
                                                                                                                                            %bitmap = "heart_broken16";
                                                                                                                                        }
                                                                                                                                        else
                                                                                                                                        {
                                                                                                                                            if (%word $= ">.<")
                                                                                                                                            {
                                                                                                                                                %bitmap = "doh16";
                                                                                                                                            }
                                                                                                                                            else
                                                                                                                                            {
                                                                                                                                                if (%word $= ":(|)")
                                                                                                                                                {
                                                                                                                                                    %bitmap = "monkey16";
                                                                                                                                                }
                                                                                                                                                else
                                                                                                                                                {
                                                                                                                                                    if (%word $= "~@~")
                                                                                                                                                    {
                                                                                                                                                        %bitmap = "poop16";
                                                                                                                                                    }
                                                                                                                                                    else
                                                                                                                                                    {
                                                                                                                                                        if (%word $= ":*")
                                                                                                                                                        {
                                                                                                                                                            %bitmap = "kiss16";
                                                                                                                                                        }
                                                                                                                                                        else
                                                                                                                                                        {
                                                                                                                                                            if (%word $= ":v")
                                                                                                                                                            {
                                                                                                                                                                %bitmap = "pacman16";
                                                                                                                                                            }
                                                                                                                                                            else
                                                                                                                                                            {
                                                                                                                                                                if (%word $= "o.O")
                                                                                                                                                                {
                                                                                                                                                                    %bitmap = "confused16";
                                                                                                                                                                }
                                                                                                                                                                else
                                                                                                                                                                {
                                                                                                                                                                    if (%word $= ":\'(")
                                                                                                                                                                    {
                                                                                                                                                                        %bitmap = "cry16";
                                                                                                                                                                    }
                                                                                                                                                                    else
                                                                                                                                                                    {
                                                                                                                                                                        if (%word $= "-_-")
                                                                                                                                                                        {
                                                                                                                                                                            %bitmap = "squint16";
                                                                                                                                                                        }
                                                                                                                                                                        else
                                                                                                                                                                        {
                                                                                                                                                                            if (%word $= "(%)")
                                                                                                                                                                            {
                                                                                                                                                                                %bitmap = "yinyang16";
                                                                                                                                                                            }
                                                                                                                                                                            else
                                                                                                                                                                            {
                                                                                                                                                                                if (%word $= "8X")
                                                                                                                                                                                {
                                                                                                                                                                                    %bitmap = "skull_crossbones16";
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!(%bitmap $= ""))
        {
            %message = setWord(%message, %idx, "<bitmap:onverse/data/live_assets/engine/ui/images/icons/chat/emoticons/" @ %bitmap @ ".png>");
        }
        %parse = getWords(%parse, 1);
        %idx++;
    }
    return %message;
}

function MainChatHUD::onChatMessage(%this, %message, %from)
{
    if (_GFriendDatabase.isIgnored(%from))
    {
        return;
    }

    %buttonlist = %from;
    %message = %this.cleanMessage(%message);
    %this.AddMessage(%this._(all), %message, %this.GetIcon(chat), %from, %buttonlist, $pref::chatColors[local,title], $pref::chatColors[local,font]);
    %this.AddMessage(%this._(local), %message, %this.GetIcon(chat), %from, %buttonlist, $pref::chatColors[local,title], $pref::chatColors[local,font]);
    %this.flashChatTab(%this._(local));
}

function MainChatHUD::onChatMessageNPC(%this, %message, %from)
{
    %message = %this.cleanMessage(%message);
    %this.AddMessage(%this._(all), %message, %this.GetIcon(chat), %from, "", $pref::chatColors[local,title], $pref::chatColors[local,font]);
    %this.AddMessage(%this._(local), %message, %this.GetIcon(chat), %from, "", $pref::chatColors[local,title], $pref::chatColors[local,font]);
    %this.flashChatTab(%this._(local));
}

function MainChatHUD::onShoutMessage(%this, %message, %from)
{
    if (_GFriendDatabase.isIgnored(%from))
    {
        return;
    }

    %buttonlist = %from;
    %message = %this.cleanMessage(%message);
    %this.AddMessage(%this._(all), %message, %this.GetIcon(shout), %from, %buttonlist, $pref::chatColors[shout,title], $pref::chatColors[shout,font]);
    %this.AddMessage(%this._(local), %message, %this.GetIcon(shout), %from, %buttonlist, $pref::chatColors[shout,title], $pref::chatColors[shout,font]);
    %this.flashChatTab(%this._(local));
}

function MainChatHUD::onServerMessage(%this, %message)
{
    %message = %this.processEmotes(%message);
    %this.AddMessage(%this._(all), %message, %this.GetIcon(svrmsg), "Server", "", $pref::chatColors[svrmsg,title], $pref::chatColors[svrmsg,font]);
    %this.AddMessage(%this._(local), %message, %this.GetIcon(svrmsg), "Server", "", $pref::chatColors[svrmsg,title], $pref::chatColors[svrmsg,font]);
    %this.AddMessage(%this._(system), %message, %this.GetIcon(svrmsg), "Server", "", $pref::chatColors[svrmsg,title], $pref::chatColors[svrmsg,font]);
}

function MainChatHUD::onServerAnnouncement(%this, %message)
{
    %message = %this.processEmotes(%message);

    for (%i=0; %i < MainChatHUD._(tabCtrl).getCount(); %i++)
    {
        %this.AddMessage(MainChatHUD._(tabCtrl).getObject(%i), %message, %this.GetIcon(announce), "Announcement", "", $pref::chatColors[announ,title], $pref::chatColors[announ,font]);
    }

    alxPlay(announceSound);
}

function MainChatHUD::onClientActionMessage(%this, %message)
{
    %message = %this.cleanMessage(%message);
    %this.AddMessage(%this._(all), %message, %this.GetIcon(alert), "Action", "", $pref::chatColors[action,title], $pref::chatColors[action,font]);
    %this.AddMessage(%this._(local), %message, %this.GetIcon(alert), "Action", "", $pref::chatColors[action,title], $pref::chatColors[action,font]);
    %this.AddMessage(%this._(system), %message, %this.GetIcon(alert), "Action", "", $pref::chatColors[action,title], $pref::chatColors[action,font]);
    %this.flashChatTab(%this._(local));
}

function MainChatHUD::onFailedAttackMessage(%this, %tooltarget)
{
    %message = %tooltarget.getDataBlock().getFeatureList();
    %this.AddMessage(%this._(all), %message, %this.GetIcon(alert), "Action", "", $pref::chatColors[action,title], $pref::chatColors[action,font]);
    %this.AddMessage(%this._(local), %message, %this.GetIcon(alert), "Action", "", $pref::chatColors[action,title], $pref::chatColors[action,font]);
    %this.AddMessage(%this._(system), %message, %this.GetIcon(alert), "Action", "", $pref::chatColors[action,title], $pref::chatColors[action,font]);
    %this.flashChatTab(%this._(local));
    alxPlay(wrongToolSound);
}

function MainChatHUD::onGameMessage(%this, %message)
{
    %this.AddMessage(%this._(all), %message, %this.GetIcon(game), "Game", "", $pref::chatColors[game,title], $pref::chatColors[game,font]);
    %this.AddMessage(%this._(game), %message, %this.GetIcon(game), "Game", "", $pref::chatColors[game,title], $pref::chatColors[game,font]);
    %this.flashChatTab(%this._(game));
}

function MainChatHUD::onSentLocalMessage(%this, %message)
{
    %message = %this.cleanMessage(%message);
    %this.AddMessage(%this._(all), %message, %this.GetIcon(chat), $currentPlayerName, "", $pref::chatColors[sntlocal,title], $pref::chatColors[sntlocal,font]);
    %this.AddMessage(%this._(local), %message, %this.GetIcon(chat), $currentPlayerName, "", $pref::chatColors[sntlocal,title], $pref::chatColors[sntlocal,font]);
    %this.flashChatTab(%this._(local));
}

function MainChatHUD::onSentShoutMessage(%this, %message)
{
    %message = %this.cleanMessage(%message);
    %this.AddMessage(%this._(all), %message, %this.GetIcon(shout), $currentPlayerName, "", $pref::chatColors[sntshout,title], $pref::chatColors[sntshout,font]);
    %this.AddMessage(%this._(local), %message, %this.GetIcon(shout), $currentPlayerName, "", $pref::chatColors[sntshout,title], $pref::chatColors[sntshout,font]);
    %this.flashChatTab(%this._(local));
}

function MainChatHUD::reply(%this)
{
    if (%this.replyTo $= "")
    {
        %this.onCommandReturn("No one to reply to.");
    }
    else
    {
        %this.setCommand("/Tell \"" @ %this.replyTo @ "\" ");
    }
}

function MainChatHUD::onPrivateMessage(%this, %from, %message)
{
    if (_GFriendDatabase.isIgnored(%from))
    {
        return;
    }

    %this.replyTo = %from;
    %buttonlist = %from;
    %message = %this.cleanMessage(%message);
    %this.AddMessage(%this._(all), %message, %this.GetIcon(privateMessage), %from, %buttonlist, $pref::chatColors[pm,title], $pref::chatColors[pm,font]);
    %this.AddMessage(%this._(pm), %message, %this.GetIcon(privateMessage), %from, %buttonlist, $pref::chatColors[pm,title], $pref::chatColors[pm,font]);
    %this.flashChatTab(%this._(pm));
    alxPlay(pmSound);
    flashTaskBar();
}

function MainChatHUD::onFriendsMessage(%this, %from, %message)
{
    %buttonlist = %from;
    %message = %this.cleanMessage(%message);
    %this.AddMessage(%this._(all), %message, %this.GetIcon(friendsMessage), %from, %buttonlist, $pref::chatColors[fm,title], $pref::chatColors[fm,font]);
    %this.AddMessage(%this._(friend), %message, %this.GetIcon(friendsMessage), %from, %buttonlist, $pref::chatColors[fm,title], $pref::chatColors[fm,font]);
    %this.flashChatTab(%this._(friend));
}

function MainChatHUD::onGuideMessage(%this, %from, %message)
{
    %buttonlist = %from;
    %message = %this.processEmotes(%message);
    %this.AddMessage(%this._(all), %message, %this.GetIcon(guidesMessage), %from, %buttonlist, $pref::chatColors[gm,title], $pref::chatColors[gm,font]);
    %this.AddMessage(%this._(Guide), %message, %this.GetIcon(guidesMessage), %from, %buttonlist, $pref::chatColors[gm,title], $pref::chatColors[gm,font]);
    %this.flashChatTab(%this._(Guide));
    alxPlay(fmSound);
    flashTaskBar();
}

function MainChatHUD::onUserToGuideMessage(%this, %from, %message)
{
    %buttonlist = %from;
    %message = %this.processEmotes(%message);
    %this.AddMessage(%this._(all), %message, %this.GetIcon(guidesMessage), %from, %buttonlist, $pref::chatColors[ugm,title], $pref::chatColors[ugm,font]);
    %this.AddMessage(%this._(Guide), %message, %this.GetIcon(guidesMessage), %from, %buttonlist, $pref::chatColors[ugm,title], $pref::chatColors[ugm,font]);
    %this.flashChatTab(%this._(Guide));
    alxPlay(fmSound);
    flashTaskBar();
}

function MainChatHUD::onGuideReplyMessage(%this, %from, %to, %message)
{
    %buttonlist = "";
    if (!(%from $= "Guide"))
    {
        %buttonlist = %to;
        %title = %from SPC "to" SPC %to;
    }
    else
    {
        %title = %from;
    }
    %message = %this.processEmotes(%message);
    %this.AddMessage(%this._(all), %message, %this.GetIcon(guideReply), %title, %buttonlist, $pref::chatColors[ugm,title], $pref::chatColors[ugm,font]);
    %this.AddMessage(%this._(Guide), %message, %this.GetIcon(guideReply), %title, %buttonlist, $pref::chatColors[ugm,title], $pref::chatColors[ugm,font]);
    %this.flashChatTab(%this._(Guide));
    alxPlay(fmSound);
    flashTaskBar();
}

function MainChatHUD::onSentPrivateMessage(%this, %to, %message)
{
    %message = %this.cleanMessage(%message);
    %title = "To" SPC %to;
    %this.AddMessage(%this._(all), %message, %this.GetIcon(privateMessage_reply), %title, "", $pref::chatColors[sntpm,title], $pref::chatColors[sntpm,font]);
    %this.AddMessage(%this._(pm), %message, %this.GetIcon(privateMessage_reply), %title, "", $pref::chatColors[sntpm,title], $pref::chatColors[sntpm,font]);
    %this.flashChatTab(%this._(pm));
}

function MainChatHUD::onSentFriendsMessage(%this, %message)
{
    %message = %this.cleanMessage(%message);
    %this.AddMessage(%this._(all), %message, %this.GetIcon(friendsMessage), $currentPlayerName, "", $pref::chatColors[sntfm,title], $pref::chatColors[sntfm,font]);
    %this.AddMessage(%this._(friend), %message, %this.GetIcon(friendsMessage), $currentPlayerName, "", $pref::chatColors[sntfm,title], $pref::chatColors[sntfm,font]);
    %this.flashChatTab(%this._(friend));
}

function MainChatHUD::onSentGuideMessage(%this, %message)
{
    %message = %this.processEmotes(%message);
    %msgType = sntugm;
    if (MasterServerConnection.hasGuideLevel())
    {
        %msgType = sntgm;
    }
    %this.AddMessage(%this._(all), %message, %this.GetIcon(guidesMessage), $currentPlayerName, "", $pref::chatColors[%msgType,title], $pref::chatColors[%msgType,font]);
    %this.AddMessage(%this._(Guide), %message, %this.GetIcon(guidesMessage), $currentPlayerName, "", $pref::chatColors[%msgType,title], $pref::chatColors[%msgType,font]);
    %this.flashChatTab(%this._(Guide));
}

function MainChatHUD::onSentGuideReplyMessage(%this, %to, %message)
{
    %message = %this.processEmotes(%message);
    %title = $currentPlayerName SPC "to" SPC %to;
    %this.AddMessage(%this._(all), %message, %this.GetIcon(guideReply), %title, "", $pref::chatColors[sntugm,title], $pref::chatColors[sntugm,font]);
    %this.AddMessage(%this._(Guide), %message, %this.GetIcon(guideReply), %title, "", $pref::chatColors[sntugm,title], $pref::chatColors[sntugm,font]);
    %this.flashChatTab(%this._(Guide));
    alxPlay(fmSound);
}

function MainChatHUD::onFriendOnline(%this, %friend)
{
    %buttonlist = %friend;
    %message = "Is now online.";
    %this.AddMessage(%this._(all), %message, %this.GetIcon(friendOnline), %friend, %buttonlist, $pref::chatColors[fndon,title], $pref::chatColors[fndon,font]);
    %this.AddMessage(%this._(friend), %message, %this.GetIcon(friendOnline), %friend, %buttonlist, $pref::chatColors[fndon,title], $pref::chatColors[fndon,font]);
    %this.AddMessage(%this._(local), %message, %this.GetIcon(friendOnline), %friend, %buttonlist, $pref::chatColors[fndon,title], $pref::chatColors[fndon,font]);
    %this.AddMessage(%this._(pm), %message, %this.GetIcon(friendOnline), %friend, %buttonlist, $pref::chatColors[fndon,title], $pref::chatColors[fndon,font]);
    %this.AddMessage(%this._(Guide), %message, %this.GetIcon(friendOnline), %friend, %buttonlist, $pref::chatColors[fndon,title], $pref::chatColors[fndon,font]);
}

function MainChatHUD::onFriendOffline(%this, %friend)
{
    %buttonlist = %friend;
    %message = "Is now offline.";
    %this.AddMessage(%this._(all), %message, %this.GetIcon(friendOffline), %friend, %buttonlist, $pref::chatColors[fndoff,title], $pref::chatColors[fndoff,font]);
    %this.AddMessage(%this._(friend), %message, %this.GetIcon(friendOffline), %friend, %buttonlist, $pref::chatColors[fndoff,title], $pref::chatColors[fndoff,font]);
    %this.AddMessage(%this._(local), %message, %this.GetIcon(friendOffline), %friend, %buttonlist, $pref::chatColors[fndoff,title], $pref::chatColors[fndoff,font]);
    %this.AddMessage(%this._(pm), %message, %this.GetIcon(friendOffline), %friend, %buttonlist, $pref::chatColors[fndoff,title], $pref::chatColors[fndoff,font]);
    %this.AddMessage(%this._(Guide), %message, %this.GetIcon(friendOffline), %friend, %buttonlist, $pref::chatColors[fndoff,title], $pref::chatColors[fndoff,font]);
}

function MainChatHUD::onFriendAccept(%this, %friend)
{
    %buttonlist = %friend;
    %message = "Would like to be your friend. <a:gamelink://acceptFriend/" @ %friend @ "><bitmap:onverse/data/live_assets/engine/ui/images/icons/chat/chat_addfriend.png><bitmap:onverse/data/live_assets/engine/ui/images/icons/chat/chat_block.png></a>";
    %this.AddMessage(%this._(all), %message, %this.GetIcon(friendRequest), %friend, %buttonlist, $pref::chatColors[fndacc,title], $pref::chatColors[fndacc,font]);
    %this.AddMessage(%this._(friend), %message, %this.GetIcon(friendRequest), %friend, %buttonlist, $pref::chatColors[fndacc,title], $pref::chatColors[fndacc,font]);
    %this.AddMessage(%this._(local), %message, %this.GetIcon(friendRequest), %friend, %buttonlist, $pref::chatColors[fndacc,title], $pref::chatColors[fndacc,font]);
    %this.AddMessage(%this._(pm), %message, %this.GetIcon(friendRequest), %friend, %buttonlist, $pref::chatColors[fndacc,title], $pref::chatColors[fndacc,font]);
}

function ChatTextBlock::onResize(%this, %x, %y)
{
    %this.getParent().resize(0, 0, %x + 4, %y + 4);
    if (%this.getCount() > 0)
    {
        %stackCtrl = %this.getObject(0);
        %stackCtrl.setPosition(getWord(%this.getExtent(), 0) - getWord(%stackCtrl.getExtent(), 0), 0);
    }
    %stackCtrl = %this.getParent().getParent();
    %stackCtrl.resize(getWord(%stackCtrl.position, 0), getWord(%stackCtrl.position, 1), getWord(%stackCtrl.Extent, 0), (getWord(%stackCtrl.Extent, 1) + %y) + 5);
}

function ChatTextBlock::handleGameURL(%this, %urlarray, %fields)
{
    if (%fields >= 1)
    {
        %arguments = getFields(%urlarray, 1);
        if (%fields >= 2)
        {
            if (getField(%urlarray, 0) $= "messageClick")
            {
                %buttonlist = getField(%arguments, 0).buttonList;
                FriendList::ShowLocalContext(0, %buttonlist);
                return 1;
            }
            else
            {
                if (getField(%urlarray, 0) $= "acceptFriend")
                {
                    %username = getField(%arguments, 0);
                    friendsListContextMenu.showContextMenu("acceptFriend", %username);
                    return 1;
                }
            }
        }
    }
    return Parent::handleGameURL(%this, %urlarray, %fields);
}

