
function InventoryManager::Create()
{
    if (!isObject(InventoryManager))
    {
        new ScriptObject(InventoryManager) {
        };
    }
    InventoryManager.isValid = 0;
}

function InventoryManager::download(%this)
{
    InventoryManager.isValid = 1;
    %this.downloadPets();
    %this.downloadTravel();
    %this.downloadTools();
    %this.downloadClothing();
    %this.downloadReserved();
    %this.downloadFurniture();
}

function clientCmdOnInventoryMessage(%msgType, %invType, %info)
{
    %msgType = detag(%msgType);
    %invType = detag(%invType);
    if (%invType $= "Pets")
    {
        InventoryManager.handlePetsMessage(%msgType, %info);
        InventoryGUI.updatePetDisplay();
    }
    else
    {
        if (%invType $= "Travel")
        {
            InventoryManager.handleTravelMessage(%msgType, %info);
            InventoryGUI.updateTravelDisplay();
        }
        else
        {
            if (%invType $= "Tools")
            {
                InventoryManager.handleToolsMessage(%msgType, %info);
                InventoryGUI.updateEquipmentDisplay();
            }
            else
            {
                if (%invType $= "Clothing")
                {
                    InventoryManager.handleClothingMessage(%msgType, %info);
                    InventoryGUI.updateClothingDisplay();
                }
                else
                {
                    if (%invType $= "Reserved")
                    {
                        InventoryManager.handleReservedMessage(%msgType, %info);
                        InventoryGUI.updateReservedDisplay();
                    }
                    else
                    {
                        if (%invType $= "Furniture")
                        {
                            InventoryManager.handleFurnitureMessage(%msgType, %info);
                            InventoryGUI.updateFurnitureDisplay();
                        }
                        else
                        {
                            error("Received invalid inventory type:" SPC %invType);
                        }
                    }
                }
            }
        }
    }
}

function InventoryManager::downloadTools(%this)
{
    commandToServer('GetToolsInventory');
}

function InventoryManager::handleToolsMessage(%this, %msgType, %info)
{
    if (%msgType $= "StartItems")
    {
        %this.clearTools();
    }
    else
    {
        if (%msgType $= "AddItems")
        {
            %count = getRecordCount(%info);
            for (%i=0; %i < %count; %i++)
            {
                %this.onAddTool(getRecord(%info, %i));
            }
            EquipBar.refresh();
        }
        else
        {
            if (%msgType $= "RemoveItems")
            {
                %count = getRecordCount(%info);
                for (%i=0; %i < %count; %i++)
                {
                    %this.onRemoveTool(getRecord(%info, %i));
                }
            }
            else
            {
                error("Received invalid tool message type:" SPC %msgType);
            }
        }
    }
}

function InventoryManager::clearTools(%this, %info)
{
    %this.equipment_num[tools] = 0;
}

function InventoryManager::getToolType(%this, %datablock, %uniqueID)
{
    return tools;
}

function InventoryManager::onAddTool(%this, %info)
{
    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 1);
    if (!isDatablock(%datablock))
    {
        error("Cannot find given tool datablock:" @ %datablock);
        return;
    }

    %type = %this.getToolType(%datablock, %uniqueID);
    if (!(%type $= ""))
    {
        %this.equipment_num[%type]++;
        %this.equipment_list[%type,%this.equipment_num[%type]] = %info;
    }
}

function InventoryManager::onRemoveTool(%this, %info)
{
    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 1);
    if (!isDatablock(%datablock))
    {
        error("Cannot find given tool datablock:" @ %datablock);
        return;
    }

    %type = %this.getToolType(%datablock, %uniqueID);
    if (!(%type $= ""))
    {
        for (%i=1; %i <= %this.equipment_num[%type]; %i++)
        {
            if (%this.equipment_list[%type,%i] $= %info)
            {
                %i++;
                break;
            }
        }

        while (%i <= %this.equipment_num[%type])
        {
            %this.equipment_list[%type,%i - 1] = %this.equipment_list[%type,%i];
            %i++;
        }

        %this.equipment_num[%type]--;
    }
    EquipBar.emptyEquipmentSlot(1, %uniqueID);
}

function InventoryManager::getToolInfo(%this, %uniqueID)
{
    for (%i=1; %i <= %this.equipment_num[tools]; %i++)
    {
        %info = %this.equipment_list[tools,%i];
        if (getField(%info, 0) == %uniqueID)
        {
            return %info;
        }
    }
    return "";
}

function InventoryManager::downloadPets(%this)
{
    commandToServer('GetPetsInventory');
}

function InventoryManager::handlePetsMessage(%this, %msgType, %info)
{
    if (%msgType $= "StartItems")
    {
        %this.clearPets();
    }
    else
    {
        if (%msgType $= "AddItems")
        {
            %count = getRecordCount(%info);
            for (%i=0; %i < %count; %i++)
            {
                %this.onAddPet(getRecord(%info, %i));
            }
            EquipBar.refresh();
        }
        else
        {
            if (%msgType $= "RemoveItems")
            {
                %count = getRecordCount(%info);
                for (%i=0; %i < %count; %i++)
                {
                    %this.onRemovePet(getRecord(%info, %i));
                }
            }
            else
            {
                if (%msgType $= "UpdateItems")
                {
                    %count = getRecordCount(%info);
                    for (%i=0; %i < %count; %i++)
                    {
                        %this.onUpdatePet(getRecord(%info, %i));
                    }
                }
                else
                {
                    error("Received invalid pet message type:" SPC %msgType);
                }
            }
        }
    }
}

function InventoryManager::clearPets(%this, %info)
{
    %this.pet_num[Pets] = 0;
}

function InventoryManager::getPetType(%this, %datablock, %uniqueID)
{
    return Pets;
}

function InventoryManager::onAddPet(%this, %info)
{
    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 1);
    if (!isDatablock(%datablock))
    {
        error("Cannot find given pet datablock:" @ %datablock);
        return;
    }

    %type = %this.getPetType(%datablock, %uniqueID);
    if (!(%type $= ""))
    {
        %this.pet_num[%type]++;
        %this.pet_list[%type,%this.pet_num[%type]] = %info;
    }
}

function InventoryManager::onUpdatePet(%this, %info)
{
    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 1);
    if (!isDatablock(%datablock))
    {
        error("Cannot find given pet datablock:" @ %datablock);
        return;
    }

    %type = %this.getPetType(%datablock, %uniqueID);
    for (%i=1; %i <= %this.pet_num[%type]; %i++)
    {
        %curInfo = %this.pet_list[%type,%i];
        if (getField(%curInfo, 0) $= getField(%info, 0))
        {
            %this.pet_list[%type,%i] = %info;
            break;
        }
    }
}

function InventoryManager::onRemovePet(%this, %info)
{
    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 1);
    if (!isDatablock(%datablock))
    {
        error("Cannot find given pet datablock:" @ %datablock);
        return;
    }

    %type = %this.getPetType(%datablock, %uniqueID);
    if (!(%type $= ""))
    {
        for (%i=1; %i <= %this.pet_num[%type]; %i++)
        {
            if (getFields(%this.pet_list[%type,%i], 0, 1) $= getFields(%info, 0, 1))
            {
                %i++;
                break;
            }
        }

        while (%i <= %this.pet_num[%type])
        {
            %this.pet_list[%type,%i - 1] = %this.pet_list[%type,%i];
            %i++;
        }

        %this.pet_num[%type]--;
    }
    EquipBar.emptyEquipmentSlot(2, %uniqueID);
}

function InventoryManager::getPetInfo(%this, %uniqueID)
{
    for (%i=1; %i <= %this.pet_num[Pets]; %i++)
    {
        %info = %this.pet_list[Pets,%i];
        if (getField(%info, 0) == %uniqueID)
        {
            return %info;
        }
    }
    return "";
}

function InventoryManager::downloadTravel(%this)
{
    commandToServer('GetTravelInventory');
}

function InventoryManager::handleTravelMessage(%this, %msgType, %info)
{
    if (%msgType $= "StartItems")
    {
        %this.clearTravel();
    }
    else
    {
        if (%msgType $= "AddItems")
        {
            %count = getRecordCount(%info);
            for (%i=0; %i < %count; %i++)
            {
                %this.onAddTravel(getRecord(%info, %i));
            }
            EquipBar.refresh();
        }
        else
        {
            if (%msgType $= "RemoveItems")
            {
                %count = getRecordCount(%info);
                for (%i=0; %i < %count; %i++)
                {
                    %this.onRemoveTravel(getRecord(%info, %i));
                }
            }
            else
            {
                error("Received invalid travel mount message type:" SPC %msgType);
            }
        }
    }
}

function InventoryManager::clearTravel(%this, %info)
{
    %this.travel_num[mounts] = 0;
}

function InventoryManager::getTravelType(%this, %datablock, %uniqueID)
{
    return mounts;
}

function InventoryManager::onAddTravel(%this, %info)
{
    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 1);

    if (!isDatablock(%datablock))
    {
        error("Cannot find given travel datablock:" @ %datablock);
        return;
    }

    %type = %this.getTravelType(%datablock, %uniqueID);
    if (!(%type $= ""))
    {
        %this.travel_num[%type]++;
        %this.travel_list[%type,%this.travel_num[%type]] = %info;
    }
}

function InventoryManager::onRemoveTravel(%this, %info)
{
    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 1);
    if (!isDatablock(%datablock))
    {
        error("Cannot find given travel datablock:" @ %datablock);
        return;
    }

    %type = %this.getTravelType(%datablock, %uniqueID);
    if (!(%type $= ""))
    {
        for (%i=1; %i <= %this.travel_num[%type]; %i++)
        {
            if (getFields(%this.travel_list[%type,%i], 0, 1) $= getFields(%info, 0, 1))
            {
                %i++;
                break;
            }
        }
        
        while (%i <= %this.travel_num[%type])
        {
            %this.travel_list[%type,%i - 1] = %this.travel_list[%type,%i];
            %i++;
        }

        %this.travel_num[%type]--;
    }
    EquipBar.emptyEquipmentSlot(3, %uniqueID);
}

function InventoryManager::getTravelInfo(%this, %uniqueID)
{
    for (%i=1; %i <= %this.travel_num[mounts]; %i++)
    {
        %info = %this.travel_list[mounts,%i];
        if (getField(%info, 0) == %uniqueID)
        {
            return %info;
        }
    }
    return "";
}

function InventoryManager::downloadFurniture(%this)
{
    commandToServer('GetFurnitureInventory');
}

function InventoryManager::handleFurnitureMessage(%this, %msgType, %info)
{
    if (%msgType $= "StartItems")
    {
        %this.clearFurniture();
    }
    else
    {
        if (%msgType $= "AddItems")
        {
            %count = getRecordCount(%info);
            for (%i=0; %i < %count; %i++)
            {
                %this.onAddFurniture(getRecord(%info, %i));
            }
        }
        else
        {
            if (%msgType $= "RemoveItems")
            {
                %count = getRecordCount(%info);
                for (%i=0; %i < %count; %i++)
                {
                    %this.onRemoveFurniture(getRecord(%info, %i));
                }
            }
            else
            {
                error("Received invalid furniture message type:" SPC %msgType);
            }
        }
    }
}

function InventoryManager::clearFurniture(%this, %info)
{
    %this.furniture_num[floor] = 0;
    %this.furniture_num[wall] = 0;
    %this.furniture_num[ceiling] = 0;
}

function InventoryManager::getFurnitureType(%this, %datablock)
{
    %type = "";

    if (%datablock.furnType == 0)
    {
        %type = floor;
    }
    else
    {
        if (%datablock.furnType == 1)
        {
            %type = ceiling;
        }
        else
        {
            if (%datablock.furnType == 2)
            {
                %type = wall;
            }
            else
            {
                error("Uknown furniture type for furniture id:" @ %uniqueID @ " type:" @ %datablock.furnType);
            }
        }
    }

    return %type;
}

function InventoryManager::onAddFurniture(%this, %info)
{
    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 1);
    if (!isDatablock(%datablock))
    {
        error("Cannot find given furniture datablock:" @ %datablock);
        return;
    }

    %type = %this.getFurnitureType(%datablock);
    if (!(%type $= ""))
    {
        %this.furniture_num[%type]++;
        %this.furniture_list[%type,%this.furniture_num[%type]] = %info;
    }
}

function InventoryManager::onRemoveFurniture(%this, %info)
{
    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 1);
    if (!isDatablock(%datablock))
    {
        error("Cannot find given furniture datablock:" @ %datablock);
        return;
    }

    %type = %this.getFurnitureType(%datablock);
    if (!(%type $= ""))
    {
        for (%i=1; %i <= %this.furniture_num[%type]; %i++)
        {
            if (%this.furniture_list[%type,%i] $= %info)
            {
                %i++;
                break;
            }
        }

        while (%i <= %this.furniture_num[%type])
        {
            %this.furniture_list[%type,%i - 1] = %this.furniture_list[%type,%i];
            %i++;
        }

        %this.furniture_num[%type]--;
    }
}

function InventoryManager::downloadClothing(%this)
{
    commandToServer('GetClothingInventory');
}

function InventoryManager::handleClothingMessage(%this, %msgType, %info)
{
    if (%msgType $= "Equipped")
    {
        %this.onEquippedClothing(%info);
    }
    else
    {
        if (%msgType $= "StartItems")
        {
            %this.clearClothing();
        }
        else
        {
            if (%msgType $= "AddItems")
            {
                %count = getRecordCount(%info);

                for (%i=0; %i < %count; %i++)
                {
                    %this.onAddClothing(getRecord(%info, %i));
                }
            }
            else
            {
                if (%msgType $= "RemoveItems")
                {
                    %count = getRecordCount(%info);
                    
                    for (%i=0; %i < %count; %i++)
                    {
                        %this.onRemoveClothing(getRecord(%info, %i));
                    }
                }
                else
                {
                    error("Received invalid clothing message type:" SPC %msgType);
                }
            }
        }
    }
}

function InventoryManager::clearClothing(%this, %info)
{
    %this.clothing_num[chest] = 0;
    %this.clothing_num[legs] = 0;
    %this.clothing_num[feet] = 0;
    %this.clothing_num[hands] = 0;
    %this.clothing_num[over] = 0;
    %this.clothing_num[hats] = 0;
    %this.clothing_num[face] = 0;
    %this.clothing_num[belt] = 0;
    %this.clothing_num[ear] = 0;
    %this.clothing_num[neck] = 0;
}

function InventoryManager::onEquippedClothing(%this, %info)
{
    if (getRecordCount(%info) != $clothing::numnonrestypes)
    {
        error("Received invalid number of equips from server.");
    }

    for (%i=1; %i <= $clothing::numnonrestypes; %i++)
    {
        %this.clothingEquip[$clothing::numrestypes + %i] = getRecord(%info, %i - 1);
    }

    %this.SetClothingEquipMap(chest);
    %this.SetClothingEquipMap(legs);
    %this.SetClothingEquipMap(feet);
    %this.SetClothingEquipMap(hands);
    %this.SetClothingEquipMap(over);
    %this.SetClothingEquipMap(hats);
    %this.SetClothingEquipMap(face);
    %this.SetClothingEquipMap(belt);
    %this.SetClothingEquipMap(ear);
    %this.SetClothingEquipMap(neck);
}

function InventoryManager::SetClothingEquipMap(%this, %name)
{
    %uniqueID = getField(%this.clothingEquip[$clothing::type[%name]], 0);
    if (%uniqueID != 0)
    {
        for (%i=1; %i <= %this.clothing_num[%name]; %i++)
        {
            if (getField(%this.clothing_list[%name,%i], 0) == %uniqueID)
            {
                %this.clothing_listEquipMap[$clothing::type[%name]] = %i;
                break;
            }
        }
    }
    else
    {
        %this.clothing_listEquipMap[$clothing::type[%name]] = 0;
    }
}

function InventoryManager::getClothingType(%this, %datablock)
{
    %type = "";

    if (%datablock.type == $clothing::type[chest])
    {
        %type = chest;
    }
    else
    {
        if (%datablock.type == $clothing::type[legs])
        {
            %type = legs;
        }
        else
        {
            if (%datablock.type == $clothing::type[feet])
            {
                %type = feet;
            }
            else
            {
                if (%datablock.type == $clothing::type[hands])
                {
                    %type = hands;
                }
                else
                {
                    if (%datablock.type == $clothing::type[over])
                    {
                        %type = over;
                    }
                    else
                    {
                        if (%datablock.type == $clothing::type[hats])
                        {
                            %type = hats;
                        }
                        else
                        {
                            if (%datablock.type == $clothing::type[face])
                            {
                                %type = face;
                            }
                            else
                            {
                                if (%datablock.type == $clothing::type[belt])
                                {
                                    %type = belt;
                                }
                                else
                                {
                                    if (%datablock.type == $clothing::type[ear])
                                    {
                                        %type = ear;
                                    }
                                    else
                                    {
                                        if (%datablock.type == $clothing::type[neck])
                                        {
                                            %type = neck;
                                        }
                                        else
                                        {
                                            error("Uknown clothing type for clothing id:" @ %uniqueID @ " type:" @ %datablock.type);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return %type;
}

function InventoryManager::onAddClothing(%this, %info)
{
    %uniqueID = getField(%info, 0);
    %canEquip = getField(%info, 1);
    %datablock = getField(%info, 2);
    if (!isDatablock(%datablock))
    {
        error("Cannot find given clothing datablock:" @ %datablock);
        return;
    }

    %type = %this.getClothingType(%datablock);
    if (!(%type $= ""))
    {
        %this.clothing_num[%type]++;
        %this.clothing_list[%type,%this.clothing_num[%type]] = %info;
    }
}

function InventoryManager::onRemoveClothing(%this, %info)
{
    %uniqueID = getField(%info, 0);
    %canEquip = getField(%info, 1);
    %datablock = getField(%info, 2);
    if (!isDatablock(%datablock))
    {
        error("Cannot find given clothing datablock:" @ %datablock);
        return;
    }

    %type = %this.getClothingType(%datablock);
    if (!(%type $= ""))
    {
        for (%i=1; %i <= %this.clothing_num[%type]; %i++)
        {
            if (%this.clothing_list[%type,%i] $= %info)
            {
                %i++;
                break;
            }
        }

        while (%i <= %this.clothing_num[%type])
        {
            %this.clothing_list[%type,%i - 1] = %this.clothing_list[%type,%i];
            %i++;
        }
        
        %this.clothing_num[%type]--;
    }
}

function InventoryManager::downloadReserved(%this)
{
    commandToServer('GetReservedClothingInventory');
}

function InventoryManager::handleReservedMessage(%this, %msgType, %info)
{
    if (%msgType $= "Equipped")
    {
        %this.onEquippedReserved(%info);
    }
    else
    {
        if (%msgType $= "StartItems")
        {
            %this.clearReservedClothing();
        }
        else
        {
            if (%msgType $= "AddItems")
            {
                %count = getRecordCount(%info);
                for (%i=0; %i < %count; %i++)
                {
                    %this.onAddReservedClothing(getRecord(%info, %i));
                }
            }
            else
            {
                if (%msgType $= "TextureChange")
                {
                    %this.OverrideSkin = getField(%info, 0);
                    %this.OverrideHair = getField(%info, 1);
                }
                else
                {
                    error("Received invalid clothing message type:" SPC %msgType);
                }
            }
        }
    }
}

function InventoryManager::clearReservedClothing(%this, %info)
{
    %this.reserved_num[hair] = 0;
    %this.reserved_num[hairc] = 0;
    %this.reserved_num[eyes] = 0;
    %this.reserved_num[lips] = 0;
    %this.reserved_num[skin] = 0;
}

function InventoryManager::onEquippedReserved(%this, %info)
{
    if (getRecordCount(%info) != $clothing::numrestypes)
    {
        error("Received invalid number of reserved equips from server.");
    }

    for (%i=1; %i <= $clothing::numrestypes; %i++)
    {
        %this.reservedEquip[%i] = getRecord(%info, %i - 1);
    }

    %this.SetReservedEquipMap(hair);
    %this.SetReservedEquipMap(eyes);
    %this.SetReservedEquipMap(lips);
}

function InventoryManager::SetReservedEquipMap(%this, %name)
{
    %uniqueID = getField(%this.reservedEquip[$clothing::type[%name]], 0);
    if (%uniqueID != 0)
    {
        for (%i=1; %i <= %this.reserved_num[%name]; %i++)
        {
            if (getField(%this.reserved_list[%name,%i], 0) == %uniqueID)
            {
                %this.reserved_listEquipMap[$clothing::type[%name]] = %i;
                break;
            }
        }
    }
    else
    {
        %this.reserved_listEquipMap[$clothing::type[%name]] = 0;
    }
}

function InventoryManager::onAddReservedClothing(%this, %info)
{
    %uniqueID = getField(%info, 0);
    %datablock = getField(%info, 1);

    if (!isDatablock(%datablock))
    {
        error("Cannot find given reserved clothing datablock:" @ %datablock);
        return;
    }

    if (%datablock.type == $clothing::type[hair])
    {
        %this.reserved_num[hair]++;
        %this.reserved_list[hair,%this.reserved_num[hair]] = %info;
    }
    else
    {
        if (%datablock.type == $clothing::type[eyes])
        {
            %this.reserved_num[eyes]++;
            %this.reserved_list[eyes,%this.reserved_num[eyes]] = %info;
        }
        else
        {
            if (%datablock.type == $clothing::type[lips])
            {
                %this.reserved_num[lips]++;
                %this.reserved_list[lips,%this.reserved_num[lips]] = %info;
            }
            else
            {
                error("Uknown reserved clothing type for id:" @ %uniqueID @ " type:" @ %datablock.type);
            }
        }
    }
}

function InventoryManager::downloadCustomAxes(%this)
{
}
