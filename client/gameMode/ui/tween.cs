// DSONOTE: Not actually executed
function Tween::onAdd(%this)
{
    %this.startValue = 0;
    %this.endValue = 1;
    %this.duration = 1;
    %this.framesPerSecond = 30;
    %this.stepCallback = "";
    %this.doneCallback = "";
    %this.value = 0;
    %this.fadeHandle = 0;
}

function Tween::start(%this)
{
    %this.stop();
    %this.value = %this.startValue;
    %this.target = %this.endValue;
    %this.delta = (%this.target - %this.value) / (%this.duration * %this.framesPerSecond);
    %this.stepMS = 1000 / %this.framesPerSecond;
    %this.Step();
}

function Tween::StartReverse(%this)
{
    %this.stop();
    %this.value = %this.endValue;
    %this.target = %this.startValue;
    %this.delta = (%this.target - %this.value) / (%this.duration * %this.framesPerSecond);
    %this.stepMS = 1000 / %this.framesPerSecond;
    %this.Step();
}

function Tween::stop(%this)
{
    if (%this.fadeHandle > 0)
    {
        cancel(%this.fadeHandle);
        %this.fadeHandle = 0;
    }
}

function Tween::Step(%this)
{
    %this.value = %this.value + %this.delta;
    if ( ((%this.delta > 0) && (%this.value >= %this.target)) || ((%this.delta < 0) && (%this.value <= %this.target))     )
    {
        %this.value = %this.target;
        if (%this.doneCallback != 0)
        {
            eval(%this.doneCallback @ "(" @ %this.value @ ");");
        }
    }
    else
    {
        if (%this.stepCallback != 0)
        {
            eval(%this.stepCallback @ "(" @ %this.value @ ");");
        }
        %this.fadeHandle = %this.schedule(%this.stepMS, "Step");
    }
}
