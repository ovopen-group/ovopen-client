new GuiControlProfile(InGameUIProfile) {
   canKeyFocus = "1";
};

new GuiControlProfile(GuiGameScrollProfile : GuiScrollProfile) {
   canKeyFocus = "0";
};

new GuiControlProfile(GuiGameTreeViewProfile : GuiTreeViewProfile) {
   canKeyFocus = "0";
};

new GuiControlProfile(GuiOnverseHUDProfile) {
   fontSize = "18";
};

new GuiControlProfile(GuiCarHUDProfile) {
   fontType = "SpeedO";
   fontSize = "78";
   fontColors[0] = "255 255 255";
   bitmap = "~/data/live_assets/engine/ui/images/hud/car_hud_ticks";
};

new GuiControlProfile(GuiChatWindowProfile : GuiOnverseWindowProfile) {
   gradient = "0";
   bitmap = "~/data/live_assets/engine/ui/images/onverse_chat";
   fillColors[0] = "0 0 0 191";
   fillColors[1] = "0 0 0 191";
   fillColors[2] = "0 0 0 191";
   fillColors[3] = "0 0 0 191";
   textOffset = "4 4";
   justify = "right";
};

new GuiControlProfile(GuiChatTabProfile : GuiTabProfile) {
   borderThickness = "0";
   fillColor = "0 0 0 0";
   fillColorHL = "0 0 0 0";
   fillColorNA = "0 0 0 0";
   borderColor = "0 0 0 0";
   borderColorHL = "0 0 0 0";
   borderColorNA = "0 0 0 0";
   bitmap = "";
};

new GuiControlProfile(GuiChatProfile : GuiDefaultProfile) {
   fontColors[0] = "255 255 255";
   fontColorLink = "0 0 255";
   canKeyFocus = "1";
};

new GuiControlProfile(GuiChatScrollProfile : GuiScrollProfile) {
   fillColor = "232 241 247 0";
   canKeyFocus = "1";
};

new GuiControlProfile(GuiChatTextEditProfile : GuiTextEditProfile) {
   fillColor = "232 241 247";
   tab = "0";
   canKeyFocus = "1";
};

new GuiControlProfile(GuiChatMessageWindowProfile : GuiWindowProfile) {
   border = "0";
   borderColor = "255 255 255 25";
   opaque = "1";
   gradient = "0";
   fillColors[0] = "0 0 0 128";
};

