
exec("./inGameUI.gui");
exec("./lagIcon.gui");

$inGameUI::currentInGameUI = inGameUI;

function getCurrentInGameUI()
{
    return $inGameUI::currentInGameUI;
}

function setCurrentInGameUI(%ui, %force)
{
    if (%force || ($inGameUI::currentInGameUI.getId() != %ui.getId()))
    {
        %doActionMode = 0;

        if ($inGameUI::currentInGameUI._(actionModeText).isVisible())
        {
            %doActionMode = 1;
        }

        $inGameUI::currentInGameUI = %ui;
        Canvas.setContent(%ui);

        if (%doActionMode)
        {
            %ui.toggleActionMode();
        }
    }
}

function inGameUIClass::onWake(%this)
{
    cursorOn();

    if (GameStatUI.enabled)
    {
        GameStatUI.disable();
        GameStatUI.Enable();
    }

    Parent::onWake(%this);
}

function inGameUIBaseClass::onWake(%this)
{
    if (isObject(%this._(currencyBitmap)))
    {
        %this._(currencyBitmap).setVisible(1);
        %this.onUpdatePoints(LocationGameConnection.CC, LocationGameConnection.PP);
    }

    if (isObject(%this._(onverseHud)))
    {
        %this._(onverseHud).setVisible(1);
    }

    %this._(actionModeText).setVisible(0);

    if (LocationGameConnection.getIsSubscriber())
    {
        %this._(currencyBitmap).setBitmap("onverse/data/live_assets/engine/ui/images/cc_pp_vip.png");
    }
    else
    {
        %this._(currencyBitmap).setBitmap("onverse/data/live_assets/engine/ui/images/cc_pp_get_vip.png");
    }
}

function inGameUIBaseClass::toggleActionMode(%this)
{
    $mvFreeLook = 0;

    if (Canvas.isCursorOn())
    {
        cursorOff();
        playerModeActionMap.push();
        %this._(actionModeText).setVisible(1);
    }
    else
    {
        cursorOn();
        playerModeActionMap.pop();
        %this._(actionModeText).setVisible(0);
    }
}

function inGameUIBaseClass::toggleUI(%this)
{
    if (WindowBar.isAwake())
    {
        Canvas.popDialog(WindowBar);
    }
    else
    {
        Canvas.pushDialog(WindowBar);
    }

    if (%this._(currencyBitmap).isVisible())
    {
        %this._(currencyBitmap).setVisible(0);
    }
    else
    {
        %this._(currencyBitmap).setVisible(1);
    }
}

function inGameUIBaseClass::toggleNames(%this)
{
    if (%this._(onverseHud).isVisible())
    {
        %this._(onverseHud).setVisible(0);
    }
    else
    {
        %this._(onverseHud).setVisible(1);
    }
}

function inGameUIBaseClass::onDoubleClick(%this, %obj, %throughWall)
{
    if (%throughWall)
    {
        if ((%obj.ownerID == 0) || (%obj.ownerID != $currentPlayerID))
        {
            return;
        }
    }

    if (%obj.isMethod(getDataBlock) && %obj.getDataBlock())
    {
        if (%obj.getDataBlock().isMethod(doDefaultAction))
        {
            %obj.getDataBlock().doDefaultAction(%obj);
        }
    }
    else
    {
        if (%obj.isMethod(doDefaultAction))
        {
            %obj.doDefaultAction();
        }
    }
}

function inGameUIBaseClass::checkDragNormal(%this, %dragCtrl, %normal)
{
    if (%normal $= "0 0 0")
    {
        return 0;
    }

    %datablock = getField(%dragCtrl.info, 1);
    if (!isDatablock(%datablock))
    {
        return 0;
    }

    if (%datablock.furnType == 1)
    {
        %angle = mAcos(VectorDot("0 0 -1", %normal));
        if (%angle > 0.785398)
        {
            return 0;
        }
    }
    else
    {
        if (%datablock.furnType == 2)
        {
            %angle = mAcos(VectorDot("0 0 1", %normal));
            if (%angle < 0.785398)
            {
                return 0;
            }
            %angle = mAcos(VectorDot("0 0 -1", %normal));
            if (%angle < 0.785398)
            {
                return 0;
            }
        }
        else
        {
            %angle = mAcos(VectorDot("0 0 1", %normal));
            if (%angle > 0.785398)
            {
                return 0;
            }
        }
    }

    return 1;
}

function inGameUIBaseClass::canDragDrop(%this, %dragCtrl, %position, %normal, %overPlayer, %alt)
{
    if (%dragCtrl.dragType $= FurnitureDrag)
    {
        return %alt || %this.checkDragNormal(%dragCtrl, %normal);
    }
    else
    {
        if (%overPlayer == LocationGameConnection.GetPlayerObject())
        {
            if (%dragCtrl.dragType $= ClothingDrag)
            {
                return 1;
            }
            if (%dragCtrl.dragType $= ReservedClothingDrag)
            {
                return 1;
            }
            if (%dragCtrl.dragType $= ReservedTextureDrag)
            {
                return 1;
            }
            if ((%dragCtrl.dragType $= EquipmentDrag) || (%dragCtrl.dragType $= EquipmentBarDrag))
            {
                return 1;
            }
        }
    }
    
    return 0;
}

function inGameUIBaseClass::onDragDrop(%this, %dragCtrl, %position, %normal, %overPlayer, %alt)
{
    if (%dragCtrl.dragType $= FurnitureDrag)
    {
        %uniqueID = getField(%dragCtrl.info, 0);
        %datablock = getField(%dragCtrl.info, 1);
        if (%alt || %this.checkDragNormal(%dragCtrl, %normal))
        {
            FurniturePlacement::PlaceFurnitureObject(%datablock, %uniqueID, %position);
        }
        else
        {
            if (getField(%dragCtrl.info, 1).furnType == 1)
            {
                MessageBoxOK("Placement Error", "You must drag ceiling objects onto a ceiling in your home.");
            }
            else
            {
                if (getField(%dragCtrl.info, 1).furnType == 2)
                {
                    MessageBoxOK("Placement Error", "You must drag wall objects onto a wall in your home.");
                }
                else
                {
                    MessageBoxOK("Placement Error", "You must drag floor objects onto a floor in your home.");
                }
            }
        }
    }
    else
    {
        if (%overPlayer == LocationGameConnection.GetPlayerObject())
        {
            if (%dragCtrl.dragType $= "ClothingDrag")
            {
                InventoryGUI.equipClothing(%dragCtrl);
                %dragCtrl.setVisible(0);
            }
            else
            {
                if (%dragCtrl.dragType $= "ReservedClothingDrag")
                {
                    InventoryGUI.equipReserved(%dragCtrl);
                    %dragCtrl.setVisible(0);
                }
                else
                {
                    if (%dragCtrl.dragType $= "ReservedTextureDrag")
                    {
                        InventoryGUI.equipTexture(%dragCtrl);
                        %dragCtrl.setVisible(0);
                    }
                    else
                    {
                        if (%dragCtrl.dragType $= "EquipmentDrag")
                        {
                            InventoryGUI.equipEquipment(%dragCtrl);
                        }
                        else
                        {
                            if (%dragCtrl.dragType $= "EquipmentBarDrag")
                            {
                                InventoryGUI.equipEquipment(%dragCtrl);
                            }
                        }
                    }
                }
            }
        }
    }
}

function inGameUIBaseClass::selectTool(%this, %toolID)
{
    EquipBar.sendEquipSlot(%toolID);
}

function inGameUIBaseClass::showAquiredPoints(%this, %count, %trans)
{
    %this._(onverseHud).addPPText(%count, getWords(%trans, 0, 2));
}

function inGameUIBaseClass::onUpdatePoints(%this, %CC, %PP)
{
    if (%CC $= "")
    {
        %CC = "0";
    }

    if (%PP $= "")
    {
        %PP = "0";
    }

    %message = "<just:right><font:Arial Bold:18><color:4080ff>" @ %PP @ "<br>" @ "<color:aa8040>" @ %CC;
    %this._(CurrencyText).setText(%message);
}

function inGameUIBaseClass::onPickedUpItem(%this)
{
    %this._(reticle).animate();
}

function inGameUIBaseClass::onShotHit(%this)
{
    %this._(reticle).showHit(16);
}

function inGameUIBaseClass::GameInventory_Added(%this)
{
}

function inGameUIBaseClass::GameInventory_Removed(%this)
{
}

function inGameUIBaseClass::GameInventory_UpdateAmmo(%this, %toolID, %unused)
{
}

function inGameUIBaseClass::GameInventory_RemoveTool(%this, %toolID)
{
}

function inGameUIBaseClass::GameInventory_ClearAll(%this)
{
}

function inGameUIBaseClass::GameInventory_EquippedTool(%this, %toolID)
{
}

function GuiTSCtrl::onMouseDown(%this, %selObject)
{
    %this.makeFirstResponder(1);
    %ctlObj = LocationGameConnection.getControlObject();

    while (%ctlObj > 0)
    {
        if (%selObject == %ctlObj)
        {
            %this.EnterPlayerMode();
            $mvFreeLook = 1;
            $mvTriggerCount0 += 2;
            return;
        }
        
        if (%ctlObj.isMethod("getControlObject"))
        {
            %ctlObj = %ctlObj.getControlObject();
        }
        else
        {
            %ctlObj = 0;
        }
    }

    $mvFreeLook = 0;
    $mvTriggerCount0 += 2;
}

function GuiTSCtrl::onMouseWheelUp(%this)
{
    zoomCam(1);
}

function GuiTSCtrl::onMouseWheelDown(%this)
{
    zoomCam(-1);
}

function GuiTSCtrl::onEnterPlayerMode(%this)
{
    $mvFreeLook = 0;
    playerModeActionMap.push();
}

function GuiTSCtrl::onExitPlayerMode(%this)
{
    playerModeActionMap.pop();

    if (isObject(getCurrentInGameUI()._(actionModeText)))
    {
        getCurrentInGameUI()._(actionModeText).setVisible(0);
    }
}

function ClientCmdOnUpdatePoints(%CC, %PP)
{
    LocationGameConnection.CC = %CC;
    LocationGameConnection.PP = %PP;
    getCurrentInGameUI().onUpdatePoints(LocationGameConnection.CC, LocationGameConnection.PP);
}

function ClientCmdPickedUpItem()
{
    getCurrentInGameUI().onPickedUpItem();
}

function ClientCmdShotHit()
{
    getCurrentInGameUI().onShotHit();
}

function CurrencyButtonClass::onClick(%this)
{
    if (LocationGameConnection.getIsSubscriber())
    {
        inGameUIContextMenu.showContextMenu("vip");
    }
    else
    {
        inGameUIContextMenu.showContextMenu("novip");
    }
}

function CurrencyButtonClass::onRightClick(%this)
{
    if (LocationGameConnection.getIsSubscriber())
    {
        inGameUIContextMenu.showContextMenu("vip");
    }
    else
    {
        inGameUIContextMenu.showContextMenu("novip");
    }
}

new GuiContextMenu(inGameUIContextMenu) {
   Profile = "GuiMenuContextProfile";
   position = "0 0";
   Extent = "640 480";
};

function inGameUIContextMenu::Initialize(%this)
{
    if (!%this.initialized)
    {
        %menuId = 0;
        %itemId = 0;
        %this.addMenu("vip", %menuId++);
        %this.addMenuItem("vip", "Get CC (Web)", %itemId++, "gotoWebPage(\"http://www.onverse.com/cashcoins/cashcoins.php\");");
        %itemId = 0;
        %this.addMenu("novip", %menuId++);
        %this.addMenuItem("novip", "Get CC (Web)", %itemId++, "gotoWebPage(\"http://www.onverse.com/cashcoins/cashcoins.php\");");
        %this.addMenuItem("novip", "Get VIP (Web)", %itemId++, "gotoWebPage(\"http://www.onverse.com/cashcoins/cashcoins.php\");");
    }

    %this.initialized = 1;
}

function inGameUIContextMenu::showContextMenu(%this, %menu, %layer)
{
    %this.Initialize();
    Parent::showContextMenu(%this, %menu, %layer);
}
