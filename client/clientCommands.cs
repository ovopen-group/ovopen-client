
_GCommandParser.addCommand("Quit", 0, 0, CC_Quit, "(Quit Onverse)");
_GCommandParser.addCommand("Help", 0, 0, CC_Help, "Show Help (Web)");
_GCommandParser.addCommand("Logoff", 0, 0, CC_Logout, "Log off");
_GCommandParser.addCommand("wordfilter", 0, 0, CC_Filter, "Toggle the bad word filter.");

function CC_Quit()
{
    MessageBoxYesNo("Quit", "Would you like to quit Onverse?", "quit();", "");
}

function CC_Help()
{
    MessageBoxYesNo("Help", "Would you like to see the game tips section at onverse.com?", "gotoWebPage(\"http://www.onverse.com/help/General/What-to-do-First.php\");", "");
}

function CC_Logout()
{
    MessageBoxYesNo("Log Off", "Would you like to log off?", "MasterServerConnection.disconnect();", "");
}

function CC_Filter()
{
    $pref::enableBadWordFilter = !$pref::enableBadWordFilter;
    return $pref::enableBadWordFilter ? "Filter Enabled" : "Filter Disabled";
}

_GCommandParser.addCommand("AddFriend", 1, 1, CC_AddFriend, "[friendName] (Add a friend to your friends list.)");
_GCommandParser.addCommand("AcceptFriend", 1, 1, CC_AcceptFriend, "[friendName] (Accept a pending friend request.)");
_GCommandParser.addCommand("DeleteFriend", 1, 1, CC_DeleteFriend, "[friendName] (Delete a friend or cancel a friend request.)");
_GCommandParser.addCommand("Ignore", 1, 1, CC_IgnoreUser, "[userName] (Add the user to your ignore list.)");
_GCommandParser.addCommand("ShowFriends", 0, 0, CC_ShowFriendsList, " (Show the friends list UI.)");

function CC_AddFriend(%username)
{
    AddFriend(%username);
    return "Adding friend " @ %username;
}

function CC_AcceptFriend(%username)
{
    AcceptFriend(%username);
    return "Accepting friend " @ %username;
}

function CC_DeleteFriend(%username)
{
    DeleteFriend(%username);
    return "Deleting friend " @ %username;
}

function CC_IgnoreUser(%username)
{
    IgnoreUser(%username);
    return "Ignoring " @ %username;
}

function CC_ShowFriendsList(%unused)
{
    ShowFriendsList();
    return "";
}

_GCommandParser.addCommand("poi", 1, 1, CC_Poi, "[poiName] (Teleport to requested poi in current instance)");
_GCommandParser.addCommand("TelPlayer", 1, 1, CC_TelPlayer, "[playerName] (Teleport to the requested player.)");
_GCommandParser.addCommand("TelHome", 0, 1, CC_TelHome, "<playerName> (Teleport to the requested player\'s home or yours if no playerName given.)");

function CC_Poi(%poiName)
{
    commandToServer('GotoPOI', %poiName);
    return "Teleporting to " @ %poiName;
}

function CC_TelPlayer(%playerName)
{
    TeleportToPlayer(%playerName);
    return "Teleporting to " @ %playerName;
}

function CC_TelHome(%playerName)
{
    TeleportToHome(%playerName);
    return "";
}

_GCommandParser.addCommand("Tell", 2, 255, CC_Tell, "[username] [message] (Send a private message to the given username)");
_GCommandParser.addCommand("Shout", 1, 255, CC_Shout, "[message] (Shout so everyone in the instance can hear you)");
_GCommandParser.addCommand("Friends", 1, 255, CC_Friends, "[message] (Send a message to all your online friends)");
_GCommandParser.addCommand("Guide", 1, 255, CC_Guides, "[message] (Send a message to any online guides)");

function RPC_PrivateMessage::onReturnInvalidUsername(%this)
{
    MainChatHUD.onCommandReturn("That username is invalid.");
}

function RPC_PrivateMessage::onReturnUserOffline(%this)
{
    MainChatHUD.onCommandReturn("That user is currently offline.");
}

function RPC_PrivateMessage::onReturnOk(%this)
{
    if (%this.type == 0)
    {
        MainChatHUD.onSentPrivateMessage(%this.friend, %this.message);
    }
    else
    {
        if (%this.type == 1)
        {
            MainChatHUD.onSentFriendsMessage(%this.message);
        }
        else
        {
            if (%this.type == 2)
            {
                MainChatHUD.onSentGuideMessage(%this.message);
            }
            else
            {
                if (%this.type == 3)
                {
                    MainChatHUD.onSentGuideReplyMessage(%this.friend, %this.message);
                }
            }
        }
    }
}

function CC_Shout(%message)
{
    Messaging::SendShoutMessage(%message);
    return "";
}

function CC_Tell(%username, %message)
{
    %rpcObject = new RPC_PrivateMessage() {
    };
    %rpcObject.privateMessage(%username, %message);
    %rpcObject.fireRPC(MasterServerConnection);
    %rpcObject.type = 0;
    %rpcObject.friend = %username;
    %rpcObject.message = %message;
    return "";
}

function CC_Friends(%message)
{
    %rpcObject = new RPC_PrivateMessage() {
    };
    %rpcObject.friendsMessage(%message);
    %rpcObject.fireRPC(MasterServerConnection);
    %rpcObject.type = 1;
    %rpcObject.message = %message;
    return "";
}

function CC_Guides(%message)
{
    %rpcObject = new RPC_PrivateMessage() {
    };
    %rpcObject.guidesMessage(%message);
    %rpcObject.fireRPC(MasterServerConnection);
    %rpcObject.type = 2;
    %rpcObject.message = %message;
    return "";
}

function CC_GuideReply(%username, %message)
{
    if (!MasterServerConnection.hasGuideLevel())
    {
        return "You do not have permision to guide reply.";
    }

    %rpcObject = new RPC_PrivateMessage() {
    };
    
    %rpcObject.guideReplyMessage(%username, %message);
    %rpcObject.fireRPC(MasterServerConnection);
    %rpcObject.type = 3;
    %rpcObject.friend = %username;
    %rpcObject.message = %message;
    return "";
}

_GCommandParser.addCommand("PermGet", 0, 0, CC_PermGet, "(Return current user permission settings)");
_GCommandParser.addCommand("PermSetTele", 1, 1, CC_PermSetTele, "[permission] (Set user teleport permission setting)");

function RPC_UserPermissions::onReturnGeneralError(%this)
{
    MessageBoxOK("Error", "An error occured while running permissions command.");
}

function RPC_UserPermissions::onReturnPermissions(%this, %tele, %home)
{
    $permissions::teleport = %tele;
    $permissions::home = %home;
    if (%this.showResult)
    {
        MainChatHUD.onCommandReturn("Teleport: " @ $permissions::teleport);
    }

    if (!(%this.eval $= ""))
    {
        eval(%this.eval);
    }
}

function CC_PermGet()
{
    %rpcObject = new RPC_UserPermissions() {
    };
    %rpcObject.getPermissions();
    %rpcObject.fireRPC(MasterServerConnection);
    %rpcObject.showResult = 1;
    return "";
}

function CC_PermSetTele(%perm)
{
    %rpcObject = new RPC_UserPermissions() {
    };
    
    %res = %rpcObject.setPermissions(%perm, "");
    if (!(%res $= ""))
    {
        %rpcObject.delete();
        return %res;
    }
    
    %rpcObject.fireRPC(MasterServerConnection);
    return "Teleport permission setting updated";
}

function CC_PermSetHome(%perm)
{
    %rpcObject = new RPC_UserPermissions() {
    };
    
    %res = %rpcObject.setPermissions("", %perm);
    if (!(%res $= ""))
    {
        %rpcObject.delete();
        return %res;
    }

    %rpcObject.fireRPC(MasterServerConnection);
    return "Home permission setting updated";
}
