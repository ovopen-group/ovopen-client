
exec("./mainMenu.gui");

function MainMenuGui::onWake(%this)
{
    Canvas.pushDialog(WindowBar);
    Canvas.pushDialog(MainChatHUD);
    Canvas.setCursor(DefaultCursor);
    cursorOn();

    if (LagIcon.isAwake())
    {
        Canvas.popDialog(LagIcon);
    }

    GameMusic.FadeIn("menus/world-map", 1);

    if (%this.gameMusicTrackEvent != 0)
    {
        cancel(%this.gameMusicTrackEvent);
    }

    %this.gameMusicTrackEvent = %this.schedule(100, "GameMusicTrack");
}

function MainMenuGui::GameMusicTrack()
{
    %this.gameMusicTrackEvent = 0;
    GameMusicNewTrack();
}

function MainMenuGui::onSleep(%this)
{
    if (%this.gameMusicTrackEvent != 0)
    {
        cancel(%this.gameMusicTrackEvent);
    }

    %this.gameMusicTrackEvent = 0;
    GameMusic.FadeOut(1);
}

function MainMenuGui::startMainMenu(%this)
{
    Canvas.popLayer(1);
    Canvas.popDialog(%this);
    Canvas.setContent(%this);
}
