
$cursorControlled = 1;

function cursorOff()
{
    if ($cursorControlled)
    {
        lockMouse(1);
    }

    Canvas.cursorOff();
}

function cursorOn()
{
    if ($cursorControlled)
    {
        lockMouse(0);
    }

    Canvas.cursorOn();
    Canvas.setCursor(DefaultCursor);
}

package CanvasCursor
{
    function GuiCanvas::checkCursor(%this)
    {
        %cursorShouldBeOn = 0;
        for (%i=0; %i < %this.getCount(); %i++)
        {
            %control = %this.getObject(%i);
            if (%control.noCursor == 0)
            {
                %cursorShouldBeOn = 1;
                break;
            }
        }

        if (%cursorShouldBeOn != %this.isCursorOn())
        {
            if (%cursorShouldBeOn)
            {
                cursorOn();
            }
            else
            {
                cursorOff();
            }
        }
    }

    function GuiCanvas::setContent(%this, %ctrl)
    {
        Parent::setContent(%this, %ctrl);
    }
    
    function GuiCanvas::pushDialog(%this, %ctrl, %layer)
    {
        if (%layer < 1)
        {
            %layer = 1;
        }

        Parent::pushDialog(%this, %ctrl, %layer);
    }

    function GuiCanvas::popDialog(%this, %ctrl)
    {
        Parent::popDialog(%this, %ctrl);
    }

    function GuiCanvas::popLayer(%this, %layer)
    {
        Parent::popLayer(%this, %layer);
    }
};

activatePackage(CanvasCursor);

