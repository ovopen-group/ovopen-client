
exec("./MessageBoxOkCancelDlg.gui");
exec("./MessageBoxOkDlg.gui");
exec("./MessageBoxYesNoDlg.gui");
exec("./MessagePopupDlg.gui");
exec("./MessageBoxEntryOkDlg.gui");

function MessageCallback(%dlg, %callback)
{
    Canvas.popDialog(%dlg);
    eval(%callback);
}

function MBSetText(%text, %frame, %msg)
{
    %ext = %text.getExtent();
    %text.setText("<just:center>" @ %msg);
    %text.forceReflow();
    %newExtent = %text.getExtent();
    %deltaY = getWord(%newExtent, 1) - getWord(%ext, 1);
    %windowPos = %frame.getPosition();
    %windowExt = %frame.getExtent();
    %frame.resize(getWord(%windowPos, 0), getWord(%windowPos, 1) - (%deltaY / 2), getWord(%windowExt, 0), getWord(%windowExt, 1) + %deltaY);
}

function MessageBoxOK(%title, %message, %callback, %layer)
{
    MBOKFrame.setText(%title);
    Canvas.pushDialog(MessageBoxOKDlg, %layer);
    MBSetText(MBOKText, MBOKFrame, %message);
    MessageBoxOKDlg.callback = %callback;
    MessageBoxOKDlg.layer = %layer;
}

function RePushMessageBox()
{
    Canvas.pushDialog(MessageBoxOKDlg, MessageBoxOKDlg.layer);
}

function MessageBoxOKDlg::onSleep(%this)
{
    %this.callback = "";
}

function MessageBoxOKCancel(%title, %message, %callback, %cancelCallback)
{
    MBOKCancelFrame.setText(%title);
    Canvas.pushDialog(MessageBoxOKCancelDlg);
    MBSetText(MBOKCancelText, MBOKCancelFrame, %message);
    MessageBoxOKCancelDlg.callback = %callback;
    MessageBoxOKCancelDlg.cancelCallback = %cancelCallback;
}

function MessageBoxOKCancelDlg::onSleep(%this)
{
    %this.callback = "";
}

function MessageBoxYesNo(%title, %message, %yesCallback, %noCallback)
{
    MBYesNoFrame.setText(%title);
    Canvas.pushDialog(MessageBoxYesNoDlg);
    MBSetText(MBYesNoText, MBYesNoFrame, %message);
    MessageBoxYesNoDlg.yesCallBack = %yesCallback;
    MessageBoxYesNoDlg.noCallback = %noCallback;
}

function MessageBoxYesNoDlg::onSleep(%this)
{
    %this.yesCallBack = "";
    %this.noCallback = "";
}

function MessagePopup(%title, %message, %delay, %eventRef)
{
    MessagePopFrame.setText(%title);
    Canvas.pushDialog(MessagePopupDlg);
    MBSetText(MessagePopText, MessagePopFrame, %message);
    MessagePopupDlg.event = 0;
    
    if (!(%eventRef $= ""))
    {
        MessagePopupDlg.event = %eventRef;
    }
    else
    {
        if (!(%delay $= ""))
        {
            MessagePopupDlg.event = schedule(%delay, 0, CloseMessagePopup);
        }
    }
}

function CloseMessagePopup()
{
    if (MessagePopupDlg.event != 0)
    {
        cancel(MessagePopupDlg.event);
    }
    Canvas.popDialog(MessagePopupDlg);
}

function MessageBoxEntryOk(%title, %message, %callback, %text)
{
    MBOKEntryFrame.setText(%title);
    Canvas.pushDialog(MessageBoxEntryOkDlg);
    MBSetText(MBOKEntryText, MBOKEntryFrame, %message);
    MessageBoxEntryOkDlg.callback = %callback;
    MBOKEntry.setText(%text);
}

function MessageBoxEntryOkDlg::onSleep(%this)
{
    %this.callback = "";
}

function MessageEntryCallback(%dlg, %callback, %arg)
{
    Canvas.popDialog(%dlg);
    eval(%callback @ "(\"" @ %arg @ "\");");
}
