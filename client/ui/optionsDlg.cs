
if (!isObject(optionsDlg))
{
    exec("./optionsDlg.gui");
    exec("./remapDlg.gui");
}

new AudioDescription(AudioChannel0) {
   volume = "1";
   isLooping = "0";
   is3D = "0";
   type = "0";
};

new AudioDescription(AudioChannel1) {
   volume = "1";
   isLooping = "0";
   is3D = "0";
   type = "1";
};

new AudioDescription(AudioChannel2) {
   volume = "1";
   isLooping = "0";
   is3D = "0";
   type = "2";
};

new AudioDescription(AudioChannel3) {
   volume = "1";
   isLooping = "0";
   is3D = "0";
   type = "3";
};

new AudioDescription(AudioChannel4) {
   volume = "1";
   isLooping = "0";
   is3D = "0";
   type = "4";
};

new AudioDescription(AudioChannel5) {
   volume = "1";
   isLooping = "0";
   is3D = "0";
   type = "5";
};

new AudioDescription(AudioChannel6) {
   volume = "1";
   isLooping = "0";
   is3D = "0";
   type = "6";
};

new AudioDescription(AudioChannel7) {
   volume = "1";
   isLooping = "0";
   is3D = "0";
   type = "7";
};

new AudioDescription(AudioChannel8) {
   volume = "1";
   isLooping = "0";
   is3D = "0";
   type = "8";
};

function ShowOptions()
{
    if (!optionsDlg.isAwake())
    {
        Canvas.pushDialog(optionsDlg);
    }
    else
    {
        Canvas.popDialog(optionsDlg);
    }
}
function OptionsTabCtrl::onSelectedTab(%this, %unused)
{
    OptRemapList.fillList();
}

function optionsDlg::onWake(%this)
{
    %this.showCustomGfxWindow(0);
    cursorOn();
    %this.LoadPrivacy();
    %this.GraphicsInit();

    OptAudioUpdate();
    OptAudioVolumeMaster.setValue($pref::Audio::masterVolume);
    OptAudioVolumeShell.setValue($pref::Audio::channelVolume[$GuiAudioType]);
    OptAudioVolumeSim.setValue($pref::Audio::channelVolume[$SimAudioType]);
    OptAudioVolumeMusic.setValue($pref::Audio::channelVolume[$MusicAudioType]);
    OptAudioDriverList.clear();
    OptAudioDriverList.add("OpenAL", 1);
    OptAudioDriverList.add("none", 2);

    %selId = OptAudioDriverList.findText($pref::Audio::driver);
    if (%selId == -1)
    {
        %selId = 0;
    }

    OptAudioDriverList.setSelected(%selId);
    OptAudioDriverList.onSelect(%selId, "");
}

function optionsDlg::GraphicsInit()
{
    %buffer = getDisplayDeviceList();
    %count = getFieldCount(%buffer);
    OptGraphicsDriverMenu.clear();
    OptScreenshotMenu.init();
    OptScreenshotMenu.setValue($pref::Video::screenShotFormat);
    
    for (%i=0; %i < %count; %i++)
    {
        OptGraphicsDriverMenu.add(getField(%buffer, %i), %i);
    }
    
    %selId = OptGraphicsDriverMenu.findText($pref::Video::displayDevice);
    if (%selId == -1)
    {
        %selId = 0;
    }

    OptGraphicsDriverMenu.setSelected(%selId);
    OptGraphicsDriverMenu.onSelect(%selId, "");
    OptQualityMenu.clear();
    OptQualityMenu.add("Lowest (Fastest)", 0);
    OptQualityMenu.add("Low", 1);
    OptQualityMenu.add("Medium", 2);
    OptQualityMenu.add("High", 3);
    OptQualityMenu.add("Best (Slowest)", 4);
    OptQualityMenu.setSelected($pref::Video::quality);
}

function optionsDlg::onSleep(%this)
{
    SaveKeyMappings();
}

function SaveKeyMappings(%this)
{
    gameModeActionMap.save("cfg/keyConfig.cfg");
}

function LoadKeyMappings(%this)
{
    return;

    echo("Loading client key mappings");
    if (isObject(gameModeActionMap))
    {
        gameModeActionMap.delete();
    }

    new ActionMap(gameModeActionMap) {
    };

    %file = new FileObject() {
    };

    %open = 0;
    if ($journal::Reading)
    {
        echo("Attempting to open journal\'s matching client key config file.");
        echo($journal::File);
        %open = %file.openForRead($journal::File @ "_keyConfig.cfg");
        if (!%open)
        {
            echo("Using default client key config file.");
            %open = %file.openForRead("cfg/keyConfig.cfg");
        }
    }
    else
    {
        %open = %file.openForRead("cfg/keyConfig.cfg");
    }

    %infoObject = "";
    if (%open)
    {
        %inInfoBlock = 0;
        while (!%file.isEOF())
        {
            %line = %file.readLine();
            %line = trim(%line);
            %infoObject = %infoObject @ %line @ " ";
        }
        %file.close();
    }

    eval(%infoObject);
    %file.delete();
    OptRemapList.fillList();
}

function OptGraphicsDriverMenu::onSelect(%this, %id, %text)
{
    if (OptGraphicsResolutionMenu.size() > 0)
    {
        %prevRes = OptGraphicsResolutionMenu.getText();
    }
    else
    {
        %prevRes = getWords($pref::Video::resolution, 0, 1);
    }

    if (isDeviceFullScreenOnly(%this.getText()))
    {
        OptGraphicsFullscreenToggle.setValue(1);
        OptGraphicsFullscreenToggle.setActive(0);
        OptGraphicsFullscreenToggle.onAction();
    }
    else
    {
        OptGraphicsFullscreenToggle.setActive(1);
    }

    if (OptGraphicsFullscreenToggle.getValue())
    {
        %prevBPP = getWord($pref::Video::resolution, 2);
    }

    OptGraphicsResolutionMenu.init(%this.getText(), OptGraphicsFullscreenToggle.getValue());
    OptGraphicsBPPMenu.init(%this.getText());
    %selId = OptGraphicsResolutionMenu.findText(%prevRes);
    if (%selId == -1)
    {
        %selId = 0;
    }

    OptGraphicsResolutionMenu.setSelected(%selId);

    if (OptGraphicsFullscreenToggle.getValue())
    {
        %selId = OptGraphicsBPPMenu.findText(%prevBPP);
        if (%selId == -1)
        {
            %selId = 0;
        }
        OptGraphicsBPPMenu.setSelected(%selId);
        OptGraphicsBPPMenu.setText(OptGraphicsBPPMenu.getTextById(%selId));
    }
    else
    {
        OptGraphicsBPPMenu.setText("Default");
    }
}

function OptGraphicsResolutionMenu::init(%this, %device, %fullScreen)
{
    %this.clear();
    %resList = getResolutionList(%device);
    %resCount = getFieldCount(%resList);
    %deskRes = getDesktopResolution();
    %count = 0;

    for (%i=0; %i < %resCount; %i++)
    {
        %res = getWords(getField(%resList, %i), 0, 1);
        if (!%fullScreen)
        {
            if (firstWord(%res) >= firstWord(%deskRes))
            {
                continue;
            }
            if (getWord(%res, 1) >= getWord(%deskRes, 1))
            {
                continue;
            }
        }
        if (%this.findText(%res) == -1)
        {
            %this.add(%res, %count);
            %count++;
        }
    }
}

function OptGraphicsFullscreenToggle::onAction(%this)
{
    %prevRes = OptGraphicsResolutionMenu.getText();
    OptGraphicsResolutionMenu.init(OptGraphicsDriverMenu.getText(), %this.getValue());
    %selId = OptGraphicsResolutionMenu.findText(%prevRes);
    if (%selId == -1)
    {
        %selId = 0;
    }
    OptGraphicsResolutionMenu.setSelected(%selId);
}

function OptGraphicsBPPMenu::init(%this, %device)
{
    %this.clear();
    if (%device $= "Voodoo2")
    {
        %this.add(16, 0);
    }
    else
    {
        %resList = getResolutionList(%device);
        %resCount = getFieldCount(%resList);
        %count = 0;

        for (%i=0; %i < %resCount; %i++)
        {
            %bpp = getWord(getField(%resList, %i), 2);
            if (%this.findText(%bpp) == -1)
            {
                %this.add(%bpp, %count);
                %count++;
            }
        }
    }
}

function OptScreenshotMenu::init(%this)
{
    if (%this.findText("PNG") == -1)
    {
        %this.add("PNG", 0);
    }
    if (%this.findText("JPEG") == -1)
    {
        %this.add("JPEG", 1);
    }
}

function OptQualityMenu::onSelect(%this, %id, %text)
{
    OptCustomButton.setVisible(%id == 5 ? 1 : 0);
}

function optionsDlg::showCustomGfxWindow(%this, %value)
{
    %this._(customGfx).setVisible(%value);
}

function optionsDlg::applyGraphics(%this, %conf)
{
    %newDriver = OptGraphicsDriverMenu.getText();
    %newRes = OptGraphicsResolutionMenu.getText();
    %newBpp = OptGraphicsBPPMenu.getText();
    %newFullScreen = OptGraphicsFullscreenToggle.getValue();
    $pref::Video::screenShotFormat = OptScreenshotMenu.getText();
    %newQuality = OptQualityMenu.getSelected();

    if (%newQuality < 5)
    {
        $pref::visibleDistanceMod = 1;
        $pref::TS::detailAdjust = 1;
        %newMipReduction = 0;
        %newIntMipReduction = 0;
        %newSkyMipReduction = 0;
        %newCompressionHint = GL_NICEST;
        %newAllowCompression = 0;
        %newMSAA = 0;
        %newAni = 0;
        %newTri = 1;
        $pref::Interior::DynamicLights = 1;
        $pref::Interior::detailAdjust = 1;
        $pref::Interior::TexturedFog = 1;
        $pref::Terrain::texDetail = 0;
        $pref::Terrain::enableDetails = 1;
        $pref::Terrain::dynamicLights = 1;
        $pref::decalsOn = 1;
        $pref::Decal::maxNumDecals = 256;
        $pref::Decal::decalTimeout = 5000;
        $pref::LightManager::sgUseDynamicShadows = 1;
        $pref::LightManager::sgDynamicShadowQuality = 0;
        $pref::LightManager::sgDynamicLightingOcclusionQuality = 0;
        $pref::Shadows = 1;
        $pref::environmentMaps = 1;

        if (%newQuality == 0)
        {
            %newTri = 0;
            %newCompressionHint = GL_FASTEST;
            $pref::visibleDistanceMod = 0.51;
            $pref::TS::detailAdjust = 0.5;
            $pref::Interior::detailAdjust = 0.5;
            %newMipReduction = 1;
            %newIntMipReduction = 1;
            %newSkyMipReduction = 1;
            $pref::Terrain::texDetail = 1;
            $pref::Terrain::enableDetails = 0;
            $pref::decalsOn = 0;
            $pref::Interior::DynamicLights = 0;
            $pref::Terrain::dynamicLights = 0;
            $pref::LightManager::sgUseDynamicShadows = 0;
            $pref::LightManager::sgDynamicShadowQuality = 6;
            $pref::LightManager::sgDynamicLightingOcclusionQuality = 6;
            $pref::environmentMaps = 0;
        }
        else
        {
            if (%newQuality == 1)
            {
                %newTri = 0;
                $pref::visibleDistanceMod = 0.75;
                $pref::TS::detailAdjust = 0.75;
                $pref::Interior::detailAdjust = 0.75;
                %newCompressionHint = GL_FASTEST;
                $pref::Interior::DynamicLights = 0;
                $pref::Terrain::dynamicLights = 0;
                $pref::Decal::maxNumDecals = 64;
                $pref::Decal::decalTimeout = 2500;
                $pref::LightManager::sgUseDynamicShadows = 0;
                $pref::LightManager::sgDynamicShadowQuality = 6;
                $pref::LightManager::sgDynamicLightingOcclusionQuality = 6;
            }
            else
            {
                if (%newQuality == 2)
                {
                    $pref::Shadows = 0.5;
                    $pref::Decal::maxNumDecals = 128;
                    $pref::LightManager::sgUseDynamicShadows = 0;
                    $pref::LightManager::sgDynamicLightingOcclusionQuality = 2;
                    $pref::LightManager::sgDynamicShadowQuality = 2;
                }
                else
                {
                    if (%newQuality == 3)
                    {
                        if ($AnisotropySupported)
                        {
                            %newAni = 2 / getOpenGLMaxAnisotropy();
                        }
                    }
                    else
                    {
                        if (%newQuality == 4)
                        {
                            if ($AnisotropySupported)
                            {
                                %max = 0;
                                for (%i=2; %i <= getOpenGLMaxAnisotropy(); %i *= %i)
                                {
                                    %max = %i;
                                }
                                %newAni = %max / getOpenGLMaxAnisotropy();
                            }

                            if ($MultisampleSupported)
                            {
                                %aaList = getOpenGLMultiSampleModes();
                                %count = getWordCount(%aaList);
                                if (%count > 2)
                                {
                                    %index = mFloor((%count - 1) / 2);
                                    %newMSAA = getWord(%aaList, %index);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    %updateDriver = (%newDriver !$= $pref::Video::displayDevice);
    %updateDriver = %updateDriver || (%newMSAA != $pref::OpenGL::multisample);
    %refreshCache = %newAni != $pref::OpenGL::textureAnisotropy;
    %refreshCache = %refreshCache || (%newTri != $pref::OpenGL::textureTrilinear);
    %refreshCache = %refreshCache || (%newMipReduction != $pref::OpenGL::mipReduction);
    %refreshCache = %refreshCache || (%newIntMipReduction != $pref::OpenGL::interiorMipReduction);
    %refreshCache = %refreshCache || (%newSkyMipReduction != $pref::OpenGL::skyMipReduction);
    %refreshCache = %refreshCache || !((%newCompressionHint $= $pref::OpenGL::compressionHint));
    %refreshCache = %refreshCache || !((%newAllowCompression $= $pref::OpenGL::allowCompression));

    if (%conf || (!%updateDriver && !%refreshCache))
    {
        $pref::Video::quality = %newQuality;
        $pref::OpenGL::multisample = %newMSAA;
        $pref::OpenGL::compressionHint = %newCompressionHint;
        $pref::OpenGL::allowCompression = %newAllowCompression;
        $pref::OpenGL::mipReduction = %newMipReduction;
        $pref::OpenGL::interiorMipReduction = %newMipReduction;
        $pref::OpenGL::skyMipReduction = %newSkyMipReduction;
        $pref::OpenGL::textureAnisotropy = %newAni;
        $pref::OpenGL::textureTrilinear = %newTri;
        setOpenGLMipReduction(%newMipReduction);
        setOpenGLInteriorMipReduction(%newIntMipReduction);
        setOpenGLSkyMipReduction(%newSkyMipReduction);
        setShadowDetailLevel($pref::Shadows);
        setOpenGLTextureCompressionHint($pref::OpenGL::compressionHint);
        if (%updateDriver)
        {
            setDisplayDevice(%newDriver, firstWord(%newRes), getWord(%newRes, 1), %newBpp, %newFullScreen);
        }
        else
        {
            if (%refreshCache)
            {
                flushTextureCache();
            }
            setScreenMode(firstWord(%newRes), getWord(%newRes, 1), %newBpp, %newFullScreen);
        }

        %this.GraphicsInit();
    }
    else
    {
        MessageBoxYesNo("Continue?", "Applying these settings will require Onverse to reload all textures. If you\'re in the world this could take a very long time. Are you sure you would like to continue?", "optionsDlg.applyGraphics(true);", "");
    }
}

$RemapCount = 0;
$RemapName[$RemapCount] = "Forward";
$RemapCmd[$RemapCount] = "moveforward";
$RemapCount++;
$RemapName[$RemapCount] = "Backward";
$RemapCmd[$RemapCount] = "movebackward";
$RemapCount++;
$RemapName[$RemapCount] = "Strafe Left";
$RemapCmd[$RemapCount] = "moveleft";
$RemapCount++;
$RemapName[$RemapCount] = "Strafe Right";
$RemapCmd[$RemapCount] = "moveright";
$RemapCount++;
$RemapName[$RemapCount] = "Turn Left";
$RemapCmd[$RemapCount] = "turnLeft";
$RemapCount++;
$RemapName[$RemapCount] = "Turn Right";
$RemapCmd[$RemapCount] = "turnRight";
$RemapCount++;
$RemapName[$RemapCount] = "Equip Mode Left";
$RemapCmd[$RemapCount] = "lockedMoveLeft";
$RemapCount++;
$RemapName[$RemapCount] = "Equip Mode Right";
$RemapCmd[$RemapCount] = "lockedMoveRight";
$RemapCount++;
$RemapName[$RemapCount] = "Toggle Walk";
$RemapCmd[$RemapCount] = "walkMode";
$RemapCount++;
$RemapName[$RemapCount] = "Toggle Auto Move";
$RemapCmd[$RemapCount] = "ToggleAutoMove";
$RemapCount++;
$RemapName[$RemapCount] = "Look Up";
$RemapCmd[$RemapCount] = "panUp";
$RemapCount++;
$RemapName[$RemapCount] = "Look Down";
$RemapCmd[$RemapCount] = "panDown";
$RemapCount++;
$RemapName[$RemapCount] = "Jump";
$RemapCmd[$RemapCount] = "jump";
$RemapCount++;
$RemapName[$RemapCount] = "Toggle First Person";
$RemapCmd[$RemapCount] = "toggleFirstPerson";
$RemapCount++;
$RemapName[$RemapCount] = "Full Third Person";
$RemapCmd[$RemapCount] = "fullzoomOutCamera";
$RemapCount++;
$RemapName[$RemapCount] = "Zoom In Camera";
$RemapCmd[$RemapCount] = "zoomInCamera";
$RemapCount++;
$RemapName[$RemapCount] = "Zoom Out Camera";
$RemapCmd[$RemapCount] = "zoomOutCamera";
$RemapCount++;

function restoreDefaultMappings()
{
    gameModeActionMap.delete();
    exec("~/client/gameMode/gameMode.bind.cs");
    OptRemapList.fillList();
}

function getMapDisplayName(%device, %action)
{
    if ( %device $= "keyboard" )
        return( %action );      
    else if ( strstr( %device, "mouse" ) != -1 )
    {
        // Substitute "mouse" for "button" in the action string:
        %pos = strstr( %action, "button" );
        if ( %pos != -1 )
        {
            %mods = getSubStr( %action, 0, %pos );
            %object = getSubStr( %action, %pos, 1000 );
            %instance = getSubStr( %object, strlen( "button" ), 1000 );
            return( %mods @ "mouse" @ ( %instance + 1 ) );
        }
        else
            error( "Mouse input object other than button passed to getDisplayMapName!" );
    }
    else if ( strstr( %device, "joystick" ) != -1 )
    {
        // Substitute "joystick" for "button" in the action string:
        %pos = strstr( %action, "button" );
        if ( %pos != -1 )
        {
            %mods = getSubStr( %action, 0, %pos );
            %object = getSubStr( %action, %pos, 1000 );
            %instance = getSubStr( %object, strlen( "button" ), 1000 );
            return( %mods @ "joystick" @ ( %instance + 1 ) );
        }
        else
       { 
          %pos = strstr( %action, "pov" );
         if ( %pos != -1 )
         {
            %wordCount = getWordCount( %action );
            %mods = %wordCount > 1 ? getWords( %action, 0, %wordCount - 2 ) @ " " : "";
            %object = getWord( %action, %wordCount - 1 );
            switch$ ( %object )
            {
               case "upov":   %object = "POV1 up";
               case "dpov":   %object = "POV1 down";
               case "lpov":   %object = "POV1 left";
               case "rpov":   %object = "POV1 right";
               case "upov2":  %object = "POV2 up";
               case "dpov2":  %object = "POV2 down";
               case "lpov2":  %object = "POV2 left";
               case "rpov2":  %object = "POV2 right";
               default:       %object = "??";
            }
            return( %mods @ %object );
         }
         else
            error( "Unsupported Joystick input object passed to getDisplayMapName!" );
      }
    }
        
    return( "??" ); 
}

function buildFullMapString(%index)
{
    %name = $RemapName[%index];
    %cmd = $RemapCmd[%index];
    
    %temp = gameModeActionMap.getBinding(%cmd);
    if (%temp $= "")
    {
        return %name TAB "";
    }

    %mapString = "";
    %count = getFieldCount(%temp);

    for (%i=0; %i < %count; %i += 2)
    {
        if (!(%mapString $= ""))
        {
            %mapString = %mapString @ ", ";
        }
        %device = getField(%temp, %i + 0);
        %object = getField(%temp, %i + 1);
        %mapString = %mapString @ getMapDisplayName(%device, %object);
    }
    return %name TAB %mapString;
}

function OptRemapList::fillList(%this)
{
    %this.clear();
    for (%i=0; %i < $RemapCount; %i++)
    {
        %this.addRow(%i, buildFullMapString(%i));
    }
}

function OptRemapList::doRemap(%this)
{
    %selId = %this.getSelectedId();
    %name = $RemapName[%selId];
    OptRemapText.setValue("REMAP \"" @ %name @ "\"");
    OptRemapInputCtrl.index = %selId;
    Canvas.pushDialog(RemapDlg);
}

function redoMapping(%device, %action, %cmd, %oldIndex, %newIndex)
{
    gameModeActionMap.bind(%device, %action, %cmd);
    OptRemapList.setRowById(%oldIndex, buildFullMapString(%oldIndex));
    OptRemapList.setRowById(%newIndex, buildFullMapString(%newIndex));
}

function findRemapCmdIndex(%command)
{
    for (%i=0; %i < $RemapCount; %i++)
    {
        if (%command $= $RemapCmd[%i])
        {
            return %i;
        }
    }
    return -1;
}

function unbindExtraActions(%command, %count)
{
    %temp = gameModeActionMap.getBinding(%command);
    if (%temp $= "")
    {
        return;
    }

    %count = getFieldCount(%temp) - (%count * 2);
    for (%i=0; %i < %count; %i += 2)
    {
        %device = getField(%temp, %i + 0);
        %action = getField(%temp, %i + 1);
        gameModeActionMap.unbind(%device, %action);
    }
}

function OptRemapInputCtrl::onInputEvent(%this, %device, %action)
{
    Canvas.popDialog(RemapDlg);

    if (%device $= "keyboard")
    {
        if (%action $= "escape")
        {
            return;
        }
    }

    %cmd = $RemapCmd[%this.index];
    %name = $RemapName[%this.index];
    %mapName = getMapDisplayName(%device, %action);
    
    %prevMap = gameModeActionMap.getCommand(%device, %action);
    if (%prevMap $= "")
    {
        unbindExtraActions(%cmd, 1);
        gameModeActionMap.bind(%device, %action, %cmd);
        OptRemapList.setRowById(%this.index, buildFullMapString(%this.index));
        return;
    }

    if (%prevMap $= %cmd)
    {
        unbindExtraActions(%cmd, 0);
        gameModeActionMap.bind(%device, %action, %cmd);
        OptRemapList.setRowById(%this.index, buildFullMapString(%this.index));
        return;
    }

    %prevMapIndex = findRemapCmdIndex(%prevMap);
    if (%prevMapIndex == -1)
    {
        MessageBoxOK("Remap Failed", "\"" @ %mapName @ "\" is already bound to a non-remappable command!");
        return;
    }

    %callback = "redoMapping(" @ %device @ ", \"" @ %action @ "\", \"" @ %cmd @ "\", " @ %prevMapIndex @ ", " @ %this.index @ ");";
    %prevCmdName = $RemapName[%prevMapIndex];
    MessageBoxYesNo("Warning", "\"" @ %mapName @ "\" is already bound to \"" @ %prevCmdName @ "\"!\nDo you wish to replace this mapping?", %callback, "");
}

function OptAudioUpdate()
{
    %text = "Vendor: " @ alGetString("AL_VENDOR") @ "\nVersion: " @ alGetString("AL_VERSION") @ "\nRenderer: " @ alGetString("AL_RENDERER") @ "\nExtensions: " @ alGetString("AL_EXTENSIONS");
    OptAudioInfo.setText(%text);
}

$AudioTestHandle = 0;

function OptAudioUpdateMasterVolume(%volume)
{
    if (%volume < 0.051)
    {
        %volume = 0.051;
    }

    if (%volume == $pref::Audio::masterVolume)
    {
        return;
    }

    alxListenerf(AL_GAIN_LINEAR, %volume);
    $pref::Audio::masterVolume = %volume;

    if (!alxIsPlaying($AudioTestHandle))
    {
        $AudioTestHandle = alxCreateSource("AudioChannel0", OptAudioFilename(0));
        alxPlay($AudioTestHandle);
    }
}

function OptAudioUpdateChannelVolume(%channel, %volume)
{
    if ((%channel < 1) || (%channel > 8))
    {
        return;
    }

    if ((%volume * $pref::Audio::masterVolume) < 0.051)
    {
        %volume = 0.051;
    }

    if (%volume == $pref::Audio::channelVolume[%channel])
    {
        return;
    }

    alxSetChannelVolume(%channel, %volume);
    $pref::Audio::channelVolume[%channel] = %volume;
    if (%channel == $MusicAudioType)
    {
        $pref::Audio::channelVolume[$MusicAudioType2] = %volume;
        VMPlayer.SetVolume(%volume);
    }

    if (!alxIsPlaying($AudioTestHandle))
    {
        $AudioTestHandle = alxCreateSource("AudioChannel" @ %channel, OptAudioFilename(%channel));
        if ($AudioTestHandle != 0)
        {
            alxPlay($AudioTestHandle);
        }
    }
}

function OptAudioFilename(%channel)
{
    return expandFilename("onverse/data/live_assets/sounds/UI/twang.ogg");
}

function OptAudioDriverList::onSelect(%this, %id, %text)
{
    if (%text $= "")
    {
        return;
    }

    if ($pref::Audio::driver $= %text)
    {
        return;
    }

    $pref::Audio::driver = %text;
    OpenALInit();
}

function optionsDlg::LoadPrivacy(%this)
{
    %this.PrivacyInit();
    
    OptTeleportPrivacyMenu.clear();
    OptTeleportPrivacyMenu.add("Loading...", -1);
    OptTeleportPrivacyMenu.setSelected(-1);
    %rpcObject = new RPC_UserPermissions() {
    };
    %rpcObject.getPermissions();
    %rpcObject.fireRPC(MasterServerConnection);
    %rpcObject.eval = "OptionsDlg.PrivacyInit();";
}

function optionsDlg::PrivacyInit(%this)
{
    OptTeleportPrivacyMenu.clear();
    OptTeleportPrivacyMenu.add("Public", 0);
    OptTeleportPrivacyMenu.add("Friends", 1);
    OptTeleportPrivacyMenu.add("Private", 2);

    if ($permissions::teleport $= "friends")
    {
        OptTeleportPrivacyMenu.setSelected(1);
    }
    else
    {
        if ($permissions::teleport $= "private")
        {
            OptTeleportPrivacyMenu.setSelected(2);
        }
        else
        {
            OptTeleportPrivacyMenu.setSelected(0);
        }
    }

    OptTravelPrivacyMenu.clear();
    OptTravelPrivacyMenu.add("Public", $Permission::Mounts::Public);
    OptTravelPrivacyMenu.add("Friends", $Permission::Mounts::Friends);
    OptTravelPrivacyMenu.add("Private", $Permission::Mounts::Private);
    OptTravelPrivacyMenu.setSelected($pref::Permissions::Mounts & $Permission::Mounts::Mask);
    OptTravelShareCheck.setValue(($pref::Permissions::Mounts & $Permission::Mounts::ShareFlag) > 0);
}

function optionsDlg::applyPrivacy(%this)
{
    if ((OptTeleportPrivacyMenu.getSelected() != -1) && (OptTeleportPrivacyMenu.getText() !$= $permissions::teleport))
    {
        %telePerm = OptTeleportPrivacyMenu.getText();
        %homePerm = "";
        %rpcObject = new RPC_UserPermissions("") {
        };

        %res = %rpcObject.setPermissions(%telePerm, %homePerm);
        if (!(%res $= ""))
        {
            %rpcObject.delete();
            return;
        }

        %rpcObject.fireRPC(MasterServerConnection);
    }

    %mountPerms = OptTravelPrivacyMenu.getSelected();
    if (OptTravelShareCheck.getValue() > 0)
    {
        %mountPerms |= $Permission::Mounts::ShareFlag;
    }

    if (%mountPerms != $pref::Permissions::Mounts)
    {
        $pref::Permissions::Mounts = %mountPerms;
        if (isObject(LocationGameConnection))
        {
            LocationGameConnection.SetMountPermissions();
        }
    }
}
