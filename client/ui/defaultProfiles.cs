$Gui::fontCacheDirectory = expandFilename("cache");
$Gui::clipboardFile = expandFilename("cache/clipboard.gui");

new GuiControlProfile(GuiDefaultProfile) {
   tab = "0";
   canKeyFocus = "0";
   hasBitmapArray = "0";
   mouseOverSelected = "0";
   gradient = "0";
   opaque = "0";
   fillColor = "192 192 192";
   fillColorHL = "220 220 220";
   fillColorNA = "220 220 220";
   border = "0";
   borderColor = "0 0 0";
   borderColorHL = "128 128 128";
   borderColorNA = "64 64 64";
   bevelColorHL = "255 255 255";
   bevelColorLL = "0 0 0";
   fontType = "Arial Bold";
   fontSize = "14";
   fontCharset = $pref::I18N::charset;
   fontColor = "255 255 255";
   fontColorHL = "100 180 220";
   fontColorNA = "0 0 0";
   bitmap = "";
   bitmapBase = "";
   textOffset = "0 0";
   Modal = "1";
   justify = "left";
   autoSizeWidth = "0";
   autoSizeHeight = "0";
   returnTab = "0";
   numbersOnly = "0";
   cursorColor = "0 0 0 255";
   soundButtonDown = "";
   soundButtonOver = "";
};

new GuiControlProfile(EditorTransparentControl) {
   opaque = "0";
   fillColor = "192 192 192 192";
};

new GuiControlProfile(GuiInputCtrlProfile) {
   tab = "1";
   canKeyFocus = "1";
};

new GuiControlProfile(GuiSolidDefaultProfile) {
   opaque = "1";
   border = "1";
};

new GuiControlProfile(GuiWindowProfile) {
   opaque = "0";
   border = "2";
   gradient = "1";
   fillColors[0] = "147 184 244 240";
   fillColors[1] = "147 184 244 240";
   fillColors[2] = "99 130 211 240";
   fillColors[3] = "99 130 211 240";
   fontColor = "255 255 255";
   fontColorHL = "0 0 0";
   fontSize = "24";
   fontType = "Arial";
   text = "GuiWindowCtrl test";
   bitmap = "~/data/live_assets/engine/ui/images/onverseWindow";
   textOffset = "18 4";
   hasBitmapArray = "1";
   justify = "left";
};

new GuiControlProfile(GuiOnverseWindowProfile) {
   opaque = "0";
   border = "2";
   gradient = "1";
   fillColors[0] = "108 140 211 255";
   fillColors[1] = "108 140 211 255";
   fillColors[2] = "108 140 211 255";
   fillColors[3] = "108 140 211 255";
   fontColor = "255 255 255";
   fontColorHL = "0 0 0";
   fontSize = "24";
   fontType = "Arial";
   text = "GuiWindowCtrl test";
   bitmap = "~/data/live_assets/engine/ui/images/onverse_window";
   textOffset = "18 4";
   hasBitmapArray = "1";
   justify = "left";
};

new GuiControlProfile(GuiWindowProfileSml) {
   opaque = "0";
   border = "2";
   gradient = "1";
   fillColors[0] = "147 184 244 240";
   fillColors[1] = "147 184 244 240";
   fillColors[2] = "99 130 211 240";
   fillColors[3] = "99 130 211 240";
   fontColor = "255 255 255";
   fontColorHL = "0 0 0";
   fontSize = "8";
   fontType = "Arial";
   text = "GuiWindowCtrl test";
   bitmap = "~/data/live_assets/engine/ui/images/onverseWindow_sml";
   textOffset = "2 2";
   hasBitmapArray = "1";
   justify = "left";
};

new GuiControlProfile(GuiPopupBackgroundShade) {
   opaque = "1";
   fillColor = "0 0 0 100";
   fillColorHL = "64 150 150";
   fillColorNA = "150 150 150";
   hasBitmapArray = "0";
};

new GuiControlProfile(GuiToolWindowProfile) {
   opaque = "1";
   border = "2";
   fillColor = "192 192 192";
   fillColorHL = "64 150 150";
   fillColorNA = "150 150 150";
   fontColor = "255 255 255";
   fontColorHL = "0 0 0";
   bitmap = "./torqueToolWindow";
   textOffset = "6 6";
};

new GuiControlProfile(GuiTabBookProfile) {
   fillColor = "255 255 255";
   fillColorHL = "64 150 150";
   fillColorNA = "150 150 150";
   fontColor = "0 0 0";
   fontColorNA = "0 0 0";
   justify = "center";
   bitmap = "~/data/live_assets/engine/ui/images/darkTab.png";
   tabWidth = "64";
   TabHeight = "24";
   TabPosition = "Top";
   tabRotation = "Horizontal";
   tab = "1";
   canKeyFocus = "1";
};

new GuiControlProfile(GuiTabPageProfile) {
   bitmap = "~/data/live_assets/engine/ui/images/darkTabPage";
   tab = "1";
};

new GuiControlProfile(GuiContentProfile) {
   opaque = "1";
   fillColor = "255 255 255";
};

new GuiControlProfile(GuiModelessDialogProfile) {
   Modal = "0";
};

new GuiControlProfile(GuiButtonProfile) {
   font = "Arial Bold";
   fontSize = "14";
   opaque = "1";
   border = "1";
   borderColor = "255 255 255 100";
   fillColor = "0 0 0 0";
   fillColorHL = "255 255 255 80";
   fillColorNA = "255 255 255 80";
   fontColor = "255 255 255";
   fontColorHL = "255 255 255";
   fixedExtent = "1";
   justify = "center";
   canKeyFocus = "0";
};

new GuiControlProfile(GuiBorderButtonProfile) {
   fontColorHL = "0 0 0";
};

new GuiControlProfile(GuiMenuBarProfile) {
   fontType = "Arial Bold";
   fontSize = "14";
   opaque = "1";
   fillColor = "40 110 170 202";
   fillColorHL = "255 255 255 100";
   border = "0";
   fontColor = "255 255 255";
   fontColorHL = "255 255 255";
   fontColorNA = "128 128 128";
   fixedExtent = "1";
   justify = "left";
   canKeyFocus = "0";
   mouseOverSelected = "1";
   bitmap = "~/data/live_assets/engine/ui/images/torqueMenu";
   hasBitmapArray = "1";
   borderColor = "255 255 255 120";
   borderColorHL = "255 255 255 0";
};

new GuiControlProfile(GuiMenuContextProfile : GuiMenuBarProfile) {
};

new GuiControlProfile(GuiButtonSmProfile : GuiButtonProfile) {
   fontSize = "14";
};

new GuiControlProfile(GuiRadioProfile) {
   fontSize = "14";
   fillColor = "232 232 232";
   fixedExtent = "1";
   bitmap = "~/data/live_assets/engine/ui/images/torqueRadio";
   hasBitmapArray = "1";
};

new GuiControlProfile(GuiScrollProfile) {
   opaque = "1";
   fillColor = "0 0 0 0";
   border = "0";
   borderThickness = "0";
   borderColor = "0 0 0";
   bitmap = "~/data/live_assets/engine/ui/images/onverseScroll";
   hasBitmapArray = "1";
};

new GuiControlProfile(GuiOnverseScrollProfile) {
   opaque = "1";
   fillColor = "0 0 0 0";
   border = "0";
   borderThickness = "0";
   borderColor = "0 0 0";
   bitmap = "~/data/live_assets/engine/ui/images/onverse_scroll";
   hasBitmapArray = "1";
};

new GuiControlProfile(VirtualScrollProfile : GuiScrollProfile) {
};

new GuiControlProfile(GuiSliderProfile) {
   bitmap = "~/data/live_assets/engine/ui/images/darkSlider";
};

new GuiControlProfile(GuiTextProfile) {
   fontColor = "255 255 255";
   fontColorLink = "40 33 184";
   fontColorLinkHL = "0 0 255";
   autoSizeWidth = "1";
   autoSizeHeight = "1";
};

new GuiControlProfile(GuiMediumTextProfile : GuiTextProfile) {
   fontSize = "24";
};

new GuiControlProfile(GuiBigTextProfile : GuiTextProfile) {
   fontSize = "36";
};

new GuiControlProfile(GuiCenterTextProfile : GuiTextProfile) {
   justify = "center";
};

new GuiControlProfile(GuiTextEditProfile) {
   opaque = "1";
   fillColor = "255 255 255";
   fillColorHL = "51 130 220 130";
   border = "0";
   borderThickness = "2";
   borderColor = "0 0 0";
   fontColor = "0 0 0";
   fontColorHL = "255 255 255";
   fontColorNA = "128 128 128";
   textOffset = "0 2";
   autoSizeWidth = "0";
   autoSizeHeight = "1";
   tab = "1";
   canKeyFocus = "1";
};

new GuiControlProfile(GuiNumEditProfile : GuiTextEditProfile) {
   numbersOnly = "1";
};

new GuiControlProfile(GuiControlListPopupProfile) {
   opaque = "1";
   fillColor = "255 255 255";
   fillColorHL = "128 128 128";
   border = "1";
   borderColor = "0 0 0";
   fontColor = "0 0 0";
   fontColorHL = "255 255 255";
   fontColorNA = "128 128 128";
   textOffset = "0 2";
   autoSizeWidth = "0";
   autoSizeHeight = "1";
   tab = "1";
   canKeyFocus = "1";
   bitmap = "~/data/live_assets/engine/ui/images/darkScroll";
   hasBitmapArray = "1";
};

new GuiControlProfile(GuiTextArrayProfile : GuiTextProfile) {
   fontColorHL = "32 100 100";
};

new GuiControlProfile(GuiTextListProfile : GuiTextProfile) {
   tab = "1";
   canKeyFocus = "1";
   borderColorHL = "255 255 255 80";
   fillColorHL = "255 255 255 80";
   fontColorHL = "255 255 255";
   fillColorNA = "255 255 255 25";
   borderColorNA = "255 255 255 25";
};

new GuiControlProfile(GuiTreeViewProfile) {
   canKeyFocus = "1";
   autoSizeHeight = "1";
   opaque = "1";
   fillColor = "0 0 0";
   fontColor = "255 255 255";
   fontColorNA = "51 130 220 130";
   fontColorSEL = "250 250 250";
   bitmap = "~/data/live_assets/engine/ui/images/shll_treeView";
};

new GuiControlProfile(GuiDirectoryTreeProfile : GuiTreeViewProfile) {
   fontColor = "40 40 40";
   fontColorSEL = "250 250 250 175";
   fillColorHL = "0 60 150";
   fontColorNA = "240 240 240";
   bitmap = "~/data/live_assets/engine/ui/images/shll_treeView";
   fontType = "Arial";
   fontSize = "14";
};

new GuiControlProfile(GuiDirectoryFileListProfile) {
   fontColor = "40 40 40";
   fontColorSEL = "250 250 250 175";
   fillColorHL = "0 60 150";
   fontColorNA = "240 240 240";
   fontType = "Arial";
   fontSize = "14";
};

new GuiControlProfile(GuiCheckBoxProfile) {
   opaque = "0";
   fillColor = "232 232 232";
   border = "0";
   borderColor = "0 0 0";
   fixedExtent = "1";
   justify = "left";
   bitmap = "~/data/live_assets/engine/ui/images/torqueCheck";
   hasBitmapArray = "1";
};

new GuiControlProfile(GuiPopUpMenuProfile) {
   opaque = "1";
   mouseOverSelected = "1";
   border = "4";
   borderThickness = "2";
   borderColor = "0 0 0";
   fontSize = "14";
   fontColor = "0 0 0";
   fixedExtent = "1";
   justify = "center";
   bitmap = "~/data/live_assets/engine/ui/images/darkScroll";
   hasBitmapArray = "1";
};

new GuiControlProfile("LoadTextProfile") {
   fontColor = "66 219 234";
   autoSizeWidth = "1";
   autoSizeHeight = "1";
};

new GuiControlProfile("GuiMLTextProfile") {
   fontColorLink = "40 33 184";
   fontColorLinkHL = "0 0 255";
};

new GuiControlProfile("GuiMLTextNoSelectProfile") {
   fontColorLink = "40 33 184";
   fontColorLinkHL = "0 0 255";
   Modal = "0";
};

new GuiControlProfile(GuiMLTextEditProfile) {
   fontColorLink = "40 33 184";
   fontColorLinkHL = "0 0 255";
   fillColor = "255 255 255";
   fillColorHL = "128 128 128";
   fontColor = "0 0 0";
   fontColorHL = "255 255 255";
   fontColorNA = "128 128 128";
   autoSizeWidth = "1";
   autoSizeHeight = "1";
   tab = "1";
   canKeyFocus = "1";
};

new GuiControlProfile(GuiToolTipProfile : GuiMenuContextProfile) {
};

new GuiControlProfile("GuiConsoleProfile") {
   fontType = "Lucida Console";
   fontSize = "12";
   fontColor = "0 0 0";
   fontColorHL = "130 130 130";
   fontColorNA = "255 0 0";
   fontColors[6] = "50 50 50";
   fontColors[7] = "50 50 0";
   fontColors[8] = "0 0 50";
   fontColors[9] = "0 50 0";
};

new GuiControlProfile(GuiProgressProfile) {
   opaque = "0";
   fillColor = "255 172 47 120";
   border = "1";
   borderColor = "255 255 255 100";
};

new GuiControlProfile(GuiProgressTextProfile) {
   fontColor = "255 255 255";
   justify = "center";
};

new GuiControlProfile(GuiBitmapBorderProfile) {
   bitmap = "~/data/live_assets/engine/ui/images/darkBorder";
   hasBitmapArray = "1";
};

new GuiControlProfile(GuiPaneProfile) {
   bitmap = "~/data/live_assets/engine/ui/images/torquePane";
   hasBitmapArray = "1";
};

new GuiControlProfile(GuiInspectorFieldProfile) {
   opaque = "0";
   fillColor = "255 255 255";
   fillColorHL = "128 128 128";
   fillColorNA = "244 244 244";
   border = "0";
   borderColor = "190 190 190";
   borderColorHL = "156 156 156";
   borderColorNA = "64 64 64";
   bevelColorHL = "255 255 255";
   bevelColorLL = "0 0 0";
   fontType = "Arial";
   fontSize = "16";
   fontColor = "32 32 32";
   fontColorNA = "0 0 0";
   tab = "1";
   canKeyFocus = "1";
};

new GuiControlProfile(GuiInspectorBackgroundProfile : GuiInspectorFieldProfile) {
   border = "5";
};

new GuiControlProfile(GuiInspectorDynamicFieldProfile : GuiInspectorFieldProfile) {
};

new GuiControlProfile("GuiInspectorTextEditProfile") {
   opaque = "0";
   border = "0";
   tab = "0";
   canKeyFocus = "1";
   fontType = "Arial";
   fontSize = "16";
   fontColor = "32 32 32";
   fontColorNA = "0 0 0";
};

new GuiControlProfile(InspectorTypeEnumProfile : GuiInspectorFieldProfile) {
   mouseOverSelected = "1";
   bitmap = "~/data/live_assets/engine/ui/images/darkScroll";
   hasBitmapArray = "1";
   opaque = "1";
   border = "1";
};

new GuiControlProfile(InspectorTypeCheckboxProfile : GuiInspectorFieldProfile) {
   bitmap = "~/data/live_assets/engine/ui/images/torqueCheck";
   hasBitmapArray = "1";
   opaque = "0";
   border = "0";
};

new GuiControlProfile(GuiInspectorTypeFileNameProfile) {
   opaque = "0";
   border = "5";
   tab = "1";
   canKeyFocus = "1";
   fontType = "Arial";
   fontSize = "16";
   justify = "center";
   fontColor = "32 32 32";
   fontColorNA = "0 0 0";
   fillColor = "255 255 255";
   fillColorHL = "128 128 128";
   fillColorNA = "244 244 244";
   borderColor = "190 190 190";
   borderColorHL = "156 156 156";
   borderColorNA = "64 64 64";
};

new GuiControlProfile(GuiTabProfile) {
   bitmap = "onverse/data/live_assets/engine/ui/images/onverseTabs";
   hasBitmapArray = "1";
   opaque = "1";
   borderThickness = "1";
   borderColor = "242 247 255";
   borderColorHL = "242 247 255";
   fillColor = "157 188 255";
   fillColorNA = "0 0 0";
   fillColorHL = "0 0 0";
   justify = "center";
   fontColor = "255 255 255";
   fontColorHL = "255 255 255";
};

%oldPlatform = $platform;

new GuiControlProfile(GuiBaseDefaultProfile) {
   tab = "0";
   canKeyFocus = "0";
   hasBitmapArray = "0";
   mouseOverSelected = "0";
   opaque = "0";
   fillColor = ( $platform $= "macos" ) ? "211 211 211" : "192 192 192";
   fillColorHL = ( $platform $= "macos" ) ? "244 244 244" : "220 220 220";
   fillColorNA = ( $platform $= "macos" ) ? "244 244 244" : "220 220 220";
   border = "0";
   borderColor = "0 0 0";
   borderColorHL = "128 128 128";
   borderColorNA = "64 64 64";
   bevelColorHL = "255 255 255";
   bevelColorLL = "0 0 0";
   fontType = "Arial";
   fontSize = "14";
   fontCharset = $pref::I18N::charset;
   fontColor = "0 0 0";
   fontColorNA = "0 0 0";
   bitmap = ( $platform $= "macos" ) ? "~/data/live_assets/engine/ui/images/osxWindow" : "~/data/live_assets/engine/ui/images/darkWindow";
   bitmapBase = "";
   textOffset = "0 0";
   Modal = "1";
   justify = "left";
   autoSizeWidth = "0";
   autoSizeHeight = "0";
   returnTab = "0";
   numbersOnly = "0";
   cursorColor = "0 0 0 255";
   soundButtonDown = "";
   soundButtonOver = "";
};

new GuiControlProfile(GuiBaseWindowProfile : GuiBaseDefaultProfile) {
   opaque = "1";
   border = "2";
   fillColor = ( $platform $= "macos" ) ? "211 211 211" : "192 192 192";
   fillColorHL = ( $platform $= "macos" ) ? "190 255 255" : "64 150 150";
   fillColorNA = ( $platform $= "macos" ) ? "255 255 255" : "150 150 150";
   fontColor = ( $platform $= "macos" ) ? "0 0 0" : "255 255 255";
   fontColorHL = ( $platform $= "macos" ) ? "200 200 200" : "0 0 0";
   text = "GuiWindowCtrl test";
   bitmap = ( $platform $= "macos" ) ? "~/data/live_assets/engine/ui/images/osxWindow" : "~/data/live_assets/engine/ui/images/darkWindow";
   textOffset = ( $platform $= "macos" ) ? "5 5" : "6 6";
   hasBitmapArray = "1";
   justify = ( $platform $= "macos" ) ? "center" : "left";
};

new GuiControlProfile(GuiBaseScrollProfile : GuiBaseDefaultProfile) {
   opaque = "1";
   fillColor = "255 255 255";
   border = "3";
   borderThickness = "2";
   borderColor = "0 0 0";
   bitmap = ( $platform $= "macos" ) ? "~/data/live_assets/engine/ui/images/osxScroll" : "~/data/live_assets/engine/ui/images/darkScroll";
   hasBitmapArray = "1";
};

new GuiControlProfile(GuiBaseTreeViewProfile : GuiBaseDefaultProfile) {
   fontSize = "13";
   fontColor = "0 0 0";
   fontColorHL = "64 150 150";
   canKeyFocus = "1";
   autoSizeHeight = "1";
   fontColorSEL = "250 250 250";
   fillColorHL = "0 60 150";
   fontColorNA = "240 240 240";
   bitmap = "~/data/live_assets/engine/ui/images/shll_treeView";
};

new GuiControlProfile(GuiBaseDirectoryTreeProfile : GuiBaseTreeViewProfile) {
   canKeyFocus = "1";
   autoSizeHeight = "1";
   fontColor = "40 40 40";
   fontColorSEL = "250 250 250 175";
   fillColorHL = "0 60 150";
   fontColorNA = "240 240 240";
   bitmap = "~/data/live_assets/engine/ui/images/shll_treeView";
   fontType = "Arial";
   fontSize = "14";
};

new GuiControlProfile(GuiBaseButtonProfile : GuiBaseDefaultProfile) {
   opaque = "1";
   border = "1";
   fontColor = "0 0 0";
   fixedExtent = "1";
   justify = "center";
   canKeyFocus = "0";
};

new GuiControlProfile(GuiBaseTabProfile : GuiBaseDefaultProfile) {
   bitmap = "";
   opaque = "1";
   borderThickness = "1";
   borderColor = "20 20 20";
   borderColorHL = "240 240 240";
   fillColor = ( $platform $= "macos" ) ? "211 211 211" : "192 192 192";
   fillColorNA = ( $platform $= "macos" ) ? "211 211 211" : "192 192 192";
   fillColorHL = ( $platform $= "macos" ) ? "211 211 211" : "192 192 192";
   justify = "center";
   fontColorHL = "0 0 0";
};

new GuiControlProfile(GuiBaseTextProfile : GuiBaseDefaultProfile) {
   fontColor = "0 0 0";
   fontColorLink = "40 33 184";
   fontColorLinkHL = "0 0 255";
   autoSizeWidth = "1";
   autoSizeHeight = "1";
};

new GuiControlProfile(GuiBaseMediumTextProfile : GuiBaseTextProfile) {
   fontSize = "24";
};

new GuiControlProfile(GuiBaseBigTextProfile : GuiBaseTextProfile) {
   fontSize = "36";
};

new GuiControlProfile(GuiBaseCenterTextProfile : GuiBaseTextProfile) {
   justify = "center";
};

new GuiControlProfile(GuiBaseTextArrayProfile : GuiBaseTextProfile) {
   fontColorHL = "32 100 100";
};

new GuiControlProfile(GuiBaseTextListProfile : GuiBaseTextProfile) {
   tab = "1";
   canKeyFocus = "1";
};

new GuiControlProfile(GuiBaseMenuBarProfile : GuiBaseDefaultProfile) {
   fontType = "Arial";
   fontSize = "15";
   opaque = "1";
   fillColor = "192 192 192";
   fillColorHL = "0 0 96";
   border = "4";
   fontColor = "0 0 0";
   fontColorHL = "255 255 255";
   fontColorNA = "128 128 128";
   fixedExtent = "1";
   justify = "center";
   canKeyFocus = "0";
   mouseOverSelected = "1";
   bitmap = "~/data/live_assets/engine/ui/images/torqueMenu";
   hasBitmapArray = "1";
   borderColor = "128 128 128";
   borderColorHL = "255 255 255";
};

$platform = %oldPlatform;

