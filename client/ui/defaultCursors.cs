if ($platform $= "macos")
{
	new GuiCursor(DefaultCursor) {
	   hotSpot = "4 4";
	   renderOffset = "0 0";
	   bitmapName = "~/data/live_assets/engine/ui/images/cursors/macCursor";
	};
}
else
{
	new GuiCursor(DefaultCursor) {
	   hotSpot = "1 1";
	   renderOffset = "0 0";
	   bitmapName = "~/data/live_assets/engine/ui/images/cursors/CUR_3darrow";
	};
}

new GuiCursor(LeftRightCursor) {
   hotSpot = "1 1";
   renderOffset = "0.5 0.5";
   bitmapName = "~/data/live_assets/engine/ui/images/cursors/CUR_leftright";
};

new GuiCursor(HandCursor) {
   hotSpot = "9 2";
   renderOffset = "0 0";
   bitmapName = "~/data/live_assets/engine/ui/images/cursors/link";
};

new GuiCursor(UpDownCursor) {
   hotSpot = "1 1";
   renderOffset = "0.5 0.5";
   bitmapName = "~/data/live_assets/engine/ui/images/cursors/CUR_updown";
};

new GuiCursor(NWSECursor) {
   hotSpot = "1 1";
   renderOffset = "0.5 0.5";
   bitmapName = "~/data/live_assets/engine/ui/images/cursors/CUR_nwse";
};

new GuiCursor(NESWCursor) {
   hotSpot = "1 1";
   renderOffset = "0.5 0.5";
   bitmapName = "~/data/live_assets/engine/ui/images/cursors/CUR_nesw";
};

new GuiCursor(MoveCursor) {
   hotSpot = "1 1";
   renderOffset = "0.5 0.5";
   bitmapName = "~/data/live_assets/engine/ui/images/cursors/CUR_move";
};

new GuiCursor(RotateCursor) {
   hotSpot = "11 18";
   bitmapName = "~/data/live_assets/engine/ui/images/cursors/CUR_rotate";
};

new GuiCursor(AvatarRotateCursor) {
   hotSpot = "1 1";
   renderOffset = "0.5 0.5";
   bitmapName = "~/data/live_assets/engine/ui/images/cursors/CUR_rotate_cam.png";
};

new GuiCursor(TextEditCursor) {
   hotSpot = "1 1";
   renderOffset = "0.5 0.5";
   bitmapName = "~/data/live_assets/engine/ui/images/cursors/CUR_textedit";
};
