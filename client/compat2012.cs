// Change back changed functions for pre-2012 build compat

function ClientCmdBeginLocationDownload(%isbuilder, %sd_id)
{
     LocationGameConnection.setTurbo(1);
     LocationGameConnection.isInGame = 0;
     $currentLocationDownloadPhase = 1;
     commandToServer('LocationDownloadAck');
}

function ClientCmdLocationDownloadPhase4(%isbuilder, %sd_id)
{
    if ($currentLocationDownloadPhase != 3)
    {
        echo("Recived old LocationDownloadPhase4");
    }

    $currentLocationDownloadPhase = 4;
    $location::builder = %isbuilder;
    LocationGameConnection.setTurbo(0);

    if ($location::builder)
    {
        $Client::LightingFile = "onverse/data/live_assets/engine/terrain/tempBuilder_ter_";
        InitializeBuilderTree();
    }
    else
    {
        $Client::LightingFile = "onverse/data/live_assets/engine/terrain/ter_";
    }

    $Client::LightingFile = $Client::LightingFile @ %sd_id;
    connectToLocationDlg.StartingReplication();
    schedule(10, 0, DoPhase4);
}

function MasterServerObject::onInstanceReady(%this, %location, %instance, %port)
{
    LocationSystem::onLocationReady(%location, %instance, MasterServerConnection.connectedAddress, %port);
}
