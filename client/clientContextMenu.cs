
package ContextMenuOverride
{
    function GuiContextMenu::showContextMenu(%this, %menu, %layer)
    {
        Canvas.pushDialog(%this, %layer);
        Parent::showContextMenu(%this, %menu);
    }

    function GuiContextMenu::addMenuItem(%this, %menuName, %itemName, %itemId, %command)
    {
        Parent::addMenuItem(%this, %menuName, %itemName, %itemId);
        %this.scriptCommand[%menuName,%itemId] = %command;
    }

    function GuiContextMenu::onMenuItemSelect(%this, %unused, %menu, %itemId, %item)
    {
        if (!(%this.scriptCommand[%menu,%itemId] $= ""))
        {
            %this.runScript(%this.scriptCommand[%menu,%itemId]);
        }
        else
        {
            error("No script command defined for context menu " @ %menu @ " item " @ %item);
        }
    }

    function GuiContextMenu::MenuClosed(%this)
    {
    }

    function GuiContextMenu::Destroy(%this)
    {
        %this.schedule(0, delete);
    }

    function GuiContextMenu::runScript(%this, %scriptText)
    {
        for (%x=0; %x < 10; %x++)
        {
            $tempVar[%x] = %this.var[%x];
        }
        eval(%scriptText);
    }

    function GuiContextMenu::doDefault(%this, %menu)
    {
        %item = %this.getFirstMenuItemVisible(%menu);
        if (%item != -1)
        {
            %this.runScript(%this.scriptCommand[%menu,%item]);
        }
        else
        {
            error("Could not find default visible menu item.");
        }
        %this.MenuClosed();
    }

};

activatePackage(ContextMenuOverride);

