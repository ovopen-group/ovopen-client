$objects::tableID[unknown] = 0;
$objects::tableID[textures] = 1;
$objects::tableID[shapes] = 2;
$objects::tableID[interiors] = 3;
$objects::tableID[scene_objects] = 4;
$objects::tableID[interior_objects] = 5;
$objects::tableID[clothing_objects] = 6;
$objects::tableID[particle_objects] = 7;
$objects::tableID[light_objects] = 8;
$objects::tableID[interactive_objects] = 9;
$objects::tableID[tool_objects] = 10;
$objects::tableID[pet_objects] = 11;
$objects::tableID[travelmount_objects] = 12;
$objects::tableID[npc_objects] = 13;
$objects::startnumTables = 4;
$objects::numTables = 14;

$objects::reverseName[$objects::tableID[unknown]] = "";
$objects::reverseName[$objects::tableID[textures]] = "textures";
$objects::reverseName[$objects::tableID[shapes]] = "shapes";
$objects::reverseName[$objects::tableID[interiors]] = "interiors";
$objects::reverseName[$objects::tableID[scene_objects]] = "scene_objects";
$objects::reverseName[$objects::tableID[interior_objects]] = "interior_objects";
$objects::reverseName[$objects::tableID[clothing_objects]] = "clothing_objects";
$objects::reverseName[$objects::tableID[particle_objects]] = "particle_objects";
$objects::reverseName[$objects::tableID[light_objects]] = "light_objects";
$objects::reverseName[$objects::tableID[interactive_objects]] = "interactive_objects";
$objects::reverseName[$objects::tableID[tool_objects]] = "tool_objects";
$objects::reverseName[$objects::tableID[pet_objects]] = "pet_objects";
$objects::reverseName[$objects::tableID[travelmount_objects]] = "travelmount_objects";
$objects::reverseName[$objects::tableID[npc_objects]] = "npc_objects";

%id = 0;
$clothing::type[hair] = %id += 1;
$clothing::type[eyes] = %id += 1;
$clothing::type[lips] = %id += 1;
$clothing::numrestypes = %id;

$clothing::type[chest] = %id += 1;
$clothing::type[hands] = %id += 1;
$clothing::type[legs] = %id += 1;
$clothing::type[feet] = %id += 1;
$clothing::type[over] = %id += 1;
$clothing::type[hats] = %id += 1;
$clothing::type[face] = %id += 1;
$clothing::type[belt] = %id += 1;
$clothing::type[ear] = %id += 1;
$clothing::type[neck] = %id += 1;
$clothing::numtypes = %id;

$clothing::numnonrestypes = $clothing::numtypes - $clothing::numrestypes;

