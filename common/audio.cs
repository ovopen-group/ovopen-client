
$GuiAudioType = 1;
$SimAudioType = 2;
$MessageAudioType = 3;
$MusicAudioType = 6;
$MusicAudioType2 = 7;

if (!isObject(AudioGui))
{

new AudioDescription(AudioGui) {
   volume = "1";
   isLooping = "0";
   is3D = "0";
   type = $GuiAudioType;
};

}

if (!isObject(AudioMessage))
{

new AudioDescription(AudioMessage) {
   volume = "1";
   isLooping = "0";
   is3D = "0";
   type = $MessageAudioType;
};

}

datablock AudioDescription(AudioDefault3d) {
   volume = "1";
   isLooping = "0";
   is3D = "1";
   referenceDistance = "20";
   maxDistance = "100";
   type = $SimAudioType;
};

datablock AudioDescription(AudioFar3d) {
   volume = "1";
   isLooping = "0";
   is3D = "1";
   referenceDistance = "100";
   maxDistance = "300";
   type = $SimAudioType;
};

datablock AudioDescription(AudioClose3d) {
   volume = "1";
   isLooping = "0";
   is3D = "1";
   referenceDistance = "10";
   maxDistance = "60";
   type = $SimAudioType;
};

datablock AudioDescription(AudioClosest3d) {
   volume = "1";
   isLooping = "0";
   is3D = "1";
   referenceDistance = "5";
   maxDistance = "30";
   type = $SimAudioType;
};

datablock AudioDescription(AudioAbsoluteClosest3d) {
   volume = "1";
   isLooping = "0";
   is3D = "1";
   referenceDistance = "2.5";
   maxDistance = "15";
   type = $SimAudioType;
};

datablock AudioDescription(AudioDefaultLooping3d) {
   volume = "1";
   isLooping = "1";
   is3D = "1";
   referenceDistance = "20";
   maxDistance = "100";
   type = $SimAudioType;
};

datablock AudioDescription(AudioFarLooping3d) {
   volume = "1";
   isLooping = "1";
   is3D = "1";
   referenceDistance = "100";
   maxDistance = "300";
   type = $SimAudioType;
};

datablock AudioDescription(AudioCloseLooping3d) {
   volume = "1";
   isLooping = "1";
   is3D = "1";
   referenceDistance = "10";
   maxDistance = "60";
   type = $SimAudioType;
};

datablock AudioDescription(AudioClosestLooping3d) {
   volume = "1";
   isLooping = "1";
   is3D = "1";
   referenceDistance = "5";
   maxDistance = "30";
   type = $SimAudioType;
};

datablock AudioDescription(AudioAbsoluteClosestLooping3d) {
   volume = "1";
   isLooping = "1";
   is3D = "1";
   referenceDistance = "2.5";
   maxDistance = "15";
   type = $SimAudioType;
};

datablock AudioDescription(Audio2D) {
   volume = "1";
   isLooping = "0";
   is3D = "0";
   type = $SimAudioType;
};

datablock AudioDescription(AudioLooping2d) {
   volume = "1";
   isLooping = "1";
   is3D = "0";
   type = $SimAudioType;
};

