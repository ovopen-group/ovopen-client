
function OnverseServerObject::onAdd(%this)
{
    %this.broadcastSet = new SimSet() {
    };
}

function OnverseServerObject::onBroadcastCall(%this, %functionName, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8, %a9, %a0)
{
    %count = %this.broadcastSet.getCount();
    for (%i=0; %i < %count; %i++)
    {
        %id = %this.broadcastSet.getObject(%i).schedule(0, %functionName, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8, %a9, %a0);
    }
    %this.schedule(0, %functionName, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8, %a9, %a0);
}

function OnverseServerObject::RegisterForBroadcast(%this, %objID)
{
    %this.broadcastSet.add(%objID);
}

function OnverseServerObject::UnRegisterForBroadcast(%this, %objID)
{
    %this.broadcastSet.remove(%objID);
}

function OnverseServerObject::onConnecting(%this)
{
    warn("Server connecting and pure virtual called.");
}

function OnverseServerObject::onDisconnect(%this)
{
    warn("Server connection disconnected and pure virtual called.");
}

function OnverseServerObject::onDropped(%this)
{
    warn("Server connection dropped and pure virtual called.");
}

function OnverseServerObject::onConnectFailed(%this)
{
    warn("Server connect failed and pure virtual called.");
}

function OnverseServerObject::onPacketError(%this)
{
    warn("Server packet error and pure virtual called.");
}

function OnverseServerObject::onServerError(%this, %unused)
{
    warn("Server error and pure virtual called.");
}

function OnverseServerObject::onRequestAuth(%this)
{
    error("Server needs authentication and pure virtual called.");
}

function OnverseServerObject::onAuthSuccess(%this, %unused, %unused)
{
    warn("Server authentication success and pure virtual called.");
}

function OnverseServerObject::onAuthFail(%this, %unused)
{
    warn("Server authentication fail and pure virtual called.");
}
